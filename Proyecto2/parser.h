/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    nada = 258,
    punto = 259,
    masMas = 260,
    menosMenos = 261,
    suma = 262,
    resta = 263,
    multiplicacion = 264,
    division = 265,
    potencia = 266,
    caracter = 267,
    cadena = 268,
    identificador = 269,
    entero = 270,
    decimal = 271,
    verdadero = 272,
    falso = 273,
    imprimir = 274,
    ptoComa = 275,
    abrePar1 = 276,
    cierraPar1 = 277,
    tipo_entero = 278,
    tipo_decimal = 279,
    tipo_caracter = 280,
    tipo_booleano = 281,
    publico = 282,
    privado = 283,
    protegido = 284,
    vacio = 285,
    igualIgual = 286,
    menorIgual = 287,
    mayorIgual = 288,
    menor = 289,
    mayor = 290,
    distintoA = 291,
    abreCor = 292,
    cierraCor = 293,
    abreLlave = 294,
    cierraLlave = 295,
    igual = 296,
    not_ = 297,
    and_ = 298,
    or_ = 299,
    convertirACadena = 300,
    convertirAEntero = 301,
    clase = 302,
    este = 303,
    hereda = 304,
    concatenar = 305,
    importar = 306,
    retornar = 307,
    sobreescribir = 308,
    principal = 309,
    nuevo = 310,
    coma = 311,
    detener = 312,
    mientras = 313,
    si = 314,
    continuar = 315,
    interrogacion = 316,
    dos_puntos = 317,
    sino_si = 318,
    sino = 319,
    selecciona = 320,
    caso = 321,
    defecto = 322,
    hacer = 323,
    para = 324,
    lista = 325,
    pila = 326,
    cola = 327,
    leer_teclado = 328,
    insertar = 329,
    apilar = 330,
    desapilar = 331,
    encolar = 332,
    desencolar = 333,
    obtener = 334,
    buscar = 335,
    tamanio = 336,
    mostrarEDD = 337,
    masIgual = 338,
    menosIgual = 339,
    porIgual = 340,
    divIgual = 341
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 69 "sintactico.y" /* yacc.c:1909  */

char TEXT [256];
class nodoArbol *NODO;
class Clase *CLASE;
class ListaClases *LISTA_CLASES;
class mPrincipal *PRINC;
class Funcion *FUNC;

#line 150 "parser.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */
