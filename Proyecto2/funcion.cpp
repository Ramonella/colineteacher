#include "funcion.h"

Funcion:: Funcion(string visibilidad,string tipo, string nombre, nodoArbol* parametros, nodoArbol *sentencias)
{
    this->visibilidad = visibilidad;
    this->tipo= tipo;
    this->nombreFuncion= nombre;
    this->parametros= parametros->hijos;
    this->sentencias= sentencias;
    this->sobreEscrita=false;
    this->esConstructor=false;
    this->nombreClase="";
    this->retornoArreglo=false;
    this->fila=0;
    this->noDimensionesRetorno=0;
}

void Funcion::setArregloRetorno(){
    this->retornoArreglo=true;
}

void Funcion::setNumeroDimenesionesRetorno(nodoArbol *dimesnieson){
    this->noDimensionesRetorno= dimesnieson->hijos.length();
}

void Funcion::cambiarAConstructor(){
    this->esConstructor=true;
}

void Funcion::setNombreClase(string nombre)
{
    this->nombreClase= nombre;
}

int Funcion::obtenerNoParametros(){
    return this->parametros.length();
}


string Funcion::obtenerCadenaParametros(){
    string cad="";

   nodoArbol *temporal;
   string tipo;
   for(int i=0; i<this->parametros.length(); i++){
       temporal= parametros.at(i);
       tipo= temporal->hijos.at(0)->valor;
       cad+=tipo;
       if(!(i == parametros.length()-1)){
           cad+="_";
       }
   }
    return cad;
}

string Funcion::obtenerFirma(){
    string par = this->obtenerCadenaParametros();
    string cad=nombreClase+"_"+this->tipo+"_"+this->nombreFuncion;
    if(!sonIguales("",par)){
        cad+="_"+par;
    }
    return cad;
}


string Funcion::obtenerFirmaSinClase(){
    string par = this->obtenerCadenaParametros();
    string cad=this->tipo+"_"+this->nombreFuncion;
    if(!sonIguales("",par)){
        cad+="_"+par;
    }
    return cad;
}
