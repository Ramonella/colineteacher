#ifndef GENERADORCODIGO_H
#define GENERADORCODIGO_H
#include<sstream>

#include<fstream>
#include <iostream>
#include <string.h>
#include <string>
#include "valor.h"
#include "nodoarbol.h"
#include "constantes.h"
#include "codigo3d.h"
#include "listaerrores.h"
#include "tablasimbolos.h"
#include "listaclases.h"
#include "nodocondicion.h"
#include "ambito.h"
#include "etiquetas.h"
#include <cstdlib>
#include "listacodigo.h"


using namespace std;


class GeneradorCodigo
{

public:

    Etiquetas *etiquetasRetorno;
    Etiquetas *etiquetasContinuar;
    Etiquetas *etiquetasBreak;
    ListaClases *clases;
    Ambito *ambiente;
    tablaSimbolos *tabla;
    ListaCodigo *codigoLista;
    Valor* resolverExpresion(nodoArbol*, string nombreClase, string nombreFuncion);
    GeneradorCodigo(ListaClases *clases, ListaCodigo *codigo);
    void setValorRet(Valor*);
    void addErrorSemantico(string tipo, string descripcion, int col, int fila);
    codigo3D *c3d;
    ListaErrores* erroresEjecucion;
    Valor* validarSumaOperacion(Valor*, Valor*);
    Valor* validarRestaOperacion(Valor*, Valor*);
    Valor* validarMultiplicacionOperacion(Valor*, Valor*);
    Valor* validarDivisionOperacion(Valor*, Valor*);
    Valor* validarPotenciaOpercion(Valor*, Valor*);
    bool esNulo(string);
    bool esVacio(string);
    bool esCadena(string);
    bool esInt(string);
    bool esBool(string);
    bool esDecimal(string);
    bool esCaracter(string);
    bool esObjeto(string);
    bool esCondicion(string);
    string crearOperacion(string resultado, string val1, string val2, string operador);
    string obtenerNoBooleano(string val);
    string obtenerIntCaracter(string val);
    string crearCadena(string val);
    Valor* caracterToCadena(string cadenaCaracter);
    Valor* concatenarCadena(Valor* cad1, Valor* cad2);
    void escribirPotencia(string base, string exponente, string tResultado);
    void analizarSentencia(nodoArbol *instruccion, string nombreClase, string nombreFuncion);
    void imprimir(nodoArbol *expresion, string nombreClase, string nombreFuncion);
    string obtnerCadenaInt(int numero);
    Valor* validarRelacional(Valor *val1, Valor *val2, string simbolo);
    Valor* resolverAND(Valor *val1, Valor *val2);
    Valor* resolverOR(Valor *val1, Valor *val2);
    Valor* resolverNOT(Valor *val1);
    Valor* convertirACondicion(Valor *val1);
    void resolverIF(nodoArbol* instruccion, string clase, string funcion);
    void generar3D();
    void generarSimbolosClase();
    string buscarPrincipal();
    string obtenerCadenaCaracter(string val);
    bool esNumericoNoArreglo(Valor *val1, Valor *val2);
    string sumarAsciiCadena(Valor *val);
    void resolverAsignacion(nodoArbol *sentencia, string clase, string funcion);
    void resolverDeclaracion(nodoArbol *sentencia, string clase, string funcion);
    void asignarPorPos(string nombre, string igual, string pos, string tipo,Valor *exp, int edd);
    Valor* resolverID(nodoArbol *id, string clase, string funcion);
    Valor* condiABool(Valor* v);
    void resolverMientras(nodoArbol *sentencia, string clase, string funcion);
    void resolverHacerMientras(nodoArbol *sentencia, string clase, string funcion);
    void resolverPara(nodoArbol *sentencia, string clase, string funcion);
    Valor* resolverTernario(nodoArbol *instruccion, string clase, string funcion);
    void resolverInstancia(QList<nodoArbol*>parametros, string clase, string funcion, string tPosVar, string sizeFun, int modo, string firmaMetodo, string nombreclaseInstaciar, nodoArbol *sent);
    void resolverContinue();
    void resolverDetener();
    void resolverInstanciaAsignacion(nodoArbol *sent, string clase, string metodo, int modo);
    Valor* llamadaFuncion(nodoArbol *sent, string clase, string metodo, bool modo, string posFinal, string clase2, string metodo2);
    void operarRetorno(nodoArbol *sentencia, string clase, string metodo);
    void declararArreglo(string tipoArreglo, string nombreArreglo, QList<nodoArbol*>dimensiones, Ambito *ambiente, string clase, string metodo, bool modo,string pos, string nombreBuscado, Simbolo *simb2 );
    QList<string> calcularArregloNs(QList<nodoArbol*>posiciones, Ambito *ambiente, string clase, string funcion);
    void operarArreglo(nodoArbol *sent, string clase, string funcion);
    string calcularSizeArreglo(QList<string> posiciones);
    void asignarArreglo(Simbolo *simb, Valor *expreisonArreglo, Ambito *ambiente, string clase, string metodo, bool modo, string posT);
    Valor*  convertirArregloCadena(string nombreVar, Ambito *ambiente, string clase, string metodo);
    bool esArregloCaracter(Simbolo *simb);
    void concatenar(nodoArbol* sentencia, string clase, string metodo);
    Valor* convertirACadena(nodoArbol *sent, string clase, string metodo);
    Valor* convertirAEntero(nodoArbol *sent, string clase, string metodo);
    void funcionConcatenar(nodoArbol *sent, string clase, string metodo);
    Valor* resolverThis(nodoArbol* sent, string clase, string metodo);
    string resolverThisPosicion(nodoArbol* sent, string clase, string metodo);
    void  asignarVar(string nombreVar, string igual, nodoArbol *expresion, int esAtributo);
    Valor* resolverAcceso(nodoArbol* nodo, Ambito *ambiente, string clase, string metodo);
    Valor* obtenerApuntadorPosArreglo(string nombreArreglo, QList<nodoArbol*> posicionesArreglo, Ambito *ambiente, string clase,string  metodo,string  posFinal, bool modo);
    Valor* posArreglo(nodoArbol *sentencia, string clase, string metodo);
    void resolverSelecciona(nodoArbol *sentencia, string clase, string metodo);
    void resolverLeerTeclado(nodoArbol *sentencia, string clase, string funcion);
    void agregarCodigo(int id, string codigo, string errores);
    void agregarHerencia();
    void agregarImportciones();
    void limpiarArrego(Simbolo* simb , bool modo, string posT);
    void asignarCadenaArregloPorPosicion(Valor *valor,Valor *valorOperacion,Ambito  *ambiente,string clase,string funcion);
    void instanciarPosArreglo(nodoArbol *sentencia, string clase, string metodo);
    void crearInstanciaPosArreglo();





};

#endif // GENERADORCODIGO_H
