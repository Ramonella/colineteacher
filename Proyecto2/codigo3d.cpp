#include "codigo3d.h"

codigo3D::codigo3D()
{

    this->noEtiquetas=0;
    this->noTemporal=0;
    this->codigo="";
}


string codigo3D::getEtiqueta(){
    this->noEtiquetas++;
    ostringstream str1;
    str1 << noEtiquetas;
    string geek = str1.str();
    string cad = "L"+geek;
    return cad;
}

void codigo3D::addSalto(string etiqueta){
    this->addCodigo("goto "+etiqueta+";");
}

void codigo3D::addEtiqueta(string et)
{
    this->addCodigo(et+":");
}

void codigo3D::addIf(string v1,string simb, string v2, string e1, string e2){
    this->addCodigo("if( "+v1+" "+simb+" "+v2+") goto "+e1+";");
    this->addCodigo("goto "+e2+";");
}

void codigo3D::addH(){
    this->addCodigo("h=h+1;");
}


void codigo3D::addS(){
    this->addCodigo("s=s+1;");
}

void codigo3D::agregarSuma(string contenedor, string v1, string v2){
    this->addCodigo(contenedor+"="+v1+"+"+v2+";");
}


void codigo3D::igualHeap(string pos, string val){
    this->addCodigo("heap["+pos+"]="+val+";");
}

void codigo3D::sacarHeap(string contenedor, string pos)
{
    this->addCodigo(contenedor+"=heap["+pos+"];");
}


void codigo3D::igualPool(string pos, string val){
    this->addCodigo("pool["+pos+"]="+val+";");
}

void codigo3D::sacarPool(string contenedor, string pos)
{
    this->addCodigo(contenedor+"=pool["+pos+"];");
}


void codigo3D::addTransferencia(string cont, string val){
    this->addCodigo(cont+"="+val+";");
}

string codigo3D::getTemporal(){
    this->noTemporal++;
        ostringstream str1;
        str1 << noTemporal;
        string geek = str1.str();
   std:: string cad = "t"+geek;
    return cad;
}

void codigo3D::addCodigo(string linea){
    this->codigo= this->codigo+ linea+"\n";
}


void codigo3D::escribirCodigo(){
   ofstream fs;
   fs.open("/home/alina/Documentos/arboles/codigo.txt");
   fs<<this->codigo;
   fs.close();
}
