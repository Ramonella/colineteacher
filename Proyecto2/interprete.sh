echo "Compilando BISON..."
 bison -o parser_interprete.cpp --defines=parser_interprete.h interprete_sintactico.y
 #Se generan los archivos parser.cpp y parser.h
 echo "Compilando FLEX..."
 flex -o scanner_interprete.cpp --header-file=scanner_interprete.h interprete_lexico.l
 #Se generan los archivos scanner.cpp y scanner.h
 echo "Generación del compilador terminada..."
