/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "sintactico.y" /* yacc.c:339  */

#include "scanner.h"
#include <iostream>
#include <QString>
#include "constantes.h"
#include "nodoarbol.h"
#include <string.h>
#include <string>
#include "clase.h"
#include "listaclases.h"
#include "mprincipal.h"
#include "funcion.h"
#include<sstream>
#include<fstream>
#include "listaerrores.h"

using namespace std;

 
//extern int yylineno=0;
extern int columna;
extern char *yytext;


extern int yylex(void);



ListaClases *nodoRaiz = new ListaClases();
ListaErrores *errores = new ListaErrores();

void setSalida(ListaClases* sal, ListaErrores *erro) {
nodoRaiz = sal;
errores = erro;
yylineno=1;
}



int yyerror(const char* mens){
	std::cout <<mens<<" "<<yytext<<yylineno<<"   "<<columna<< std::endl;
    string cad;
    while (*yytext != '\0') {
        string pp= string(1,*yytext);
        *yytext++;
        cad+=pp;
            }


    string cad2;
    while (*mens != '\0') {
        string pp= string(1,*mens);
        *mens++;
        cad2+=pp;
            }
    errores->insertarError("Sintactico",cad2+" "+cad,yylineno,columna);
    cout<<cad<<endl;
	return 0;
}


//nodoArbol* nodoRaiz = new nodoArbol("raiz","");



#line 132 "parser.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.h".  */
#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    nada = 258,
    punto = 259,
    masMas = 260,
    menosMenos = 261,
    suma = 262,
    resta = 263,
    multiplicacion = 264,
    division = 265,
    potencia = 266,
    caracter = 267,
    cadena = 268,
    identificador = 269,
    entero = 270,
    decimal = 271,
    verdadero = 272,
    falso = 273,
    imprimir = 274,
    ptoComa = 275,
    abrePar1 = 276,
    cierraPar1 = 277,
    tipo_entero = 278,
    tipo_decimal = 279,
    tipo_caracter = 280,
    tipo_booleano = 281,
    publico = 282,
    privado = 283,
    protegido = 284,
    vacio = 285,
    igualIgual = 286,
    menorIgual = 287,
    mayorIgual = 288,
    menor = 289,
    mayor = 290,
    distintoA = 291,
    abreCor = 292,
    cierraCor = 293,
    abreLlave = 294,
    cierraLlave = 295,
    igual = 296,
    not_ = 297,
    and_ = 298,
    or_ = 299,
    convertirACadena = 300,
    convertirAEntero = 301,
    clase = 302,
    este = 303,
    hereda = 304,
    concatenar = 305,
    importar = 306,
    retornar = 307,
    sobreescribir = 308,
    principal = 309,
    nuevo = 310,
    coma = 311,
    detener = 312,
    mientras = 313,
    si = 314,
    continuar = 315,
    interrogacion = 316,
    dos_puntos = 317,
    sino_si = 318,
    sino = 319,
    selecciona = 320,
    caso = 321,
    defecto = 322,
    hacer = 323,
    para = 324,
    lista = 325,
    pila = 326,
    cola = 327,
    leer_teclado = 328,
    insertar = 329,
    apilar = 330,
    desapilar = 331,
    encolar = 332,
    desencolar = 333,
    obtener = 334,
    buscar = 335,
    tamanio = 336,
    mostrarEDD = 337,
    masIgual = 338,
    menosIgual = 339,
    porIgual = 340,
    divIgual = 341
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 69 "sintactico.y" /* yacc.c:355  */

char TEXT [256];
class nodoArbol *NODO;
class Clase *CLASE;
class ListaClases *LISTA_CLASES;
class mPrincipal *PRINC;
class Funcion *FUNC;

#line 268 "parser.cpp" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 299 "parser.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  59
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   772

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  87
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  72
/* YYNRULES -- Number of rules.  */
#define YYNRULES  231
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  484

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   341

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   248,   248,   249,   252,   257,   261,   267,   276,   284,
     293,   301,   311,   316,   317,   320,   328,   330,   336,   343,
     350,   355,   361,   368,   375,   386,   392,   399,   405,   412,
     419,   426,   434,   444,   452,   461,   463,   467,   469,   472,
     476,   478,   480,   482,   484,   487,   489,   491,   494,   496,
     503,   507,   517,   519,   521,   523,   525,   527,   530,   531,
     534,   536,   538,   540,   542,   544,   546,   548,   550,   552,
     554,   556,   558,   560,   562,   564,   566,   568,   570,   572,
     574,   576,   582,   594,   606,   618,   630,   643,   654,   665,
     676,   687,   699,   710,   718,   726,   737,   747,   764,   765,
     766,   767,   768,   770,   784,   798,   807,   816,   824,   834,
     843,   855,   863,   873,   883,   895,   897,   899,   901,   911,
     923,   925,   927,   930,   934,   942,   953,   961,   970,   979,
     988,   996,  1005,  1012,  1020,  1027,  1035,  1043,  1053,  1062,
    1071,  1079,  1081,  1084,  1086,  1089,  1091,  1094,  1103,  1114,
    1122,  1130,  1138,  1148,  1149,  1150,  1151,  1152,  1153,  1155,
    1158,  1161,  1169,  1172,  1180,  1183,  1185,  1189,  1197,  1204,
    1212,  1220,  1223,  1231,  1239,  1242,  1250,  1254,  1263,  1271,
    1275,  1285,  1288,  1290,  1292,  1304,  1325,  1327,  1329,  1330,
    1331,  1333,  1334,  1335,  1336,  1337,  1338,  1339,  1345,  1347,
    1355,  1364,  1373,  1382,  1392,  1400,  1409,  1417,  1427,  1434,
    1442,  1450,  1460,  1469,  1478,  1489,  1498,  1508,  1518,  1528,
    1536,  1544,  1552,  1562,  1572,  1581,  1591,  1600,  1609,  1619,
    1630,  1632
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "nada", "punto", "masMas", "menosMenos",
  "suma", "resta", "multiplicacion", "division", "potencia", "caracter",
  "cadena", "identificador", "entero", "decimal", "verdadero", "falso",
  "imprimir", "ptoComa", "abrePar1", "cierraPar1", "tipo_entero",
  "tipo_decimal", "tipo_caracter", "tipo_booleano", "publico", "privado",
  "protegido", "vacio", "igualIgual", "menorIgual", "mayorIgual", "menor",
  "mayor", "distintoA", "abreCor", "cierraCor", "abreLlave", "cierraLlave",
  "igual", "not_", "and_", "or_", "convertirACadena", "convertirAEntero",
  "clase", "este", "hereda", "concatenar", "importar", "retornar",
  "sobreescribir", "principal", "nuevo", "coma", "detener", "mientras",
  "si", "continuar", "interrogacion", "dos_puntos", "sino_si", "sino",
  "selecciona", "caso", "defecto", "hacer", "para", "lista", "pila",
  "cola", "leer_teclado", "insertar", "apilar", "desapilar", "encolar",
  "desencolar", "obtener", "buscar", "tamanio", "mostrarEDD", "masIgual",
  "menosIgual", "porIgual", "divIgual", "$accept", "INICIO",
  "LISTA_CLASES_", "CLASE_", "CUERPO_CLASE", "IMPORTAR", "PRINCIPAL",
  "LISTA_ELEMENTOS_CLASE", "FUNCION", "COR_VACIOS", "LISTA_EXPRESIONES",
  "PARAMETROS_LLAMADA", "INSTANCIA", "TIPO_DATO", "VISIBILIDAD",
  "L_PARAMETROS", "PARAMETROS_FUNCION", "CUERPO_FUNCION", "INSTRUCCIONES",
  "INSTRUCCION", "DECLA_ATRIBUTO", "IGUALES", "ASIGNACION", "MOSTRAR_EDD",
  "DECLARACION", "COL_ARREGLO", "DECLA_ARREGLO", "CONTINUAR", "DETENER",
  "RETORNO", "IMPRIMIR", "CONCATENAR", "CONVERTIR_A_CADENA",
  "CONVERTIR_A_ENTERO", "MIENTRAS", "IF", "SINO", "SINO_SI", "L_SINO_SI",
  "SI", "SELECCIONA", "CASO", "DEFECTO", "ELEMENTO_SELECCIONA",
  "LISTA_SELECCIONA", "CUERPO_SELECCIONA", "HACER_MIENTRAS", "PARA",
  "DECLA_LISTA", "DECLA_COLA", "DECLA_PILA", "LEER_TECLADO",
  "OP_RELACIONAL", "EXPRESION", "OR", "AND", "NOT", "RELACIONAL",
  "ARITMETICA", "MUL", "POT", "UNARIO", "NEG", "VALOR", "ID",
  "LLAMADA_FUNCION", "POS_ARREGLO", "ACCESO", "ESTE", "ATRI", "NULO",
  "TERNARIO", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341
};
# endif

#define YYPACT_NINF -294

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-294)))

#define YYTABLE_NINF -199

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     623,  -294,   724,  -294,  -294,   427,  -294,  -294,  -294,  -294,
     645,  -294,  -294,  -294,   705,     0,     2,    22,    30,    68,
      43,   265,  -294,  -294,     1,  -294,  -294,  -294,    49,    48,
    -294,  -294,   252,    88,    93,  -294,   184,  -294,   139,   173,
     191,  -294,  -294,  -294,  -294,  -294,  -294,   664,   686,  -294,
      99,   183,   185,  -294,   686,   686,    96,   210,   217,  -294,
    -294,  -294,   237,   686,   686,   686,   705,   705,  -294,  -294,
    -294,  -294,  -294,  -294,   705,   705,   705,   705,  -294,  -294,
     157,   157,   157,  -294,    -2,   208,   215,   686,  -294,  -294,
     240,   242,   249,   251,  -294,   139,   173,   191,  -294,   244,
     136,    48,     7,  -294,    88,    88,   193,    93,    93,  -294,
     247,   248,   250,   259,   260,   270,   275,  -294,  -294,  -294,
    -294,   293,   293,   293,  -294,   686,  -294,   262,  -294,  -294,
     258,   288,  -294,  -294,  -294,  -294,   296,  -294,   584,   294,
     303,   304,   314,  -294,   548,  -294,    26,   281,   309,  -294,
    -294,  -294,   291,   313,   321,  -294,   686,   686,   686,   320,
     686,   322,   686,   686,   489,   208,  -294,  -294,   428,   297,
     288,  -294,    27,   231,   324,   302,   306,   308,  -294,   584,
    -294,  -294,   338,   288,   326,    42,    83,   288,   345,    91,
    -294,  -294,  -294,  -294,  -294,  -294,   291,   208,   340,   346,
    -294,   347,  -294,   348,   350,   339,   352,   353,   354,   359,
     361,   363,  -294,  -294,  -294,  -294,   365,  -294,  -294,   371,
      20,  -294,  -294,   476,  -294,   297,   288,   142,   297,   312,
     337,   342,  -294,  -294,   297,  -294,   288,   355,   579,    82,
     297,   288,   288,   143,   124,  -294,  -294,  -294,  -294,  -294,
    -294,   686,   686,   372,   686,   373,   686,   686,  -294,  -294,
      51,  -294,   300,   366,   125,   384,  -294,   388,   686,   391,
     394,   396,   392,   397,   297,   398,   400,   402,   331,  -294,
     404,  -294,   405,   406,  -294,  -294,  -294,  -294,  -294,   410,
     419,  -294,   140,  -294,  -294,  -294,  -294,   420,   421,   423,
    -294,   108,    98,   113,   131,   135,  -294,   288,  -294,   375,
     385,   383,  -294,   297,  -294,   443,  -294,  -294,   686,  -294,
     297,   297,   288,   579,   123,   444,   445,  -294,   447,  -294,
     450,   451,   579,   132,  -294,  -294,  -294,   686,   371,   454,
    -294,   686,   686,  -294,   686,   417,   300,   686,   686,  -294,
    -294,  -294,  -294,  -294,  -294,  -294,   455,   297,  -294,  -294,
     140,  -294,  -294,  -294,  -294,  -294,  -294,  -294,  -294,  -294,
    -294,   579,  -294,   579,  -294,   686,   686,   457,   458,   459,
    -294,   460,  -294,  -294,  -294,   297,  -294,  -294,   686,  -294,
    -294,  -294,  -294,  -294,  -294,  -294,   686,   462,   426,  -294,
     463,   464,   465,   470,   371,   472,   437,   474,   686,  -294,
    -294,  -294,  -294,  -294,  -294,  -294,  -294,  -294,   300,   300,
     300,  -294,  -294,  -294,  -294,   477,   686,   297,   297,   466,
     686,   453,   686,   484,   486,   482,   485,   487,   492,  -294,
      25,  -294,  -294,    92,  -294,   493,   488,   495,  -294,   297,
    -294,  -294,  -294,   498,   686,  -294,   686,   461,  -294,  -294,
    -294,   115,   499,    39,   500,  -294,  -294,   503,   467,   297,
    -294,  -294,  -294,   505,   173,   135,  -294,   510,   297,  -294,
     297,  -294,  -294,  -294
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,   230,     0,   185,   184,     0,   182,   183,   186,   187,
       0,    46,    47,    45,     0,     0,     0,     0,     0,     0,
       0,     2,     4,     6,     0,   188,   189,     3,   159,   162,
     164,   166,   168,   171,   174,   176,   179,   181,   191,   193,
     192,   195,   194,   196,   160,   180,   117,     0,     0,   199,
     200,     0,     0,   165,     0,     0,     0,     0,     0,     1,
       5,     7,     0,     0,     0,     0,     0,     0,   158,   155,
     156,   153,   154,   157,     0,     0,     0,     0,   177,   178,
       0,     0,     0,    37,     0,    35,     0,     0,   197,   190,
       0,     0,     0,     0,     8,   205,   207,   206,   204,     0,
       0,   161,     0,   163,   169,   170,   167,   172,   173,   175,
       0,     0,     0,     0,     0,     0,     0,   218,   208,   210,
     209,   201,   203,   202,    38,     0,   115,     0,   126,   127,
       0,    44,    40,    41,    42,    43,     0,    13,     0,     0,
       0,     0,     0,    19,     0,    17,     0,     0,     0,    95,
      96,    97,     0,     0,     0,    10,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    36,   116,    14,     0,     0,
       0,    18,     0,     0,     0,     0,     0,     0,    12,     0,
      24,    23,     0,   198,     0,     0,     0,    44,     0,     0,
      92,    93,    94,    20,     9,    15,     0,   231,     0,     0,
     213,     0,   215,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   229,   219,   221,   220,     0,    44,    53,     0,
       0,    48,    50,     0,    30,     0,     0,     0,     0,     0,
       0,     0,    22,    21,     0,    33,     0,     0,     0,    85,
       0,     0,   198,     0,     0,    11,   211,   212,   214,   216,
     217,     0,     0,     0,     0,     0,     0,     0,    54,   198,
       0,    52,     0,     0,     0,     0,    55,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    58,
       0,    79,     0,     0,    63,    65,    64,    60,    66,     0,
       0,    70,   134,    69,    71,    72,    73,     0,     0,     0,
      78,     0,     0,     0,     0,     0,    28,     0,    16,     0,
       0,     0,    27,     0,    34,     0,    84,    82,     0,    29,
       0,     0,     0,     0,    90,     0,     0,   224,     0,   226,
       0,     0,     0,   118,    49,    51,    57,     0,     0,     0,
     121,     0,     0,   120,     0,     0,     0,     0,     0,    56,
      59,    80,    61,    62,    67,    68,     0,     0,   135,   132,
     136,    75,    74,    76,   103,   104,   102,    99,   100,   101,
      98,     0,    81,     0,    77,     0,     0,     0,     0,     0,
      32,     0,    86,    26,    25,     0,    89,    87,     0,   222,
     223,   225,   227,   228,   114,   112,     0,     0,     0,   122,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   130,
     137,   133,   107,   108,   110,   109,   106,   105,     0,     0,
       0,    39,    31,    91,   119,     0,     0,     0,     0,     0,
       0,   113,     0,     0,     0,     0,     0,     0,     0,   123,
       0,   128,   129,     0,   138,     0,     0,     0,   111,     0,
     149,   151,   150,     0,     0,   146,     0,     0,   141,   142,
     143,     0,     0,     0,     0,   131,   124,     0,     0,     0,
     145,   144,   147,     0,     0,     0,   152,     0,     0,   140,
       0,   125,   139,   148
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -294,  -294,  -294,   511,   -90,   516,   387,  -294,  -122,  -152,
    -294,   159,  -293,   -79,   -78,  -294,  -120,  -179,  -294,   261,
     399,  -117,    75,  -294,  -163,  -174,  -153,  -294,  -294,  -294,
    -294,  -294,  -206,  -204,  -294,  -294,   182,   190,  -294,  -294,
    -294,  -294,  -294,    90,  -294,  -294,  -294,  -294,  -141,  -140,
    -138,  -294,  -294,   -10,   -29,   490,   491,   538,   480,   146,
     151,   478,  -294,   555,   -56,   -53,   -49,   -55,  -220,   158,
    -294,  -294
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    20,    21,    22,    94,    23,   143,   144,   145,   185,
      84,    49,   316,   219,    24,   220,   234,   224,   278,   279,
     148,   375,   280,   281,   282,    50,   283,   284,   285,   286,
     287,   288,    25,    26,   291,   292,   358,   359,   360,   293,
     294,   458,   459,   460,   461,   444,   295,   296,   149,   150,
     151,   300,    74,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,   121,
      43,    44
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      52,    95,    98,   305,    96,   221,   190,   191,    97,   192,
     155,   169,   239,   146,   147,   222,   171,   289,    85,   290,
     124,    54,   181,    55,   118,   118,   118,   119,   119,   119,
     386,   120,   120,   120,    57,   102,    56,   243,    86,   394,
     183,   226,   261,    59,    90,    91,   306,   453,    62,   308,
     225,    63,    46,     5,   125,   312,   236,   232,   305,   172,
     173,   319,   194,   184,   184,   146,   147,   240,   189,   156,
     324,  -113,   289,  -113,   290,   243,   262,   127,   412,   237,
     414,   454,   297,   298,    46,   299,   333,    18,    48,    58,
     186,    65,   332,    63,   227,   345,   165,    75,    76,   334,
     172,   173,    81,   -83,    77,   242,   245,  -113,   213,   335,
      64,   214,    80,   364,   365,   215,   313,    82,   372,    87,
      48,   320,   321,   318,   238,    46,    46,   197,   184,  -198,
    -198,  -198,   455,   244,   380,    92,    87,   297,   298,   -44,
     299,   383,   384,    80,   -88,    93,    47,   198,   199,   366,
     201,   374,   203,   204,   366,   470,   307,   322,   456,   457,
      87,    48,    48,   260,   388,   323,  -198,   301,   304,    87,
     302,     5,   366,   396,   303,    92,   366,    81,   409,   184,
     237,   456,   457,   405,   371,   154,   373,   321,   376,    78,
      79,   367,   368,   369,   370,    82,   367,   368,   369,   370,
      66,    67,   385,   356,   357,    88,   422,    89,  -198,  -198,
    -198,  -198,   104,   105,   367,   368,   369,   370,   367,   368,
     369,   370,   301,   304,     5,   302,   107,   108,   317,   303,
      99,   110,   111,   112,   113,   114,   115,   116,   117,   122,
     123,   325,   326,   305,   328,   187,   330,   331,   441,   442,
     130,   100,    63,   126,   132,   133,   134,   135,   339,    66,
      67,   188,   128,   131,   129,   152,   153,   404,   157,   158,
     465,   159,   132,   133,   134,   135,    11,    12,    13,   136,
     160,   161,   398,    68,    69,    70,    71,    72,    73,   137,
     479,   162,    11,    12,    13,   187,   163,   164,   167,   482,
     166,   483,   138,   139,   132,   133,   134,   135,   382,   168,
     170,   188,    17,   387,   217,   174,    19,   175,   176,   140,
     141,   142,   395,   132,   133,   134,   135,   397,   177,   193,
      92,   400,   401,   195,   402,   196,   223,   406,   407,   436,
     437,   438,   200,   229,   202,   264,   228,   230,   431,   231,
     265,   140,   141,   142,   132,   133,   134,   135,   233,   241,
     251,   413,   246,   415,   235,   416,   417,   309,   247,   248,
     249,   349,   250,   252,   253,   254,    15,    16,   423,    18,
     255,   267,   256,   268,   257,   259,   424,   258,   269,   270,
     271,   272,   310,   314,   327,   329,   273,   311,   435,   274,
     275,   140,   141,   142,   276,   337,   336,   301,   475,   338,
     474,   340,   343,   277,   303,   341,   440,   342,   344,   346,
     445,   347,   446,   348,   351,   352,   353,  -198,    46,   216,
     354,  -198,  -198,  -198,  -198,  -198,  -198,  -198,  -198,   355,
     361,   362,   217,   363,   467,   377,   468,  -198,    47,  -198,
     218,   132,   133,   134,   135,   379,   378,   381,  -198,  -198,
    -198,  -198,  -198,  -198,    48,  -198,   389,   390,  -198,   391,
    -198,  -198,   392,   393,   399,   403,   408,   263,   418,   419,
     420,    47,   426,  -198,   425,   427,   428,   429,  -198,  -198,
     264,   430,   432,   433,   332,   265,   434,   439,   447,   132,
     133,   134,   135,     5,   449,   443,   448,   450,   463,   451,
    -198,  -198,  -198,  -198,   452,   462,   266,   464,   466,   472,
     476,    15,    16,   469,    18,   477,   267,   480,   268,   478,
     481,   180,    60,   269,   270,   271,   272,    61,   473,   350,
     421,   273,   410,   182,   274,   275,   140,   141,   142,   276,
     411,   471,    53,   101,   106,   109,   103,    45,   277,     0,
       0,     0,   131,   205,   206,   207,   208,   209,   210,   211,
     212,   132,   133,   134,   135,    11,    12,    13,   136,     0,
       0,     0,     1,     0,     0,     0,     0,     2,   178,     0,
       0,     3,     4,     5,     6,     7,     8,     9,   131,     0,
      10,   179,   139,     0,     0,     0,     0,   132,   133,   134,
     135,    11,    12,    13,   136,     0,     0,     0,   140,   141,
     142,    14,     0,     0,    15,    16,     1,    18,     0,     0,
       0,     2,     0,     0,   315,     3,     4,     5,     6,     7,
       8,     9,     0,     0,    10,     0,    51,     0,     1,     0,
      11,    12,    13,     2,     0,     0,     0,     3,     4,     5,
       6,     7,     8,     9,     0,    14,    10,     1,    15,    16,
      17,    18,     2,     0,    19,     0,     3,     4,     5,     6,
       7,     8,     9,     0,     0,    10,    83,    14,     0,     1,
      15,    16,     0,    18,     2,     0,     0,     0,     3,     4,
       5,     6,     7,     8,     9,     0,    14,    10,     1,    15,
      16,     0,    18,     2,     0,     0,     0,     3,     4,     5,
       6,     7,     8,     9,     0,     0,    10,     1,    14,     0,
       0,    15,    16,     0,    18,     0,     3,     4,     5,     6,
       7,     8,     9,     0,     0,    10,     0,     0,     0,     0,
      15,    16,     0,    18,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    15,
      16,     0,    18
};

static const yytype_int16 yycheck[] =
{
      10,    57,    57,   223,    57,   168,   147,   147,    57,   147,
     100,   131,   186,    92,    92,   168,   138,   223,    47,   223,
      22,    21,   144,    21,    80,    81,    82,    80,    81,    82,
     323,    80,    81,    82,     4,    64,    14,   189,    48,   332,
      14,    14,    22,     0,    54,    55,   225,    22,    47,   228,
     170,    44,     1,    14,    56,   234,    14,   179,   278,   138,
     138,   240,   152,    37,    37,   144,   144,   187,   147,    62,
     244,    20,   278,    22,   278,   227,    56,    87,   371,    37,
     373,    56,   223,   223,     1,   223,   260,    48,    37,    21,
     146,    43,    41,    44,   173,   274,   125,     9,    10,   262,
     179,   179,     4,    20,    11,    14,   196,    56,   164,   262,
      61,   164,     4,     5,     6,   164,   236,     4,    20,    37,
      37,   241,   242,    41,    41,     1,     1,   156,    37,     4,
       5,     6,    40,   189,   313,    39,    37,   278,   278,    14,
     278,   320,   321,     4,    20,    49,    21,   157,   158,    41,
     160,    20,   162,   163,    41,    40,    14,    14,    66,    67,
      37,    37,    37,   219,    41,    41,    41,   223,   223,    37,
     223,    14,    41,    41,   223,    39,    41,     4,   357,    37,
      37,    66,    67,   346,   301,    49,   303,   307,   305,     5,
       6,    83,    84,    85,    86,     4,    83,    84,    85,    86,
       7,     8,   322,    63,    64,    22,   385,    22,    83,    84,
      85,    86,    66,    67,    83,    84,    85,    86,    83,    84,
      85,    86,   278,   278,    14,   278,    75,    76,   238,   278,
      13,    74,    75,    76,    77,    78,    79,    80,    81,    81,
      82,   251,   252,   463,   254,    14,   256,   257,   427,   428,
       1,    14,    44,    38,    23,    24,    25,    26,   268,     7,
       8,    30,    22,    14,    22,    14,    22,   346,    21,    21,
     449,    21,    23,    24,    25,    26,    27,    28,    29,    30,
      21,    21,   338,    31,    32,    33,    34,    35,    36,    40,
     469,    21,    27,    28,    29,    14,    21,     4,    40,   478,
      38,   480,    53,    54,    23,    24,    25,    26,   318,    21,
      14,    30,    47,   323,    14,    21,    51,    14,    14,    70,
      71,    72,   332,    23,    24,    25,    26,   337,    14,    20,
      39,   341,   342,    20,   344,    14,    39,   347,   348,   418,
     419,   420,    22,    41,    22,    14,    22,    41,   404,    41,
      19,    70,    71,    72,    23,    24,    25,    26,    20,    14,
      21,   371,    22,   373,    38,   375,   376,    55,    22,    22,
      22,    40,    22,    21,    21,    21,    45,    46,   388,    48,
      21,    50,    21,    52,    21,    14,   396,    22,    57,    58,
      59,    60,    55,    38,    22,    22,    65,    55,   408,    68,
      69,    70,    71,    72,    73,    21,    40,   463,   463,    21,
     463,    20,    20,    82,   463,    21,   426,    21,    21,    21,
     430,    21,   432,    21,    20,    20,    20,     0,     1,     1,
      20,     4,     5,     6,     7,     8,     9,    10,    11,    20,
      20,    20,    14,    20,   454,    70,   456,    20,    21,    22,
      22,    23,    24,    25,    26,    72,    71,    14,    31,    32,
      33,    34,    35,    36,    37,    38,    22,    22,    41,    22,
      43,    44,    22,    22,    20,    58,    21,     1,    21,    21,
      21,    21,    56,    56,    22,    22,    22,    22,    61,    62,
      14,    21,    20,    56,    41,    19,    22,    20,    14,    23,
      24,    25,    26,    14,    22,    39,    20,    22,    20,    22,
      83,    84,    85,    86,    22,    22,    40,    22,    20,    20,
      20,    45,    46,    62,    48,    22,    50,    22,    52,    62,
      20,   144,    21,    57,    58,    59,    60,    21,   463,   278,
     381,    65,   360,   144,    68,    69,    70,    71,    72,    73,
     360,   461,    14,    63,    74,    77,    65,     2,    82,    -1,
      -1,    -1,    14,    74,    75,    76,    77,    78,    79,    80,
      81,    23,    24,    25,    26,    27,    28,    29,    30,    -1,
      -1,    -1,     3,    -1,    -1,    -1,    -1,     8,    40,    -1,
      -1,    12,    13,    14,    15,    16,    17,    18,    14,    -1,
      21,    53,    54,    -1,    -1,    -1,    -1,    23,    24,    25,
      26,    27,    28,    29,    30,    -1,    -1,    -1,    70,    71,
      72,    42,    -1,    -1,    45,    46,     3,    48,    -1,    -1,
      -1,     8,    -1,    -1,    55,    12,    13,    14,    15,    16,
      17,    18,    -1,    -1,    21,    -1,     1,    -1,     3,    -1,
      27,    28,    29,     8,    -1,    -1,    -1,    12,    13,    14,
      15,    16,    17,    18,    -1,    42,    21,     3,    45,    46,
      47,    48,     8,    -1,    51,    -1,    12,    13,    14,    15,
      16,    17,    18,    -1,    -1,    21,    22,    42,    -1,     3,
      45,    46,    -1,    48,     8,    -1,    -1,    -1,    12,    13,
      14,    15,    16,    17,    18,    -1,    42,    21,     3,    45,
      46,    -1,    48,     8,    -1,    -1,    -1,    12,    13,    14,
      15,    16,    17,    18,    -1,    -1,    21,     3,    42,    -1,
      -1,    45,    46,    -1,    48,    -1,    12,    13,    14,    15,
      16,    17,    18,    -1,    -1,    21,    -1,    -1,    -1,    -1,
      45,    46,    -1,    48,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    -1,    48
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     8,    12,    13,    14,    15,    16,    17,    18,
      21,    27,    28,    29,    42,    45,    46,    47,    48,    51,
      88,    89,    90,    92,   101,   119,   120,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   157,   158,   150,     1,    21,    37,    98,
     112,     1,   140,   144,    21,    21,    14,     4,    21,     0,
      90,    92,    47,    44,    61,    43,     7,     8,    31,    32,
      33,    34,    35,    36,   139,     9,    10,    11,     5,     6,
       4,     4,     4,    22,    97,   141,   140,    37,    22,    22,
     140,   140,    39,    49,    91,   151,   152,   153,   154,    13,
      14,   142,   141,   143,   146,   146,   145,   147,   147,   148,
      74,    75,    76,    77,    78,    79,    80,    81,   151,   152,
     153,   156,   156,   156,    22,    56,    38,   140,    22,    22,
       1,    14,    23,    24,    25,    26,    30,    40,    53,    54,
      70,    71,    72,    93,    94,    95,   100,   101,   107,   135,
     136,   137,    14,    22,    49,    91,    62,    21,    21,    21,
      21,    21,    21,    21,     4,   141,    38,    40,    21,   103,
      14,    95,   100,   101,    21,    14,    14,    14,    40,    53,
      93,    95,   107,    14,    37,    96,   151,    14,    30,   100,
     135,   136,   137,    20,    91,    20,    14,   141,   140,   140,
      22,   140,    22,   140,   140,    74,    75,    76,    77,    78,
      79,    80,    81,   151,   152,   153,     1,    14,    22,   100,
     102,   111,   113,    39,   104,   103,    14,   100,    22,    41,
      41,    41,    95,    20,   103,    38,    14,    37,    41,   112,
     103,    14,    14,    96,   151,    91,    22,    22,    22,    22,
      22,    21,    21,    21,    21,    21,    21,    21,    22,    14,
     151,    22,    56,     1,    14,    19,    40,    50,    52,    57,
      58,    59,    60,    65,    68,    69,    73,    82,   105,   106,
     109,   110,   111,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   126,   127,   133,   134,   135,   136,   137,
     138,   151,   152,   153,   154,   155,   104,    14,   104,    55,
      55,    55,   104,   103,    38,    55,    99,   140,    41,   104,
     103,   103,    14,    41,   112,   140,   140,    22,   140,    22,
     140,   140,    41,   112,   111,   113,    40,    21,    21,   140,
      20,    21,    21,    20,    21,   104,    21,    21,    21,    40,
     106,    20,    20,    20,    20,    20,    63,    64,   123,   124,
     125,    20,    20,    20,     5,     6,    41,    83,    84,    85,
      86,   108,    20,   108,    20,   108,   108,    70,    71,    72,
     104,    14,   140,   104,   104,   103,    99,   140,    41,    22,
      22,    22,    22,    22,    99,   140,    41,   140,   151,    20,
     140,   140,   140,    58,   100,   111,   140,   140,    21,   104,
     123,   124,    99,   140,    99,   140,   140,   140,    21,    21,
      21,    98,   104,   140,   140,    22,    56,    22,    22,    22,
      21,   151,    20,    56,    22,   140,   100,   100,   100,    20,
     140,   104,   104,    39,   132,   140,   140,    14,    20,    22,
      22,    22,    22,    22,    56,    40,    66,    67,   128,   129,
     130,   131,    22,    20,    22,   104,    20,   140,   140,    62,
      40,   130,    20,   109,   152,   154,    20,    22,    62,   104,
      22,    20,   104,   104
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    87,    88,    88,    89,    89,    89,    89,    90,    90,
      90,    90,    91,    91,    91,    92,    93,    94,    94,    94,
      94,    94,    94,    94,    94,    95,    95,    95,    95,    95,
      95,    95,    95,    96,    96,    97,    97,    98,    98,    99,
     100,   100,   100,   100,   100,   101,   101,   101,   102,   102,
     102,   102,   103,   103,   103,   104,   104,   104,   105,   105,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   107,   107,   107,   107,   107,   107,   107,   107,
     107,   107,   107,   107,   107,   107,   107,   107,   108,   108,
     108,   108,   108,   109,   109,   109,   109,   109,   109,   109,
     109,   110,   111,   111,   111,   112,   112,   112,   113,   113,
     114,   115,   116,   117,   118,   118,   119,   120,   121,   122,
     123,   124,   125,   125,   126,   126,   126,   126,   127,   128,
     129,   130,   130,   131,   131,   132,   132,   133,   134,   135,
     136,   137,   138,   139,   139,   139,   139,   139,   139,   140,
     140,   141,   141,   142,   142,   143,   143,   144,   144,   145,
     145,   145,   146,   146,   146,   147,   147,   148,   148,   148,
     149,   149,   150,   150,   150,   150,   150,   150,   150,   150,
     150,   150,   150,   150,   150,   150,   150,   150,   151,   152,
     153,   154,   154,   154,   155,   155,   155,   155,   156,   156,
     156,   156,   156,   156,   156,   156,   156,   156,   156,   156,
     156,   156,   156,   156,   156,   156,   156,   156,   156,   156,
     157,   158
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     2,     1,     2,     3,     5,
       4,     6,     3,     2,     3,     5,     4,     1,     2,     1,
       2,     3,     3,     2,     2,     5,     5,     4,     4,     4,
       3,     6,     5,     2,     3,     1,     3,     2,     3,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     3,     3,     2,     3,     2,     3,     3,     1,     2,
       1,     2,     2,     1,     1,     1,     1,     2,     2,     1,
       1,     1,     1,     1,     2,     2,     2,     2,     1,     1,
       2,     2,     4,     2,     4,     3,     5,     5,     3,     5,
       4,     6,     2,     2,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     2,     3,     3,     3,     3,     3,
       3,     5,     4,     2,     4,     3,     4,     1,     3,     5,
       2,     2,     3,     5,     7,     9,     4,     4,     5,     5,
       2,     5,     1,     2,     1,     2,     2,     3,     5,     4,
       3,     1,     1,     1,     2,     3,     2,     7,     9,     8,
       8,     8,     7,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     1,     3,     1,     2,     1,     3,     1,     3,
       3,     1,     3,     3,     1,     3,     1,     2,     2,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     1,     1,     1,     1,     1,     1,     3,     1,     2,
       2,     3,     3,     3,     3,     3,     3,     3,     1,     1,
       1,     4,     4,     3,     4,     3,     4,     4,     1,     3,
       3,     3,     6,     6,     5,     6,     5,     6,     6,     3,
       1,     5
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 248 "sintactico.y" /* yacc.c:1646  */
    { cout<<"aceptada"<<endl;nodoRaiz->clasesArchivo= (yyvsp[0].LISTA_CLASES)->clasesArchivo; nodoRaiz->fila = yylineno; nodoRaiz->esExpresion=false; }
#line 1885 "parser.cpp" /* yacc.c:1646  */
    break;

  case 3:
#line 249 "sintactico.y" /* yacc.c:1646  */
    {nodoRaiz->esExpresion = true; nodoRaiz->expresion= (yyvsp[0].NODO);}
#line 1891 "parser.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 252 "sintactico.y" /* yacc.c:1646  */
    { 
    (yyval.LISTA_CLASES) = new ListaClases(); 
    (yyval.LISTA_CLASES)->insertarClase((yyvsp[0].CLASE)); 
   
    }
#line 1901 "parser.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 257 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.LISTA_CLASES)= (yyvsp[-1].LISTA_CLASES); (yyval.LISTA_CLASES)->insertarClase((yyvsp[0].CLASE));
       
        }
#line 1910 "parser.cpp" /* yacc.c:1646  */
    break;

  case 6:
#line 262 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.LISTA_CLASES) = new ListaClases(); 
            (yyval.LISTA_CLASES)->addImportacion((yyvsp[0].NODO));
            
        }
#line 1920 "parser.cpp" /* yacc.c:1646  */
    break;

  case 7:
#line 268 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.LISTA_CLASES)=(yyvsp[-1].LISTA_CLASES);
            (yyval.LISTA_CLASES)->addImportacion((yyvsp[0].NODO));
           

        }
#line 1931 "parser.cpp" /* yacc.c:1646  */
    break;

  case 8:
#line 277 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[0].CLASE);
            (yyval.CLASE)->nombreClase=(yyvsp[-1].TEXT);
            (yyval.CLASE)->agregarNombreClaseFunciones((yyvsp[-1].TEXT));
            
            
        }
#line 1943 "parser.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 285 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[0].CLASE);
            (yyval.CLASE)->nombreClase=(yyvsp[-3].TEXT);
            (yyval.CLASE)->herencia=(yyvsp[-1].TEXT);
            (yyval.CLASE)->agregarNombreClaseFunciones((yyvsp[-3].TEXT));
            
            
        }
#line 1956 "parser.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 294 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[0].CLASE);
            (yyval.CLASE)->nombreClase=(yyvsp[-1].TEXT);
            (yyval.CLASE)->agregarNombreClaseFunciones((yyvsp[-1].TEXT));

        }
#line 1967 "parser.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 302 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[0].CLASE);
            (yyval.CLASE)->nombreClase=(yyvsp[-3].TEXT);
            (yyval.CLASE)->herencia=(yyvsp[-1].TEXT);
            (yyval.CLASE)->agregarNombreClaseFunciones((yyvsp[-3].TEXT));

        }
#line 1979 "parser.cpp" /* yacc.c:1646  */
    break;

  case 12:
#line 312 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[-1].CLASE);
            
        }
#line 1988 "parser.cpp" /* yacc.c:1646  */
    break;

  case 13:
#line 316 "sintactico.y" /* yacc.c:1646  */
    {(yyval.CLASE)= new Clase();}
#line 1994 "parser.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 317 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Error al generar cuerpo de la clase ");}
#line 2000 "parser.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 321 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("IMPORTAR",(yyvsp[-2].TEXT));
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 2010 "parser.cpp" /* yacc.c:1646  */
    break;

  case 16:
#line 328 "sintactico.y" /* yacc.c:1646  */
    {(yyval.PRINC) = new mPrincipal((yyvsp[0].NODO));}
#line 2016 "parser.cpp" /* yacc.c:1646  */
    break;

  case 17:
#line 331 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)= new Clase();
            (yyval.CLASE)->insertarFuncion((yyvsp[0].FUNC));
            
        }
#line 2026 "parser.cpp" /* yacc.c:1646  */
    break;

  case 18:
#line 337 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)= new Clase();
            (yyvsp[0].FUNC)->sobreEscrita= true;
            (yyval.CLASE)->insertarFuncion((yyvsp[0].FUNC));
            
        }
#line 2037 "parser.cpp" /* yacc.c:1646  */
    break;

  case 19:
#line 344 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)= new Clase();
            (yyval.CLASE)->insertarPrincipal((yyvsp[0].PRINC));
            
        }
#line 2047 "parser.cpp" /* yacc.c:1646  */
    break;

  case 20:
#line 350 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.CLASE)= new Clase();
        (yyval.CLASE)->insertarAtributo((yyvsp[-1].NODO));
       (yyval.CLASE)->fila = (yylsp[-1]).first_line; 
    }
#line 2057 "parser.cpp" /* yacc.c:1646  */
    break;

  case 21:
#line 356 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[-2].CLASE);
            (yyval.CLASE)->insertarAtributo((yyvsp[-1].NODO));
            
        }
#line 2067 "parser.cpp" /* yacc.c:1646  */
    break;

  case 22:
#line 362 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)= (yyvsp[-2].CLASE);
            (yyvsp[0].FUNC)->sobreEscrita= true;
            (yyval.CLASE)->insertarFuncion((yyvsp[0].FUNC));
            
        }
#line 2078 "parser.cpp" /* yacc.c:1646  */
    break;

  case 23:
#line 369 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.CLASE)= (yyvsp[-1].CLASE);
        (yyval.CLASE)->insertarFuncion((yyvsp[0].FUNC));
       

        }
#line 2089 "parser.cpp" /* yacc.c:1646  */
    break;

  case 24:
#line 376 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)= (yyvsp[-1].CLASE);
            (yyval.CLASE)->insertarPrincipal((yyvsp[0].PRINC));
            
        }
#line 2099 "parser.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 387 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion ((yyvsp[-4].NODO)->valor,(yyvsp[-3].NODO)->valor,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->fila = (yylsp[-4]).first_line; 
            
        }
#line 2109 "parser.cpp" /* yacc.c:1646  */
    break;

  case 26:
#line 393 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion ((yyvsp[-4].NODO)->valor,VACIO,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->fila = (yylsp[-4]).first_line;
            
        }
#line 2119 "parser.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 400 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion (PUBLICO,(yyvsp[-3].NODO)->valor,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->fila = (yylsp[-3]).first_line;
            
        }
#line 2129 "parser.cpp" /* yacc.c:1646  */
    break;

  case 28:
#line 406 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion (PUBLICO,VACIO,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->fila = (yylsp[-3]).first_line;
            
        }
#line 2139 "parser.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 413 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion ((yyvsp[-3].NODO)->valor,VACIO,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->cambiarAConstructor();
            (yyval.FUNC)->fila = (yylsp[-3]).first_line;
            
        }
#line 2150 "parser.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 420 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion (PUBLICO,VACIO,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->cambiarAConstructor();
            (yyval.FUNC)->fila = (yylsp[-2]).first_line;
            
        }
#line 2161 "parser.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 427 "sintactico.y" /* yacc.c:1646  */
    {
             (yyval.FUNC) = new Funcion ((yyvsp[-5].NODO)->valor,(yyvsp[-4].NODO)->valor,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
             (yyval.FUNC)->setArregloRetorno();
             (yyval.FUNC)->setNumeroDimenesionesRetorno((yyvsp[-3].NODO));
             (yyval.FUNC)->fila = (yylsp[-5]).first_line;
             
        }
#line 2173 "parser.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 435 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.FUNC) = new Funcion (PUBLICO,(yyvsp[-4].NODO)->valor,(yyvsp[-2].TEXT),(yyvsp[-1].NODO),(yyvsp[0].NODO));
            (yyval.FUNC)->setArregloRetorno();
            (yyval.FUNC)->setNumeroDimenesionesRetorno((yyvsp[-3].NODO));
            (yyval.FUNC)->fila = (yylsp[-4]).first_line;
            
        }
#line 2185 "parser.cpp" /* yacc.c:1646  */
    break;

  case 33:
#line 445 "sintactico.y" /* yacc.c:1646  */
    {
             (yyval.NODO)= new nodoArbol("COR_VACIOS","");
             nodoArbol *n= new nodoArbol("COR_VACIOS","");  
             (yyval.NODO)->addHijo(n);
             (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2197 "parser.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 453 "sintactico.y" /* yacc.c:1646  */
    {
             (yyval.NODO)= (yyvsp[-2].NODO);
             nodoArbol *n= new nodoArbol("COR_VACIOS","");  
             (yyval.NODO)->addHijo(n);
             (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2209 "parser.cpp" /* yacc.c:1646  */
    break;

  case 35:
#line 461 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("LISTA_EXPRESION",""); (yyval.NODO)->addHijo((yyvsp[0].NODO));(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2216 "parser.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 463 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-2].NODO); (yyval.NODO)->addHijo((yyvsp[0].NODO));(yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2223 "parser.cpp" /* yacc.c:1646  */
    break;

  case 37:
#line 467 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("LISTA_EXPRESION",""); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2230 "parser.cpp" /* yacc.c:1646  */
    break;

  case 38:
#line 469 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2237 "parser.cpp" /* yacc.c:1646  */
    break;

  case 39:
#line 472 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("INSTANCIA", (yyvsp[-1].TEXT));
    (yyval.NODO)->addHijo((yyvsp[0].NODO)); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2245 "parser.cpp" /* yacc.c:1646  */
    break;

  case 40:
#line 476 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_ENTERO,T_ENTERO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2252 "parser.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 478 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_DECIMAL,T_DECIMAL); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2259 "parser.cpp" /* yacc.c:1646  */
    break;

  case 42:
#line 480 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_CARACTER,T_CARACTER); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2266 "parser.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 482 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_BOOLEANO,T_BOOLEANO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2273 "parser.cpp" /* yacc.c:1646  */
    break;

  case 44:
#line 484 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2280 "parser.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 487 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2287 "parser.cpp" /* yacc.c:1646  */
    break;

  case 46:
#line 489 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2294 "parser.cpp" /* yacc.c:1646  */
    break;

  case 47:
#line 491 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2301 "parser.cpp" /* yacc.c:1646  */
    break;

  case 48:
#line 494 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("PARAMETROS","");(yyval.NODO)->addHijo((yyvsp[0].NODO)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2308 "parser.cpp" /* yacc.c:1646  */
    break;

  case 49:
#line 497 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= (yyvsp[-2].NODO);
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 2319 "parser.cpp" /* yacc.c:1646  */
    break;

  case 50:
#line 503 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("PARAMETROS", ""); 
    (yyval.NODO)->addHijo((yyvsp[0].NODO)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2328 "parser.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 508 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)=(yyvsp[-2].NODO);
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2339 "parser.cpp" /* yacc.c:1646  */
    break;

  case 52:
#line 517 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2346 "parser.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 519 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("PARAMETROS",""); (yyval.NODO)->fila = (yylsp[-1]).first_line;
       (yyval.NODO)->columna = columna;}
#line 2353 "parser.cpp" /* yacc.c:1646  */
    break;

  case 54:
#line 521 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Error al ingresar parametros de funcion ");}
#line 2359 "parser.cpp" /* yacc.c:1646  */
    break;

  case 55:
#line 523 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("INSTRUCCIONES",""); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2366 "parser.cpp" /* yacc.c:1646  */
    break;

  case 56:
#line 525 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2373 "parser.cpp" /* yacc.c:1646  */
    break;

  case 57:
#line 527 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Error en instruccion ");}
#line 2379 "parser.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 530 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol("INSTRUCCIONES",""); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 2385 "parser.cpp" /* yacc.c:1646  */
    break;

  case 59:
#line 531 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 2391 "parser.cpp" /* yacc.c:1646  */
    break;

  case 60:
#line 534 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2398 "parser.cpp" /* yacc.c:1646  */
    break;

  case 61:
#line 536 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2405 "parser.cpp" /* yacc.c:1646  */
    break;

  case 62:
#line 538 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2412 "parser.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 540 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2419 "parser.cpp" /* yacc.c:1646  */
    break;

  case 64:
#line 542 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2426 "parser.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 544 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2433 "parser.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 546 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2440 "parser.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 548 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2447 "parser.cpp" /* yacc.c:1646  */
    break;

  case 68:
#line 550 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2454 "parser.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 552 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2461 "parser.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 554 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2468 "parser.cpp" /* yacc.c:1646  */
    break;

  case 71:
#line 556 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2475 "parser.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 558 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2482 "parser.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 560 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2489 "parser.cpp" /* yacc.c:1646  */
    break;

  case 74:
#line 562 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2496 "parser.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 564 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2503 "parser.cpp" /* yacc.c:1646  */
    break;

  case 76:
#line 566 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2510 "parser.cpp" /* yacc.c:1646  */
    break;

  case 77:
#line 568 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2517 "parser.cpp" /* yacc.c:1646  */
    break;

  case 78:
#line 570 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2524 "parser.cpp" /* yacc.c:1646  */
    break;

  case 79:
#line 572 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2531 "parser.cpp" /* yacc.c:1646  */
    break;

  case 80:
#line 574 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2538 "parser.cpp" /* yacc.c:1646  */
    break;

  case 81:
#line 576 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2545 "parser.cpp" /* yacc.c:1646  */
    break;

  case 82:
#line 583 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","1");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
        (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2561 "parser.cpp" /* yacc.c:1646  */
    break;

  case 83:
#line 595 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","2");
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
        (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2576 "parser.cpp" /* yacc.c:1646  */
    break;

  case 84:
#line 607 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","3");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
        (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2592 "parser.cpp" /* yacc.c:1646  */
    break;

  case 85:
#line 619 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","4");
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
        (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2608 "parser.cpp" /* yacc.c:1646  */
    break;

  case 86:
#line 631 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","5");
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2625 "parser.cpp" /* yacc.c:1646  */
    break;

  case 87:
#line 644 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","1");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2640 "parser.cpp" /* yacc.c:1646  */
    break;

  case 88:
#line 655 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","2");
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2654 "parser.cpp" /* yacc.c:1646  */
    break;

  case 89:
#line 666 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","3");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2669 "parser.cpp" /* yacc.c:1646  */
    break;

  case 90:
#line 677 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","4");
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2684 "parser.cpp" /* yacc.c:1646  */
    break;

  case 91:
#line 688 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO","5");
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo((yyvsp[-5].NODO));
        (yyval.NODO)->fila = (yylsp[-5]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2700 "parser.cpp" /* yacc.c:1646  */
    break;

  case 92:
#line 700 "sintactico.y" /* yacc.c:1646  */
    { 
        nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "6");
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
          (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "6");
          (yyval.NODO)->addHijo((yyvsp[0].NODO));
          (yyval.NODO)->addHijo((yyvsp[-1].NODO));
          (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
      }
#line 2715 "parser.cpp" /* yacc.c:1646  */
    break;

  case 93:
#line 711 "sintactico.y" /* yacc.c:1646  */
    {
          (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "7");
          (yyval.NODO)->addHijo((yyvsp[0].NODO));
          (yyval.NODO)->addHijo((yyvsp[-1].NODO));
          (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
      }
#line 2727 "parser.cpp" /* yacc.c:1646  */
    break;

  case 94:
#line 719 "sintactico.y" /* yacc.c:1646  */
    {
          (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "8");
          (yyval.NODO)->addHijo((yyvsp[0].NODO));
          (yyval.NODO)->addHijo((yyvsp[-1].NODO));
          (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
      }
#line 2739 "parser.cpp" /* yacc.c:1646  */
    break;

  case 95:
#line 727 "sintactico.y" /* yacc.c:1646  */
    {
    nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
    (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "6");
    (yyval.NODO)->addHijo((yyvsp[0].NODO));
    (yyval.NODO)->addHijo(v);
    (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;


    }
#line 2754 "parser.cpp" /* yacc.c:1646  */
    break;

  case 96:
#line 738 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "7");
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
        (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2768 "parser.cpp" /* yacc.c:1646  */
    break;

  case 97:
#line 748 "sintactico.y" /* yacc.c:1646  */
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        (yyval.NODO)= new nodoArbol("DECLA_ATRIBUTO", "8");
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->addHijo(v);
(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 2781 "parser.cpp" /* yacc.c:1646  */
    break;

  case 98:
#line 764 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("/=","/=");}
#line 2787 "parser.cpp" /* yacc.c:1646  */
    break;

  case 99:
#line 765 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("+=","+=");}
#line 2793 "parser.cpp" /* yacc.c:1646  */
    break;

  case 100:
#line 766 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("-=","-=");}
#line 2799 "parser.cpp" /* yacc.c:1646  */
    break;

  case 101:
#line 767 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("*=","*=");}
#line 2805 "parser.cpp" /* yacc.c:1646  */
    break;

  case 102:
#line 768 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("=","=");}
#line 2811 "parser.cpp" /* yacc.c:1646  */
    break;

  case 103:
#line 771 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ASIGNACION", "1");
            nodoArbol *num = new nodoArbol(T_ENTERO, "1");
            nodoArbol *exp = new nodoArbol("SUMA","");
            nodoArbol *igual = new nodoArbol("=","=");
            exp->addHijo((yyvsp[-1].NODO));
            exp->addHijo(num);
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo(igual);
            (yyval.NODO)->addHijo(exp);
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2829 "parser.cpp" /* yacc.c:1646  */
    break;

  case 104:
#line 785 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ASIGNACION", "1");
            nodoArbol *num = new nodoArbol(T_ENTERO, "1");
            nodoArbol *exp = new nodoArbol("RESTA","");
            nodoArbol *igual = new nodoArbol("=","=");
            exp->addHijo((yyvsp[-1].NODO));
            exp->addHijo(num);
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo(igual);
            (yyval.NODO)->addHijo(exp);
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2847 "parser.cpp" /* yacc.c:1646  */
    break;

  case 105:
#line 799 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ASIGNACION", "2");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2860 "parser.cpp" /* yacc.c:1646  */
    break;

  case 106:
#line 808 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ASIGNACION", "3");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2873 "parser.cpp" /* yacc.c:1646  */
    break;

  case 107:
#line 816 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("ASIGNACION", "1");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 2886 "parser.cpp" /* yacc.c:1646  */
    break;

  case 108:
#line 825 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ASIGNACION", "1");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2899 "parser.cpp" /* yacc.c:1646  */
    break;

  case 109:
#line 835 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ASIGNACION", "4");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 2912 "parser.cpp" /* yacc.c:1646  */
    break;

  case 110:
#line 843 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ASIGNACION","5");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

        }
#line 2926 "parser.cpp" /* yacc.c:1646  */
    break;

  case 111:
#line 856 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("MOSTRAR_EDD", "");
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 2937 "parser.cpp" /* yacc.c:1646  */
    break;

  case 112:
#line 864 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLARACION","1");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2951 "parser.cpp" /* yacc.c:1646  */
    break;

  case 113:
#line 874 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLARACION","2");
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2964 "parser.cpp" /* yacc.c:1646  */
    break;

  case 114:
#line 884 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLARACION","3");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 2978 "parser.cpp" /* yacc.c:1646  */
    break;

  case 115:
#line 895 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("COL_ARREGLO",""); (yyval.NODO)->addHijo((yyvsp[-1].NODO)); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2985 "parser.cpp" /* yacc.c:1646  */
    break;

  case 116:
#line 897 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-3].NODO); (yyval.NODO)->addHijo((yyvsp[-1].NODO)); (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;}
#line 2992 "parser.cpp" /* yacc.c:1646  */
    break;

  case 117:
#line 899 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Error al crear posicion de arreglo ");}
#line 2998 "parser.cpp" /* yacc.c:1646  */
    break;

  case 118:
#line 902 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ARREGLO","1");
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 3012 "parser.cpp" /* yacc.c:1646  */
    break;

  case 119:
#line 912 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ARREGLO","2");
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 3027 "parser.cpp" /* yacc.c:1646  */
    break;

  case 120:
#line 923 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("CONTINUAR","CONTINUAR"); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3034 "parser.cpp" /* yacc.c:1646  */
    break;

  case 121:
#line 925 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("DETENER","DETENER"); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3041 "parser.cpp" /* yacc.c:1646  */
    break;

  case 122:
#line 927 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("RETORNO","RETORNO"); (yyval.NODO)->addHijo((yyvsp[-1].NODO)); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3048 "parser.cpp" /* yacc.c:1646  */
    break;

  case 123:
#line 930 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("IMPRIMIR",""); (yyval.NODO)->addHijo((yyvsp[-2].NODO)); (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3055 "parser.cpp" /* yacc.c:1646  */
    break;

  case 124:
#line 935 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("CONCATENAR", "1");
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->fila = (yylsp[-6]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 3067 "parser.cpp" /* yacc.c:1646  */
    break;

  case 125:
#line 943 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("CONCATENAR", "2");
        (yyval.NODO)->addHijo((yyvsp[-6].NODO));
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->fila = (yylsp[-8]).first_line;
        (yyval.NODO)->columna = columna;

    }
#line 3081 "parser.cpp" /* yacc.c:1646  */
    break;

  case 126:
#line 954 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("CONVERTIR_A_CADENA","");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3092 "parser.cpp" /* yacc.c:1646  */
    break;

  case 127:
#line 962 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("CONVERTIR_A_ENTERO","");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3103 "parser.cpp" /* yacc.c:1646  */
    break;

  case 128:
#line 971 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("MIENTRAS","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3115 "parser.cpp" /* yacc.c:1646  */
    break;

  case 129:
#line 980 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI_1","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3127 "parser.cpp" /* yacc.c:1646  */
    break;

  case 130:
#line 989 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SINO","");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3138 "parser.cpp" /* yacc.c:1646  */
    break;

  case 131:
#line 997 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","1");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3150 "parser.cpp" /* yacc.c:1646  */
    break;

  case 132:
#line 1006 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("L_SINO_SI","");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3161 "parser.cpp" /* yacc.c:1646  */
    break;

  case 133:
#line 1013 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= (yyvsp[-1].NODO);
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3172 "parser.cpp" /* yacc.c:1646  */
    break;

  case 134:
#line 1021 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","1");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3183 "parser.cpp" /* yacc.c:1646  */
    break;

  case 135:
#line 1028 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","2");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3195 "parser.cpp" /* yacc.c:1646  */
    break;

  case 136:
#line 1036 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","3");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3207 "parser.cpp" /* yacc.c:1646  */
    break;

  case 137:
#line 1044 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","4");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3220 "parser.cpp" /* yacc.c:1646  */
    break;

  case 138:
#line 1054 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SELECCIONA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3232 "parser.cpp" /* yacc.c:1646  */
    break;

  case 139:
#line 1063 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("CASO","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3244 "parser.cpp" /* yacc.c:1646  */
    break;

  case 140:
#line 1072 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("DEFECTO", "");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3255 "parser.cpp" /* yacc.c:1646  */
    break;

  case 141:
#line 1079 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3262 "parser.cpp" /* yacc.c:1646  */
    break;

  case 142:
#line 1081 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3269 "parser.cpp" /* yacc.c:1646  */
    break;

  case 143:
#line 1084 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("CUERPO_SELECCIONA",""); (yyval.NODO)->addHijo((yyvsp[0].NODO)); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3276 "parser.cpp" /* yacc.c:1646  */
    break;

  case 144:
#line 1086 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->addHijo((yyvsp[0].NODO));(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3283 "parser.cpp" /* yacc.c:1646  */
    break;

  case 145:
#line 1089 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3290 "parser.cpp" /* yacc.c:1646  */
    break;

  case 146:
#line 1091 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("CUERPO_SELECCIONA",""); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3297 "parser.cpp" /* yacc.c:1646  */
    break;

  case 147:
#line 1095 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("HACER_MIENTRAS","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-5].NODO));
            (yyval.NODO)->fila = (yylsp[-6]).first_line;
            (yyval.NODO)->columna = columna;
        }
#line 3309 "parser.cpp" /* yacc.c:1646  */
    break;

  case 148:
#line 1104 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("PARA","");
            (yyval.NODO)->addHijo((yyvsp[-6].NODO));
            (yyval.NODO)->addHijo((yyvsp[-4].NODO));
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-8]).first_line;
            (yyval.NODO)->columna = columna;
        }
#line 3323 "parser.cpp" /* yacc.c:1646  */
    break;

  case 149:
#line 1115 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("DECLA_LISTA",(yyvsp[-6].TEXT));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->fila = (yylsp[-7]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3334 "parser.cpp" /* yacc.c:1646  */
    break;

  case 150:
#line 1123 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("DECLA_COLA",(yyvsp[-6].TEXT));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->fila = (yylsp[-7]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3345 "parser.cpp" /* yacc.c:1646  */
    break;

  case 151:
#line 1131 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("DECLA_PILA",(yyvsp[-6].TEXT));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->fila = (yylsp[-7]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3356 "parser.cpp" /* yacc.c:1646  */
    break;

  case 152:
#line 1139 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("LEER_TECLADO",(yyvsp[-2].TEXT));
            (yyval.NODO)->addHijo((yyvsp[-4].NODO));
            (yyval.NODO)->fila = (yylsp[-6]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3367 "parser.cpp" /* yacc.c:1646  */
    break;

  case 153:
#line 1148 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("<","<");}
#line 3373 "parser.cpp" /* yacc.c:1646  */
    break;

  case 154:
#line 1149 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(">",">");}
#line 3379 "parser.cpp" /* yacc.c:1646  */
    break;

  case 155:
#line 1150 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("<=","<=");}
#line 3385 "parser.cpp" /* yacc.c:1646  */
    break;

  case 156:
#line 1151 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(">=",">=");}
#line 3391 "parser.cpp" /* yacc.c:1646  */
    break;

  case 157:
#line 1152 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("!=","!=");}
#line 3397 "parser.cpp" /* yacc.c:1646  */
    break;

  case 158:
#line 1153 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("==","==");}
#line 3403 "parser.cpp" /* yacc.c:1646  */
    break;

  case 159:
#line 1155 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3410 "parser.cpp" /* yacc.c:1646  */
    break;

  case 160:
#line 1158 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3417 "parser.cpp" /* yacc.c:1646  */
    break;

  case 161:
#line 1162 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("OR", "");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3429 "parser.cpp" /* yacc.c:1646  */
    break;

  case 162:
#line 1169 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3436 "parser.cpp" /* yacc.c:1646  */
    break;

  case 163:
#line 1173 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("AND", "");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3448 "parser.cpp" /* yacc.c:1646  */
    break;

  case 164:
#line 1180 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3455 "parser.cpp" /* yacc.c:1646  */
    break;

  case 165:
#line 1183 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("NOT",""); (yyval.NODO)->addHijo((yyvsp[0].NODO)); (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3462 "parser.cpp" /* yacc.c:1646  */
    break;

  case 166:
#line 1185 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3469 "parser.cpp" /* yacc.c:1646  */
    break;

  case 167:
#line 1190 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("RELACIONAL", (yyvsp[-1].NODO)->valor);
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3481 "parser.cpp" /* yacc.c:1646  */
    break;

  case 168:
#line 1198 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= (yyvsp[0].NODO);
            (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3491 "parser.cpp" /* yacc.c:1646  */
    break;

  case 169:
#line 1205 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SUMA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3503 "parser.cpp" /* yacc.c:1646  */
    break;

  case 170:
#line 1213 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("RESTA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3515 "parser.cpp" /* yacc.c:1646  */
    break;

  case 171:
#line 1220 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3522 "parser.cpp" /* yacc.c:1646  */
    break;

  case 172:
#line 1224 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("MULTIPLICACION","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3534 "parser.cpp" /* yacc.c:1646  */
    break;

  case 173:
#line 1232 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("DIVISION","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3546 "parser.cpp" /* yacc.c:1646  */
    break;

  case 174:
#line 1239 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3553 "parser.cpp" /* yacc.c:1646  */
    break;

  case 175:
#line 1243 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("POTENCIA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3565 "parser.cpp" /* yacc.c:1646  */
    break;

  case 176:
#line 1250 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO); (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3572 "parser.cpp" /* yacc.c:1646  */
    break;

  case 177:
#line 1255 "sintactico.y" /* yacc.c:1646  */
    {
             nodoArbol *nEntero = new nodoArbol(T_ENTERO, "1");
            (yyval.NODO)= new nodoArbol("SUMA","");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo(nEntero);
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3585 "parser.cpp" /* yacc.c:1646  */
    break;

  case 178:
#line 1264 "sintactico.y" /* yacc.c:1646  */
    { nodoArbol *nEntero = new nodoArbol(T_ENTERO, "1");
            (yyval.NODO)= new nodoArbol("RESTA","");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo(nEntero);
            (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
        }
#line 3597 "parser.cpp" /* yacc.c:1646  */
    break;

  case 179:
#line 1271 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3604 "parser.cpp" /* yacc.c:1646  */
    break;

  case 180:
#line 1276 "sintactico.y" /* yacc.c:1646  */
    {

            nodoArbol *nEntero = new nodoArbol(T_ENTERO, "-1");
            (yyval.NODO)= new nodoArbol("MULTIPLICACION","");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->addHijo(nEntero);
            (yyval.NODO)->fila = (yylsp[0]).first_line;
            (yyval.NODO)->columna = columna;
        }
#line 3618 "parser.cpp" /* yacc.c:1646  */
    break;

  case 181:
#line 1285 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3625 "parser.cpp" /* yacc.c:1646  */
    break;

  case 182:
#line 1288 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_ENTERO, (yyvsp[0].TEXT));(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3632 "parser.cpp" /* yacc.c:1646  */
    break;

  case 183:
#line 1290 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_DECIMAL, (yyvsp[0].TEXT));(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3639 "parser.cpp" /* yacc.c:1646  */
    break;

  case 184:
#line 1292 "sintactico.y" /* yacc.c:1646  */
    {
                string c ="";
                int t = strlen((yyvsp[0].TEXT));
                int indice = 1;
                while(indice < t-1){
                c = c + (yyvsp[0].TEXT)[indice];
               indice++;
                }
                (yyval.NODO) = new nodoArbol(T_CADENA, c);
                (yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;
         }
#line 3656 "parser.cpp" /* yacc.c:1646  */
    break;

  case 185:
#line 1306 "sintactico.y" /* yacc.c:1646  */
    {
               string c1 ="";
               int t = strlen((yyvsp[0].TEXT));
               int indice = 1;
               while(indice < t-1){
                    c1 = c1+(yyvsp[0].TEXT)[indice];
                    indice++;
                   }

                char c= c1.at(0);
                int n = (int)c;
                ostringstream str1;
                str1 << n;
                string g= str1.str();    
               (yyval.NODO) = new nodoArbol(T_CARACTER, g);
               (yyval.NODO)->fila = (yylsp[0]).first_line;
                (yyval.NODO)->columna = columna;

        }
#line 3680 "parser.cpp" /* yacc.c:1646  */
    break;

  case 186:
#line 1325 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_BOOLEANO, "1");(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3687 "parser.cpp" /* yacc.c:1646  */
    break;

  case 187:
#line 1327 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_BOOLEANO, "0");(yyval.NODO)->fila = (yylsp[0]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3694 "parser.cpp" /* yacc.c:1646  */
    break;

  case 188:
#line 1329 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3700 "parser.cpp" /* yacc.c:1646  */
    break;

  case 189:
#line 1330 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= (yyvsp[0].NODO);}
#line 3706 "parser.cpp" /* yacc.c:1646  */
    break;

  case 190:
#line 1331 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);(yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;}
#line 3713 "parser.cpp" /* yacc.c:1646  */
    break;

  case 191:
#line 1333 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3719 "parser.cpp" /* yacc.c:1646  */
    break;

  case 192:
#line 1334 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3725 "parser.cpp" /* yacc.c:1646  */
    break;

  case 193:
#line 1335 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3731 "parser.cpp" /* yacc.c:1646  */
    break;

  case 194:
#line 1336 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3737 "parser.cpp" /* yacc.c:1646  */
    break;

  case 195:
#line 1337 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3743 "parser.cpp" /* yacc.c:1646  */
    break;

  case 196:
#line 1338 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 3749 "parser.cpp" /* yacc.c:1646  */
    break;

  case 197:
#line 1339 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Error en expresion ");}
#line 3755 "parser.cpp" /* yacc.c:1646  */
    break;

  case 198:
#line 1345 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("ID",(yyvsp[0].TEXT));}
#line 3761 "parser.cpp" /* yacc.c:1646  */
    break;

  case 199:
#line 1348 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("LLAMADA_FUNCION",(yyvsp[-1].TEXT));
        (yyval.NODO)->hijos= (yyvsp[0].NODO)->hijos;
        (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 3772 "parser.cpp" /* yacc.c:1646  */
    break;

  case 200:
#line 1356 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("POS_ARREGLO",(yyvsp[-1].TEXT));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
        (yyval.NODO)->fila = (yylsp[-1]).first_line;
        (yyval.NODO)->columna = columna;
    }
#line 3783 "parser.cpp" /* yacc.c:1646  */
    break;

  case 201:
#line 1365 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ACCESO", "1");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;
			
		}
#line 3796 "parser.cpp" /* yacc.c:1646  */
    break;

  case 202:
#line 1374 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ACCESO", "2");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3809 "parser.cpp" /* yacc.c:1646  */
    break;

  case 203:
#line 1383 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ACCESO", "3");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3822 "parser.cpp" /* yacc.c:1646  */
    break;

  case 204:
#line 1393 "sintactico.y" /* yacc.c:1646  */
    {

            (yyval.NODO) = new nodoArbol("ESTE", "1");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;
		}
#line 3834 "parser.cpp" /* yacc.c:1646  */
    break;

  case 205:
#line 1401 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ESTE", "2");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3846 "parser.cpp" /* yacc.c:1646  */
    break;

  case 206:
#line 1410 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ESTE", "3");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3858 "parser.cpp" /* yacc.c:1646  */
    break;

  case 207:
#line 1418 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO) = new nodoArbol("ESTE", "4");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3870 "parser.cpp" /* yacc.c:1646  */
    break;

  case 208:
#line 1428 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ATRI", "");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[0]).first_line;
            (yyval.NODO)->columna = columna;
		}
#line 3881 "parser.cpp" /* yacc.c:1646  */
    break;

  case 209:
#line 1435 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ATRI", "");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[0]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3893 "parser.cpp" /* yacc.c:1646  */
    break;

  case 210:
#line 1443 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("ATRI", "");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[0]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3905 "parser.cpp" /* yacc.c:1646  */
    break;

  case 211:
#line 1451 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("INSERTAR", "");
            n->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3919 "parser.cpp" /* yacc.c:1646  */
    break;

  case 212:
#line 1461 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("APILAR", "");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
            (yyval.NODO)->columna = columna;
		}
#line 3932 "parser.cpp" /* yacc.c:1646  */
    break;

  case 213:
#line 1470 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("DESAPILAR", "");
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3945 "parser.cpp" /* yacc.c:1646  */
    break;

  case 214:
#line 1479 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("ENCOLAR", "");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
            (yyval.NODO)->columna = columna;


		}
#line 3960 "parser.cpp" /* yacc.c:1646  */
    break;

  case 215:
#line 1490 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("DESENCOLAR", "");
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3973 "parser.cpp" /* yacc.c:1646  */
    break;

  case 216:
#line 1499 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("OBTENER", "");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 3987 "parser.cpp" /* yacc.c:1646  */
    break;

  case 217:
#line 1509 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("BUSCAR", "");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-3]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4001 "parser.cpp" /* yacc.c:1646  */
    break;

  case 218:
#line 1519 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("TAMANIO", "");
            (yyval.NODO)= new nodoArbol("ATRI","");
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[0]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4014 "parser.cpp" /* yacc.c:1646  */
    break;

  case 219:
#line 1529 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)=(yyvsp[-2].NODO);
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4026 "parser.cpp" /* yacc.c:1646  */
    break;

  case 220:
#line 1537 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)=(yyvsp[-2].NODO);
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4038 "parser.cpp" /* yacc.c:1646  */
    break;

  case 221:
#line 1545 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)=(yyvsp[-2].NODO);
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4050 "parser.cpp" /* yacc.c:1646  */
    break;

  case 222:
#line 1553 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("INSERTAR", "");
            n->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)=(yyvsp[-5].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-5]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4064 "parser.cpp" /* yacc.c:1646  */
    break;

  case 223:
#line 1563 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("APILAR", "");
            n->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)=(yyvsp[-5].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-5]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4078 "parser.cpp" /* yacc.c:1646  */
    break;

  case 224:
#line 1573 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("DESAPILAR", "");
            (yyval.NODO)=(yyvsp[-4].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-4]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4091 "parser.cpp" /* yacc.c:1646  */
    break;

  case 225:
#line 1582 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("ENCOLAR", "");
            n->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)=(yyvsp[-5].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-5]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4105 "parser.cpp" /* yacc.c:1646  */
    break;

  case 226:
#line 1592 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("DESENCOLAR", "");
            (yyval.NODO)=(yyvsp[-4].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-4]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4118 "parser.cpp" /* yacc.c:1646  */
    break;

  case 227:
#line 1601 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("OBTENER", "");
            n->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)=(yyvsp[-5].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-5]).first_line;
            (yyval.NODO)->columna = columna;
		}
#line 4131 "parser.cpp" /* yacc.c:1646  */
    break;

  case 228:
#line 1610 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("BUSCAR", "");
            n->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)=(yyvsp[-5].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-5]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4145 "parser.cpp" /* yacc.c:1646  */
    break;

  case 229:
#line 1620 "sintactico.y" /* yacc.c:1646  */
    {
            nodoArbol *n = new nodoArbol("TAMANIO", "");
            (yyval.NODO)=(yyvsp[-2].NODO);
            (yyval.NODO)->addHijo(n);
            (yyval.NODO)->fila = (yylsp[-2]).first_line;
            (yyval.NODO)->columna = columna;

		}
#line 4158 "parser.cpp" /* yacc.c:1646  */
    break;

  case 230:
#line 1630 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol("NULO", "NULO"); (yyval.NODO)->columna= columna; (yyval.NODO)->fila = (yylsp[0]).first_line; }
#line 4164 "parser.cpp" /* yacc.c:1646  */
    break;

  case 231:
#line 1633 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("TERNARIO","");
        (yyval.NODO)->columna = columna;
        (yyval.NODO)->fila = (yylsp[-4]).first_line;
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));
    }
#line 4177 "parser.cpp" /* yacc.c:1646  */
    break;


#line 4181 "parser.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
