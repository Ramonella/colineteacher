#ifndef TEMPORAL_H
#define TEMPORAL_H


#include <string>
#include<string.h>
#include <sstream>
#include <iostream>


using namespace std;

class Temporal
{
public:
    string nombre;
    double valor;
    bool esUsado;
    Temporal(string nom, double val);
    void usado(bool val);
    string obtenerHTML();
    Temporal* clonar();
};

#endif // TEMPORAL_H
