#ifndef TABLASIMBOLOS_H
#define TABLASIMBOLOS_H

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <string.h>
#include "simbolo.h"
#include <QList>
#include "constantes.h"
#include "ambito.h"
using namespace std;

class tablaSimbolos
{
private:
    int existeListaLocal(string cadenaAmbito, string nombre);
    int existeListaGlobal(string cadenaAmbito, string nombre);
    string getTipoLocal(string cadenaAmbito, string nombre);
    string getTipoGlobal(string cadenaAmbito, string nombre);
    int posLocal(string cadenaAmbito, string nombre);
    int posGlobal(string cadenaAmbito, string nombre);
public:
    QList<Simbolo*> listaSimbolos;
    void insertarSimbolosClase(QList<Simbolo*> simbs);
    string escribirHTMLTabla();
    tablaSimbolos();
    int esAtributo(string nombre, Ambito *ambitos);
    string obtenerTipo(string nombre, Ambito *ambitos, int modo);
    int obtenerPosLocal(string nombre, Ambito *ambitos);
    int obtenerPosGlobal(string nombre, Ambito *ambitos);
    int sizeClase(string nombre);
    int sizeFuncion(string clase, string metodo);
    string obtenerFirmaMetodo(string nombreClase, int noParametros, string nombreFuncion);
    string obtenerTipoFuncion(string firma);
    Simbolo* obtenerNombreParametro(string ambito, int pos);
    Simbolo* obtenerSimbolo(string nombreArreglo, Ambito *ambiente, int esAtributo);
    Simbolo* buscarGlobal(string cadenaAmbito, string nombre);
    Simbolo* buscarLocal(string cadenaAmbito, string nombre);
    void setArregloNs(string nombre,Ambito *ambiente, int esAtributo, QList<string>arreglo);
    bool arregloGlobal(string cadenaAmbito, string nombre, QList<string>arreglo);
    bool arregloLocal(string cadenaAmbito, string nombre, QList<string>arreglo);
    QList<Simbolo*> obtenerAtributosClase(string nombre);
    QList<Simbolo*>obtenerParametrosFuncion(string firmaFuncion);
    int obtenerPosAtributoAcceso(string nombreClase, string nombreAtributo);
    string obtenerPosTipoSimboloAcceso(string nombreClase, string nombreAtributo);
    string obtenerTipoAtributoAcceso(string nombreClase, string nombreAtributo);
    int esAtributoAcceso(string nombre, Ambito *ambiente);





};

#endif // TABLASIMBOLOS_H
