#ifndef ETIQUETAS_H
#define ETIQUETAS_H
#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <string.h>
#include <QList>
using namespace std;

class Etiquetas
{
public:
    QList<string> etiquetas;
    void insertarEtiqueta(string);
    string obtenerActual();
    void eliminarEtiqueta();
    Etiquetas();
};

#endif // ETIQUETAS_H
