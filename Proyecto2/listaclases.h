#ifndef LISTACLASES_H
#define LISTACLASES_H
#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <QList>
#include "nodoarbol.h"
#include "clase.h"
#include "constantes.h"

class ListaClases
{
public:
    nodoArbol* expresion;
    bool esExpresion;
    QList<Clase*> clasesArchivo;
    QList<nodoArbol*>importaciones;
    bool existeClase(string nombre);
    void insertarClase(Clase *nueva);
    void addImportacion(nodoArbol *nueva);
    int fila=0;
    ListaClases();
    Clase* obtenerClaseNombre(string nombre);
};

#endif // LISTACLASES_H
