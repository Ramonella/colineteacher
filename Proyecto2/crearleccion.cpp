#include "crearleccion.h"
#include "ui_crearleccion.h"

CrearLeccion::CrearLeccion(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CrearLeccion)
{
    ui->setupUi(this);
}

CrearLeccion::~CrearLeccion()
{
    delete ui;
}

void CrearLeccion::on_btnCrear_clicked()
{

    QString titulo, explicacion, codigo, tarea, pruebas;
    titulo = ui->txtTitulo->toPlainText();
    explicacion = ui->txtExplicacion->toPlainText();
    codigo = ui->txtEjemplo->toPlainText();
    tarea = ui->txtTarea->toPlainText();
    pruebas= ui->txtPrueba->toPlainText();
    string t1= titulo.toUtf8().constData();
    string e = explicacion.toUtf8().constData();
    string cod = codigo.toUtf8().constData();
    string t= tarea.toUtf8().constData();
    string p = pruebas.toUtf8().constData();
    Leccion *nueva = new Leccion(t1,e,cod,t,p,tipoLeecion,p,titulo);
    this->lecciones->guardarLeccion(nueva);
    pantalla1 *n = new pantalla1();
    n->show();
    n->lecciones= this->lecciones;
    n->mostrarLeccionesBotones();
    cout<<n->lecciones->lecciones.length()<<endl;
    this->hide();


}
