#ifndef LECCION_H
#define LECCION_H

#include <iostream>
#include <sstream>
#include <string.h>
#include <string>
#include "constantes.h"
#include <QCommandLinkButton>
#include <QString>
using namespace std;

class Leccion
{
private slots:
    void realizarLeccion();



public:
    QString titulo2;
    QCommandLinkButton *boton;
    string titulo;
    string explicacion;
    string ejemplo;
    string enunciadoTarea;
    string prueba;
    string tipoLeccion;
    string resultado;
    Leccion(string titulo, string explicacion, string ejemplo, string tarea, string prueba, string tipo, string resultado,QString t);
};

#endif // LECCION_H
