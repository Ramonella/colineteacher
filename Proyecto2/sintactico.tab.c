/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "sintactico.y" /* yacc.c:339  */

#include "scanner.h"
#include <iostream>
#include <QString>
#include "constantes.h"
#include "nodoarbol.h"
#include <string.h>
#include <string>
#include "clase.h"
#include "listaclases.h"
#include "mprincipal.h"


using namespace std;

 
extern int yylineno;
extern int columna;
extern char *yytext;


extern int yylex(void);

int yyerror(const char* mens){
	std::cout <<mens<<" "<<yytext<< std::endl;
	return 0;
}


//nodoArbol* nodoRaiz = new nodoArbol("raiz","");
ListaClases *nodoRaiz = new ListaClases();

void setSalida(ListaClases* sal) {
nodoRaiz = sal;
}


#line 104 "sintactico.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    punto = 258,
    masMas = 259,
    menosMenos = 260,
    suma = 261,
    resta = 262,
    multiplicacion = 263,
    division = 264,
    potencia = 265,
    caracter = 266,
    cadena = 267,
    identificador = 268,
    entero = 269,
    decimal = 270,
    verdadero = 271,
    falso = 272,
    imprimir = 273,
    ptoComa = 274,
    abrePar1 = 275,
    cierraPar1 = 276,
    tipo_entero = 277,
    tipo_decimal = 278,
    tipo_caracter = 279,
    tipo_booleano = 280,
    publico = 281,
    privado = 282,
    protegido = 283,
    vacio = 284,
    igualIgual = 285,
    menorIgual = 286,
    mayorIgual = 287,
    menor = 288,
    mayor = 289,
    distintoA = 290,
    abreCor = 291,
    cierraCor = 292,
    abreLlave = 293,
    cierraLlave = 294,
    igual = 295,
    not_ = 296,
    and_ = 297,
    or_ = 298,
    convertirACadena = 299,
    convertirAEntero = 300,
    clase = 301,
    este = 302,
    hereda = 303,
    concatenar = 304,
    importar = 305,
    retornar = 306,
    sobreescribir = 307,
    principal = 308,
    nuevo = 309,
    coma = 310,
    detener = 311,
    mientras = 312,
    si = 313,
    continuar = 314,
    interrogacion = 315,
    dos_puntos = 316,
    sino_si = 317,
    sino = 318,
    selecciona = 319,
    caso = 320,
    defecto = 321,
    hacer = 322,
    para = 323,
    lista = 324,
    pila = 325,
    cola = 326,
    leer_teclado = 327
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 41 "sintactico.y" /* yacc.c:355  */

char TEXT [256];
class nodoArbol *NODO;
class Clase *CLASE;
class ListaClases *LISTA_CLASES;
class mPrincipal *PRINC;

#line 222 "sintactico.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 239 "sintactico.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   358

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  73
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  63
/* YYNRULES -- Number of rules.  */
#define YYNRULES  148
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  307

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   327

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   195,   195,   198,   199,   201,   206,   214,   218,   227,
     229,   230,   235,   236,   238,   239,   240,   241,   242,   243,
     246,   247,   249,   250,   252,   255,   256,   257,   258,   259,
     261,   262,   263,   266,   267,   274,   275,   277,   278,   281,
     282,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     293,   294,   295,   296,   297,   298,   299,   300,   301,   302,
     309,   310,   313,   321,   329,   339,   340,   342,   350,   360,
     361,   362,   364,   367,   373,   382,   384,   387,   389,   396,
     402,   409,   414,   420,   425,   431,   437,   445,   447,   449,
     451,   452,   454,   455,   457,   458,   460,   462,   464,   466,
     468,   470,   488,   489,   490,   491,   492,   493,   495,   497,
     503,   505,   511,   513,   514,   517,   523,   528,   534,   540,
     542,   548,   554,   556,   562,   565,   570,   575,   578,   583,
     585,   586,   587,   597,   610,   611,   612,   613,   614,   615,
     619,   621,   623,   625,   626,   627,   628,   629,   630
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "punto", "masMas", "menosMenos", "suma",
  "resta", "multiplicacion", "division", "potencia", "caracter", "cadena",
  "identificador", "entero", "decimal", "verdadero", "falso", "imprimir",
  "ptoComa", "abrePar1", "cierraPar1", "tipo_entero", "tipo_decimal",
  "tipo_caracter", "tipo_booleano", "publico", "privado", "protegido",
  "vacio", "igualIgual", "menorIgual", "mayorIgual", "menor", "mayor",
  "distintoA", "abreCor", "cierraCor", "abreLlave", "cierraLlave", "igual",
  "not_", "and_", "or_", "convertirACadena", "convertirAEntero", "clase",
  "este", "hereda", "concatenar", "importar", "retornar", "sobreescribir",
  "principal", "nuevo", "coma", "detener", "mientras", "si", "continuar",
  "interrogacion", "dos_puntos", "sino_si", "sino", "selecciona", "caso",
  "defecto", "hacer", "para", "lista", "pila", "cola", "leer_teclado",
  "$accept", "INICIO", "LISTA_CLASES_", "CLASE_", "CUERPO_CLASE",
  "PRINCIPAL", "LISTA_ELEMENTOS_CLASE", "FUNCION", "LISTA_EXPRESIONES",
  "PARAMETROS_LLAMADA", "INSTANCIA", "TIPO_DATO", "VISIBILIDAD",
  "L_PARAMETROS", "PARAMETROS_FUNCION", "CUERPO_FUNCION", "INSTRUCCIONES",
  "INSTRUCCION", "ASIGNACION", "DECLARACION", "COL_ARREGLO",
  "DECLA_ARREGLO", "CONTINUAR", "DETENER", "RETORNO", "IMPRIMIR",
  "CONCATENAR", "CONVERTIR_A_CADENA", "CONVERTIR_A_ENTERO", "MIENTRAS",
  "IF", "SINO", "SINO_SI", "L_SINO_SI", "SI", "SELECCIONA", "CASO",
  "DEFECTO", "ELEMENTO_SELECCIONA", "LISTA_SELECCIONA",
  "CUERPO_SELECCIONA", "HACER_MIENTRAS", "PARA", "DECLA_LISTA",
  "DECLA_COLA", "DECLA_PILA", "LEER_TECLADO", "OP_RELACIONAL", "EXPRESION",
  "OR", "AND", "NOT", "RELACIONAL", "ARITMETICA", "MUL", "POT", "UNARIO",
  "NEG", "VALOR", "ID", "LLAMADA_FUNCION", "POS_ARREGLO", "ACCESO", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327
};
# endif

#define YYPACT_NINF -101

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-101)))

#define YYTABLE_NINF -30

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -18,    17,    37,   -18,  -101,   -16,  -101,  -101,   105,     2,
    -101,    23,  -101,  -101,  -101,  -101,  -101,  -101,  -101,    55,
    -101,    51,  -101,   204,  -101,    61,   313,  -101,   200,    38,
      23,    62,  -101,  -101,  -101,    23,    23,    96,   103,  -101,
    -101,   111,   -10,  -101,    33,  -101,    38,    38,    38,    38,
      23,    23,  -101,    95,  -101,    97,    13,   118,  -101,   119,
     121,   122,   268,   128,   126,   133,   129,   134,    38,   135,
     143,   144,   146,   140,   111,   127,  -101,   142,  -101,  -101,
    -101,  -101,  -101,  -101,   145,   148,  -101,     1,  -101,  -101,
    -101,  -101,  -101,  -101,  -101,  -101,  -101,  -101,  -101,    66,
    -101,  -101,  -101,  -101,    38,    38,   195,  -101,   257,   268,
    -101,   132,   268,   268,   268,   111,   305,  -101,  -101,    50,
    -101,  -101,  -101,  -101,   268,   279,  -101,  -101,   150,   130,
     120,  -101,  -101,   228,    79,   160,  -101,    89,  -101,   171,
    -101,   268,   268,  -101,   268,   123,    97,   137,   139,   141,
     268,   -11,  -101,  -101,  -101,  -101,  -101,   155,    38,  -101,
    -101,     1,   169,  -101,  -101,  -101,   174,  -101,  -101,  -101,
      -2,  -101,   153,   268,   172,   179,   180,   149,  -101,   183,
    -101,  -101,   268,   268,   279,   279,  -101,  -101,  -101,  -101,
    -101,  -101,   279,   279,   279,   279,  -101,  -101,   193,   197,
     198,   218,   173,   166,   187,   188,   189,    -9,   268,  -101,
    -101,  -101,  -101,  -101,  -101,   225,  -101,   268,  -101,   209,
     231,  -101,  -101,   268,  -101,   120,  -101,    79,    79,    92,
     160,   160,  -101,    38,    38,   213,   268,   268,   178,   184,
     182,   111,  -101,   268,   234,  -101,  -101,  -101,  -101,    -1,
    -101,  -101,    -4,  -101,   235,   270,   256,   267,   277,   282,
     281,    38,   285,   268,  -101,   268,   244,  -101,  -101,  -101,
       0,   287,   294,    97,    97,    97,   289,  -101,  -101,  -101,
     290,   249,    38,  -101,  -101,  -101,   102,   293,   306,   307,
     308,  -101,   296,    38,  -101,  -101,  -101,    38,   311,   312,
     314,  -101,  -101,  -101,  -101,  -101,  -101
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     2,     3,     0,     1,     4,     0,     0,
       5,    29,    25,    26,    27,    28,    31,    32,    30,     0,
       8,     0,    11,     0,    10,     0,     0,     6,     0,     0,
       0,     0,     7,    13,    12,     0,    29,     0,     0,    29,
      36,     0,     0,    33,     0,    19,     0,     0,     0,     0,
       0,     0,   140,    63,    35,     0,   140,     0,    37,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    39,     0,    43,    44,
      46,    45,    41,    47,     0,     0,    51,    83,    50,    52,
      53,    54,    56,    55,    57,    59,   143,   144,   145,     0,
      17,     9,    16,    18,     0,     0,     0,    34,     0,     0,
     141,   142,     0,     0,     0,     0,     0,   133,   132,   140,
     130,   131,   134,   135,     0,     0,   136,   137,     0,   108,
     110,   112,   114,   116,   119,   122,   124,   127,   129,   138,
      70,     0,     0,    69,     0,     0,     0,     0,     0,     0,
       0,    63,    38,    40,    42,    48,    49,     0,     0,    84,
      81,    85,     0,    58,    15,    14,     0,    64,    62,    22,
       0,    20,     0,     0,     0,     0,     0,     0,   128,     0,
     113,    71,     0,     0,     0,     0,   107,   104,   105,   102,
     103,   106,     0,     0,     0,     0,   125,   126,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    79,
      86,    82,   146,   147,   148,     0,    23,     0,    65,     0,
       0,    75,    76,     0,   139,   109,   111,   117,   118,   115,
     120,   121,   123,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    67,     0,     0,    24,    21,    66,    72,     0,
      77,    78,     0,    87,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    95,     0,     0,    90,    91,    92,
       0,     0,     0,     0,     0,     0,     0,    68,    80,    73,
       0,     0,     0,    94,    93,    96,     0,     0,     0,     0,
       0,   101,     0,     0,    89,    60,    61,     0,     0,     0,
       0,    74,    88,    97,    98,   100,    99
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -101,  -101,  -101,   329,   325,   316,  -101,   317,  -101,   131,
    -101,    -8,  -101,  -101,    45,   -45,  -101,   266,  -101,   -21,
     192,  -101,  -101,  -101,  -101,  -101,  -101,   -28,   -27,  -101,
    -101,   186,   190,  -101,  -101,  -101,  -101,  -101,    74,  -101,
    -101,  -101,  -101,  -101,  -101,  -101,  -101,  -101,  -100,  -101,
     163,   165,   227,   161,   -73,   -79,   159,  -101,   239,   -36,
     194,   196,   -23
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,    10,    22,    23,    24,   170,   110,
     167,    41,    26,    42,    29,    45,    75,    76,   287,    77,
     111,    78,    79,    80,    81,    82,    83,   126,   127,    86,
      87,   159,   160,   161,    88,    89,   267,   268,   269,   270,
     253,    90,    91,    92,    93,    94,    95,   192,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,    96,
      97,    98,   139
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      25,   100,   101,   102,   103,    53,   168,    43,   171,   172,
     242,    54,   174,   175,   176,    25,    84,    85,    38,   216,
     262,    99,     8,   145,   179,   109,   -29,   173,     1,   106,
       5,   243,     9,   108,   107,   264,    74,     6,   151,   283,
       8,   198,   199,    28,   200,    55,    56,    84,    85,   109,
     206,    57,    99,   217,   263,    12,    13,    14,    15,   164,
     165,   265,   266,   157,   158,   265,   266,    74,    30,   162,
     108,    31,    58,   219,    35,    46,    44,    59,    60,   177,
      48,    49,    61,    47,    62,   163,   109,   193,   194,    63,
      64,    65,    66,   196,   197,   104,   105,    67,   184,   185,
      68,    69,    70,    71,    72,    73,   295,   296,   244,    50,
      39,   227,   228,   209,   230,   231,    51,   246,    11,    12,
      13,    14,    15,   249,    52,   202,   212,    12,    13,    14,
      15,    16,    17,    18,    19,   106,   254,   255,   112,   113,
      56,   114,   115,   260,    20,    57,   141,   140,   143,    12,
      13,    14,    15,   142,   144,   146,   147,   148,    21,   149,
     150,   154,   183,   280,   155,   281,   152,   156,   173,   181,
     195,    59,    60,   182,   162,   208,    61,   203,    62,   204,
     201,   205,   119,    63,    64,    65,    66,   215,   250,   251,
     218,    67,   237,   220,    68,    69,    70,    71,    72,    73,
     221,   222,   116,   224,   223,   259,   117,   118,   119,   120,
     121,   122,   123,    39,   233,   124,   278,    11,   234,   235,
     238,    40,    12,    13,    14,    15,    12,    13,    14,    15,
      16,    17,    18,    19,   184,   185,   125,   294,   236,    59,
      60,   239,   240,    32,   241,   108,   247,   256,   302,   166,
     248,   252,   303,   258,   257,   261,   271,    21,   186,   187,
     188,   189,   190,   191,   116,   288,   289,   290,   117,   118,
     119,   120,   121,   122,   123,   116,   273,   124,   169,   117,
     118,   119,   120,   121,   122,   123,   116,   274,   124,   272,
     117,   118,   119,   120,   121,   122,   123,   275,   125,   124,
     277,    59,    60,   276,   279,   282,   285,   286,   291,   125,
     293,   292,    59,    60,   297,   301,   117,   118,   119,   120,
     121,   122,   123,    59,    60,   124,    36,   298,   299,   300,
     304,   305,     7,   306,    27,    12,    13,    14,    15,    33,
      34,   153,    37,   207,   284,   225,   245,   210,   226,    59,
      60,   211,   180,   229,   232,   178,   213,     0,   214
};

static const yytype_int16 yycheck[] =
{
       8,    46,    47,    48,    49,    41,   106,    28,   108,   109,
      19,    21,   112,   113,   114,    23,    44,    44,    26,    21,
      21,    44,    38,    68,   124,    36,    13,    36,    46,    40,
      13,    40,    48,    20,    55,    39,    44,     0,    74,    39,
      38,   141,   142,    20,   144,    55,    13,    75,    75,    36,
     150,    18,    75,    55,    55,    22,    23,    24,    25,   104,
     105,    65,    66,    62,    63,    65,    66,    75,    13,     3,
      20,    20,    39,   173,    13,    30,    38,    44,    45,   115,
      35,    36,    49,    21,    51,    19,    36,     8,     9,    56,
      57,    58,    59,     4,     5,    50,    51,    64,     6,     7,
      67,    68,    69,    70,    71,    72,     4,     5,   208,    13,
      13,   184,   185,   158,   193,   194,    13,   217,    13,    22,
      23,    24,    25,   223,    13,   146,   162,    22,    23,    24,
      25,    26,    27,    28,    29,    40,   236,   237,    20,    20,
      13,    20,    20,   243,    39,    18,    20,    19,    19,    22,
      23,    24,    25,    20,    20,    20,    13,    13,    53,    13,
      20,    19,    42,   263,    19,   265,    39,    19,    36,    19,
      10,    44,    45,    43,     3,    20,    49,    40,    51,    40,
      57,    40,    13,    56,    57,    58,    59,    13,   233,   234,
      37,    64,    19,    21,    67,    68,    69,    70,    71,    72,
      21,    21,     7,    20,    55,   241,    11,    12,    13,    14,
      15,    16,    17,    13,    21,    20,   261,    13,    21,    21,
      54,    21,    22,    23,    24,    25,    22,    23,    24,    25,
      26,    27,    28,    29,     6,     7,    41,   282,    20,    44,
      45,    54,    54,    39,    55,    20,    37,    69,   293,    54,
      19,    38,   297,    71,    70,    21,    21,    53,    30,    31,
      32,    33,    34,    35,     7,   273,   274,   275,    11,    12,
      13,    14,    15,    16,    17,     7,    20,    20,    21,    11,
      12,    13,    14,    15,    16,    17,     7,    20,    20,    19,
      11,    12,    13,    14,    15,    16,    17,    20,    41,    20,
      19,    44,    45,    21,    19,    61,    19,    13,    19,    41,
      61,    21,    44,    45,    21,    19,    11,    12,    13,    14,
      15,    16,    17,    44,    45,    20,    13,    21,    21,    21,
      19,    19,     3,    19,     9,    22,    23,    24,    25,    23,
      23,    75,    29,   151,   270,   182,   215,   161,   183,    44,
      45,   161,   125,   192,   195,   116,   162,    -1,   162
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    46,    74,    75,    76,    13,     0,    76,    38,    48,
      77,    13,    22,    23,    24,    25,    26,    27,    28,    29,
      39,    53,    78,    79,    80,    84,    85,    77,    20,    87,
      13,    20,    39,    78,    80,    13,    13,    29,    84,    13,
      21,    84,    86,    92,    38,    88,    87,    21,    87,    87,
      13,    13,    13,   132,    21,    55,    13,    18,    39,    44,
      45,    49,    51,    56,    57,    58,    59,    64,    67,    68,
      69,    70,    71,    72,    84,    89,    90,    92,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   107,   108,
     114,   115,   116,   117,   118,   119,   132,   133,   134,   135,
      88,    88,    88,    88,    87,    87,    40,    92,    20,    36,
      82,    93,    20,    20,    20,    20,     7,    11,    12,    13,
      14,    15,    16,    17,    20,    41,   100,   101,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   135,
      19,    20,    20,    19,    20,    88,    20,    13,    13,    13,
      20,   132,    39,    90,    19,    19,    19,    62,    63,   104,
     105,   106,     3,    19,    88,    88,    54,    83,   121,    21,
      81,   121,   121,    36,   121,   121,   121,   132,   131,   121,
     125,    19,    43,    42,     6,     7,    30,    31,    32,    33,
      34,    35,   120,     8,     9,    10,     4,     5,   121,   121,
     121,    57,    92,    40,    40,    40,   121,    93,    20,    88,
     104,   105,   132,   133,   134,    13,    21,    55,    37,   121,
      21,    21,    21,    55,    20,   123,   124,   127,   127,   126,
     128,   128,   129,    21,    21,    21,    20,    19,    54,    54,
      54,    55,    19,    40,   121,    82,   121,    37,    19,   121,
      88,    88,    38,   113,   121,   121,    69,    70,    71,   132,
     121,    21,    21,    55,    39,    65,    66,   109,   110,   111,
     112,    21,    19,    20,    20,    20,    21,    19,    88,    19,
     121,   121,    61,    39,   111,    19,    13,    91,    84,    84,
      84,    19,    21,    61,    88,     4,     5,    21,    21,    21,
      21,    19,    88,    88,    19,    19,    19
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    73,    74,    75,    75,    76,    76,    77,    77,    78,
      79,    79,    79,    79,    80,    80,    80,    80,    80,    80,
      81,    81,    82,    82,    83,    84,    84,    84,    84,    84,
      85,    85,    85,    86,    86,    87,    87,    88,    88,    89,
      89,    90,    90,    90,    90,    90,    90,    90,    90,    90,
      90,    90,    90,    90,    90,    90,    90,    90,    90,    90,
      91,    91,    92,    92,    92,    93,    93,    94,    94,    95,
      96,    97,    98,    99,    99,   100,   101,   102,   103,   104,
     105,   106,   106,   107,   107,   107,   107,   108,   109,   110,
     111,   111,   112,   112,   113,   113,   114,   115,   116,   117,
     118,   119,   120,   120,   120,   120,   120,   120,   121,   122,
     122,   123,   123,   124,   124,   125,   125,   126,   126,   126,
     127,   127,   127,   128,   128,   129,   129,   129,   130,   130,
     131,   131,   131,   131,   131,   131,   131,   131,   131,   131,
     132,   133,   134,   135,   135,   135,   135,   135,   135
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     3,     4,     3,     2,     4,
       1,     1,     2,     2,     5,     5,     4,     4,     4,     3,
       1,     3,     2,     3,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     3,     2,     2,     3,     1,
       2,     1,     2,     1,     1,     1,     1,     1,     2,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     1,
       2,     2,     4,     2,     4,     3,     4,     4,     6,     2,
       2,     3,     5,     7,     9,     4,     4,     5,     5,     2,
       5,     1,     2,     1,     2,     2,     3,     5,     4,     3,
       1,     1,     1,     2,     3,     2,     7,     9,     9,     9,
       9,     7,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     3,     1,     2,     1,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     1,     2,     2,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     2,     2,     1,     1,     1,     3,     3,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 195 "sintactico.y" /* yacc.c:1646  */
    {  std::cout <<(yyvsp[0].LISTA_CLASES)->clasesArchivo.length()<< std::endl; nodoRaiz->clasesArchivo= (yyvsp[0].LISTA_CLASES)->clasesArchivo; std::cout <<"Cadena Aceptada"<< std::endl;}
#line 1555 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 198 "sintactico.y" /* yacc.c:1646  */
    {(yyval.LISTA_CLASES) = new ListaClases(); (yyval.LISTA_CLASES)->insertarClase((yyvsp[0].CLASE)); std::cout<<"Entrad clase "<<(yyvsp[0].CLASE)->nombreClase<<endl;}
#line 1561 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 199 "sintactico.y" /* yacc.c:1646  */
    {(yyval.LISTA_CLASES)= (yyvsp[-1].LISTA_CLASES); (yyval.LISTA_CLASES)->insertarClase((yyvsp[0].CLASE)); std::cout<<"Entrad clase "<<(yyvsp[0].CLASE)->nombreClase<<endl;}
#line 1567 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 202 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[0].CLASE);
            (yyval.CLASE)->nombreClase=(yyvsp[-1].TEXT);
        }
#line 1576 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 207 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[0].CLASE);
            (yyval.CLASE)->nombreClase=(yyvsp[-2].TEXT);
            (yyval.CLASE)->herencia=(yyvsp[-1].TEXT);
        }
#line 1586 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 215 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.CLASE)=(yyvsp[-1].CLASE);
        }
#line 1594 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 218 "sintactico.y" /* yacc.c:1646  */
    {(yyval.CLASE)= new Clase();}
#line 1600 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 227 "sintactico.y" /* yacc.c:1646  */
    {(yyval.PRINC) = new mPrincipal((yyvsp[0].NODO));}
#line 1606 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 231 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.CLASE)= new Clase();
        (yyval.CLASE)->insertarPrincipal((yyvsp[0].PRINC));
    }
#line 1615 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 246 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("LISTA_EXPRESION",""); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 1621 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 247 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-2].NODO); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 1627 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 249 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("LISTA_EXPRESION","");}
#line 1633 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 250 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 1639 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 252 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("INSTANCIA", (yyvsp[-1].TEXT));
    (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 1646 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 255 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_ENTERO,T_ENTERO);}
#line 1652 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 256 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_DECIMAL,T_DECIMAL);}
#line 1658 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 257 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_CARACTER,T_CARACTER);}
#line 1664 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 258 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(T_BOOLEANO,T_BOOLEANO);}
#line 1670 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 259 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT));}
#line 1676 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 261 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT));}
#line 1682 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 262 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT));}
#line 1688 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 263 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT));}
#line 1694 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 266 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("PARAMETROS","");(yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 1700 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 268 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= (yyvsp[-2].NODO);
    (yyval.NODO)->addHijo((yyvsp[0].NODO));
    }
#line 1708 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 274 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 1714 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 275 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("PARAMETROS",""); }
#line 1720 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 277 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("INSTRUCCIONES","");}
#line 1726 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 278 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 1732 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 281 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol("INSTRUCCIONES",""); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 1738 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 282 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 1744 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 284 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1750 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 285 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 1756 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 286 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1762 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 287 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1768 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 288 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1774 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 289 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1780 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 290 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1786 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 291 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 1792 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 292 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 1798 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 293 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 1804 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 314 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLARACION","1");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));

    }
#line 1816 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 322 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLARACION","2");
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));

    }
#line 1827 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 330 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLARACION","3");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[0].NODO));

    }
#line 1839 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 339 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("COL_ARREGLO",""); (yyval.NODO)->addHijo((yyvsp[-1].NODO));}
#line 1845 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 340 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-3].NODO); (yyval.NODO)->addHijo((yyvsp[-1].NODO));}
#line 1851 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 343 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ARREGLO","1");
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));

    }
#line 1863 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 351 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("DECLA_ARREGLO","2");
        (yyval.NODO)->addHijo((yyvsp[-5].NODO));
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-3].NODO));
        (yyval.NODO)->addHijo((yyvsp[-1].NODO));

    }
#line 1876 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 360 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("CONTINUAR","CONTINUAR");}
#line 1882 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 361 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("DETENER","DETENER");}
#line 1888 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 362 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("RETORNO","RETORNO"); (yyval.NODO)->addHijo((yyvsp[-1].NODO));}
#line 1894 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 364 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("IMPRIMIR",""); (yyval.NODO)->addHijo((yyvsp[-2].NODO));}
#line 1900 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 368 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("CONCATENAR", "1");
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));
    }
#line 1910 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 374 "sintactico.y" /* yacc.c:1646  */
    {
        (yyval.NODO)= new nodoArbol("CONCATENAR", "2");
        (yyval.NODO)->addHijo((yyvsp[-6].NODO));
        (yyval.NODO)->addHijo((yyvsp[-4].NODO));
        (yyval.NODO)->addHijo((yyvsp[-2].NODO));

    }
#line 1922 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 382 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("CONVERTIR_A_CADENA",""); (yyval.NODO)->addHijo((yyvsp[-1].NODO));}
#line 1928 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 384 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("CONVERTIR_A_ENTERO",""); (yyval.NODO)->addHijo((yyvsp[-1].NODO));}
#line 1934 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 390 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI_1","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 1944 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 397 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SINO","");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 1953 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 403 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SINO_SI","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 1963 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 410 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("L_SINO_SI","");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 1972 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 415 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= (yyvsp[-1].NODO);
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 1981 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 421 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","1");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 1990 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 426 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","2");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2000 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 432 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","3");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2010 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 438 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SI","4");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2021 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 488 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("<","<");}
#line 2027 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 489 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(">",">");}
#line 2033 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 490 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("<=","<=");}
#line 2039 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 491 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol(">=",">=");}
#line 2045 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 492 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("!=","!=");}
#line 2051 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 493 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("==","==");}
#line 2057 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 495 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2063 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 498 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("OR", "");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2073 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 503 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2079 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 506 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("AND", "");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2089 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 511 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2095 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 513 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol("NOT",""); (yyval.NODO)->addHijo((yyvsp[0].NODO));}
#line 2101 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 514 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2107 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 115:
#line 518 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("RELACIONAL", (yyvsp[-1].NODO)->valor);
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2117 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 116:
#line 524 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= (yyvsp[0].NODO);
        }
#line 2125 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 529 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("SUMA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2135 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 118:
#line 535 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("RESTA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2145 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 540 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2151 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 120:
#line 543 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("MULTIPLICACION","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2161 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 549 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("DIVISION","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2171 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 122:
#line 554 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2177 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 123:
#line 557 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("POTENCIA","");
            (yyval.NODO)->addHijo((yyvsp[-2].NODO));
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2187 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 124:
#line 562 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2193 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 125:
#line 566 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("MENOS_MENOS","");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        }
#line 2202 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 126:
#line 571 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("MAS_MAS","");
            (yyval.NODO)->addHijo((yyvsp[-1].NODO));
        }
#line 2211 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 127:
#line 575 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2217 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 128:
#line 579 "sintactico.y" /* yacc.c:1646  */
    {
            (yyval.NODO)= new nodoArbol("NEGATIVO","");
            (yyval.NODO)->addHijo((yyvsp[0].NODO));
        }
#line 2226 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 129:
#line 583 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2232 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 130:
#line 585 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_ENTERO, (yyvsp[0].TEXT));}
#line 2238 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 131:
#line 586 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_DECIMAL, (yyvsp[0].TEXT));}
#line 2244 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 132:
#line 587 "sintactico.y" /* yacc.c:1646  */
    {
                string c ="";
                int t = strlen((yyvsp[0].TEXT));
                int indice = 1;
                while(indice < t-1){
                c = c + (yyvsp[0].TEXT)[indice];
               indice++;
                }
                (yyval.NODO) = new nodoArbol(T_CADENA, c);
         }
#line 2259 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 133:
#line 599 "sintactico.y" /* yacc.c:1646  */
    {
               string c ="";
               int t = strlen((yyvsp[0].TEXT));
               int indice = 1;
               while(indice < t-1){
                    c = c +(yyvsp[0].TEXT)[indice];
                    indice++;
                   }
               (yyval.NODO) = new nodoArbol(T_CARACTER, c);

        }
#line 2275 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 134:
#line 610 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_BOOLEANO, VERDADERO);}
#line 2281 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 135:
#line 611 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO) = new nodoArbol(T_BOOLEANO, FALSO);}
#line 2287 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 136:
#line 612 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[0].NODO);}
#line 2293 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 137:
#line 613 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= (yyvsp[0].NODO);}
#line 2299 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 139:
#line 615 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)=(yyvsp[-1].NODO);}
#line 2305 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 140:
#line 619 "sintactico.y" /* yacc.c:1646  */
    {(yyval.NODO)= new nodoArbol((yyvsp[0].TEXT),(yyvsp[0].TEXT));}
#line 2311 "sintactico.tab.c" /* yacc.c:1646  */
    break;


#line 2315 "sintactico.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
