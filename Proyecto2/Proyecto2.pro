#-------------------------------------------------
#
# Project created by QtCreator 2018-12-21T22:08:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Proyecto2
TEMPLATE = app


SOURCES += main.cpp\
        principal.cpp \
    valor.cpp \
    nodoarbol.cpp \
    parser.cpp \
    scanner.cpp \
    codigo3d.cpp \
    generadorcodigo.cpp \
    error_.cpp \
    listaerrores.cpp \
    parser_interprete.cpp \
    scanner_interprete.cpp \
    temporal.cpp \
    analizadorinterprete.cpp \
    listatemporales.cpp \
    constantes.cpp \
    tablasimbolos.cpp \
    funcion.cpp \
    clase.cpp \
    listaclases.cpp \
    listafunciones.cpp \
    mprincipal.cpp \
    nodocondicion.cpp \
    ambito.cpp \
    simbolo.cpp \
    etiquetas.cpp \
    pantalla1.cpp \
    crearleccion.cpp \
    participarleccion.cpp \
    leccion.cpp \
    listalecciones.cpp \
    elementocodigo.cpp \
    listacodigo.cpp \
    myworker.cpp \
    calificarleccion.cpp

HEADERS  += principal.h \
    valor.h \
    constantes.h \
    nodoarbol.h \
    lexico.l \
    parser.h \
    scanner.h \
    sintactico.y \
    codigo3d.h \
    generadorcodigo.h \
    error_.h \
    listaerrores.h \
    parser_interprete.h \
    scanner_interprete.h \
    temporal.h \
    analizadorinterprete.h \
    listatemporales.h \
    tablasimbolos.h \
    funcion.h \
    clase.h \
    listaclases.h \
    listafunciones.h \
    mprincipal.h \
    nodocondicion.h \
    ambito.h \
    simbolo.h \
    etiquetas.h \
    pantalla1.h \
    crearleccion.h \
    participarleccion.h \
    leccion.h \
    listalecciones.h \
    elementocodigo.h \
    listacodigo.h \
    myworker.h \
    calificarleccion.h

FORMS    += principal.ui \
    pantalla1.ui \
    crearleccion.ui \
    participarleccion.ui

DISTFILES += \
    ejecutar.sh \
    interprete_lexico.l \
    interprete_sintactico.y \
    interprete.sh
