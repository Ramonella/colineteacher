#include "participarleccion.h"
#include "ui_participarleccion.h"
#include "pantalla1.h"
#include "principal.h"
#include "calificarleccion.h"
#include <QMessageBox>

participarLeccion::participarLeccion(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::participarLeccion)
{
    ui->setupUi(this);
    this->titulo="";
    this->explicacion="";
    this->ejemplo="";
    this->tarea="";
    this->respuesta="";
}

participarLeccion::~participarLeccion()
{
    delete ui;
}

void participarLeccion::mostrarDatos(){
    QString nombreLeeccion = QString::fromStdString(titulo);
    QString explicacionL = QString::fromStdString(explicacion);
    QString ejemploL= QString::fromStdString(ejemplo);
    QString tareaL = QString::fromStdString(tarea);
    ui->label->setText(nombreLeeccion);
    ui->txtExplicacion->setText(explicacionL);
    ui->txtEjemplo->setText(ejemploL);
    ui->txtTarea->setText(tareaL);

}

void participarLeccion::on_btnCompilar_clicked()
{
    Principal *p = new Principal();
    QString codigo = ui->txtEjemplo->toPlainText();
    p->show();
    p->lecciones= this->lecciones;
    p->cLeccion(codigo);
    this->hide();
}

void participarLeccion::on_actionRegresar_2_triggered()
{
   pantalla1 *n = new pantalla1();
   n->show();
   n->lecciones= this->lecciones;
   n->mostrarLeccionesBotones();
   this->hide();
}

void participarLeccion::on_btnCalificar_clicked()
{
    CalificarLeccion *cal = new CalificarLeccion();
    QString ingresada = ui->txtIngreso->toPlainText();
    string  t = ingresada.toUtf8().constData();
    bool c = cal->calificar(t,this->respuesta);
    if(c){
        QMessageBox msgBox;
        msgBox.setText("Correcto!");
        msgBox.exec();
    }else{
        QMessageBox msgBox;
        msgBox.setText("Incorrecto!");
        msgBox.exec();
    }

}
