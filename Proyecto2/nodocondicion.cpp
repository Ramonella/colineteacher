#include "nodocondicion.h"

nodoCondicion::nodoCondicion(string codigo)
{
 this->codigo = codigo;

}



void nodoCondicion::addEtiquetasVerdaderas(QList<string> ets){
    for(int i=0; i<ets.length();i++){
        this->verdaderas.push_back(ets.at(i));
    }
}


void nodoCondicion::addEtiquetasFalsas(QList<string> ets){
    for(int i=0; i<ets.length();i++){
        this->falsas.push_back(ets.at(i));
    }
}


void nodoCondicion::addFalsa(string etq){
    this->falsas.push_back(etq);
}

void nodoCondicion::addVerdadera(string etq){
    this->verdaderas.push_back(etq);
}

string nodoCondicion::getCodigo(){
    return this->codigo;
}


string nodoCondicion::getEtiquetasVerdaderas(){
    string cadena="";
    for(int i=0; i<this->verdaderas.length(); i++){
        cadena+=this->verdaderas.at(i)+":\n";

    }
    return cadena;
}


string nodoCondicion::getEtiquetasFalsas(){
    string cadena="";
    for(int i=0; i<this->falsas.length(); i++){
        cadena+=this->falsas.at(i)+":\n";

    }
    return cadena;
}



void nodoCondicion::cambiarEtiquetas(){

    QList<string> falsasTemporales ;
    QList<string> verdaderasTemporales;

    for(int i=0; i<this->verdaderas.length(); i++){
        verdaderasTemporales.push_back(verdaderas.at(i));

    }

    for(int i=0; i<this->falsas.length(); i++){
        falsasTemporales.push_back(falsas.at(i));

    }


    this->verdaderas.clear();
    this->falsas.clear();
    for(int i = 0; i< falsasTemporales.length(); i++){
        this->addVerdadera(falsasTemporales.at(i));
    }

    for(int i = 0; i< verdaderasTemporales.length(); i++){
        this->addFalsa(verdaderasTemporales.at(i));
    }

}



















