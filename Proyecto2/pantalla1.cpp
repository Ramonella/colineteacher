#include "pantalla1.h"
#include "ui_pantalla1.h"
#include "principal.h"
#include "crearleccion.h"
#include <QInputDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QDir>
#include "participarleccion.h"
#include <QMessageBox>


pantalla1::pantalla1(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::pantalla1)
{
    lecciones = new ListaLecciones();
    ui->setupUi(this);
    ui->comboBox->addItem("Todas");
    ui->comboBox->addItem("Lecciones A-Coach");
    ui->comboBox->addItem("Lecciones G-Coach");

}

pantalla1::pantalla1(ListaLecciones *lecciones){
    this->lecciones = lecciones;
    ui->setupUi(this);
    ui->comboBox->addItem("Todas");
    ui->comboBox->addItem("Lecciones A-Coach");
    ui->comboBox->addItem("Lecciones G-Coach");
    cout<<this->lecciones->lecciones.length()<<endl;
    cout<<"holis"<<endl;
    cout<<this->lecciones->lecciones.length()<<endl;
}

pantalla1::~pantalla1()
{
    delete ui;
}

void pantalla1::mostrarLeccionesBotones(){
    for(int i =0; i<this->lecciones->lecciones.length();i++){
      ui->listWidget->addItem(lecciones->lecciones.at(i)->titulo2+" - "+QString::fromStdString(lecciones->lecciones.at(i)->tipoLeccion));
    }

}

void pantalla1::on_Editor_clicked()
{

    Principal *nuevoLibre = new Principal();
    nuevoLibre->show();
    nuevoLibre->lecciones= this->lecciones;
    this->hide();
}

void pantalla1::on_btbACoach_clicked()
{
    CrearLeccion *nueva = new CrearLeccion();
    nueva->tipoLeecion=ACOACH;
    nueva->show();
    nueva->lecciones=this->lecciones;
    this->hide();
}

void pantalla1::on_listWidget_doubleClicked(const QModelIndex &index)
{
   QList<QListWidgetItem*> nombreLeccion = ui->listWidget->selectedItems();
   QString nombreActual="";
   for(int i=0; i<nombreLeccion.length();i++){
       nombreActual = nombreLeccion.at(i)->text();
   }

   Leccion *buscada = this->lecciones->obtener(nombreActual.toUtf8().constData());
   if(buscada!= NULL){
       participarLeccion *n = new participarLeccion();
       n->show();
       n->titulo= buscada->titulo;
       n->explicacion= buscada->explicacion;
       n->ejemplo= buscada->ejemplo;
       n->tarea= buscada->enunciadoTarea;
       n->respuesta= buscada->resultado;
       n->lecciones= this->lecciones;
       n->mostrarDatos();
       this->hide();
   }

}

void pantalla1::on_btbGCoach_clicked()
{
    CrearLeccion *nueva = new CrearLeccion();
    nueva->tipoLeecion=GCOACH;
    nueva->show();
    nueva->lecciones=this->lecciones;
    this->hide();

}

void pantalla1::on_btnBuscar_clicked()
{
    QString nombre = ui->lineEdit->text();
    Leccion *buscada = this->lecciones->obtenerPorNombre(nombre.toUtf8().constData());
    if(buscada!= NULL){
        ui->listWidget->clear();
        ui->listWidget->addItem(QString::fromStdString(buscada->titulo)+" - "+QString::fromStdString(buscada->tipoLeccion));
        QMessageBox msgBox;
        msgBox.setText("La leccion "+ nombre+" fue encontrada");
        msgBox.exec();
    }else{
        QMessageBox msgBox;
        msgBox.setText("La leccion "+ nombre+", no existe");
        msgBox.exec();
    }
}

void pantalla1::on_comboBox_currentIndexChanged(const QString &arg1)
{


    QString c = ui->comboBox->itemText(ui->comboBox->currentIndex());
    string h = c.toUtf8().constData();
    cout<<h<<endl;
    if(sonIguales(h,"todas")){
        ui->listWidget->clear();
        this->mostrarLeccionesBotones();
    }else if(sonIguales(h,"Lecciones G-Coach")){
        ui->listWidget->clear();

        QList<Leccion*>lecc = this->lecciones->obtenerLeccionesTipo(GCOACH);
          cout<<lecc.length()<<endl;
        for(int i =0; i<lecc.length();i++){
          ui->listWidget->addItem(lecc.at(i)->titulo2+" - "+QString::fromStdString(lecc.at(i)->tipoLeccion));
        }

    }else if(sonIguales(h, "Lecciones A-Coach")){
        ui->listWidget->clear();
        QList<Leccion*>lecc = this->lecciones->obtenerLeccionesTipo(ACOACH);
        cout<<lecc.length()<<endl;
        for(int i =0; i<lecc.length();i++){
          ui->listWidget->addItem(lecc.at(i)->titulo2+" - "+QString::fromStdString(lecc.at(i)->tipoLeccion));
        }

    }
}
