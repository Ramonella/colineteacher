

%{
#include "parser.h"
#include <iostream>
#include <QString>
#include <stdio.h>
#define YY_USER_ACTION {yylloc.first_line = yylineno; yylloc.last_line = yylineno;}
int columna =0;
%}



simple "//".*                                
multi [/][*][^*]*[*]+([^/*][^*]*[*]+)*[/]   

path  "\""[^'\"']*"\""
comentario "/*"[^'*']*"*/"
entero_ [0-9]+
decimal_ [0-9]+("."[0-9]+)
id [a-zñA-ZÑ][a-zñA-ZÑ0-9_]*
car  "\'"[^'\'']"\'"
v_nulo "\'"("\\0")"\'"

%option noyywrap
%option case-insensitive
%option yylineno

%%

"+=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return masIgual; }
"-=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menosIgual; }
"*=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return porIgual; }
"/=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return divIgual; }


"nada" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return nada; }
{v_nulo} {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return nada; }

"insertar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return insertar; }
"apilar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return apilar; }
"desapilar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return desapilar; }
"encolar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return encolar; }
"desencolar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return desencolar; }
"obtener" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return obtener; }
"buscar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return buscar; }
"tamanio" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return tamanio; }
"mostrarEDD" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mostrarEDD; }



"." {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return punto; }
"convertirACadena" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return convertirACadena; }
"convertirAEntero" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return convertirAEntero; }
"clase" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return clase; }
"este" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return este; }
"hereda_de" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return hereda; }
"concatenar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return concatenar; }
"importar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return importar; }
"retornar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return retornar; }
"@sobreescribir" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return sobreescribir; }
"principal" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return principal; }
"nuevo" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return nuevo; }
"," {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return coma; }
"detener" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return detener; }
"mientras" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mientras; }
"si" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return si; }
"continuar" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return continuar; }
"?" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return interrogacion; }
":" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return dos_puntos; }
"sino si" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return sino_si; }
"sino" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return sino; }
"selecciona" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return selecciona; }
"caso" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return caso; }
"defecto" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return defecto; }
"hacer" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return hacer; }
"para" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return para; }
"lista" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return lista; }
"pila" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return pila; }
"cola" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cola; }
"leer_teclado" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return leer_teclado; }








"entero" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return tipo_entero; }

"decimal" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return tipo_decimal; }


"caracter" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return tipo_caracter; }

"booleano"  {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return tipo_booleano; }


"publico" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return publico; }

"protegido" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return protegido; }

"privado"  {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return privado; }

"vacio" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return vacio; }



"!" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return not_; }

"&&" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return and_; }

"||" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return or_; }

"==" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return igualIgual; }

"<=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menorIgual; }

">=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mayorIgual; }



"<" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menor; }


">" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return mayor; }


"!=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return distintoA; }



"[" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return abreCor; }

"]" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cierraCor; }

"{" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return abreLlave; }


"}" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cierraLlave; }


"=" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return igual; }




"(" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return abrePar1; }
")" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cierraPar1; }
";" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return ptoComa; }

"imprimir" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return imprimir; }
"++" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return masMas; }
"--" { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return menosMenos; }
"+" {std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return suma; }
"-" { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return resta; }
"*" {std::cout <<yytext<< std::endl;  columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return multiplicacion; }
"/" { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return division; }
"^" { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return potencia; }
"verdadero" { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return verdadero; }
"falso" { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return falso; }


{car}          {std::cout <<yytext<< std::endl;  columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return caracter; }

{path}          { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return cadena; }
{id}          {std::cout <<yytext<< std::endl;  columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return identificador; }
{entero_}        { std::cout <<yytext<< std::endl;columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return entero; }
{decimal_}        { std::cout <<yytext<< std::endl; columna=columna+strlen(yylval.TEXT); strcpy(yylval.TEXT, yytext); return decimal; }

{simple}    { std::cout <<yytext<< std::endl; /*Se ignoran*/ }
{multi}    { std::cout <<yytext<< std::endl; /*Se ignoran*/ }
[\n] {columna=0; }
[[:blank:]]     { /*Se ignoran*/ }
.               {std::cout <<yytext<<"Error Lexico "<< std::endl;}
%%
