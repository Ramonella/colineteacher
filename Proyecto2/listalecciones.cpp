#include "listalecciones.h"

ListaLecciones::ListaLecciones()
{

}


bool ListaLecciones::existe(string nombre){
    Leccion *temporal;
    for(int i=0; i< this->lecciones.length();i++){
        temporal = lecciones.at(i);
        if(sonIguales(temporal->titulo, nombre)){
            return true;
        }
    }
    return false;
}


void ListaLecciones::guardarLeccion(Leccion *nueva){
    if(!existe(nueva->titulo)){
        this->lecciones.push_back(nueva);
    }else{

    }
}


Leccion* ListaLecciones::obtener(string nombre){
    Leccion *temporal;
    for(int i=0; i< this->lecciones.length();i++){
        temporal = lecciones.at(i);
        if(sonIguales(temporal->titulo+" - "+temporal->tipoLeccion, nombre)){
            return temporal;
        }
    }
    return NULL;
}

QList<Leccion*> ListaLecciones::obtenerLeccionesTipo(string tipo){
    Leccion *temporal;
    QList<Leccion*>lecc;
    for(int i=0; i< this->lecciones.length();i++){
        temporal = lecciones.at(i);
        if(sonIguales(temporal->tipoLeccion, tipo)){
            lecc.push_back(temporal);
        }
    }
    return lecc;

}


Leccion* ListaLecciones::obtenerPorNombre(string nombre){
    Leccion *temporal;
    for(int i=0; i< this->lecciones.length();i++){
        temporal = lecciones.at(i);
        if(sonIguales(temporal->titulo, nombre)){
            return temporal;
        }
    }
    return NULL;

}
