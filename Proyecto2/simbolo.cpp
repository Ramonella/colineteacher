#include "simbolo.h"

Simbolo::Simbolo()
{
    visibilidad = NO_TIENE;
     nombrecorto="";
     tipoSimbolo="";//arreglo, objeto, var, edd
     tipoElemento="";//int, persona, char, decimal
    ambito=""; //persona1_entero_metodobotnio
    rol ="";//vairable local, parametro, retorno
     apuntador=-1;
     tamanio =0;//size de una clase o metodo
    noParametros =-1;
    parametrosFuncionCadena="";//entero_caracter_obj
    nombreFuncion ="";
    declaAtributo=NULL;
     noDimensiones =0;
     this->asignaAtributo= NULL;

}

void Simbolo::setValoresVariable(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size){
    this->nombrecorto=nombreC;
    this->tipoSimbolo= tipoSimb;
    this->tipoElemento= tipoEle;
    this->ambito= ambito;
    this->rol= rol;
    this->apuntador= apu;
    this->tamanio= size;
}

void Simbolo::setValoresArreglo(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size, int nodimensiones, QList<nodoArbol*>listaDim){
    this->nombrecorto= nombreC;
    this->tipoSimbolo= tipoSimb;
    this->tipoElemento= tipoEle;
    this->ambito= ambito;
    this->rol = rol;
    this->apuntador= apu;
    this->tamanio= size;
    this->noDimensiones= nodimensiones;
    this->dimensiones= listaDim;
}


void Simbolo::setValoresLista(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size){
    this->nombrecorto= nombreC;
    this->tipoSimbolo= tipoSimb;
    this->tipoElemento= tipoEle;
    this->ambito= ambito;
    this->rol= rol;
    this->apuntador= apu;
    this->tamanio= size;
}


void Simbolo::setValoresCola(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size){
    this->nombrecorto= nombreC;
    this->tipoSimbolo= tipoSimb;
    this->tipoElemento= tipoEle;
    this->ambito= ambito;
    this->rol= rol;
    this->apuntador= apu;
    this->tamanio= size;
}


void Simbolo::setValoresPila(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size){
    this->nombrecorto= nombreC;
    this->tipoSimbolo= tipoSimb;
    this->tipoElemento= tipoEle;
    this->ambito= ambito;
    this->rol= rol;
    this->apuntador= apu;
    this->tamanio= size;
}

string Simbolo::getHTMLSimbolo(){
    string cadenaSimbolo="<tr>\n"
                         "<td>"+this->nombrecorto+"</td>\n"
                          "<td>"+this->ambito+"</td>\n"
            "<td>"+this->tipoSimbolo+"</td>\n"
            "<td>"+this->rol+"</td>\n"
            "<td>"+this->visibilidad+"</td>\n"
            "<td>"+this->tipoElemento+"</td>\n"
            "<td>"+intToCadena(this->apuntador)+"</td>\n"
            "<td>"+intToCadena(tamanio)+"</td>\n"
            "<td>"+intToCadena(noParametros)+"</td>\n"
            "<td>"+this->parametrosFuncionCadena+"</td>\n"
            "<td>"+this->nombreFuncion+"</td>\n"
            "<td>"+intToCadena(noDimensiones)+"</td>\n"
            "</tr>\n";


return cadenaSimbolo;
}

void Simbolo:: setValoresFuncion(string nombreC, string tipo, string tipoS, string ambito, string rol, int apu, int size, int noParametros, string cadParametros, string nombreFun){
    this->nombrecorto= nombreC;
        this->tipoElemento=tipo;
        this->tipoSimbolo = tipoS;
        this->ambito= ambito;
        this->rol = rol;
        this->apuntador= apu;
        this->tamanio = size;
        this->noParametros= noParametros;
        this->parametrosFuncionCadena= cadParametros;
        this->nombreFuncion= nombreFun;
}
