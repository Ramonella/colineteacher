#ifndef MYWORKER_H
#define MYWORKER_H


#include <QObject>
#include <QMutex>
#include <QWaitCondition>
#include "listacodigo.h"
#include <string>
#include <string.h>
#include <iostream>
#include <sstream>
#include "elementocodigo.h"

class MyWorker : public QObject
{
    Q_OBJECT
public:
    explicit MyWorker(QObject *parent = 0);
    ~MyWorker();
    void restart();
    void pause();
    ListaCodigo *lCodigo;
    int noLineas;
    int lineas3D;
    string codTemporal;
    elementoCodigo *codTemp;

signals:
    void finished();

public slots:
    void doWork();
    void doWork2();
    void cancelWork();

private:
    QMutex m_continue;
    QWaitCondition m_pauseManager;
    bool m_cancelRequested;
    bool m_pauseRequired;
};

#endif // MYWORKER_H
