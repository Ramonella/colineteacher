
%{
#include "scanner_interprete.h"
#include <iostream>
#include <QString>
#include "constantes.h"
#include "nodoarbol.h"
#include <string.h>
#include <string>
#include <sstream>

using namespace std;



extern int colInterprete;
extern char *yytext;


extern int int3Dlex(void);

nodoArbol* raizInterprete = new nodoArbol("raiz","");

void setNodoRaizInterprete(nodoArbol* sal) {
raizInterprete = sal;

}

int int3Derror(const char* mens){
	std::cout <<mens<<" "<<yytext<< std::endl;
	return 0;
}




%}
%locations
%error-verbose
%define api.prefix {int3D}
%union{
char TEXT [256];
class nodoArbol *NODO;
}

//Terminales


%token<TEXT> vacio
%token<TEXT> heap_
%token<TEXT> stack_
%token<TEXT> pool_
%token<TEXT> print
%token<TEXT> goto_
%token<TEXT> call
%token<TEXT> menor
%token<TEXT> mayor
%token<TEXT> menorIgual
%token<TEXT> mayorIgual
%token<TEXT> igualIgual
%token<TEXT> distintoA
%token<TEXT> suma_
%token<TEXT> resta_
%token<TEXT> multiplicacion_
%token<TEXT> division_


%token<TEXT> abreCor
%token<TEXT> cierraCor
%token<TEXT> igual
%token<TEXT> puntoComa
%token<TEXT> dosPuntos
%token<TEXT> abrePar
%token<TEXT> cierraPar
%token<TEXT> identificador_
%token<TEXT> entero_
%token<TEXT> decimal_

%token<TEXT> si
%token<TEXT> abreLlave
%token<TEXT> cierraLlave
%token<TEXT> coma
%token<TEXT> inStr
%token<TEXT> inNum
%token<TEXT> leerTeclado

%type<NODO> INICIO
%type<NODO> NEGATIVO
%type<NODO> VAL
%type<NODO> ASIG
%type<NODO> ETIQUETA
%type<NODO> SALTO
%type<NODO> RELACIONAL

%type<NODO> OPE_RELACIONAL
%type<NODO> OPE_ARITMETICO
%type<NODO> TIPO_ED
%type<NODO> GET_ED

%type<NODO> ASIG_ED
%type<NODO> LLAMADA
%type<NODO> IMPRIMIR
%type<NODO> INSTRUCCION


%type<NODO> INSTRUCCIONES
%type<NODO> FINAL_FUNCION
%type<NODO> INICIO_FUNCION
%type<NODO> TRANSFERENCIA
%type<NODO> INSTR
%type<NODO> INNUM
%type<NODO> LEER_TECLADO




%start INICIO
%%


INICIO: INSTRUCCIONES abrePar{raizInterprete->addHijo($1);raizInterprete->fila =@2.first_line;};

INSTRUCCIONES: INSTRUCCION 
    {
    //cout<<"ando en "<<@1.first_line<<endl;
        $$= new nodoArbol("INSTRUCCIONES","");
        $$->addHijo($1);
}
 
        |INSTRUCCIONES INSTRUCCION
    {
    // cout<<"ando en "<<@2.first_line<<endl;
        $$=$1;
        $$->addHijo($2);
    };

INSTRUCCION: ASIG{$$=$1;}
        |ASIG_ED{$$=$1;}
        |GET_ED{$$=$1;}
        |LLAMADA{$$=$1;}
        |IMPRIMIR{$$=$1;}
        |RELACIONAL{$$=$1;}
        |ETIQUETA{$$=$1;}
        |SALTO{$$=$1;}
        |INICIO_FUNCION{$$=$1;}
        |FINAL_FUNCION{$$=$1;}
        |TRANSFERENCIA{$$=$1;}
        |INSTR{$$=$1;}
        |INNUM{$$=$1;}
        |LEER_TECLADO{$$=$1;};

LEER_TECLADO: leerTeclado abrePar cierraPar puntoComa{
    $$= new nodoArbol("leerTeclado", "leerTeclado");
    $$->fila = @1.first_line;

};

INSTR:inStr abrePar cierraPar puntoComa{ $$= new nodoArbol("instr", "instr"); $$->fila = @1.first_line;};
INNUM:inNum abrePar cierraPar puntoComa{ $$= new nodoArbol("innum", "innum"); $$->fila = @1.first_line;};

INICIO_FUNCION: vacio identificador_ abrePar cierraPar abreLlave {$$ = new nodoArbol("INICIO_FUNCION",$2); $$->fila = @1.first_line;};

FINAL_FUNCION: cierraLlave{$$= new nodoArbol("FINAL_FUNCION",""); $$->fila = @1.first_line;};

IMPRIMIR: print abrePar entero_ coma VAL cierraPar puntoComa
{
    $$= new nodoArbol("IMPRIMIR",$3);
    $$->addHijo($5);
    $$->fila = @1.first_line;
};

LLAMADA: call identificador_ abrePar cierraPar puntoComa
    {
        $$= new nodoArbol("LLAMADA", $2);
        $$->fila = @1.first_line;
    };

ASIG_ED: TIPO_ED abreCor VAL cierraCor igual VAL puntoComa
{
    $$= new nodoArbol("ASIG_ED",$1->valor);
    $$->addHijo($3);
    $$->addHijo($6);
    $$->fila = @1.first_line;
};

GET_ED: identificador_ igual TIPO_ED abreCor VAL cierraCor puntoComa
    {
        nodoArbol *id= new nodoArbol($1,$1);
        $$= new nodoArbol("GET_ED",$3->valor);
        $$->addHijo(id);
        $$->addHijo($5);
        $$->fila = @1.first_line;

    };

TIPO_ED: stack_{$$= new nodoArbol("stack","stack");}
        |heap_{$$= new nodoArbol("heap","heap");}
        |pool_{$$= new nodoArbol("pool","pool");};

RELACIONAL: si abrePar VAL OPE_RELACIONAL VAL cierraPar goto_ identificador_ puntoComa goto_ identificador_ puntoComa
    {
    nodoArbol *etiV= new nodoArbol($8,$8);
    nodoArbol *etiF= new nodoArbol($11,$11);
    $$= new nodoArbol("RELACIONAL",$4->valor);
        $$->addHijo($3);
        $$->addHijo($5);
        $$->addHijo(etiV);
        $$->addHijo(etiF);
        $$->fila = @1.first_line;
    };


SALTO: goto_ identificador_ puntoComa{$$= new nodoArbol("SALTO",$2); $$->fila = @1.first_line;};

ETIQUETA: identificador_ dosPuntos{$$= new nodoArbol("ETIQUETA", $1); $$->fila = @1.first_line;};

ASIG: identificador_ igual VAL OPE_ARITMETICO VAL puntoComa
    {
        nodoArbol *id= new nodoArbol($1,$1);
        string h = $4->valor;
        $$= new nodoArbol("ASIG",h);
        $$->addHijo(id);
        $$->addHijo($3);
        $$->addHijo($5);
        $$->fila = @1.first_line;
    };

TRANSFERENCIA: identificador_ igual VAL puntoComa
    {
        $$= new nodoArbol("TRANSFERENCIA",$1);
        $$->addHijo($3);
        $$->fila = @1.first_line;
    };


NEGATIVO: resta_ identificador_{$$= new nodoArbol("ID",$2); $$->fila = @1.first_line;}
        |resta_ entero_{$$= new nodoArbol(T_ENTERO,$2); $$->fila = @1.first_line;}
        |resta_ decimal_{$$= new nodoArbol(T_DECIMAL,$2); $$->fila = @1.first_line;};


VAL: NEGATIVO {$$= new nodoArbol("NEGATIVO", ""); $$->addHijo($1); $$->fila = @1.first_line;}
        |identificador_{$$ = new nodoArbol("ID", $1); $$->fila = @1.first_line;}
        |entero_{$$ = new nodoArbol(T_ENTERO, $1); $$->fila = @1.first_line;}
        |decimal_{$$ = new nodoArbol(T_DECIMAL, $1); $$->fila = @1.first_line;};


OPE_ARITMETICO: suma_{$$= new nodoArbol("+","+");}
        |resta_{$$= new nodoArbol("-","-");}
        |multiplicacion_{$$= new nodoArbol("*","*");}
        |division_{$$= new nodoArbol("/","/");};

OPE_RELACIONAL: menor{$$= new nodoArbol("<","<");}
        |mayor{$$= new nodoArbol(">",">");}
        |menorIgual{$$= new nodoArbol("<=","<=");}
        |mayorIgual{$$= new nodoArbol(">=",">=");}
        |igualIgual{$$= new nodoArbol("==","==");}
        |distintoA{$$= new nodoArbol("!=","!=");};




	


