#ifndef ERROR__H
#define ERROR__H

#include <sstream>
#include <iostream>
#include <string.h>
#include <string>
using namespace std;

class Error_
{
public:
    int columna=0;
    int fila=0;
    string tipo="";
    string descripcion="";
    void setValores(string tipo, string desc);
    string getHTML();
    Error_();
};

#endif // ERROR__H
