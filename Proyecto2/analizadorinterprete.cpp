#include "analizadorinterprete.h"
#include  "parser_interprete.h"
#include "scanner_interprete.h"
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <sstream>
#include "elementocodigo.h"


extern int int3Dparse();
extern void setNodoRaizInterprete(nodoArbol* sal);

analizadorInterprete::analizadorInterprete(Principal *p, ListaCodigo *codigo,bool modo)
{
    this->formP= p;
    this->raiz3D = new nodoArbol("raiz","");
    this->temporales= new ListaTemporales();
    this->codigoT = codigo;
    this->modo= modo;
    iniciarEDDs();
}


string analizadorInterprete::convertirString(double numero){
    ostringstream str1;
    str1 << numero;
    string geek = str1.str();
    return geek;
}



void analizadorInterprete::guarTemp(int linea)
{

    if(modo){
    string codTemporal = this->escribirTemporales();
    string codigoStack= this->escribirStack();
    string codigoHeap = this->escribirHeap();
    string codigoPool = this->escribirPool();
    string codImpresion = this->cadenaImpresion;
    elementoCodigo *nuevo = new elementoCodigo();
    nuevo->identificador = linea;
    nuevo->heap= codigoHeap;
    nuevo->stack= codigoStack;
    nuevo->pool= codigoPool;
    nuevo->impresion= codImpresion;
    nuevo->temporales= codTemporal;
    this->codigoT->elementos.push_back(nuevo);}

}

void analizadorInterprete::iniciarEDDs(){


    for(int i=0; i< 20000;i++){
        this->heap[i]=0;
    }


    for(int i=0; i< 2000;i++){
        this->stack[i]=0;
    }


    for(int i=0; i< 20000;i++){
        this->pool[i]=0;
    }
}





string analizadorInterprete::escribirTemporales(){
    string tabla = inicioPagina+"<table id =\"customers\">"+"<tr><th>Nombre</th> <th>Valor</th></tr>";
    Temporal *temp;
    for(int i =0; i<this->temporales->listado.length(); i++){
       temp= temporales->listado.at(i);
       tabla+=temp->obtenerHTML();
    }
    tabla+="</table></body></html>";
    return tabla;

}

string analizadorInterprete::escribirHeap(){

    string tabla = inicioPagina+"<table id =\"customers\">"+"<tr><th>Posicion</th> <th>Valor</th></tr>";
    for(int i=0; i<500;i++){
        tabla+="<tr><td>"+this->convertirString(i)+"</td><td>"+ this->convertirString(this->heap[i])+"</td></tr>";
    }
    tabla+="</table></body></html>";
    ofstream fs;
    fs.open("/home/alina/Documentos/arboles/heap.html");
    fs<<tabla;
    fs.close();
    return tabla;


}

string analizadorInterprete::escribirPool(){

    string tabla = inicioPagina+"<table id =\"customers\">"+"<tr><th>Posicion</th> <th>Valor</th></tr>";
    for(int i=0; i<500;i++){
        tabla+="<tr><td>"+this->convertirString(i)+"</td><td>"+this->convertirString( this->pool[i])+"</td></tr>";
    }
    tabla+="</table></body></html>";
    ofstream fs;
    fs.open("/home/alina/Documentos/arboles/pool.html");
    fs<<tabla;
    fs.close();
    return tabla;
}


string analizadorInterprete::escribirStack(){
    string tabla = inicioPagina+"<table id =\"customers\">"+"<tr><th>Posicion</th> <th>Valor</th></tr>";
    for(int i=0; i<500;i++){
        tabla+="<tr><td>"+this->convertirString(i)+"</td><td>"+this->convertirString( this->stack[i])+"</td></tr>";
    }
    tabla+="</table></body></html>";
    ofstream fs;
    fs.open("/home/alina/Documentos/arboles/stack.html");
    fs<<tabla;
    fs.close();
    return tabla;
}


void analizadorInterprete::ejecutar3D(string codigo, string principal){

    Temporal* p = new Temporal("p",0);
    Temporal* h = new Temporal("h",0);
    Temporal* s = new Temporal("s",0);
    this->temporales->insertarTemporal(p);
    this->temporales->insertarTemporal(h);
    this->temporales->insertarTemporal(s);


    if(codigo.compare("")!=0){
        ejecutarAnalizador(codigo);
        this->sentencias= this->raiz3D->hijos;
        this->noFilas= this->raiz3D->fila;

        bool banderaE= true;

        nodoArbol* sentTemporal, *sentTempo2;
        for(int i =0; i< sentencias.at(0)->hijos.length();i++){
            if(banderaE){
                sentTemporal= sentencias.at(0)->hijos.at(i);
                if(sentTemporal->etiqueta.compare("INICIO_FUNCION")==0){
                    if(sentTemporal->valor.compare(principal)==0){
                        for(int j =i; j<this->sentencias.at(0)->hijos.length();j++){
                            sentTempo2= this->sentencias.at(0)->hijos.at(j);

                            if(sentTempo2->etiqueta.compare("FINAL_FUNCION")==0){
                                banderaE=false;
                                break;
                            }

                            if(sentTempo2->etiqueta.compare("SALTO")==0){
                                this->ejecutarInstruccion(sentTempo2);
                                banderaE=false;
                                break;
                            }

                            if(sentTempo2->etiqueta.compare("RELACIONAL")==0){
                                this->ejecutarInstruccion(sentTempo2);
                                banderaE=false;
                                break;
                            }
                            this->ejecutarInstruccion(sentTempo2);
                        }
                    }
                }


            }else{
                break;
            }
        }


        this->ir_a="";
        this->etiqueta="";
    }else{

    }

}


void analizadorInterprete::inStr(){
    double d = temporales->obtenerValorTemporal("h");
    double vS= temporales->obtenerValorTemporal("s");
        int posCadenaRetorno = (int)d;
        int posValor = posCadenaRetorno-1;
        int s = (int)vS;
        double ValorConvertir = this->heap[posValor];
             string noCad = intToCadena(ValorConvertir);
             this->heap[posCadenaRetorno]= s;
             this->pool[s]=noCad.length();
             s++;
             char c;
             for (int i=0; i<noCad.length();i++){
                c= noCad.at(i);
                int n = (int)c;
                this->pool[s]= n;
                s++;
             }
             this->pool[s]=0;
             s++;
             Temporal *ts= new Temporal("s",s);
             this->temporales->modificarTemporal(ts);
}




void analizadorInterprete::leerTeclado(){
    int h = (int) temporales->obtenerValorTemporal("h");
    int posTipoMensaje = h-2;
    int posApuntCadena = h-1;
    string cadena = this->imprimir_str(posApuntCadena);
    QString cCadena= QString::fromStdString(cadena);
    int tipoMsj =(int) heap[posTipoMensaje];
    if(tipoMsj==1){
        //entero
        bool ok=false;
        int i =0;
        while(!ok){
             i = QInputDialog::getInt(formP, "Leer un entero ",
                                         cCadena,0,-2147483647,2147483647,1, &ok);
        }
        if (ok){
           heap[h]=i;
        }
    }else if(tipoMsj==2){
        bool ok= false;
        QString resp="";
        while(!ok){
            resp= QInputDialog::getText(formP, "leer un caracter",
                                                 cCadena, QLineEdit::Normal,
                                                 QDir::home().dirName(), &ok);
        }

        if(ok){
            std::string v = resp.toUtf8().constData();
            string c1 ="";
                           int t = v.length();
                           int indice = 1;
                           while(indice < t-1){
                                c1 = c1+v[indice];
                                indice++;
                               }

                            char c= c1.at(0);
                            int n = (int)c;
                            this->heap[h]= n;
        }

    }else if(tipoMsj ==3){
        // decimal
        bool ok=false;
        double d =0;
        while(!ok){
        d = QInputDialog::getDouble(formP, "Leer decimal",cCadena,0,-2147483647,2147483647,3, &ok);
        }
        if (ok){
           heap[h]=d;
        }

    }else if(tipoMsj == 4){
        bool ok = false;
        QString respuesta;
        QStringList items;
            items << "verdadero" << "falso";
        while(!ok){
                respuesta= QInputDialog::getItem(formP, "Leer booleano",
                                                     cCadena, items, 0, false, &ok);
        }
        if(ok){
            std::string v = respuesta.toUtf8().constData();
            if(sonIguales(v, "verdadero")){
                heap[h]=1;
            }else{
                heap[h]=0;
            }
        }

    }



}

void analizadorInterprete::inNum(){
    double dH = temporales->obtenerValorTemporal("h");
    int h = (int)dH;
    int posValor = h-1;
    int posBandera = h-2;
    double valor = this->heap[posValor];
    double bandera= this->heap[posBandera];
    if(bandera ==1){
        int n=(int)valor;
        heap[h]=n;
    }else if(bandera ==2){

        int posPool = (int)valor;
        int sizeCadena = (int)this->pool[posPool];
         int v = posPool+1;
        string resC="";
        char caracter;
        int t;
        while(true){
            t=(int) this->pool[v];
            caracter= (char)t;
            //cout<<"caracter: "<<caracter<<endl;
            if(caracter==0){
                break;
            }
            else{
                string pp= string(1,caracter);
                //cout<<pp<<endl;
                resC+=pp;

                v=v+1;
            }
        }

        int num = atoi(resC.c_str());
        if(num==0 && !sonIguales(resC,"0")){
            char c;
            int sum=0;
            for (int i=0; i<resC.length();i++){
               c= resC.at(i);
               int n = (int)c;
               sum+=n;
            }
            this->heap[h]=sum;

        }else{
            heap[h]=num;
        }
    }

}





void analizadorInterprete::saltar(string nombreSalto){

    nodoArbol *sentTemporal, *sentTempo2;
    bool banderaE= true;
    for(int i=0; i<this->sentencias.at(0)->hijos.length();i++){
        if(banderaE){
            sentTemporal= this->sentencias.at(0)->hijos.at(i);
            if(sentTemporal->etiqueta.compare("ETIQUETA")==0){
                if(sentTemporal->valor.compare(nombreSalto) == 0){
                    for(int j =i; j<this->sentencias.at(0)->hijos.length(); j++){
                       sentTempo2= this->sentencias.at(0)->hijos.at(j);

                       if(sentTempo2->etiqueta.compare("INICIO_FUNCION")==0){
                          banderaE=false;
                          break;
                       }



                       if(sentTempo2->etiqueta.compare("FINAL_FUNCION")==0){
                           banderaE=false;
                           break;
                       }


                       if(sentTempo2->etiqueta.compare("SALTO")==0){
                           this->ejecutarInstruccion(sentTempo2);
                           banderaE=false;
                           break;
                       }


                       if(sentTempo2->etiqueta.compare("RELACIONAL")==0){
                           this->ejecutarInstruccion(sentTempo2);
                           banderaE=false;
                           break;
                       }

                       this->ejecutarInstruccion(sentTempo2);

                    }
                }
            }

        }else{
            break;
        }
    }



}


void analizadorInterprete::ejecutarAnalizador(string codigo){
    QString qstr = QString::fromStdString(codigo);
    QFile file2("temp2.txt");
    if (file2.open(QFile::WriteOnly | QFile::Truncate)) {
        QTextStream stream1(&file2);
        stream1 << qstr;
    }
    const char* x2 = "temp2.txt";
    FILE* input2 = fopen(x2, "r" );
    int3Drestart(input2);
    setNodoRaizInterprete(this->raiz3D);
    int3Dparse();
   // this->raiz3D->graficarArbol(this->raiz3D);
}



void analizadorInterprete::ejecutarInstruccion(nodoArbol *instruccion)
{
    string nombreNodo = instruccion->getEtiqueta();

   // cout<<nombreNodo<<endl;
    if(nombreNodo.compare("IMPRIMIR")==0){
        this->imprimir(instruccion);
        guarTemp(instruccion->fila);
    }

    else if(nombreNodo.compare("LLAMADA") == 0){
        string nombre= instruccion->valor;
        this->llamadaFuncion(nombre);
        guarTemp(instruccion->fila);

    }

    else if(nombreNodo.compare("ASIG_ED") == 0){

        this->resolverEDD(1,instruccion);
        guarTemp(instruccion->fila);
    }

    else if(nombreNodo.compare("GET_ED") == 0){
        this->resolverEDD(2,instruccion);
        guarTemp(instruccion->fila);
    }

    else if(nombreNodo.compare("RELACIONAL") == 0){
        this->resolverRelacional(instruccion);
        guarTemp(instruccion->fila);

    }

    else if(nombreNodo.compare("SALTO") == 0){
        string nombreSalto =instruccion->valor;
        this->saltar(nombreSalto);
        guarTemp(instruccion->fila);

    }

    else if(nombreNodo.compare("ETIQUETA") == 0){
        guarTemp(instruccion->fila);

    }

    else if(nombreNodo.compare("ASIG") == 0){
        this->resolverOperacion(instruccion);
        guarTemp(instruccion->fila);
    }


    else if(nombreNodo.compare("TRANSFERENCIA") == 0){
        this->resolverTransferencia(instruccion);
        guarTemp(instruccion->fila);

    }else if(sonIguales(nombreNodo, "instr")){
        this->inStr();
        guarTemp(instruccion->fila);
    }else if(sonIguales(nombreNodo, "innum")){
        this->inNum();
        guarTemp(instruccion->fila);
    }else if(sonIguales(nombreNodo, "leerTeclado")){
        this->leerTeclado();
        guarTemp(instruccion->fila);
    }

}



void analizadorInterprete::resolverTransferencia(nodoArbol *nodo){
    string nombreId = nodo->valor;
   // std:://cout<<nombreId<<endl;
    nodoArbol *valor = nodo->hijos.at(0);
    //cout<<valor->valor<<endl;
    double val = this->resolverValor(valor);
    if(val!= noExiste){
        Temporal *temp = new Temporal(nombreId,val);
        this->temporales->insertarTemporal(temp);
    }
}


void analizadorInterprete::llamadaFuncion(string nombreFuncion){

   //Funcion de se realiza una llamada en 3D
    nodoArbol *sentTemporal, *sentTempo2;
    bool banderaE=true;
    string nombreFun;

    for(int i=0; this->sentencias.at(0)->hijos.length(); i++){
        if(banderaE){
            sentTemporal= this->sentencias.at(0)->hijos.at(i);
            if(esIgual(sentTemporal->etiqueta, "INICIO_FUNCION")){
                nombreFun=sentTemporal->valor;
                if(esIgual(nombreFuncion, nombreFun)){

                    ListaTemporales *temps = this->temporales->clonar();//guarnado temporales y asi no se sobreescriban (recursividad)
                    for(int j =i; j<this->sentencias.at(0)->hijos.length();j++){
                        sentTempo2= this->sentencias.at(0)->hijos.at(j);
                        if(esIgual("FINAL_FUNCION",sentTempo2->etiqueta)){
                           banderaE=false;
                           break;
                        }

                        if(esIgual("SALTO",sentTempo2->etiqueta)){
                            this->ejecutarInstruccion(sentTempo2);
                            banderaE=false;
                            break;
                        }

                        if(esIgual("RELACIONAL",sentTempo2->etiqueta)){
                            this->ejecutarInstruccion(sentTempo2);
                            banderaE=false;
                            break;
                        }
                        this->ejecutarInstruccion(sentTempo2);

                    }
                    //restaurando los terminales antes de la llamada
                    for(int x =0; x<temps->listado.length();x++){
                        if(sonIguales(temps->listado.at(x)->nombre, "h") ||
                              sonIguales(temps->listado.at(x)->nombre, "s") /*||
                              sonIguales(temps->listado.at(x)->nombre, "p")  */ ){

                        }else{
                           this->temporales->insertarTemporal(temps->listado.at(x));
                        }

                    }
                }
            }

        }else{
            break;
        }
    }
}

void analizadorInterprete::imprimir(nodoArbol *sentencia)
{
    string tipoImpresion= sentencia->valor;
    int t =atoi(tipoImpresion.c_str());
    nodoArbol *exp = sentencia->hijos.at(0);
    double d = this->resolverValor(exp);

    if(d!= noExiste){

        if(t == 100){
            //es un arreglo de caracteres
            int posHeap = (int)d;
            int posInicio = posHeap+1;
            int posPool = (int)this->heap[posInicio];
            int pValor= (int)this->pool[posPool];

            char c;
            string res="";
            string car="";
            while(posPool!=0){
                c=(char)pValor;
                car = string(1,c);
                res+=c;
                posInicio++;
                posPool= (int)heap[posInicio];
                pValor=(int)pool[posPool];

            }

            this->agregarImpresion(res);




        }
        if(t==99){//caracter;
           int va= (int)d;
           char c=(char)va;
           string str=string(1,c);
           this->agregarImpresion(str);

        }

        if(t==115){//string

            int pos = (int)d;
           std:://cout<<"llamando a imprimir "<<pos<<endl;
           string c= this->imprimir_str(pos);
           this->agregarImpresion(c);


        }

        if(t==105){//int
            ostringstream str1;
            str1 << d;
            string geek = str1.str();
            this->agregarImpresion(geek);

        }

        if(t==102){//double
            ostringstream str1;
            str1 << d;
            string geek = str1.str();
            this->agregarImpresion(geek);
        }

        if(t==98){//bool
            //cout<<"impresion bool "<<d<<endl;
            if(d==1){
                this->agregarImpresion(VERDADERO);
            }else{
                this->agregarImpresion(FALSO);
            }
        }

        if(t==97){//arreglo

        }

    }


}


string analizadorInterprete::imprimir_str(int pos)
{
    double posHeap = this->heap[pos];
    int posPool = (int)posHeap;
     int v = posPool+1;


    //cout<<"posicion inicial "<<v<<endl;
    string resC="";
    char caracter;
    int t;
    while(true){
        t=(int) this->pool[v];
        caracter= (char)t;
        //cout<<"caracter: "<<caracter<<endl;
        if(caracter==0){
            break;
        }
        else{
            string pp= string(1,caracter);
            //cout<<pp<<endl;
            resC+=pp;
            //cout<<resC<<endl;
            v=v+1;
        }
    }

    //cout<<"resultadnte: "<<resC<<endl;
    return resC;

}

void analizadorInterprete::agregarImpresion(string valor){

    this->cadenaImpresion= cadenaImpresion+valor+"\n";
}




void analizadorInterprete::resolverEDD(int tipo, nodoArbol *sentencia){

    string edd= sentencia->valor;
    if(tipo ==1){ //entra stack[t1]=2

        nodoArbol *posicion, *valorAsignar;
        posicion= sentencia->hijos.at(0);
        valorAsignar= sentencia->hijos.at(1);

        double v1= this->resolverValor(posicion);
        double v2 = this->resolverValor(valorAsignar);

        if(v1!= noExiste && v2 != noExiste){
            if(edd.compare("stack")==0){//stack
                int pos = (int)v1;
                this->stack[pos]=v2;
            }

            if(edd.compare("heap")==0){//heap
                int pos = (int)v1;
                this->heap[pos]=v2;
            }

            if(edd.compare("pool")==0){//pool
                int pos = (int)v1;
                this->pool[pos]=v2;
            }
        }
    }


    if(tipo==2){//sale t1=stack[t2]
        string nombreId = sentencia->hijos.at(0)->valor;
        nodoArbol* posicion = sentencia->hijos.at(1);
       double pos= resolverValor(posicion);
       if(pos!= noExiste){
           if(edd.compare("stack")==0){//stack
               int p =(int)pos;
               if(p>=0){
               double v= this->stack[p];
               Temporal* temp= new Temporal(nombreId,v);
               this->temporales->insertarTemporal(temp);
               }else{

                   cout<<"Valor del stack "<<p<<"==========================================================================================="<<posicion->valor<<endl;
               }
           }

           if(edd.compare("heap")==0){//heap
               int p =(int)pos;

               if(p>=0){
                   double v= this->heap[p];
                   Temporal* temp= new Temporal(nombreId,v);
                   this->temporales->insertarTemporal(temp);
               }else{
                   cout<<"Valor del heap "<<p<<"==========================================================================================="<<posicion->valor<<endl;
               }

           }

           if(edd.compare("pool")==0){//pool
               int p =(int)pos;
               if(p>=0){
                   double v= this->pool[p];
                   Temporal* temp= new Temporal(nombreId,v);
                   this->temporales->insertarTemporal(temp);
               }else{
                   cout<<"Valor del pool "<<p<<"==========================================================================================="<<posicion->valor<<endl;
               }

           }


       }



    }

}


void analizadorInterprete:: resolverRelacional(nodoArbol *sentencia){
    string simbolo= sentencia->valor;
    nodoArbol* val1= sentencia->hijos.at(0);
    nodoArbol* val2 = sentencia->hijos.at(1);
    string etiqV= sentencia->hijos.at(2)->valor;
    string etiqF= sentencia->hijos.at(3)->valor;
    bool res= this->evaluarCondicion(simbolo, val1, val2);
    if(res== true){
     this->ir_a= etiqV;
       this->saltar(etiqV);

    }else{
        this->ir_a=etiqF;
        this->saltar(etiqF);

    }
}


bool analizadorInterprete::evaluarCondicion(string simbolo, nodoArbol *val1, nodoArbol *val2)
{
    double v1 = this->resolverValor(val1);
    double v2 = this->resolverValor(val2);
    if(v1!=noExiste && v2!= noExiste){
        if(simbolo.compare("==")==0){
            if(v1==v2){
                return true;
            }
            return false;
        }

        if(simbolo.compare("<")==0){
            if(v1<v2){
                return true;
            }
            return false;
        }


        if(simbolo.compare(">")==0){
            if(v1>v2){
                return true;
            }
            return false;
        }


        if(simbolo.compare("<=")==0){
            if(v1<=v2){
                return true;
            }
            return false;
        }

        if(simbolo.compare(">=")==0){
            if(v1>=v2){
                return true;
            }
            return false;
        }

        if(simbolo.compare("!=")==0){
            if(v1!=v2){
                return true;
            }
            return false;
        }


    }
    return false;

}

void analizadorInterprete:: resolverOperacion(nodoArbol *sentencia){
    string operador = sentencia->valor;
    nodoArbol* val1= sentencia->hijos.at(1);
    nodoArbol* val2= sentencia->hijos.at(2);
    string valCont= sentencia->hijos.at(0)->valor;

    if(operador.compare("+")==0){
        double v1= this->resolverValor(val1);
        double v2 = this->resolverValor(val2);
        if(v1!= noExiste && v2!= noExiste){
            double res = v1+v2;
            Temporal* nuevo = new Temporal(valCont,res);
            this->temporales->insertarTemporal(nuevo);
        }
    }


    if(operador.compare("-")==0){
        double v1= this->resolverValor(val1);
        double v2 = this->resolverValor(val2);
        if(v1!= noExiste && v2!= noExiste){
            double res = v1-v2;
            Temporal* nuevo = new Temporal(valCont,res);
            this->temporales->insertarTemporal(nuevo);
        }
    }


    if(operador.compare("*")==0){
        double v1= this->resolverValor(val1);
        double v2 = this->resolverValor(val2);
        if(v1!= noExiste && v2!= noExiste){
            double res = v1*v2;
            Temporal* nuevo = new Temporal(valCont,res);
            this->temporales->insertarTemporal(nuevo);
        }
    }



    if(operador.compare("/")==0){
        double v1= this->resolverValor(val1);
        double v2 = this->resolverValor(val2);
        if(v1!= noExiste && v2!= noExiste){
            double res = v1/v2;
            Temporal* nuevo = new Temporal(valCont,res);
            this->temporales->insertarTemporal(nuevo);
        }
    }

}



double analizadorInterprete:: resolverValor(nodoArbol *val){

    string nombreNodo = val->etiqueta;
    //cout<<nombreNodo<<endl;
   // cout<<val->valor<<endl;

    if(nombreNodo.compare(T_ENTERO)==0){
       // cout<<val->valor<<endl;
        double num = atof(val->valor.c_str());
        return num;
    }

    if(nombreNodo.compare(T_DECIMAL)==0){

        double num = atof(val->valor.c_str());
        return num;
    }

    if(nombreNodo.compare("ID")==0){
        double valor = temporales->obtenerValorTemporal(val->valor);

        //cout<<val->valor<<endl;
        if(valor!= noExiste){
           // cout<<valor<<endl;
        }
        return valor;
    }

    if(nombreNodo.compare("NEGATIVO")==0){
        nodoArbol *v2 = val->hijos.at(0);
        string  nombre = v2->etiqueta;
       // cout<<nombre<<endl;
       // cout<<v2->valor<<endl;

        if(nombre.compare(T_ENTERO)==0){
            double num = atof(v2->valor.c_str());
           // cout<<num<<endl;
            return num*-1;
        }

        if(nombre.compare(T_DECIMAL)==0){

            double num = atof(v2->valor.c_str());
           // cout<<num<<endl;
            return num*-1;
        }

        if(nombre.compare("ID")==0){
            double valor = temporales->obtenerValorTemporal(v2->valor);
            //cout<<valor<<endl;
            return valor*-1;
        }


    }

    return noExiste;

}


bool analizadorInterprete::esIgual(string cad1, string cad2){


    string cad11="";
    string cad22="";


    for(int i =0; i<cad1.length();i++){
        char c = toupper(cad1.at(i));
        cad11=cad11+c;
    }

    for(int i =0; i<cad2.length();i++){
        char c = toupper(cad2.at(i));
        cad22=cad22+c;
    }

    if(cad11.compare(cad22)==0){
        return true;
    }else{
        return false;
    }
}



