#ifndef LISTAFUNCIONES_H
#define LISTAFUNCIONES_H
#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <QList>
#include "nodoarbol.h"
#include "mprincipal.h"
#include "listafunciones.h"
#include "constantes.h"
#include "simbolo.h"
#include "ambito.h"
#include "funcion.h"

using namespace std;

class ListaFunciones
{
public:
    int noFilas;
    QList<Funcion*> funciones;
    ListaFunciones();
    void insertarFuncion(Funcion *nueva);
    bool existeFuncion(Funcion * nueva);
    void agregarNombreClase(string nombre);
    void insertarFuncionHeredad(Funcion *nueva, string clase);
};

#endif // LISTAFUNCIONES_H
