#include "generadorcodigo.h"



GeneradorCodigo::GeneradorCodigo(ListaClases *clases, ListaCodigo *codigo)
{
    erroresEjecucion = new ListaErrores();
    c3d = new codigo3D();
    this->clases= clases;
    this->ambiente= new Ambito();
    this->tabla= new tablaSimbolos();
    this->etiquetasBreak= new Etiquetas();
    this->etiquetasContinuar= new Etiquetas();
    this->etiquetasRetorno= new Etiquetas();
    this->codigoLista = codigo;
}

void GeneradorCodigo::addErrorSemantico(string tipo, string descripcion, int col, int fila)

{
    erroresEjecucion->insertarError(tipo, descripcion,fila, col);
}



void GeneradorCodigo::agregarCodigo(int id, string codigo, string errores){
    codigoLista->insertarCodigo(id,codigo,errores);

}



void GeneradorCodigo::generar3D(){

    this->erroresEjecucion= new ListaErrores();
  //1. llenar la tabla de simbolos
  this->generarSimbolosClase();


  //2. generar importaciones


 /*3. Aqui se genera el codigo de clase por clase y funcion por funcion*/

    Clase *claseTemporal;
    nodoArbol *sentTemporal;
    string nombreClase="";
    string nombreFuncion="";
    Funcion *funTemporal;

    for(int i =0; i< this->clases->clasesArchivo.length();i++){
        this->ambiente= new Ambito();
        claseTemporal = this->clases->clasesArchivo.at(i);
        nombreClase = claseTemporal->nombreClase;
        ambiente->addAmbito(nombreClase);

        //2. traducimos funcion por funcion
        for(int j=0; j<claseTemporal->funciones->funciones.length(); j++){
            funTemporal = claseTemporal->funciones->funciones.at(j);
            ambiente->addAmbito(funTemporal->obtenerFirma());
            nombreFuncion = funTemporal->obtenerFirma();
            this->c3d->addCodigo("");
            this->c3d->addCodigo("void "+ nombreFuncion+"() {");
            this->c3d->addCodigo("");
            this->agregarCodigo(funTemporal->fila,c3d->codigo, erroresEjecucion->imprimirErrores());
            string etiquetaRet= this->c3d->getEtiqueta();
            this->etiquetasRetorno->insertarEtiqueta(etiquetaRet);

            if(funTemporal->esConstructor){
                //inicializar los atributos que tengan asignacion
                QList<Simbolo*>atributos = tabla->obtenerAtributosClase(nombreClase);
                Simbolo* tempS;
                for(int z=0; z<atributos.length();z++){
                   tempS= atributos.at(z);
                   if(tempS->declaAtributo!= NULL){
                       this->analizarSentencia(tempS->declaAtributo,nombreClase,nombreFuncion);
                   }

                   if(tempS->asignaAtributo!= NULL){
                       this->analizarSentencia(tempS->asignaAtributo, nombreClase, nombreFuncion);
                   }

                }


            }

            cout<<funTemporal->sentencias->hijos.length()<<endl;
            for(int k =0; k<funTemporal->sentencias->hijos.length();k++){
                sentTemporal = funTemporal->sentencias->hijos.at(k);
                if(sentTemporal != NULL){
                cout<<sentTemporal->etiqueta<<endl;
                this->analizarSentencia(sentTemporal,nombreClase,nombreFuncion);}

            }
            this->c3d->addCodigo("");
            this->c3d->addEtiqueta(etiquetaRet);
            etiquetasRetorno->eliminarEtiqueta();
            this->c3d->addCodigo("}");
            ambiente->salirAmbito();
        }

        /*Metodo principal*/

        if(claseTemporal->principal!= NULL){
            nombreFuncion = nombreClase+"_PRINCIPAL";
            ambiente->addAmbito(nombreFuncion);
            c3d->addCodigo("void "+ nombreFuncion+"(){");
            string etiquetaR= c3d->getEtiqueta();

            this->etiquetasRetorno->insertarEtiqueta(etiquetaR);
            for(int j = 0; j< claseTemporal->principal->sentencias->hijos.length();j++){
                sentTemporal = claseTemporal->principal->sentencias->hijos.at(j);
                this->analizarSentencia(sentTemporal,nombreClase, nombreFuncion);
            }
            c3d->addCodigo("");
            c3d->addEtiqueta(etiquetaR);
            etiquetasRetorno->eliminarEtiqueta();
            c3d->addCodigo("}");
            ambiente->salirAmbito();
        }

        ambiente->salirAmbito();
    }
    c3d->addCodigo("(");

codigoLista->mostrarCodigo();
}


void GeneradorCodigo::agregarHerencia(){
    Clase *claseTemporal;
    QList<nodoArbol*>atributosHeredados;
    QList<Funcion*>funcionesHeredadas;
    for(int i =0; i<this->clases->clasesArchivo.length(); i++){
        claseTemporal = this->clases->clasesArchivo.at(i);
        if(!sonIguales(claseTemporal->herencia,"")){
            Clase *clasePadre = clases->obtenerClaseNombre(claseTemporal->herencia);
            if(clasePadre != NULL){
                cout<<"si existe la clase padre"<<endl;
                atributosHeredados = clasePadre->obtenerAtributosHeredados();
                funcionesHeredadas= clasePadre->obtenerFuncionesHeredadas();

                //insertando atributos
                for(int j =0; j<atributosHeredados.length();j++){
                    this->clases->clasesArchivo.at(i)->atributos.push_back(atributosHeredados.at(j));
                }

                //insertnado funciones heredadas
                for(int j =0; j<funcionesHeredadas.length();j++){
                    this->clases->clasesArchivo.at(i)->insertarFuncionHeredada(funcionesHeredadas.at(j));
                }


            }
        }
    }
}



string GeneradorCodigo::resolverThisPosicion(nodoArbol *sent, string clase, string metodo){
    string tipo = sent->valor;
    if(sonIguales(tipo, "2")){
        //este.id
        string nombre= sent->hijos.at(0)->valor;
        Simbolo *simb = tabla->obtenerSimbolo(nombre, ambiente, 1);
        if(simb!=NULL){
            string t1= c3d->getTemporal();
            string t2= c3d->getTemporal();
            string t3= c3d->getTemporal();
            string t4 = c3d->getTemporal();
           // string t5= c3d->getTemporal();
            c3d->addCodigo("//==== RESOLVIENDO UN THIS");
            c3d->addCodigo(t1+"= p+0;//pos del this");
            c3d->addCodigo(t2+"=stack["+t1+"];//obteniendo apuntaro al heap");
            c3d->addCodigo(t3+"=heap["+t2+"];//apuntador");
            c3d->addCodigo(t4+"="+t3+"+"+intToCadena(simb->apuntador)+";// pos de "+ simb->nombrecorto);
           /* c3d->addCodigo(t5+"=heap["+t4+"]; //obtenedo el valor que se encuentra en el heap");
            Valor *v= new Valor();
            v->tipo= simb->tipoElemento;
            v->valor= t4;
            return v;*/
            return t4;

        }else{
            erroresEjecucion->insertarError(SEMANTICO, "El atributo "+ nombre+" no existe en el ambito actual",sent->fila,sent->columna);
        }

    }else if(sonIguales(tipo, "1")){

    }else if(sonIguales(tipo, "3"))
    {

    } else{

    }
return "";

}



Valor* GeneradorCodigo::resolverThis(nodoArbol *sent, string clase, string metodo){
    string tipo = sent->valor;
    Valor *error = new Valor();
    if(sonIguales(tipo, "2")){
        //este.id
        string nombre= sent->hijos.at(0)->valor;
        Simbolo *simb = tabla->obtenerSimbolo(nombre, ambiente, 1);
        if(simb!=NULL){
            string t1= c3d->getTemporal();
            string t2= c3d->getTemporal();
            string t3= c3d->getTemporal();
            string t4 = c3d->getTemporal();
            string t5= c3d->getTemporal();
            c3d->addCodigo("//==== RESOLVIENDO UN THIS");
            c3d->addCodigo(t1+"= p+0;//pos del this");
            c3d->addCodigo(t2+"=stack["+t1+"];//obteniendo apuntaro al heap");
            c3d->addCodigo(t3+"=heap["+t2+"];//apuntador");
            c3d->addCodigo(t4+"="+t3+"+"+intToCadena(simb->apuntador)+";// pos de "+ simb->nombrecorto);
            c3d->addCodigo(t5+"=heap["+t4+"]; //obtenedo el valor que se encuentra en el heap");
            Valor *v= new Valor();
            v->tipo= simb->tipoElemento;
            v->valor= t5;
            return v;

        }else{
            erroresEjecucion->insertarError(SEMANTICO, "El atributo "+ nombre+" no existe en el ambito actual",sent->fila, sent->columna);
            return error;
        }

    }else if(sonIguales(tipo, "1")){
        //este.acceso
        nodoArbol *elemento = sent->hijos.at(0);
        if(sonIguales(elemento->valor,"1")){
            nodoArbol *id = elemento->hijos.at(0);
            string nombreId = id->valor;
            int esAtributo = tabla->esAtributo(nombreId,this->ambiente);
            if(esAtributo==1){
                Valor *ret = this->resolverAcceso(elemento, this->ambiente,clase, metodo);
                return ret;
            }else{
                erroresEjecucion->insertarError(SEMANTICO, "Error, no es un atributo "+ nombreId+", no se pudo realizar acceso", elemento->fila, elemento->columna);
                return error;
            }
        }else{
            //por si es un acceso con llamada o arreglo[]
            erroresEjecucion->insertarError(SEMANTICO, "Acceso no valido para realizar el this", elemento->fila, elemento->columna);
            return error;
        }


    }else if(sonIguales(tipo, "3"))
    {
        //este.posArreglo
        nodoArbol *elemento = sent->hijos.at(0);
        string nombre= elemento->valor;
        QList<nodoArbol*>dimensiones = elemento->hijos.at(0)->hijos;
        int esAtributo = tabla->esAtributo(nombre, this->ambiente);
        string tipoArreglo = tabla->obtenerTipo(nombre, this->ambiente,esAtributo);
        if(esAtributo == 1){
            Valor *apuntador = this->obtenerApuntadorPosArreglo(nombre,dimensiones,ambiente,clase,metodo,"",false);
            if(!esNulo(apuntador->tipo)){
                string temp2_5= c3d->getTemporal();
                c3d->addCodigo(temp2_5+"=heap["+apuntador->valor+"]; //valor que trael el objeto");
                Valor *resp = new Valor();
                resp->tipo= tipoArreglo;
                resp->valor= temp2_5;
                resp->setReferencia("heap",apuntador->valor);
                return resp;
            }
        }else{
            erroresEjecucion->insertarError(SEMANTICO, "El arreglo "+nombre+", no es un atributo, no se puede realizar this", sent->fila, sent->columna);
            return error;
        }

    }else{
        //este.llamadaFuncion
        nodoArbol * elementoThis = sent->hijos.at(0);
        Valor *v = this->llamadaFuncion(elementoThis,clase, metodo,true,"","","");
        return v;

    }

    return error;

}

string GeneradorCodigo::buscarPrincipal(){
    Clase *claseTemporal;
    if(this->clases->clasesArchivo.length()==1){
        claseTemporal = this->clases->clasesArchivo.at(0);
        if(claseTemporal->principal!= NULL){
            return claseTemporal->nombreClase+"_PRINCIPAL";
        }else{
            return "";
        }
    }

    if(this->clases->clasesArchivo.length()>1){
        for(int i =this->clases->clasesArchivo.length()-1; 0<i; i--){
            claseTemporal = this->clases->clasesArchivo.at(i);
            if(claseTemporal->principal!= NULL){
                return claseTemporal->nombreClase+"_PRINCIPAL";
            }
        }
    }
    return "";
}

void GeneradorCodigo::generarSimbolosClase(){
    //agregar herencia en las clases
    this->agregarHerencia();
    Clase *claseTemporal;
    QList<Simbolo*> simbs;
    for(int i=0; i< this->clases->clasesArchivo.length(); i++){
        claseTemporal= this->clases->clasesArchivo.at(i);
        simbs= claseTemporal->generarSimbolosClase(this->erroresEjecucion);
        this->tabla->insertarSimbolosClase(simbs);
    }

}



void GeneradorCodigo::analizarSentencia(nodoArbol *nodo, string clase, string metodo){

    string nombreNodo = nodo->etiqueta;
    //std:://cout<< nombreNodo<< endl;
    if(nombreNodo.compare("raiz")==0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        for(int i=0;i<hijos.length();i+=1)
        {
           this->analizarSentencia(hijos.at(i),clase,metodo);
        }
    }

    if(sonIguales(nombreNodo, "LEER_TECLADO")){
        this->resolverLeerTeclado(nodo, clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }
    if(sonIguales(nombreNodo, "SELECCIONA")){
        this->resolverSelecciona(nodo, clase, metodo);
       // this->agregarCodigo(nodo->fila,this->c3d->codigo);
    }

    if(sonIguales(nombreNodo,"PARA")){
       // //cout<<nombreNodo<<endl;
        this->resolverPara(nodo, clase, metodo);
       // this->agregarCodigo(nodo->fila,this->c3d->codigo);
    }

    if(nombreNodo.compare("INSTRUCCIONES")==0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        for(int i=0;i<hijos.length();i+=1)
        {
           this->analizarSentencia(hijos.at(i),clase,metodo);
        }

    }
    if(sonIguales(nombreNodo, "ACCESO")){
       this->resolverAcceso(nodo, ambiente, clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }

    if(nombreNodo.compare("IMPRIMIR")==0){
        this->imprimir(nodo->hijos.at(0), clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());

    }
    if(sonIguales("SI",nombreNodo)){

        this->resolverIF(nodo,clase, metodo);
        //this->agregarCodigo(nodo->fila,this->c3d->codigo);
    }
    if(sonIguales("ASIGNACION",nombreNodo)){
        this->resolverAsignacion(nodo,clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }
    if(sonIguales("CONTINUAR" , nombreNodo)){
        c3d->addCodigo("goto "+this->etiquetasContinuar->obtenerActual()+"; //haciendo un continuar");
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }


    if(sonIguales("DETENER",nombreNodo))
    {
        c3d->addCodigo("goto "+this->etiquetasBreak->obtenerActual()+"; //haciendo un detener");
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }


    if(sonIguales("MIENTRAS", nombreNodo)){
        this->resolverMientras(nodo,clase, metodo);
       // this->agregarCodigo(nodo->fila,this->c3d->codigo);
    }

    if(sonIguales("HACER_MIENTRAS", nombreNodo)){
        this->resolverHacerMientras(nodo, clase, metodo);
       // this->agregarCodigo(nodo->fila,this->c3d->codigo);
    }


    if(sonIguales(nombreNodo, "LLAMADA_FUNCION")){
        this->llamadaFuncion(nodo, clase, metodo, true,"","","");
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }
    if(sonIguales(nombreNodo, "RETORNO")){
        this->operarRetorno(nodo, clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }

    if(sonIguales(nombreNodo, "DECLA_ARREGLO")){
        this->operarArreglo(nodo,clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }

    if(sonIguales(nombreNodo, "CONCATENAR"))
{
        this->concatenar(nodo, clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }
    if(sonIguales(nombreNodo, "DECLARACION")){
        this->resolverDeclaracion(nodo, clase, metodo);
        this->agregarCodigo(nodo->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    }
}




void GeneradorCodigo::resolverPara(nodoArbol *sentencia, string clase, string funcion){
    nodoArbol *declaPara = sentencia->hijos.at(0);
    nodoArbol *expresion = sentencia->hijos.at(1);
    nodoArbol *asignacion = sentencia->hijos.at(2);
    QList<nodoArbol*>sentencias = sentencia->hijos.at(3)->hijos;
    this->ambiente->addPara();
    this->analizarSentencia(declaPara, clase, funcion);
    string etiqBreak= c3d->getEtiqueta();
    string etiqContinue = c3d->getEtiqueta();
    string etiqCiclo = c3d->getEtiqueta();
    this->etiquetasBreak->insertarEtiqueta(etiqBreak);
    this->etiquetasContinuar->insertarEtiqueta(etiqContinue);
    c3d->addCodigo("//-------------- RESOLVIENDO CICLO PARA ----------------");
    c3d->addCodigo(etiqCiclo+": //etiqueta del ciclo para ");
    Valor *v= resolverExpresion(expresion, clase, funcion);
    v = this->convertirACondicion(v);
    if(esCondicion(v->tipo)){
        c3d->addCodigo(v->cond->codigo);
        c3d->addCodigo(v->cond->getEtiquetasVerdaderas());
        this->agregarCodigo(declaPara->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
        for(int i = 0; i<sentencias.length();i++){
            this->analizarSentencia(sentencias.at(i),clase, funcion);
        }
        c3d->addCodigo(etiqContinue+": //etiqueta continuar del ciclo para");
        this->analizarSentencia(asignacion, clase, funcion);
        c3d->addCodigo("goto "+etiqCiclo+";");
        c3d->addCodigo(v->cond->getEtiquetasFalsas());
        c3d->addCodigo(etiqBreak+": //etiqueta break del ciclo para");
        this->etiquetasBreak->eliminarEtiqueta();
        this->etiquetasContinuar->eliminarEtiqueta();
    }else{
        this->erroresEjecucion->insertarError(SEMANTICO, "Expresion no validar para ciclo para ", expresion->fila, expresion->columna );
    }

    this->ambiente->salirAmbito();


}


void GeneradorCodigo::resolverSelecciona(nodoArbol *sentencia, string clase, string metodo){
    nodoArbol* expresion = sentencia->hijos.at(0);
    QList<nodoArbol*> lcasos = sentencia->hijos.at(1)->hijos;
    c3d->addCodigo("// ================== RESOLVIENDO SWITCH ====================");
    string etiqBreak = c3d->getEtiqueta();
    this->etiquetasBreak->insertarEtiqueta(etiqBreak);
    //Separando los caso y los defecto
    QList<nodoArbol*> casos ;
    QList<nodoArbol*> defectos;
    nodoArbol *casoTemporal;
    for(int i =0; i<lcasos.length();i++){
        casoTemporal = lcasos.at(i);
        if(sonIguales(casoTemporal->etiqueta, "CASO")){
            casos.push_back(casoTemporal);
        }else{
            defectos.push_back(casoTemporal);
        }
    }
    Valor *retExpresion = this->resolverExpresion(expresion,clase, metodo);
    retExpresion = this->condiABool(retExpresion);
    this->agregarCodigo(expresion->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
    if(esInt(retExpresion->tipo) || esDecimal(retExpresion->tipo) || esCaracter(retExpresion->tipo)){
        //resolviendo las expresiones de todos los casos
        bool bandera = true;
        Valor *resExpCaso;
        string etiqTemp;
        QList<string> valoresCaso;
        QList<string>casosEtiquetas;
        for(int i =0; i<casos.length(); i++ ){
            casoTemporal = casos.at(i);
            resExpCaso = this->resolverExpresion(casoTemporal->hijos.at(0),clase, metodo);
            resExpCaso= this->condiABool(resExpCaso);
            if(sonIguales(resExpCaso->tipo, retExpresion->tipo)){
                bandera = bandera && true;
                etiqTemp= c3d->getEtiqueta();
                valoresCaso.push_back(resExpCaso->valor);
                casosEtiquetas.push_back(etiqTemp);
            }else{
                etiqTemp= c3d->getEtiqueta();
                valoresCaso.push_back(resExpCaso->valor);
                casosEtiquetas.push_back(etiqTemp);
                bandera =false;
            }
        }

        if(valoresCaso.length() == casos.length() && 
            valoresCaso.length()== casosEtiquetas.length()){
                //Generar todas las condiciones de las listada de casos
                string valorTemporal, etiqVTemp, etiqF;
                for(int i = 0; i<valoresCaso.length(); i++){
                    valorTemporal = valoresCaso.at(i);
                    etiqVTemp = casosEtiquetas.at(i);
                    etiqF= c3d->getEtiqueta();
                    c3d->addCodigo("if("+retExpresion->valor+"=="+valorTemporal +") goto "+ etiqVTemp+";\n goto "+etiqF+";");
                    c3d->addCodigo(etiqF+": //etiqueta falsa del caso "+ intToCadena(i+1));
                  //  this->agregarCodigo(casos.at(i)->fila,this->c3d->codigo, erroresEjecucion->imprimirErrores());
                }
                //escribir codigo para etiqueta defecto 
                string etiqDefecto = c3d->getEtiqueta();
                c3d->addCodigo("goto "+etiqDefecto+"; //saltar a la etiqueta defecto ");

                //Esccribie el codigo de los casos
            QList<nodoArbol*> sentenciasC;
            for(int i =0; i<casos.length(); i++){
                casoTemporal = casos.at(i);
                sentenciasC= casoTemporal->hijos.at(1)->hijos;
                c3d->addCodigo(casosEtiquetas.at(i)+":");
                this->ambiente->addCaso();
                for(int j=0; j<sentenciasC.length();j++){
                    this->analizarSentencia(sentenciasC.at(j),clase, metodo);
                }
                this->ambiente->salirAmbito();
            }

            //escribir el caso del defecto
            c3d->addCodigo(etiqDefecto+": //etiqueda defecto");
            this->ambiente->addDefecto();
            if(defectos.length()>0){
                nodoArbol *defec= defectos.at(0);
                sentenciasC = defec->hijos.at(0)->hijos;
                for(int j=0; j<sentenciasC.length(); j++){
                    this->analizarSentencia(sentenciasC.at(j),clase, metodo);
                }   
            }
            this->ambiente->salirAmbito();
            c3d->addCodigo(etiqBreak+": // break de evaluar el switch ");
            c3d->addCodigo("// FIN DE RESOLVER EL SWITCH");

        

            }else{

            }

    }else{
        erroresEjecucion->insertarError(SEMANTICO, "No es valido el tipo para la expresion del Switch " + retExpresion->tipo, expresion->fila, expresion->columna);
    }


  this->etiquetasBreak->eliminarEtiqueta();
}

void GeneradorCodigo::resolverDeclaracion(nodoArbol *sentencia, string clase, string funcion){
   string tipo = sentencia->valor;
    string id= sentencia->hijos.at(1)->valor;
    int esAtributo = tabla->esAtributo(id, this->ambiente);
    Simbolo *s= tabla->obtenerSimbolo(id,this->ambiente,esAtributo);
    if(s!= NULL){
            if(s->asignaAtributo!= NULL){
                cout<<s->asignaAtributo->etiqueta<<endl;
                this->analizarSentencia(s->asignaAtributo,clase, funcion);
            }
    }
}

void GeneradorCodigo::operarArreglo(nodoArbol *sent, string clase, string funcion){

    string tipo = sent->hijos.at(0)->valor;
    string nombreArreglo = sent->hijos.at(1)->valor;
    QList<nodoArbol*>dimensiones = sent->hijos.at(2)->hijos;
    this->declararArreglo(tipo,nombreArreglo,dimensiones,ambiente,clase, funcion,false,"","",NULL);
    int esAtributo = tabla->esAtributo(nombreArreglo, this->ambiente);
    Simbolo *simb = tabla->obtenerSimbolo(nombreArreglo, ambiente, esAtributo);
    if(simb!= NULL){
        if(simb->asignaAtributo!=NULL){
            this->analizarSentencia(simb->asignaAtributo, clase, funcion);
        }
    }

}


string GeneradorCodigo::calcularSizeArreglo(QList<string> posiciones){

    string tempSize = c3d->getTemporal();
    c3d->addCodigo(tempSize+"=1; //iniciando el tamanio del arreglo");
    for(int i =0;i<posiciones.length();i++ ){
        c3d->addCodigo(tempSize+"="+tempSize+"*"+posiciones.at(i)+";");
    }
    return tempSize;
}

void GeneradorCodigo::declararArreglo(string tipoArreglo, string nombreArreglo, QList<nodoArbol *> dimensiones, Ambito *ambiente, string clase, string metodo, bool modo, string pos, string nombreBuscado, Simbolo *simb2){
   int esAtributo =3;
   Simbolo *simb= NULL;

   if(modo){
       simb = simb2;
       if(simb!= NULL){
           if(sonIguales(simb->rol, "ATRIBUTO")){
               esAtributo= 1;
           }else{
               esAtributo=2;
           }
       }

   }else{
       esAtributo = tabla->esAtributo(nombreArreglo, ambiente);
       simb = tabla->obtenerSimbolo(nombreArreglo, ambiente,esAtributo);
   }

   if(esAtributo==1 || esAtributo==2){
       if(sonIguales(simb->tipoSimbolo, "ARREGLO")){

           if(esAtributo==1){
               //es un arreglo atributo
               int posArreglo = tabla->obtenerPosGlobal(nombreArreglo, ambiente);
               if(posArreglo!=-1){
                   string posA= intToCadena(posArreglo);
                   string t1=c3d->getTemporal();
                   string t2=c3d->getTemporal();
                   string t3=c3d->getTemporal();
                   string t4=c3d->getTemporal();
                   c3d->addCodigo("//=================creando arreglo atributo================");
                   c3d->addCodigo(t1+"=p+0; //pos this del arreglo");
                   c3d->addCodigo(t2+"=stack["+t1+"];//obteniendio apuntador del arreglo en el heap");
                   c3d->addCodigo(t3+"=heap["+t2+"];");
                   c3d->addCodigo(t4+"="+t3+"+"+posA+";//pos del heap donde guarda apuntador deh heap para el arreglo "+nombreArreglo);
                   c3d->addCodigo("heap["+t4+"]=h; //escribiendo apuntador del heap donde incia el arreglo "+nombreArreglo);

                   QList<string>tamanio= this->calcularArregloNs(dimensiones,ambiente, clase,metodo);

                   if(tamanio.length()==dimensiones.length()){
                       simb->arregloNs=tamanio;
                       //tabla->setArregloNs(nombreArreglo,ambiente,esAtributo,tamanio);
                       string tempRes= this->calcularSizeArreglo(tamanio);
                       if(!sonIguales(tempRes, "")){

                           c3d->addCodigo(tempRes+"="+tempRes+"+0;//size del arreglo "+nombreArreglo); //extra
                           c3d->addCodigo("heap[h]="+tempRes+"; //insertando el tamanio del arreglo linealizado "+ nombreArreglo);
                           c3d->addCodigo("h=h+1;");
                           string t1_2= c3d->getTemporal();
                           c3d->addCodigo(t1_2+"="+tempRes+"+0;//anhadiendo un poscione mas ");
                           c3d->addCodigo("h=h+"+t1_2+";");

                       }
                   }else{
                      erroresEjecucion->insertarError(SEMANTICO,"Ha ocurrido un error al resolver las dimensiones del arreglo atributo "+nombreArreglo,dimensiones.at(0)->fila, dimensiones.at(0)->columna);
                   }



               }else{
                   erroresEjecucion->insertarError(SEMANTICO,"No existe el arreglo atributo "+nombreArreglo,dimensiones.at(0)->fila, dimensiones.at(0)->columna);
               }


           }else{
               //es un arreglo local
               string  posArreglo ="-1";
               if(modo){
                   posArreglo=pos;
               }else{
                   int posA= tabla->obtenerPosLocal(nombreArreglo, ambiente);
                   posArreglo = intToCadena(posA);
               }

               if(!sonIguales(posArreglo,"-1")){
                   string t1,t2;
                   t1= c3d->getTemporal();
                   t2= c3d->getTemporal();
                   c3d->addCodigo("//declarando un arreglo local");
                   if(modo){
                   c3d->addCodigo(t1+"= 0+ "+posArreglo+";// pos del arreglo "+nombreArreglo);
                 }else{
                      c3d->addCodigo(t1+"= p+ "+posArreglo+";// pos del arreglo "+nombreArreglo);
                   }

                   c3d->addCodigo("stack["+t1+"]=h;// ingresando al stack el apuntador al heap para "+nombreArreglo);
                   c3d->addCodigo(t2+"=h+1;");
                   c3d->addCodigo("heap[h]="+t2+";");
                   c3d->addCodigo("h=h+1;");
                   c3d->addCodigo("//calculadno el tamanio del arreglo");
                   QList<string> tamanios = this->calcularArregloNs(dimensiones, ambiente, clase, metodo);
                   if(tamanios.length()==dimensiones.length()){
                      simb->arregloNs=tamanios;
                       //tabla->setArregloNs(nombreArreglo,ambiente,esAtributo,tamanios);
                       string nTemporal, tempRes;
                       tempRes=this->calcularSizeArreglo(tamanios);

                       /*for(int k=0; k<tamanios.length();k++){
                           nTemporal= tamanios.at(k);
                           if(k==0){
                               string temp1_1= c3d->getTemporal();
                               tempRes= c3d->getTemporal();
                               c3d->addCodigo(temp1_1+"= "+nTemporal+"-1; //calculando n real");
                               c3d->addCodigo(tempRes+"="+temp1_1+"-0; //iReal columna "+intToCadena(k));
                           }else{
                               string temp1_1= c3d->getTemporal();
                               string temp2_1= c3d->getTemporal();
                               string temp3_1= c3d->getTemporal();
                               c3d->addCodigo(temp1_1+"="+nTemporal+"-1;//calculando el n real");
                               c3d->addCodigo(temp2_1+"="+temp1_1+"*"+tempRes+";//multiplicando por el n "+intToCadena(k));
                               c3d->addCodigo(temp3_1+"="+temp2_1+"+"+temp1_1+";");
                               tempRes= c3d->getTemporal();
                               c3d->addCodigo(tempRes+"="+temp3_1+"-0;//i real de la columna "+intToCadena(k));

                           }
                       }*/

                       if(!sonIguales(tempRes,"")){
                           c3d->addCodigo(tempRes+"="+tempRes+"+0; //size del arreglo "+ nombreArreglo);
                           c3d->addCodigo("heap[h]="+tempRes+";");
                           c3d->addCodigo("h=h+1;");
                           c3d->addCodigo("h=h+"+tempRes+";");

                       }

                   }else{
                       erroresEjecucion->insertarError(SEMANTICO,"Ha ocurrido un error al resolver dimensiones para arreglo local "+ nombreArreglo,dimensiones.at(0)->fila, dimensiones.at(0)->columna);
                   }


               }else{
                   erroresEjecucion->insertarError(SEMANTICO,"El arreglo local "+ nombreArreglo+" no existe",dimensiones.at(0)->fila, dimensiones.at(0)->columna);

               }




           }
       }else{
erroresEjecucion->insertarError(SEMANTICO,"La variable "+ nombreArreglo+", no es un arreglo",dimensiones.at(0)->fila, dimensiones.at(0)->columna);
       }

   }else{
erroresEjecucion->insertarError(SEMANTICO,"No existe el arreglo "+ nombreArreglo+" "+ ambiente->getAmbitos(), dimensiones.at(0)->fila, dimensiones.at(0)->columna);
   }



}


QList<string>GeneradorCodigo::calcularArregloNs(QList<nodoArbol *> posiciones, Ambito *ambiente, string clase, string funcion)
{
    nodoArbol* posTemporal;
    QList<string>retornoTamnio;
    Valor *elemento;

    for(int i =0; i< posiciones.length(); i++){
        posTemporal = posiciones.at(i);
        elemento = this->resolverExpresion(posTemporal, clase, funcion);
        if(esInt(elemento->tipo)){
            retornoTamnio.push_back(elemento->valor);
        }else{
            erroresEjecucion->insertarError(SEMANTICO, "El tipo "+ elemento->tipo+", no es valido para el tamanio de un arreglo, este debe ser entero", posTemporal->fila,posTemporal->columna);
        }
    }
    return retornoTamnio;
}

void GeneradorCodigo::resolverMientras(nodoArbol *sentencia, string clase, string funcion){
    nodoArbol *expresion= sentencia->hijos.at(0);
    QList<nodoArbol*> instrucciones = sentencia->hijos.at(1)->hijos;
    string lCiclo = c3d->getEtiqueta();
    string etiqBreak = c3d->getEtiqueta();
    string etiqContinue = c3d->getEtiqueta();
    this->etiquetasBreak->insertarEtiqueta(etiqBreak);
    this->etiquetasContinuar->insertarEtiqueta(etiqContinue);
    c3d->addCodigo("// =================================== Resolviendo un ciclo mientras =============================");
    c3d->addCodigo(lCiclo+"://etiqueta del ciclo mientras");
    c3d->addCodigo(etiqContinue+": //etiquetal del continue");
    Valor *valExp = this->resolverExpresion(expresion,clase, funcion);
    valExp = this->convertirACondicion(valExp);
    agregarCodigo(expresion->fila, c3d->codigo,erroresEjecucion->imprimirErrores());

    if(esCondicion(valExp->tipo)){
        this->ambiente->addMientras();
        c3d->addCodigo(valExp->cond->codigo);
        c3d->addCodigo(valExp->cond->getEtiquetasVerdaderas());
        for(int i=0; i< instrucciones.length(); i++){
            this->analizarSentencia(instrucciones.at(i),clase, funcion);
        }
        c3d->addCodigo("goto "+ lCiclo+";");
        c3d->addCodigo(valExp->cond->getEtiquetasFalsas());
        c3d->addCodigo(etiqBreak+": //etiqueta del break ");

        this->ambiente->salirAmbito();
        this->etiquetasBreak->eliminarEtiqueta();
        this->etiquetasContinuar->eliminarEtiqueta();

    }else{
       erroresEjecucion->insertarError(SEMANTICO,"Ha ocurrido un error al resolver la expresion de mientras", expresion->fila, expresion->columna);
    }








}



void GeneradorCodigo::resolverHacerMientras(nodoArbol *sentencia, string clase, string funcion){

    nodoArbol *expresion= sentencia->hijos.at(0);
    cout<<expresion->etiqueta<<endl;
    QList<nodoArbol*> instrucciones = sentencia->hijos.at(1)->hijos;

    c3d->addCodigo("//------- Resolviendo un hacer mientras =------------");
    string etiqCiclo = this->c3d->getEtiqueta();
    string etiqBreak = this->c3d->getEtiqueta();
    string etiqContinue = this->c3d->getEtiqueta();
    etiquetasBreak->insertarEtiqueta(etiqBreak);
    etiquetasContinuar->insertarEtiqueta(etiqContinue);
    c3d->addCodigo(etiqCiclo+":");
    this->agregarCodigo(sentencia->fila,c3d->codigo, erroresEjecucion->imprimirErrores());
    ambiente->addHacer();
    nodoArbol *temp;
    for(int i=0; i< instrucciones.length();i++){
        temp= instrucciones.at(i);
        cout<<temp->etiqueta<<endl;
        this->analizarSentencia(temp,clase, funcion);
       // this->agregarCodigo(temp->fila, c3d->codigo);
    }
    this->c3d->addCodigo(etiqContinue+": //etiqueta conitunue del ciclo");
    Valor *retExpresion= this->resolverExpresion(expresion, clase, funcion);
    retExpresion= this->convertirACondicion(retExpresion);
    if(esCondicion(retExpresion->tipo)){

        this->c3d->addCodigo(retExpresion->cond->codigo);
        this->c3d->addCodigo(retExpresion->cond->getEtiquetasVerdaderas());
        this->c3d->addCodigo("goto "+etiqCiclo+";");
        this->c3d->addCodigo(retExpresion->cond->getEtiquetasFalsas());
        this->c3d->addCodigo(etiqBreak+":");
        this->agregarCodigo(expresion->fila,c3d->codigo, erroresEjecucion->imprimirErrores());
        ambiente->salirAmbito();

    }else{
        erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al resolver la expresion para repetir mientras es de tipo "+ retExpresion->tipo, expresion->fila, expresion->columna);
        this->ambiente->salirAmbito();
        etiquetasBreak->eliminarEtiqueta();
                etiquetasContinuar->eliminarEtiqueta();
    }

}


void GeneradorCodigo::resolverInstanciaAsignacion(nodoArbol *sent, string clase, string metodo, int modo){
    string nombreVar= sent->hijos.at(0)->valor;
    string tipoVar = tabla->obtenerTipo(nombreVar, ambiente, modo);
    string igual = sent->hijos.at(1)->valor;
    nodoArbol *expresion = sent->hijos.at(2);

    string nombreClaseInstanciar = expresion->valor;
    QList<nodoArbol*> parametrosInstancia = expresion->hijos.at(0)->hijos;
    int noParametros = parametrosInstancia.length();

    string firmaMetodo = tabla->obtenerFirmaMetodo(nombreClaseInstanciar,noParametros, nombreClaseInstanciar);
    int sizeFuncActual = tabla->sizeFuncion(clase, metodo);

    if(sonIguales(tipoVar, nombreClaseInstanciar) &&
        !sonIguales("",firmaMetodo)){
        if(modo==1){
                int posVar = tabla->obtenerPosGlobal(nombreVar,ambiente);
                if(posVar!=-1){
                    int sizeC= tabla->sizeClase(tipoVar);
                    string tClase = intToCadena(sizeC);
                    string t1,t2,t3,t4;
                    string posC = intToCadena(posVar);
                    t1= c3d->getTemporal();
                    t2= c3d->getTemporal();
                    t3= c3d->getTemporal();
                    t4= c3d->getTemporal();
                    c3d->addCodigo(t1+"=p+0;// pos this de "+nombreVar);
                    c3d->addCodigo(t2+"=stack["+t1+"];//apuntador del heap de "+nombreVar);
                    c3d->addCodigo(t3+"=heap["+t2+"]; //posicion real del heapo donde inicia "+nombreVar);
                    c3d->addCodigo(t4+"="+t3+"+"+posC+"; // pos real del atributo "+nombreVar);
                    c3d->addCodigo("heap["+t4+"]=h; //guardando pa pos real donde inicia el objeto "+nombreVar);
                    c3d->addCodigo("h=h+"+tClase+";//reservando el espacio de memoria para el nuevo objeto "+nombreVar);
                    this->resolverInstancia(parametrosInstancia,clase,metodo,posC,intToCadena(sizeFuncActual),modo,firmaMetodo,nombreClaseInstanciar,sent);
                }else{
                    erroresEjecucion->insertarError(SEMANTICO, "No se puede instanciar el atributo "+nombreVar+", no existe", sent->hijos.at(0)->fila, sent->hijos.at(0)->columna);
                }
                
            }else if(modo ==2){
                int posVar= tabla->obtenerPosLocal(nombreVar, ambiente);
                if(posVar!= -1){
                    c3d->addCodigo("//----------- Realizando un instancia a una variable local ----------");
                    string t1,t2;
                    string posC = intToCadena(posVar);
                    int sizeC= tabla->sizeClase(tipoVar);
                    string tClase = intToCadena(sizeC);    
                    t1= c3d->getTemporal();
                    t2= c3d->getTemporal();
                    c3d->addCodigo(t1+" = p +"+posC+"; //pos de "+nombreVar);
                    c3d->addCodigo("stack["+t1+"]= h;");
                    c3d->addCodigo(t2+" = h+1;");
                    c3d->addCodigo("heap[h]="+t2+";");
                    c3d->addCodigo("h=h+1;");
                    c3d->addCodigo("h = h + "+tClase+"; //reservando espacio de memoria");
                    string sFun = intToCadena(sizeFuncActual);
                   this->resolverInstancia(parametrosInstancia,clase,metodo,posC,sFun,modo,firmaMetodo, nombreClaseInstanciar,sent);
                }else{
                    erroresEjecucion->insertarError(SEMANTICO, "No se puede instanciar la objeto local "+nombreVar+", no existe", sent->hijos.at(0)->fila, sent->hijos.at(0)->columna);
                }
                
            }else{
                erroresEjecucion->insertarError(SEMANTICO, "No se puede realizar instancia de  "+nombreVar+", no existe", sent->hijos.at(0)->fila, sent->hijos.at(0)->columna);
            }

    }else{
        erroresEjecucion->insertarError(SEMANTICO, "No se puede realizar instancia de  "+nombreVar+", no existe, no coinciden los tipos  "+nombreClaseInstanciar+" y "+firmaMetodo, sent->hijos.at(0)->fila, sent->hijos.at(0)->columna);
    }

}



void GeneradorCodigo::operarRetorno(nodoArbol *sentencia, string clase, string metodo)
{
    nodoArbol *exp = sentencia->hijos.at(0);
    Valor *v= this->resolverExpresion(exp,clase,metodo);
    v= this->condiABool(v);
    string tipoFun = tabla->obtenerTipoFuncion(metodo);
    if(!sonIguales(tipoFun,"")){
        if(sonIguales(tipoFun, v->tipo) || (sonIguales(tipoFun, T_CARACTER) && esCadena(v->tipo))){
            int posRet= tabla->obtenerPosLocal("RETORNO",ambiente);
            if(posRet!=-1){
                string t1=c3d->getTemporal();
                c3d->addCodigo(t1+"= p+"+intToCadena(posRet)+";// pos del retorno del metodo "+metodo);
                c3d->addCodigo("stack["+t1+"]="+v->valor+";");
                c3d->addCodigo("goto "+this->etiquetasRetorno->obtenerActual()+";");
            }
        }else if(esNulo(v->tipo)){

            int posRet= tabla->obtenerPosLocal("RETORNO", ambiente);
            string t1= c3d->getTemporal();
            c3d->addCodigo(t1+"= p+"+intToCadena(posRet)+";// pos del retorno del metodo "+metodo);
            c3d->addCodigo("stack["+t1+"]="+v->valor+";");
            c3d->addCodigo("goto "+this->etiquetasRetorno->obtenerActual()+";");
        }else{
            erroresEjecucion->insertarError(SEMANTICO, "Incompatibilidad de tipos entre la funcion y el retorno "+tipoFun+" con "+v->tipo+" en la fucnion "+ metodo,exp->fila, exp->columna);
        }
    }else{
        erroresEjecucion->insertarError("Semantico", "No se ha encontrado la funcion con el nombre de "+ metodo, sentencia->fila, sentencia->columna);
    }
}




void GeneradorCodigo::concatenar(nodoArbol *sentencia, string clase, string metodo){

    string nombreVar = sentencia->hijos.at(0)->valor;
    string tipoConcatenar= sentencia->valor;
    nodoArbol *exp1= sentencia->hijos.at(1);
    Valor *resolverExp1= this->resolverExpresion(exp1,clase, metodo);
    resolverExp1= this->condiABool(resolverExp1);
    Valor *cadArreglo = this->convertirArregloCadena(nombreVar, ambiente, clase, metodo);
    int a = tabla->esAtributo(nombreVar, this->ambiente);
    Simbolo *simb = this->tabla->obtenerSimbolo(nombreVar, this->ambiente,a);
    if(simb!= NULL && esCadena(cadArreglo->tipo)){

    if(sonIguales(tipoConcatenar, "1")){

        if(esCadena(cadArreglo->tipo)){
            if(esCadena(resolverExp1->tipo)){
                Valor *va2 = concatenarCadena(cadArreglo,resolverExp1);
                int at= this->tabla->esAtributo(nombreVar, ambiente);
                Simbolo *s= tabla->obtenerSimbolo(nombreVar,ambiente,at);
                if(s!=NULL){
                    this->asignarArreglo(s,va2,ambiente, clase, metodo,true,"");
                }

            }

        }else{
            erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error, "+ nombreVar+", no es un arreglo de caracteres",sentencia->hijos.at(0)->fila, sentencia->hijos.at(0)->columna);
        }

    }else{
        nodoArbol *exp2= sentencia->hijos.at(2);
        Valor *resolverExp2 = this->resolverExpresion(exp2, clase, metodo);
        resolverExp2= this->condiABool(resolverExp2);
        if(!esNulo(resolverExp2->tipo))
        {
            QString comodin="";
            bool bandera = false;

            if (esInt(resolverExp2->tipo)){
                comodin = "#E";
                bandera= true;
            }else if(esDecimal(resolverExp2->tipo)){
                comodin = "#D";
                bandera= true;
            }else if(esBool(resolverExp2->tipo)){
                comodin = "#B";
                bandera= true;
            }else{
                erroresEjecucion->insertarError("Semantico", "El tipo del tercer parametros debe de ser entero, decimal o booleano, no de tipo "+ resolverExp2->tipo,sentencia->hijos.at(2)->fila, sentencia->hijos.at(2)->columna);
            }
            //instrucciones para concatenar
            if(bandera){
                if(esCadena(exp1->etiqueta)){
                    string cad = exp1->valor;
                    QString  valorCadena = QString::fromStdString(cad);
                    int posComodin= valorCadena.indexOf(comodin,0);
                    if(posComodin!= -1){
                        QString parte1 = valorCadena.left(posComodin);
                        QString parte2 = valorCadena.right(valorCadena.length()-posComodin-2);
                        string ca1 = parte1.toUtf8().constData();
                        string ca2 = parte2.toUtf8().constData();
                        nodoArbol *c1 = new nodoArbol(T_CADENA, ca1);
                        c1->fila= exp1->fila;
                        c1->columna= exp1->columna;

                        nodoArbol *c2 = new nodoArbol(T_CADENA, ca2);
                        c2->fila= exp1->fila;
                        c2->columna= exp1->columna;


                        Valor *val1 = this->resolverExpresion(c1, clase, metodo);
                        Valor *val2 = this->resolverExpresion(c2, clase, metodo);

                        if(esCadena(val1->tipo)){
                            if(esCadena(val2->tipo)){
                                nodoArbol *valor2 = new nodoArbol("CONVERTIR_A_CADENA","");
                                valor2->addHijo(exp2);
                                valor2->fila= exp2->fila;
                                valor2->columna= exp2->columna;

                                Valor *retExp2= this->resolverExpresion(valor2,clase, metodo);

                                if(esCadena(retExp2->tipo)){
                                    Valor *x1 = this->concatenarCadena(val1,retExp2);
                                    if(esCadena(x1->tipo)){
                                        Valor *final = concatenarCadena(x1,val2);
                                        if(esCadena(final->tipo)){
                                            Valor *cadAsignar = concatenarCadena(cadArreglo,final);
                                            this->asignarArreglo(simb,cadAsignar,ambiente,clase, metodo,true,"");
                                        }else{
                                            erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al concantenar con la variable "+ nombreVar,sentencia->fila, sentencia->columna);
                                        }
                                    }else{
                                       erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al concantenar con la variable "+ nombreVar,sentencia->fila, sentencia->columna);
                                    }
                                }else{
                                    erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al concantenar con la variable "+ nombreVar,sentencia->fila, sentencia->columna);

                                }

                            }else{
                                erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al concantenar con la variable "+ nombreVar,sentencia->fila, sentencia->columna);
                            }
                        }else{
                            erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al concantenar con la variable "+ nombreVar,sentencia->fila, sentencia->columna);

                        }
                    }else{
                        erroresEjecucion->insertarError(SEMANTICO, "No se encuentra el comodin  en la expresion a asignar",sentencia->fila, sentencia->columna);
                    }
                }else{
                   erroresEjecucion->insertarError(SEMANTICO, "La expresion a concatenar en "+ nombreVar+" debe de ser una cadena",sentencia->fila, sentencia->columna);
                }

            }else{
                erroresEjecucion->insertarError(SEMANTICO, "Incompatibilidad de tipos para concatenar en "+ nombreVar+", estos deben de ser entero, decimal y boolenano. e ingreso  "+ resolverExp2->tipo,sentencia->fila, sentencia->columna);
            }


        }else{
           erroresEjecucion->insertarError("Semantico", "Ha ocurrido un error al resolver expresion a incrustar dentro de la cadena", exp2->fila, exp2->columna);
        }


    }

}else{

        erroresEjecucion->insertarError(SEMANTICO, "No existe  "+ nombreVar+", no se puede realizar concatenacion ",sentencia->fila, sentencia->columna);

    }
}








Valor* GeneradorCodigo::llamadaFuncion(nodoArbol *sent, string clase, string metodo, bool modo, string posFinal, string clase2, string metodo2){

    string nombreFunc= sent->valor;
    QList<nodoArbol*> parametrosFunc = sent->hijos;
    int noParametros = parametrosFunc.length();

    int sizeFunActual = -1;
        string firmaMetodo = "";
        string c ="";
        string tipoFunc = "";

   if(modo){
       sizeFunActual = tabla->sizeFuncion(clase, metodo);
          firmaMetodo = tabla->obtenerFirmaMetodo(clase, noParametros,nombreFunc);
            c = "tamanioFuncion: "+ intToCadena(sizeFunActual)+"Buscado con: clase: "+clase +"  Metodo:"+ metodo+ " Firma: "+firmaMetodo+" buscado con:  nombrefunc: "+nombreFunc;
           tipoFunc = tabla->obtenerTipoFuncion(firmaMetodo);


   }else{
       //es un acceso 
        sizeFunActual = tabla->sizeFuncion(clase, metodo);
       firmaMetodo = tabla->obtenerFirmaMetodo(clase2, noParametros,nombreFunc);
         c = "tamanioFuncion: "+ intToCadena(sizeFunActual)+"Buscado con: clase: "+clase2 +"  Metodo:"+ metodo2+ " Firma: "+firmaMetodo+" buscado con:  nombrefunc: "+nombreFunc;
        tipoFunc = tabla->obtenerTipoFuncion(firmaMetodo);



   }

    

    if((sizeFunActual!= -1) && !sonIguales(firmaMetodo,"")){
        c3d->addCodigo("//Resolviendo parametros de llamada");
        QList<Valor*>valores;
        nodoArbol *expresionTemporal;

        for(int g =0; g<parametrosFunc.length();g++){
            expresionTemporal= parametrosFunc.at(g);
            Valor *v= this->resolverExpresion(expresionTemporal, clase,metodo);
            v= this->condiABool(v);
            valores.push_back(v);
        }


string fAct = intToCadena(sizeFunActual);
       if(modo){
        string t1,t2,t3,t4;
        t1=c3d->getTemporal();
        t2= c3d->getTemporal();
        t3= c3d->getTemporal();
        t4= c3d->getTemporal();
        c3d->addCodigo(t1+"= p+0;");
        c3d->addCodigo(t2+"=stack["+t1+"];");
        c3d->addCodigo(t3+"=p+"+fAct+"; // size de la funcion "+firmaMetodo);
        c3d->addCodigo(t4+"="+t2+"+0;");
        c3d->addCodigo("stack["+t3+"]="+t2+";");

       } else{
          //aqui va cuano es una acceso
           string temp1_1= c3d->getTemporal();
           string temp1_2= c3d->getTemporal();
           string l1_1= temp1_1+"=p+"+intToCadena(sizeFunActual)+";";
           string l1_2= temp1_2+"="+temp1_1+"+0;";
           string l1_3 = "stack["+temp1_2+"]="+posFinal+";";
           c3d->addCodigo(l1_1);
           c3d->addCodigo(l1_2);
           c3d->addCodigo(l1_3);

       }


    string claseTemporal = clase;
    string metodoTemporal = metodo;
    if(!modo){
        claseTemporal = clase2;
        metodoTemporal= metodo;
    }



        //parametros
        if(parametrosFunc.length()>0){


            c3d->addCodigo("//Asgnando parametros de la llamada a funcion");

            Simbolo *simb;
            int cont=1;
          //  //cout<<"parametros "<<parametrosFunc.length()<<endl;
           if(valores.length() == parametrosFunc.length()){
               for(int j=0;j<parametrosFunc.length();j++){
                   simb=tabla->obtenerNombreParametro(claseTemporal+"_"+firmaMetodo,cont);
                   Valor *v= valores.at(j);
                   if(simb!= NULL){
                       string t1_1=c3d->getTemporal();
                       c3d->addCodigo(t1_1+"=p+"+fAct+";");
                       string t2_1= c3d->getTemporal();
                       c3d->addCodigo(t2_1+"="+t1_1+"+"+intToCadena(cont)+";//pos del parametro "+intToCadena(cont));
                       cont++;

                       if(sonIguales(simb->tipoSimbolo, "ARREGLO") && simb->declaAtributo!= NULL){
                           c3d->addCodigo("// ------- DECLARANDO PAREAMETRO DE ARREGLO ");
                            this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,claseTemporal, metodoTemporal, true, t2_1,claseTemporal+"_"+metodoTemporal,simb);
                           // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, metodo,true,);
                       }


                       if(esCaracter(v->tipo) && sonIguales(v->tipoSimbolo, "ARREGLO")){

                           c3d->addCodigo("stack["+t2_1+"]="+v->referencia+"; //referencia del arreglo asignado al stack el parametro");
                           // c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                       }else{

                           if(esArregloCaracter(simb) && esCadena(v->tipo) ){
                               c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                               c3d->addCodigo("// aquiiii "+v->tipo+"  "+v->valor+" "+claseTemporal+" "+metodoTemporal);
                                this->asignarArreglo(simb,v,this->ambiente,claseTemporal,metodoTemporal,false,t2_1);
                               c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");

                           }else{
                               c3d->addCodigo("stack["+t2_1+"]="+v->valor+"; //asignando al stack el parametro");
                           }

                       }                     

                   }else{
                       erroresEjecucion->insertarError(SEMANTICO, "No existe el parametro de la posicion  "+ intToCadena(cont) +" en la llamada a "+firmaMetodo, sent->fila, sent->columna);
                   }

               }
           }else{
               erroresEjecucion->insertarError(SEMANTICO, "No coincide el numero de parametros con la llamada a  "+ firmaMetodo, sent->fila, sent->columna);
           }
        }else{
            c3d->addCodigo("// llamada no posee parametros");
        }



        //realizando llamada
        c3d->addCodigo("p=p+"+fAct+";");
        c3d->addCodigo("call "+firmaMetodo+"();");
        int sizeFirma= tabla->sizeFuncion(claseTemporal,firmaMetodo)-1;
        string fFirma = intToCadena(sizeFirma);
        string t5,t6;
        t5= c3d->getTemporal();
        t6= c3d->getTemporal();
        c3d->addCodigo(t5+"=p+"+fFirma+";");
        c3d->addCodigo(t6+"=stack["+t5+"];");
        c3d->addCodigo("p=p-"+fAct+";");

        Valor *v= new Valor();
        v->tipo = tipoFunc;
        v->valor=t6;
        v->setReferencia("stack",t5);
        return v;



    }else{
        erroresEjecucion->insertarError(SEMANTICO, c+"   NO SE PUEede realizar la llamada firmametodo es "+ firmaMetodo+" y el size acutal es "+ intToCadena(sizeFunActual) , sent->fila, sent->columna);

    }


     Valor *v= new Valor();
     return v;

}



void GeneradorCodigo::resolverInstancia(QList<nodoArbol*>parametros, string clase, string funcion, string tPosVar, string sizeFun, int modo, string firmaMetodo, string nombreclaseInstaciar, nodoArbol *sent){

    c3d->addCodigo("//Resolviendo parametros de llamada");
            QList<Valor*>valores;
            nodoArbol *expresionTemporal;
            for(int g =0; g<parametros.length();g++){
                expresionTemporal= parametros.at(g);
                Valor *v= this->resolverExpresion(expresionTemporal, clase,funcion);
                v= this->condiABool(v);
                valores.push_back(v);
            }
    if(modo==1){
        string t5,t6,t7,t8,t9,t10;
         t5= c3d->getTemporal();
         t6= c3d->getTemporal();
         t7= c3d->getTemporal();
        t8= c3d->getTemporal();
        t9= c3d->getTemporal();
        t10= c3d->getTemporal();
        c3d->addCodigo("//----------------- INSTANCIA GLOBAL ---------------");
        c3d->addCodigo("//Guardando la referencia al this del objeto para la llamada al constructor ");
        c3d->addCodigo(t5+"=p+0;");
        c3d->addCodigo(t6+"=stack["+t5+"];//apuntador al heal de ");
        c3d->addCodigo(t7+"=heap["+t6+"];//posicion real donde incia el objeto ");
        c3d->addCodigo(t8+"="+t7+"+"+tPosVar+";//pos real donde incia el objeto ");
        c3d->addCodigo(t9+"=p+"+sizeFun+";//tamanio de la funcion actual "+funcion);
        c3d->addCodigo(t10+"="+t9+"+0;//pos this para la nuesva instancia de ");
        c3d->addCodigo("stack["+t10+"]="+t8+";//guaradno el puntero del this en el stack");



        //parametros
        if(parametros.length()>0){


            c3d->addCodigo("//Asgnando parametros de la llamada a funcion");

            Simbolo *simb;
            int cont=1;
          //  //cout<<"parametros "<<parametrosFunc.length()<<endl;
           if(valores.length() == parametros.length()){
               for(int j=0;j<parametros.length();j++){
                   simb=tabla->obtenerNombreParametro(nombreclaseInstaciar+"_"+firmaMetodo,cont);
                   Valor *v= valores.at(j);
                   if(simb!= NULL){
                       string t1_1=c3d->getTemporal();
                       c3d->addCodigo(t1_1+"=p+"+sizeFun+";");
                       string t2_1= c3d->getTemporal();
                       c3d->addCodigo(t2_1+"="+t1_1+"+"+intToCadena(cont)+";//pos del parametro "+intToCadena(cont));
                       cont++;

                       if(sonIguales(simb->tipoSimbolo, "ARREGLO") && simb->declaAtributo!= NULL){
                           c3d->addCodigo("// ------- DECLARANDO PAREAMETRO DE ARREGLO ");
                          // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,claseTemporal, metodoTemporal, true, t2_1,claseTemporal+"_"+metodoTemporal,simb);
                            this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, funcion, true, t2_1,clase+"_"+funcion,simb);
                           // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, metodo,true,);
                       }


                       if(esCaracter(v->tipo) && sonIguales(v->tipoSimbolo, "ARREGLO")){

                           c3d->addCodigo("stack["+t2_1+"]="+v->referencia+"; //referencia del arreglo asignado al stack el parametro");
                           // c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                       }else{

                           if(esArregloCaracter(simb) && esCadena(v->tipo) ){
                               c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                               c3d->addCodigo("// aquiiii "+v->tipo+"  "+v->valor+" "+clase+" "+funcion+" "+firmaMetodo);
                                this->asignarArreglo(simb,v,this->ambiente,clase,funcion,false,t2_1);
                               c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");

                           }else{
                               c3d->addCodigo("stack["+t2_1+"]="+v->valor+"; //asignando al stack el parametro");
                           }

                       }

                   }else{
                       erroresEjecucion->insertarError(SEMANTICO, "No existe el parametro de la posicion  "+ intToCadena(cont) +" en la llamada a "+firmaMetodo, sent->fila, sent->columna);
                   }

               }
           }else{
               erroresEjecucion->insertarError(SEMANTICO, "No coincide el numero de parametros con la llamada a  "+ firmaMetodo, sent->fila, sent->columna);
           }
        }else{
            c3d->addCodigo("// llamada no posee parametros");
        }





        c3d->addCodigo("p=p+"+sizeFun+";");
        c3d->addCodigo("call "+firmaMetodo+"();");
        c3d->addCodigo("p=p-"+sizeFun+";");

                    


    }else if(modo ==2){
        string t3,t4,t5,t6;
        t3= c3d->getTemporal();
        t4= c3d->getTemporal();
        t5= c3d->getTemporal();
        t6= c3d->getTemporal();
        c3d->addCodigo("//-------------- INSTANCIA LOCAL -------------------------");
        c3d->addCodigo(t3+"= p+ "+tPosVar+";");
        c3d->addCodigo(t4+"=stack["+t3+"];");
        c3d->addCodigo(t5+" = p + "+sizeFun+";");
        c3d->addCodigo(t6+"= "+t5+"+0;");
        c3d->addCodigo("stack["+t6+"]="+t4+";");




        //parametros
        if(parametros.length()>0){


            c3d->addCodigo("//Asgnando parametros de la llamada a funcion");

            Simbolo *simb;
            int cont=1;
          //  //cout<<"parametros "<<parametrosFunc.length()<<endl;
           if(valores.length() == parametros.length()){
               for(int j=0;j<parametros.length();j++){
                   simb=tabla->obtenerNombreParametro(nombreclaseInstaciar+"_"+firmaMetodo,cont);
                   Valor *v= valores.at(j);
                   if(simb!= NULL){
                       string t1_1=c3d->getTemporal();
                       c3d->addCodigo(t1_1+"=p+"+sizeFun+";");
                       string t2_1= c3d->getTemporal();
                       c3d->addCodigo(t2_1+"="+t1_1+"+"+intToCadena(cont)+";//pos del parametro "+intToCadena(cont));
                       cont++;

                       if(sonIguales(simb->tipoSimbolo, "ARREGLO") && simb->declaAtributo!= NULL){
                           c3d->addCodigo("// ------- DECLARANDO PAREAMETRO DE ARREGLO ");
                          // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,claseTemporal, metodoTemporal, true, t2_1,claseTemporal+"_"+metodoTemporal,simb);
                            this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, funcion, true, t2_1,clase+"_"+funcion,simb);
                           // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, metodo,true,);
                       }


                       if(esCaracter(v->tipo) && sonIguales(v->tipoSimbolo, "ARREGLO")){

                           c3d->addCodigo("stack["+t2_1+"]="+v->referencia+"; //referencia del arreglo asignado al stack el parametro");
                           // c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                       }else{

                           if(esArregloCaracter(simb) && esCadena(v->tipo) ){
                               c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                               c3d->addCodigo("// aquiiii "+v->tipo+"  "+v->valor+" "+clase+" "+funcion+" "+firmaMetodo);
                                this->asignarArreglo(simb,v,this->ambiente,clase,funcion,false,t2_1);
                               c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");

                           }else{
                               c3d->addCodigo("stack["+t2_1+"]="+v->valor+"; //asignando al stack el parametro");
                           }

                       }

                   }else{
                       erroresEjecucion->insertarError(SEMANTICO, "No existe el parametro de la posicion  "+ intToCadena(cont) +" en la llamada a "+firmaMetodo, sent->fila, sent->columna);
                   }

               }
           }else{
               erroresEjecucion->insertarError(SEMANTICO, "No coincide el numero de parametros con la llamada a  "+ firmaMetodo, sent->fila, sent->columna);
           }
        }else{
            c3d->addCodigo("// llamada no posee parametros");
        }





        c3d->addCodigo("p=p+"+sizeFun+";");
        c3d->addCodigo("call "+firmaMetodo+"();");
        c3d->addCodigo("p=p-"+sizeFun+";");

    }


}


void GeneradorCodigo::limpiarArrego(Simbolo *simb, bool modo, string posT){


    //int esAtributo = tabla->esAtributo(nombreArreglo,ambiente);
    bool esAtributo = sonIguales(simb->rol,"ATRIBUTO");
    //if(esAtributo==1 || esAtributo==2){
        string tempSize = c3d->getTemporal();
        string tempPos0= this->c3d->getTemporal();
         int u=-1;
        if(esAtributo){
            u=1;
            int posA = simb->apuntador;//tabla->obtenerPosGlobal(nombreArreglo, ambiente);
            string posArreglo = intToCadena(posA);
            if(posA!=-1){
            string t1= c3d->getTemporal();
            string t2= c3d->getTemporal();
            string t3= c3d->getTemporal();
            string t4= c3d->getTemporal();
            string t5= c3d->getTemporal();
            c3d->addCodigo(t1+"= p+0;//pos this del objeto");
            c3d->addCodigo(t2+"=stack["+t1+"];//apuntador al heap del objeto");
            c3d->addCodigo(t3+"=heap["+t2+"];//apunt al heap donde inicia el objeto ");
            c3d->addCodigo(t4+"="+t3+"+"+posArreglo+";//apuntador a posicion donde inicia el arreglo");
            c3d->addCodigo(t5+"=heap["+t4+"];//inicia el arreglo");
            c3d->addCodigo(tempSize+"=heap["+t5+"]; //tempSIZE ATRIBUTO ");
            c3d->addCodigo(tempPos0+"="+t5+"+1;//pos 0 del arreglo");
            }else{
                erroresEjecucion->insertarError2(SEMANTICO,"No existe el arreglo atributo "+ simb->nombrecorto);
            }

        }else {
            string posArreglo ="";
            if(!modo){
                posArreglo= posT;
            }else{
                int posA= simb->apuntador;//tabla->obtenerPosLocal(nombreArreglo,ambiente);
                posArreglo = intToCadena(posA);
            }

            if(!sonIguales(posArreglo,"")){
                u=1;
                string t1= c3d->getTemporal();
                string t2= c3d->getTemporal();
                string t3= c3d->getTemporal();
                c3d->addCodigo(t1+"=p+"+posArreglo+";//pos del arreglo");
                 c3d->addCodigo(t2+"=stack["+t1+"];//apuntador al heal del arreglo");
                 c3d->addCodigo(t3+"=heap["+t2+"];//apuntador del heap donde inicia la cadena");
                 c3d->addCodigo(tempSize+"=heap["+t3+"];// apuntadodor  al size del arreglo "+ simb->nombrecorto);
                // c3d->addCodigo(tempSize+"=heap["+tempSize+"];//  size del arreglo "+ simb->nombrecorto);
                 c3d->addCodigo(tempPos0+"="+t3+"+1;//pos 0 donde inciar el arreglo "+simb->nombrecorto);



            }else{
                erroresEjecucion->insertarError2(SEMANTICO, "No existe el arreglo local "+ simb->nombrecorto);
            }


        }


                            if(u==1){

                           string tCont = c3d->getTemporal();
                           c3d->addCodigo(tCont+"=0; //contador de las posiciones del arreglo");

                                string etiq1V= c3d->getEtiqueta();
                                string etiq1F= c3d->getEtiqueta();
                                string l1="if("+tCont+"<"+tempSize+")goto "+etiq1V+";\n goto "+etiq1F+";";
                                string l2 = etiq1V+":";
                                string l4 = "heap["+tempPos0+"]=0;";
                                string l5= tempPos0+"="+tempPos0+"+1;";
                                string l6= tCont+"="+tCont+"+1;";
                                string l7= "goto "+etiq1V+";";
                                string l8= etiq1F+": ";
                                  c3d->addCodigo(l1);
                                  c3d->addCodigo(l2);
                                  c3d->addCodigo(l4);
                                  c3d->addCodigo(l5);
                                  c3d->addCodigo(l6);
                                  c3d->addCodigo(l7);
                                  c3d->addCodigo(l8);
                            }


}


void GeneradorCodigo::asignarArreglo(Simbolo* simb , Valor *expreisonArreglo, Ambito *ambiente, string clase, string metodo, bool modo, string posT){

    //this->limpiarArrego(simb,modo, posT);
    //int esAtributo = tabla->esAtributo(nombreArreglo,ambiente);
    bool esAtributo = sonIguales(simb->rol,"ATRIBUTO");
    //if(esAtributo==1 || esAtributo==2){
        string tempSize = c3d->getTemporal();
        string tempPos0= this->c3d->getTemporal();

        if(esAtributo){
            int posA = simb->apuntador;//tabla->obtenerPosGlobal(nombreArreglo, ambiente);
            string posArreglo = intToCadena(posA);
            if(posA!=-1){
            string t1= c3d->getTemporal();
            string t2= c3d->getTemporal();
            string t3= c3d->getTemporal();
            string t4= c3d->getTemporal();
            string t5= c3d->getTemporal();
            c3d->addCodigo(t1+"= p+0;//pos this del objeto");
            c3d->addCodigo(t2+"=stack["+t1+"];//apuntador al heap del objeto");
            c3d->addCodigo(t3+"=heap["+t2+"];//apunt al heap donde inicia el objeto ");
            c3d->addCodigo(t4+"="+t3+"+"+posArreglo+";//apuntador a posicion donde inicia el arreglo");
            c3d->addCodigo(t5+"=heap["+t4+"];//inicia el arreglo");
            c3d->addCodigo(tempSize+"=heap["+t5+"]; //tempSIZE ATRIBUTO ");
            c3d->addCodigo(tempPos0+"="+t5+"+1;//pos 0 del arreglo");
            }else{
                erroresEjecucion->insertarError2(SEMANTICO,"No existe el arreglo atributo "+ simb->nombrecorto);
            }

        }else {
            string posArreglo ="";
            if(!modo){
                posArreglo= posT;
            }else{
                int posA= simb->apuntador;//tabla->obtenerPosLocal(nombreArreglo,ambiente);
                posArreglo = intToCadena(posA);
            }

            if(!sonIguales(posArreglo,"")){
                string t1= c3d->getTemporal();
                string t2= c3d->getTemporal();
                string t3= c3d->getTemporal();
                if(modo){
                c3d->addCodigo(t1+"=p+"+posArreglo+";//pos del arreglo");
            }
                else{
                    c3d->addCodigo(t1+"=0+"+posArreglo+";//pos del arreglo");}

                 c3d->addCodigo(t2+"=stack["+t1+"];//apuntador al heal del arreglo");
                 c3d->addCodigo(t3+"=heap["+t2+"];//apuntador del heap donde inicia la cadena");
                 c3d->addCodigo(tempSize+"=heap["+t3+"];//  SIZEEEEEEEEEEEEEE  apuntadodor  al size del arreglo "+ simb->nombrecorto);
                // c3d->addCodigo(tempSize+"=heap["+tempSize+"];//  size del arreglo "+ simb->nombrecorto);
                 c3d->addCodigo(tempPos0+"="+t3+"+1;//pos 0 donde inciar el arreglo "+simb->nombrecorto);



            }else{
                erroresEjecucion->insertarError2(SEMANTICO, "No existe el arreglo local "+ simb->nombrecorto);
            }


        }


        //
        //var retExp = this.resolverExpresion(expresionArreglo,ambitos,clase,metodo);
                            if(esCadena(expreisonArreglo->tipo)){

                            string tempApuntCadena = expreisonArreglo->valor;
                                string temp1= c3d->getTemporal();

                                string tempSizeCadeaPool= c3d->getTemporal();
                                string tempPos0Cadena = c3d->getTemporal();
                                string tempoCaracterCadena= c3d->getTemporal();


                                string l1= temp1+"=heap["+tempApuntCadena+"];//pos del pool donde se reserva la cadena";
                                string l2= tempSizeCadeaPool+"=pool["+temp1+"]; //size de la cadena ";
                                string l3 = tempPos0Cadena+"="+temp1+"+1;//pos 0 donde inicia la cadena";
                                string l4= tempoCaracterCadena+"=pool["+tempPos0Cadena+"];//scaando el caracter del pool ";
                                c3d->addCodigo(l1);
                                c3d->addCodigo(l2);
                                c3d->addCodigo(l3);
                                c3d->addCodigo(l4);

                                string etiq1V= c3d->getEtiqueta();
                                string etiq1F= c3d->getEtiqueta();
                                string etiq2V= c3d->getEtiqueta();
                                string etiq2F = c3d->getEtiqueta();

                                string l5="if("+tempSizeCadeaPool+"<="+tempSize+")goto "+etiq1V+";\n goto "+etiq1F+";";
                                string l6 = etiq1V+":";
                                string l7= "if("+tempoCaracterCadena+"!=0)goto "+etiq2V+";\n goto "+etiq2F+";";
                                string l8= etiq2V+":";
                                string l9 = "heap["+tempPos0+"]=s;";
                                string l10 = "pool[s]="+tempoCaracterCadena+";";
                                string l11= "s=s+1;";
                                string l12= tempPos0+"="+tempPos0+"+1;";
                                string l13= tempPos0Cadena+"="+tempPos0Cadena+"+1;";
                                string l14= tempoCaracterCadena+"=pool["+tempPos0Cadena+"];";
                                string l15= "goto "+etiq1V+";";
                                string l16= etiq2F+": ";
                                string l17= etiq1F+": ";
                                  c3d->addCodigo(l5);
                                  c3d->addCodigo(l6);
                                  c3d->addCodigo(l7);
                                  c3d->addCodigo(l8);
                                  c3d->addCodigo(l9);
                                  c3d->addCodigo(l10);
                                  c3d->addCodigo(l11);
                                  c3d->addCodigo(l12);
                                  c3d->addCodigo(l13);
                                  c3d->addCodigo(l14);
                                  c3d->addCodigo(l15);
                                  c3d->addCodigo(l16);
                                  c3d->addCodigo(l17);


                               /* var l1_5="jle, "+tempSizeCadena+", "+tempSize+", "+etiq1V+";";
                                var l1_6= "jmp, , , "+ etiq1F+";";
                                var l1_18= "jmp, , , "+ etiq1V+";";
                                var l1_7= etiq1V+":";
                                var l1_8="jne, "+tempoCaracterCadena+", 36, "+etiq2V+";";
                                var l1_9="jmp, , , "+etiq2F+";";
                                var l1_19= "jmp, , , "+ etiq2V+";";
                                var l1_10=etiq2V+":";
                                var l1_11="<=, "+tempPos0+", "+tempoCaracterCadena+", heap; // guardando el caracter ";
                                var l1_12="+, "+tempPos0+", 1, "+tempPos0+"; // incremnetnado la pos del arreglo";
                                var l1_13="+, "+tempPos0Cadena+", 1, "+tempPos0Cadena+"; // incrementando la pos de la cadena";
                                var l1_14= "=>, "+tempPos0Cadena+", "+tempoCaracterCadena+", heap; // sacandor el caracter del heap cadena";
                                var l1_15= "jmp, , , "+ etiq1V+";";
                                var l1_20= "jmp, , , "+ etiq2F+";";
                                var l1_16=etiq2F+":";
                                //var l1_177 = "<= , "+tempPos0+", 36, heap; // extraaaaaaaaaaa";
                                var l1_21= "jmp, , , "+ etiq1F+";";
                                var l1_17 = etiq1F+":";
                                c3d->addCodigo(l1_5);
                                c3d->addCodigo(l1_6);
                                c3d->addCodigo(l1_18);
                                c3d->addCodigo(l1_7);
                                c3d->addCodigo(l1_8);
                                c3d->addCodigo(l1_9);
                                c3d->addCodigo(l1_19);
                                c3d->addCodigo(l1_10);
                                c3d->addCodigo(l1_11);
                                c3d->addCodigo(l1_12);
                                c3d->addCodigo(l1_13);
                                c3d->addCodigo(l1_14);
                                c3d->addCodigo(l1_15);
                                c3d->addCodigo(l1_20);
                                c3d->addCodigo(l1_16);
                                //c3d->addCodigo(l1_177); ////////////////////////////////////////////
                                c3d->addCodigo(l1_21);
                                c3d->addCodigo(l1_17);*/

                            }


  /*  }else{
        erroresEjecucion->insertarError(SEMANTICO, "El arreglo "+ nombreArreglo+", no existe");
    }*/
}



bool GeneradorCodigo::esArregloCaracter(Simbolo *simb){
    if(sonIguales(simb->tipoSimbolo,"ARREGLO") &&
            sonIguales(simb->tipoElemento, "CARACTER")){
        return true;
    }

    return false;
}



Valor* GeneradorCodigo::convertirArregloCadena(string nombreVar, Ambito *ambiente, string clase, string metodo){

    int esAtributo = tabla->esAtributo(nombreVar, ambiente);
    Valor *ret = new Valor();
    Simbolo *simboloVar = tabla->obtenerSimbolo(nombreVar,ambiente,esAtributo);
    if(simboloVar!=NULL){
        if(esArregloCaracter(simboloVar)){
            string inicioArreglo ="";
            string posSize="";
            string valSize="";
             if(esAtributo == 1){
                int posA = tabla->obtenerPosGlobal(nombreVar, ambiente);
                if(posA!=-1){
                    string posArreglo = intToCadena(posA);
                    string t1= c3d->getTemporal();
                    string t2= c3d->getTemporal();
                    string t3= c3d->getTemporal();
                    string t4= c3d->getTemporal();
                    posSize= c3d->getTemporal();
                    valSize= c3d->getTemporal();
                    inicioArreglo= c3d->getTemporal();

                    string l1 = t1+"=p+0;";
                    string l2 = t2+"=stack["+t1+"];";
                    string l3 = t3+"=heap["+t2+"];";
                    string l4 = t4+"="+t3+"+"+posArreglo+";";
                    string l5 = posSize+"=heap["+t4+"];";
                    string l6 = valSize+"=heap["+posSize+"];";
                    string l7 = inicioArreglo+"=1+"+posSize+";";
                    c3d->addCodigo("//convirtiendo un arreglo global a cadena");
                    c3d->addCodigo(l1);
                    c3d->addCodigo(l2);
                    c3d->addCodigo(l3);
                    c3d->addCodigo(l4);
                    c3d->addCodigo(l5);
                    c3d->addCodigo(l6);
                    c3d->addCodigo(l7);


                }else{
                    erroresEjecucion->insertarError2(SEMANTICO, "No existe el arreglo "+ nombreVar+", error a conversion a cadena");
                    return ret;
                }

            }else if(esAtributo ==2){
                int posA= tabla->obtenerPosLocal(nombreVar,ambiente);
                if(posA!=-1){
                    string t1= c3d->getTemporal();
                    string t2= c3d->getTemporal();
                    posSize= c3d->getTemporal();
                    valSize= c3d->getTemporal();
                    inicioArreglo= c3d->getTemporal();
                    string posArreglo = intToCadena(posA);
                    string l1= t1+"= p + "+posArreglo+";";
                    string l2 = t2+"=stack["+t1+"];";
                    string l3 = posSize+"=heap["+t2+"];";
                    string l4= valSize+"=heap["+posSize+"]; //valor del size del arreglo "+ nombreVar;
                    string l5= inicioArreglo+"="+posSize+"+1;//apuntador donde inicia el arreglo "+ nombreVar;
                    c3d->addCodigo("//Convertir un arreglo local a cadena");
                    c3d->addCodigo(l1);
                    c3d->addCodigo(l2);
                    c3d->addCodigo(l3);
                    c3d->addCodigo(l4);
                    c3d->addCodigo(l5);

                }else{
                    erroresEjecucion->insertarError2(SEMANTICO, "No existe el arreglo local "+nombreVar+", no se pudo convertir a cadena");
                    return ret;
                }
            }


             if(!sonIguales(inicioArreglo, "")){
                 string etiqCiclo= c3d->getEtiqueta();
                 string etigV= c3d->getEtiqueta();
                 string etiF= c3d->getEtiqueta();
                 string valCaracter= c3d->getTemporal();
                 string t1=c3d->getTemporal();
                 string t2= c3d->getTemporal();
                 string contCaracter= c3d->getTemporal();
                 string tPosPool= c3d->getTemporal();
                 string l0 = tPosPool+"=heap["+inicioArreglo+"];";
                 string l1 = valCaracter+"=pool["+tPosPool+"];// valor  del primer caracter";
                 string l2 = t1+"=h;//apuntador a cadena";
                 string l3="heap["+t1+"]=s;\n h=h+1;";
                 string l4 = t2+"=s;";
                 string l5 = "s=s+1;";
                 string l6=contCaracter+"=0;";
                 string l1v= this->c3d->getEtiqueta();
                 string l1f= c3d->getEtiqueta();
                 string l7 = etiqCiclo+":";
                 string l61 = "if("+tPosPool+"!=0 )goto "+l1v+";\n goto "+l1f+";";
                 string l62=l1v+":";

                 string l8 = "if("+valCaracter+"!=0)goto "+etigV+";";
                 string l9 = "goto "+etiF+";";
                 string l10 = etigV+":";
                 string l11= "pool[s]="+valCaracter+";";
                 string l12 = "s=s+1;";
                 string l13= contCaracter+"="+contCaracter+"+1;";

                 string l14= inicioArreglo+"="+inicioArreglo+"+1;";

                 string l144 = tPosPool+"=heap["+inicioArreglo+"];";
                 string l15= valCaracter+"=pool["+tPosPool+"];";
                 string l16= "goto "+etiqCiclo+";";
                 string l21=l1f+":";
                 string l17=etiF+":";
                 string l18 = "pool["+t2+"]="+contCaracter+";";
                 string l19="pool[s]=0;";
                 string l20="s=s+1;";

                 c3d->addCodigo(l0);
                 c3d->addCodigo(l1);
                 c3d->addCodigo(l2);
                 c3d->addCodigo(l3);
                 c3d->addCodigo(l4);
                 c3d->addCodigo(l5);
                 c3d->addCodigo(l6);
                 c3d->addCodigo(l7);
                 c3d->addCodigo(l61);
                 c3d->addCodigo(l62);

                 c3d->addCodigo(l8);
                 c3d->addCodigo(l9);
                 c3d->addCodigo(l10);
                 c3d->addCodigo(l11);
                 c3d->addCodigo(l12);
                 c3d->addCodigo(l13);
                 c3d->addCodigo(l14);
                 c3d->addCodigo(l144);
                 c3d->addCodigo(l15);
                 c3d->addCodigo(l16);
                 c3d->addCodigo(l21);
                 c3d->addCodigo(l17);
                 c3d->addCodigo(l18);
                 c3d->addCodigo(l19);
                 c3d->addCodigo(l20);

                 //c3d->addCodigo("h=h+1;");
                 Valor *v = new Valor();
                 v->crearCadena(t1);
                return v;



             }else{

             }


        }
    }

    return ret;
}






void GeneradorCodigo::resolverAsignacion(nodoArbol *sentencia, string clase, string funcion){

    string tipoAsig= sentencia->valor;
    cout<<tipoAsig<<endl;
    int pos=-1;

    if(sonIguales(tipoAsig,"1")){

        //id iguales  expresion
        string nombreVar= sentencia->hijos.at(0)->valor;
        string igual = sentencia->hijos.at(1)->valor;
        nodoArbol* expresion = sentencia->hijos.at(2);
        int esAtributo = tabla->esAtributo(nombreVar, this->ambiente);

        if(sonIguales(expresion->etiqueta, "INSTANCIA")){
            this->resolverInstanciaAsignacion(sentencia, clase, funcion, esAtributo);
        }else{
            Valor *retExpresion = this->resolverExpresion(expresion, clase, funcion);
            retExpresion= this->condiABool(retExpresion);
            string t1,t2,t3,tPosicion;
            string tipo="";
            Simbolo *s =tabla->obtenerSimbolo(nombreVar,ambiente, esAtributo);

             if(s!=NULL){
                        if(sonIguales(s->tipoSimbolo, "ARREGLO") && esCadena(retExpresion->tipo) ){
                            this->asignarArreglo(s,retExpresion,ambiente, clase, funcion,true,"");
                            return;
                        }
                    }
        if(esAtributo ==1){
            //es atributo
            pos= tabla->obtenerPosGlobal(nombreVar,this->ambiente);
            string pos1= intToCadena(pos);
            t1=c3d->getTemporal();
            t2= c3d->getTemporal();
            t3= c3d->getTemporal();
            tPosicion = c3d->getTemporal();
            /*==== Obtenieidno el Acceso de un atributo =====*/
            c3d->addCodigo(t1+" = p + 0; //posicion del this");
            c3d->addCodigo(t2+ "= stack[ "+ t1+"]; //apuntador al heap");
            c3d->addCodigo(t3+" = heap[ "+t2+"];//apuntador donde inicia el objeto");
            c3d->addCodigo(tPosicion+" = "+t3+" + "+pos1+";//posicion real de atributo "+ nombreVar);
        }else if(esAtributo == 2){
            //es local
            tPosicion= c3d->getTemporal();
            pos= tabla->obtenerPosLocal(nombreVar, ambiente);
            string pos1= intToCadena(pos);
            c3d->addCodigo(tPosicion+" = p + "+pos1+"; //pos real de var local "+nombreVar);
        }else{
            //cout<<"ambitos "<<this->ambiente->getAmbitos()<<endl;
            this->addErrorSemantico(SEMANTICO,"No se puede realizar la asignacion, debido a que no existe la variable "+nombreVar+" en el ambito "+this->ambiente->getAmbitos(), sentencia->fila, sentencia->columna);
            return;
        }

        if(pos!=-1){
            string pos1= intToCadena(pos);
            tipo= tabla->obtenerTipo(nombreVar,this->ambiente,esAtributo);
            this->asignarPorPos(nombreVar,igual,tPosicion,tipo,retExpresion,esAtributo);
        }



}

    }else if(sonIguales(tipoAsig,"2")){
        //este igaules expresion
        nodoArbol *expThis = sentencia->hijos.at(0);
        string igual = sentencia->hijos.at(1)->valor;
        nodoArbol *expresion = sentencia->hijos.at(2);
        Valor *v= this->resolverExpresion(expresion, clase, funcion);
        v= this->condiABool(v);
        string pos = this->resolverThisPosicion(expThis,clase,funcion);
        if(!sonIguales(pos,"")){
            c3d->addCodigo("heap["+pos+"]="+v->valor+";// posicion del this "+pos);
        }



    }else if(sonIguales(tipoAsig,"3")){
        //acceso iguales expresion
        nodoArbol *accesoAsigna = sentencia->hijos.at(0);
       string igual = sentencia->hijos.at(1)->valor;
        nodoArbol *expresion = sentencia->hijos.at(2);
        Valor *retExpresion = this->resolverExpresion(expresion,clase, funcion);
        cout<<retExpresion->valor<<endl;
        Valor *respuetaAcceso = this->resolverAcceso(accesoAsigna,this->ambiente,clase, funcion);
        if(!esNulo(respuetaAcceso->tipo)){
            if(!esNulo(respuetaAcceso->referencia)){
                string t= c3d->getTemporal();
                c3d->addCodigo(t+"="+respuetaAcceso->estructura+"["+respuetaAcceso->referencia+"]; //obtenieidno el valor de la vairable");
                Valor *valor = new Valor();
                valor->valor= t;
                valor->tipo= respuetaAcceso->tipo;
                valor->setReferencia(respuetaAcceso->estructura,respuetaAcceso->referencia);
                Valor *valorOperacion= new Valor();
                valorOperacion=retExpresion;

                if(sonIguales(igual,"+=")){
                    valorOperacion = validarSumaOperacion(valor,retExpresion);

                }else if(sonIguales(igual,"+=")){
                    valorOperacion = validarRestaOperacion(valor,retExpresion);

                }else if(sonIguales(igual,"*=")){
                    valorOperacion = validarMultiplicacionOperacion(valor, retExpresion);

                }else if(sonIguales(igual,"/=")){
                    valorOperacion = validarDivisionOperacion(valor, retExpresion);

                }
                if(sonIguales(valor->tipo, valorOperacion->tipo) || esNulo(valorOperacion->tipo)){
                    c3d->addCodigo(respuetaAcceso->estructura+"["+respuetaAcceso->referencia+"]="+valorOperacion->valor+"; //ASIGNANDO VALOR DE UN ACCESO");

                }else if(esCaracter(valor->tipo) && esCadena(valorOperacion->tipo))
                {
                   // erroresEjecucion->insertarError(SEMANTICO,"Tipo "+retExpresion->tipo+" TENGO QUE ASINGAR ESTE ARREGLO ",sentencia->fila, sentencia->columna);

                   this->asignarCadenaArregloPorPosicion(valor,valorOperacion,ambiente,clase,funcion);

                }

                else{

                    erroresEjecucion->insertarError(SEMANTICO,"Tipo "+valorOperacion->tipo+", no es valido para asignar a un acceso :(  "+valorOperacion->tipo+" "+valor->tipo,sentencia->fila, sentencia->columna);
                }


            }else{

               erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al resolver el acceso", sentencia->fila, sentencia->columna);
            }
        }else{
             erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al resolver el acceso", sentencia->fila, sentencia->columna);

        }




    }else if(sonIguales(tipoAsig,"4")){
        //posArrego igaules expresion
        nodoArbol *pos_Arreglo = sentencia->hijos.at(0);
        string igual = sentencia->hijos.at(1)->valor;
        nodoArbol *expresion = sentencia->hijos.at(2);
        string nombreArreglo = pos_Arreglo->valor;
        QList<nodoArbol*> dimensiones = pos_Arreglo->hijos.at(0)->hijos;
        int esAtributo = tabla->esAtributo(nombreArreglo,this->ambiente);
        string tipoArreglo = tabla->obtenerTipo(nombreArreglo, this->ambiente,esAtributo);
        c3d->addCodigo("// ----------- ASIGNAR UNA POSICION DEL ARREGLO ----------");
        Valor *apuntador = this->posArreglo(pos_Arreglo,clase, funcion);
        if(!esNulo(apuntador->tipo)){
            Valor *ret = this->resolverExpresion(expresion,clase, funcion);
            if(sonIguales(ret->tipo, tipoArreglo) || esNulo(ret->tipo)){
                string l = apuntador->estructura+"["+apuntador->referencia+"]="+ret->valor+"; //asignacion de una posiciones de un arreglo  <3 ";
                c3d->addCodigo(l);
            }else{
                this->erroresEjecucion->insertarError(SEMANTICO, "No se puede asignar posicion al arreglo "+ nombreArreglo+", tipos invalidos "+ tipoArreglo +" y "+ret->tipo, pos_Arreglo->fila, pos_Arreglo->columna);

            }
        }else{
            erroresEjecucion->insertarError(SEMANTICO, "No se ha podido calular el apuntador al arreglo "+ nombreArreglo,pos_Arreglo->fila, pos_Arreglo->columna);
        }
    }else if(sonIguales(tipoAsig, "5")){
        instanciarPosArreglo(sentencia, clase,funcion);
    }

}


void GeneradorCodigo::instanciarPosArreglo(nodoArbol *sentencia, string clase, string metodo){



    nodoArbol *pos_Arreglo = sentencia->hijos.at(0);
    int modo = this->tabla->esAtributo(pos_Arreglo->valor,this->ambiente);
    string igual = sentencia->hijos.at(1)->valor;
    nodoArbol *expresion = sentencia->hijos.at(2);
    string nombreArreglo = pos_Arreglo->valor;
    QList<nodoArbol*> dimensiones = pos_Arreglo->hijos.at(0)->hijos;
    int esAtributo = tabla->esAtributo(nombreArreglo,this->ambiente);
    string tipoArreglo = tabla->obtenerTipo(nombreArreglo, this->ambiente,esAtributo);
    c3d->addCodigo("// ----------- ASIGNAR UNA POSICION DEL ARREGLO ----------");
    Valor *apuntador =this->posArreglo(pos_Arreglo,clase, metodo);
    if(!esNulo(apuntador->tipo)){
        if(!esNulo(apuntador->referencia)){

            string nombreClaseInstanciar = expresion->valor;
            QList<nodoArbol*> parametros = expresion->hijos.at(0)->hijos;
            int noParametros = parametros.length();
            string firmaMetodo = tabla->obtenerFirmaMetodo(nombreClaseInstanciar,noParametros, nombreClaseInstanciar);
            int sizeFuncActual = tabla->sizeFuncion(clase, metodo);
            if(sonIguales(tipoArreglo, nombreClaseInstanciar) &&
                !sonIguales("",firmaMetodo)){
                int sizeC= tabla->sizeClase(tipoArreglo);
                string tClase = intToCadena(sizeC);
               // string t2 = c3d->getTemporal();
                c3d->addCodigo("// ------------ Realizando insntancia de una posicion de una arreglo -----------");
                c3d->addCodigo(apuntador->estructura+"["+apuntador->referencia+"]=h;");
               // c3d->addCodigo(t2+"=h+1;");
                //c3d->addCodigo("heap[h]="+t2+";");
                //c3d->addCodigo("h=h+1;");
                c3d->addCodigo("h=h+"+tClase+";");

                string t11= c3d->getTemporal();
                string t22 = c3d->getTemporal();
                c3d->addCodigo("//------------------------ Agregando la referencia -------------");
                c3d->addCodigo(t11+"=p + "+intToCadena(sizeFuncActual)+";");
                c3d->addCodigo(t22+"="+t11+"+0; ");
                c3d->addCodigo("stack["+t22+"]="+apuntador->referencia+";");

                QList<Valor*>valores;
                nodoArbol *expresionTemporal;
                for(int g =0; g<parametros.length();g++){
                    expresionTemporal= parametros.at(g);
                    Valor *v= this->resolverExpresion(expresionTemporal, clase,metodo);
                    v= this->condiABool(v);
                    valores.push_back(v);
                }


                //parametros
                if(parametros.length()>0){


                    c3d->addCodigo("//Asgnando parametros de la llamada a funcion");

                    Simbolo *simb;
                    int cont=1;
                  //  //cout<<"parametros "<<parametrosFunc.length()<<endl;
                   if(valores.length() == parametros.length()){
                       for(int j=0;j<parametros.length();j++){
                           simb=tabla->obtenerNombreParametro(nombreClaseInstanciar+"_"+firmaMetodo,cont);
                           Valor *v= valores.at(j);
                           if(simb!= NULL){
                               string t1_1=c3d->getTemporal();
                               c3d->addCodigo(t1_1+"=p+"+intToCadena(sizeFuncActual)+";");
                               string t2_1= c3d->getTemporal();
                               c3d->addCodigo(t2_1+"="+t1_1+"+"+intToCadena(cont)+";//pos del parametro "+intToCadena(cont));
                               cont++;

                               if(sonIguales(simb->tipoSimbolo, "ARREGLO") && simb->declaAtributo!= NULL){
                                   c3d->addCodigo("// ------- DECLARANDO PAREAMETRO DE ARREGLO ");
                                  // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,claseTemporal, metodoTemporal, true, t2_1,claseTemporal+"_"+metodoTemporal,simb);
                                    this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, metodo, true, t2_1,clase+"_"+metodo,simb);
                                   // this->declararArreglo(simb->tipoElemento,simb->nombrecorto,simb->dimensiones,this->ambiente,clase, metodo,true,);
                               }


                               if(esCaracter(v->tipo) && sonIguales(v->tipoSimbolo, "ARREGLO")){

                                   c3d->addCodigo("stack["+t2_1+"]="+v->referencia+"; //referencia del arreglo asignado al stack el parametro");
                                   // c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                               }else{

                                   if(esArregloCaracter(simb) && esCadena(v->tipo) ){
                                       c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                                       c3d->addCodigo("// aquiiii "+v->tipo+"  "+v->valor+" "+clase+" "+metodo+" "+firmaMetodo);
                                        this->asignarArreglo(simb,v,this->ambiente,clase,metodo,false,t2_1);
                                       c3d->addCodigo("//aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");

                                   }else{
                                       c3d->addCodigo("stack["+t2_1+"]="+v->valor+"; //asignando al stack el parametro");
                                   }

                               }

                           }else{
                               erroresEjecucion->insertarError(SEMANTICO, "No existe el parametro de la posicion  "+ intToCadena(cont) +" en la llamada a "+firmaMetodo, sentencia->fila, sentencia->columna);
                           }

                       }
                   }else{
                       erroresEjecucion->insertarError(SEMANTICO, "No coincide el numero de parametros con la llamada a  "+ firmaMetodo, sentencia->fila, sentencia->columna);
                   }
                }else{
                    c3d->addCodigo("// llamada no posee parametros");
                }





                c3d->addCodigo("p=p+"+intToCadena(sizeFuncActual)+";");
                c3d->addCodigo("call "+firmaMetodo+"();");
                c3d->addCodigo("p=p-"+intToCadena(sizeFuncActual)+";");




            }else{
                erroresEjecucion->insertarError(SEMANTICO, "Error, no se ha encontrado el constructor para dicha asignacion ", expresion->fila, expresion->columna);
            }
        }else{
           erroresEjecucion->insertarError(SEMANTICO, "No se ha podido resolver la posicione del arreglo a instanciar. Referencia nula. ",sentencia->fila, sentencia->columna);
        }


    }else{
        erroresEjecucion->insertarError(SEMANTICO, "No se ha podido resolver la posicione del arreglo a instanciar ",sentencia->fila, sentencia->columna);
    }

}



void GeneradorCodigo::asignarPorPos(string nombre, string igual, string pos, string tipo, Valor *exp, int edd){

    if(sonIguales(igual,"=")){
        if(sonIguales(tipo, exp->tipo) || esNulo(exp->tipo)){
            if(edd ==1){
                //heap
                c3d->addCodigo("heap["+pos+"]="+exp->valor+";");
            }else if(edd ==2){
                c3d->addCodigo("stack["+pos+"]="+exp->valor+";");
            }
        }else{
            erroresEjecucion->insertarError2(SEMANTICO, "Incompatibilidad de tipos para la asignacion de "+nombre+", "+tipo +" y "+exp->tipo);
            return;
        }
        return;
    }


    Valor *v= new Valor();
    string t1= c3d->getTemporal();
    if(edd==1){
        c3d->addCodigo(t1+"=heap["+pos+"];//accediento al valor de "+nombre);
        v->valor=t1;
        v->tipo= tipo;

    }else if(edd==2){
        c3d->addCodigo(t1+"=stack["+pos+"];//accediento al valor de "+nombre);
        v->valor=t1;
        v->tipo= tipo;
    }
   Valor *res= new Valor();
   if(!esNulo(v->tipo)){
       if(sonIguales(igual,"+=")){
           res= this->validarSumaOperacion(v,exp);
       }
       if(sonIguales(igual,"-=")){
           res= this->validarRestaOperacion(v,exp);

       }
       if(sonIguales(igual,"*=")){
           res= this->validarMultiplicacionOperacion(v,exp);

       }
       if(sonIguales(igual,"/=")){
           res= this->validarDivisionOperacion(v,exp);
       }
   }
   if(sonIguales(tipo, res->tipo) || sonIguales(tipo, NULO)){
       if(edd ==1){
           //heap
           c3d->addCodigo("heap["+pos+"]="+res->valor+";");
       }else if(edd ==2){
           c3d->addCodigo("stack["+pos+"]="+res->valor+";");
       }
   }else{
       erroresEjecucion->insertarError2(SEMANTICO, "Incompatibilidad de tipos para la asignacion de "+nombre+", "+tipo +" y "+exp->tipo);
   }


}

void GeneradorCodigo::resolverIF(nodoArbol *sent, string clase, string funcion){

    string etiqSalida= c3d->getEtiqueta();
    string tipoIF= sent->valor;
    nodoArbol *nIf = sent->hijos.at(0);
    nodoArbol *exp = nIf->hijos.at(0);
    QList<nodoArbol*>sentenciasIf= nIf->hijos.at(1)->hijos;
    Valor *res = this->resolverExpresion(exp,clase, funcion);
    res= this->convertirACondicion(res);


    if(esCondicion(res->tipo)){


        this->ambiente->addSi();
        cout<<ambiente->getAmbitos()<<endl;
        c3d->addCodigo(res->cond->getCodigo());
        c3d->addCodigo(res->cond->getEtiquetasVerdaderas());
         agregarCodigo(exp->fila, c3d->codigo,erroresEjecucion->imprimirErrores());
         for(int i=0; i< sentenciasIf.length(); i++){
             this->analizarSentencia(sentenciasIf.at(i),clase,funcion);
         }
         ambiente->salirAmbito();
         c3d->addCodigo("goto "+etiqSalida+"; //etiqueta de salida del if");
         c3d->addCodigo(res->cond->getEtiquetasFalsas());

         if(sonIguales(tipoIF,"3") || sonIguales(tipoIF,"4")){
             QList<nodoArbol*> l_sinoSi = sent->hijos.at(1)->hijos;
             for(int i =0; i<l_sinoSi.length();i++){
                 QList<nodoArbol*> st= l_sinoSi.at(i)->hijos.at(1)->hijos;
                 nodoArbol *exp = l_sinoSi.at(i)->hijos.at(0);
                 Valor *resultado= this->resolverExpresion(exp,clase,funcion);
                 resultado = this->convertirACondicion(resultado);

                 if(esCondicion(resultado->tipo)){
                     this->ambiente->addSi();
                     cout<<ambiente->getAmbitos()<<endl;
                     c3d->addCodigo(resultado->cond->getCodigo());
                     c3d->addCodigo(resultado->cond->getEtiquetasVerdaderas());
                      agregarCodigo(exp->fila, c3d->codigo,erroresEjecucion->imprimirErrores());
                     for(int j =0; j<st.length();j++){
                         this->analizarSentencia(st.at(j),clase,funcion);
                     }
                     ambiente->salirAmbito();
                     c3d->addCodigo("goto "+etiqSalida+"; //etiqueta de salida del if");
                     c3d->addCodigo(resultado->cond->getEtiquetasFalsas());
                 }else{
                     erroresEjecucion->insertarError(SEMANTICO,"Expresion no valida para condicion de una sentencia IF", exp->fila, exp->columna);

                 }
             }
         }

         if(sonIguales(tipoIF,"2")){
             QList<nodoArbol*>instrucciones = sent->hijos.at(1)->hijos.at(0)->hijos;
             this->ambiente->addElse();
             for(int i =0; i<instrucciones.length();i++){
                 this->analizarSentencia(instrucciones.at(i), clase,funcion);
             }
             c3d->addCodigo("goto "+etiqSalida+"; //etiqueta de salida del if");
             this->ambiente->salirAmbito();


         }

         if(sonIguales(tipoIF,"4")){
             QList<nodoArbol*>instrucciones = sent->hijos.at(2)->hijos.at(0)->hijos;
             this->ambiente->addElse();
             for(int i =0; i<instrucciones.length();i++){
                 this->analizarSentencia(instrucciones.at(i),clase,funcion);
             }
             c3d->addCodigo("goto "+etiqSalida+"; //etiqueta de salida del if");
             this->ambiente->salirAmbito();

         }
        c3d->addEtiqueta(etiqSalida);

    }else{
erroresEjecucion->insertarError(SEMANTICO,"Expresion no valida para condicion de una sentencia IF", exp->fila, exp->columna);
    }
}

void GeneradorCodigo::imprimir(nodoArbol *expresion, string nombreClase, string nombreFuncion){
    Valor *v= this->resolverExpresion(expresion,nombreClase, nombreFuncion);
//std:://cout<< v->tipo<< endl;
     if(esCaracter(v->tipo) && sonIguales(v->tipoSimbolo, "ARREGLO")){
        c3d->addCodigo("print(100, "+ v->valor+");");
     }

    else if(esInt(v->tipo)){
        this->c3d->addCodigo("print(105,"+v->valor+");");
    }
    else if(esDecimal(v->tipo)){
        this->c3d->addCodigo("print(102,"+v->valor+");");
    }
    else if(esCadena(v->tipo)){

        this->c3d->addCodigo("print(115,"+v->valor+");");
    } else  if(esCaracter(v->tipo)){

          this->c3d->addCodigo("print(99, "+v->valor+");");

    }
   else if(esBool(v->tipo)){
       // string n = this->obtenerNoBooleano(v->valor);
         this->c3d->addCodigo("print(98, "+v->valor+");");

    }else if(esCondicion(v->tipo)){
        string t = c3d->getTemporal();
        string lSal= c3d->getEtiqueta();
        c3d->addTransferencia(t,"3");
        c3d->addCodigo(v->cond->codigo);
        c3d->addCodigo(v->cond->getEtiquetasVerdaderas());
        c3d->addTransferencia(t,"1");
        c3d->addSalto(lSal);
        c3d->addCodigo(v->cond->getEtiquetasFalsas());
        c3d->addTransferencia(t,"0");
        c3d->addEtiqueta(lSal);
        this->c3d->addCodigo("print(98, "+t+");");
    }


}







Valor* GeneradorCodigo::resolverAcceso(nodoArbol *nodo, Ambito *ambiente, string clase, string metodo){
string tipoAcceso = nodo->valor;
Valor *r = new Valor();
if(sonIguales(tipoAcceso, "1")){
    //inicia con un id
     string nombreVar = nodo->hijos.at(0)->valor;
     QList<nodoArbol*>elementosAcceso = nodo->hijos.at(1)->hijos;
     int esAtributo = tabla->esAtributo(nombreVar,ambiente);
     Simbolo *simb = tabla->obtenerSimbolo(nombreVar,ambiente, esAtributo);
     if(simb!= NULL){
         string posVariable ="";
         string tipoElemento="";
         string posFinal="";
         bool esObj;
         string nombreElemento;
         int banderaED =2; //1 stack , 2 heap, 3 pool
         string rolSimbolo = simb->rol;
         if(esAtributo==1){
             int posV = tabla->obtenerPosGlobal(nombreVar,ambiente);
             tipoElemento= tabla->obtenerTipo(nombreVar,ambiente, esAtributo);
            if(posV!= -1){
                string posVariable = intToCadena(posV);
                string t1 = c3d->getTemporal();
                string t2 = c3d->getTemporal();
                string t3 = c3d->getTemporal();
                posFinal = c3d->getTemporal();
                c3d->addCodigo("//resolviendo un acceso a un atributo");
                c3d->addCodigo(t1+"=p+0;");
                c3d->addCodigo(t2+"=stack["+t1+"]; //apuntador al heap");
                c3d->addCodigo(t3+"=heap["+t2+"];");
                c3d->addCodigo(posFinal +"= "+t3+"+"+posVariable+";");
                esObj = this->esObjeto(tipoElemento);
            }else{
                erroresEjecucion->insertarError(SEMANTICO, "No se puede resolver el accceso de "+ nombreVar+", no existe dicha variable",nodo->hijos.at(0)->fila, nodo->hijos.at(0)->columna);
                return r;
            }
         }else{
             //es una variable local
             int posV= tabla->obtenerPosLocal(nombreVar,ambiente);
             tipoElemento= tabla->obtenerTipo(nombreVar,ambiente, esAtributo);
             if(posV!=-1){
                 posVariable= intToCadena(posV);
                 string t1= c3d->getTemporal();
                 string t2 = c3d->getTemporal();
                 posFinal= c3d->getTemporal();
                 c3d->addCodigo("// resolviendo un acceso local");
                 c3d->addCodigo(t1+"= p+"+posVariable+"; //pos del objeto");
                 c3d->addCodigo(posFinal+"=stack["+t1+"]; //apuntador al heap del objeto");
                 esObj= this->esObjeto(tipoElemento);

             }else{
                erroresEjecucion->insertarError(SEMANTICO, "No se puede resolver el accceso de "+ nombreVar+", no existe dicha variable local",nodo->hijos.at(0)->fila, nodo->hijos.at(0)->columna);
                return r;
            }
         }//final del else de una variable local y del atributo 


         bool bandera = true;
         nodoArbol *elementoTemporal;
         for(int i =0; i<elementosAcceso.length(); i++){
             elementoTemporal = elementosAcceso.at(i);
             if(bandera){
                 string modoAcceso = elementoTemporal->etiqueta;

                 if(sonIguales(modoAcceso, "ID")){
                    nombreElemento = elementoTemporal->valor;
                    int posVar = tabla->obtenerPosAtributoAcceso(tipoElemento, nombreElemento);
                    rolSimbolo = tabla->obtenerPosTipoSimboloAcceso(tipoElemento, nombreElemento);
                    if(posVar!= -1 && !(sonIguales(rolSimbolo,""))){
                        string temp1_4 = c3d->getTemporal();
                        string l1_4="";
                        if(banderaED==2){
                            //heap
                            l1_4= temp1_4+"=heap["+posFinal+"]; //recuperando pos inicial del objeto";
                            c3d->addCodigo(l1_4);

                        }else if(banderaED==1){
                            //stack
                            l1_4= temp1_4+"=stack["+posFinal+"]; //recuperando pos inicial del objeto";
                            string l1_5= temp1_4+"=heap["+temp1_4+"];//apuntador inicial del objeto";
									this->c3d->addCodigo(l1_4);
									this->c3d->addCodigo(l1_5);

                        }
                        string l4 = posFinal+"="+temp1_4+"+"+intToCadena(posVar)+"; // posd inicial del otro objeto o valor de una variable comun";
                        c3d->addCodigo(l4);
                        tipoElemento= tabla->obtenerTipoAtributoAcceso(tipoElemento, nombreElemento);
                        banderaED=2;
                    }else{
                        erroresEjecucion->insertarError(SEMANTICO, "No existe el elemento "+ nombreElemento,elementoTemporal->fila, elementoTemporal->columna);
                        return r;
                    }

                 }

                 else if(sonIguales(modoAcceso,"LLAMADA_FUNCION")){
                    rolSimbolo="";
                    nombreElemento= elementoTemporal->valor;
                    QList<nodoArbol*>parametros= elementoTemporal->hijos;
                    string firmaMetodo = tabla->obtenerFirmaMetodo(tipoElemento,parametros.length(),nombreElemento);
                    if(banderaED==1){
                        string temp1_4= c3d->getTemporal();
                        string l1_4 = temp1_4+ "= stack["+posFinal+"]; //recuperando pos inicial del objeto 888";
                        c3d->addCodigo(l1_4);
                        c3d->addCodigo(posFinal+"="+temp1_4+"+0;");
                    }
                    Valor *retLlamda= this->llamadaFuncion(elementoTemporal,clase,metodo,false,posFinal,tipoElemento,firmaMetodo);
                    //var retLlamada = this.llamada_funcion(elementoTemporal,ambitos,tipoElemento,firmaMetodo,posFinal,false);
                    string tipoFuncion = tabla->obtenerTipoFuncion(firmaMetodo);

                    if(sonIguales(retLlamda->tipo, tipoFuncion) /*|| esNulo(retLlamda->tipo)*/){
                        tipoElemento = retLlamda->tipo;
                        posFinal = retLlamda->referencia;
                        banderaED=1;
                    }/*else if(esNulo(retLlamda->tipo)){
                        tipoElemento = NULO;
                        posFinal = posFinal;
                        //banderaED=1;
                    }*/else{
                        erroresEjecucion->insertarError("Semantico", "Tipos no coinciden para la funcion en un acceso "+ firmaMetodo +", con de tipo "+ tipoFuncion+" y viene "+ retLlamda->tipo, elementoTemporal->fila, elementoTemporal->columna);
                        break;
                    }


                 }else if(sonIguales(modoAcceso, "POS_ARREGLO")){

                     rolSimbolo="";
                     nombreElemento =elementoTemporal->valor;
                     QList<nodoArbol*> posicionesArreglo = elementoTemporal->hijos.at(0)->hijos;
                     int esAtr = tabla->esAtributo(nombreElemento,this->ambiente);
                     string tipoArreglo = tabla->obtenerTipo(nombreElemento,this->ambiente,esAtr);

                     if(banderaED==1){
                         string temp1_4= c3d->getTemporal();
                         string l1 = temp1_4+"=stack["+posFinal+"]; //recuperando pos inicial del objeto 888 arreglo";
                         string l2 = posFinal+"= "+temp1_4+"+0; ";
                         c3d->addCodigo(l1);
                         c3d->addCodigo(l2);
                     }

                     Valor *apuntadorArreglo= this->obtenerApuntadorPosArreglo(nombreElemento,posicionesArreglo,this->ambiente,clase,metodo,posFinal,true);
                     if(!esNulo(apuntadorArreglo->tipo)){
                         if(sonIguales(tipoArreglo,T_CARACTER)){
                             banderaED=3;
                             //string t = c3d->getTemporal();
                            // c3d->addCodigo(t+"=heap["+apuntadorArreglo->valor+"];");
                            //c3d->addCodigo(posFinal+"="+t+";");
                         }else{
                             banderaED=2;
                         }
                        posFinal = apuntadorArreglo->valor;
                         tipoElemento= tipoArreglo;

                         bandera = this->esObjeto(tipoElemento);
                     }else{
                         erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al resolver la posicion del arreglo "+ nombreElemento+", es un acceso",elementoTemporal->fila, elementoTemporal->columna);
                     }

                 }else {
                     rolSimbolo="";
                     string temp1_4= c3d->getTemporal();
                     c3d->addCodigo(temp1_4+"=heap["+posFinal+"]; //recuperando pos inicial del objeto ");
                     posFinal = temp1_4;

                     if(sonIguales(modoAcceso, "INSERTAR")){

                                      }else if(sonIguales(modoAcceso, "APILAR")){

                                      }else if(sonIguales(modoAcceso, "DESAPILAR")){

                                      }else if(sonIguales(modoAcceso, "ENCOLAR")){

                                      }else if(sonIguales(modoAcceso, "DESENCOLAR")){

                                      }else if(sonIguales(modoAcceso, "OBTENER")){

                                      }else if(sonIguales(modoAcceso, "BUSCAR")){

                                      }else if(sonIguales(modoAcceso, "TAMANIO")){
                         bandera = false;
                         string temp1= c3d->getTemporal();
                         c3d->addCodigo(temp1+"=heap["+posFinal+"]; //obteniendio el size del arreglo");
                         Valor *retSize = new Valor();
                         retSize->tipo= T_ENTERO;
                         retSize->valor=temp1;
                         return retSize;


                                      }




                 }




             }//fin de que no se puede continura ya que bandera = false;
         }//fin del ciclo

         string val = "heap";
         if(banderaED ==1){
             val = "stack";
         }else if(banderaED==3){
             val= "pool";
         }
         string tempR = c3d->getTemporal();
         string l_R = tempR+"="+val+"["+posFinal+"]; //valor a retornar del acceso";
         c3d->addCodigo(l_R);
         Valor *ret= new Valor();
         ret->valor= tempR;
         ret->tipo= tipoElemento;
         ret->setReferencia(val,posFinal);
         if(sonIguales(rolSimbolo, "ARREGLO"))
         {
             ret->tipoSimbolo= "ARREGLO";
         }
         return ret;


     }else{
         erroresEjecucion->insertarError(SEMANTICO, "La variable "+ nombreVar+", no existe. Imposible resolver acceso",nodo->hijos.at(0)->fila, nodo->hijos.at(0)->columna);
     }
  }else if(sonIguales(tipoAcceso,"2")){


    nodoArbol *nodoPosArreglo = nodo->hijos.at(0);
    QList<nodoArbol*>elementosAcceso = nodo->hijos.at(1)->hijos;
    string nombreElemento =nodoPosArreglo->valor;
    int esAtributo = tabla->esAtributo(nombreElemento, this->ambiente);
    Simbolo *simb = this->tabla->obtenerSimbolo(nombreElemento, this->ambiente, esAtributo);
    if(simb!= NULL){
        string tipoSimbolo = simb->tipoSimbolo;
        string tipoElemento=simb->tipoElemento;
        string posFinal="";
        bool esObj;
        int banderaED =2; //1 stack , 2 heap, 3 pool
        string rolSimbolo = simb->rol;



        //Ejecuntado la primera posicion de arreglo

        QList<nodoArbol*> posicionesArreglo = nodoPosArreglo->hijos.at(0)->hijos;
        int esAtr = tabla->esAtributo(nombreElemento,this->ambiente);
        string tipoArreglo = tabla->obtenerTipo(nombreElemento,this->ambiente,esAtr);
        Valor *ret_posArreglo= this->posArreglo(nodoPosArreglo,clase,metodo);
        if(!esNulo(ret_posArreglo->tipo)){
            if(!esNulo(ret_posArreglo->referencia)){
                posFinal = ret_posArreglo->referencia;
            }else{
               erroresEjecucion->insertarError(SEMANTICO, "Error, referencia nula al analizar el posicion del arreglo" +nombreElemento +" para resolvver el acceso ", nodoPosArreglo->fila, nodoPosArreglo->columna);
            }
        }else{
           erroresEjecucion->insertarError(SEMANTICO, "Error, valor nulo  al analizar el posicion del arreglo" +nombreElemento +" para resolvver el acceso ", nodoPosArreglo->fila, nodoPosArreglo->columna);
        }

        if(!sonIguales(posFinal,"")){
            nodoArbol *elementoTemporal;
         for(int i =0; i<elementosAcceso.length();i++){
             elementoTemporal= elementosAcceso.at(i);
             string modoAcceso = elementoTemporal->etiqueta;

             if(sonIguales(modoAcceso, "ID")){
                nombreElemento = elementoTemporal->valor;
                int posVar = tabla->obtenerPosAtributoAcceso(tipoElemento, nombreElemento);
                rolSimbolo = tabla->obtenerPosTipoSimboloAcceso(tipoElemento, nombreElemento);
                if(posVar!= -1 && !(sonIguales(rolSimbolo,""))){
                    string temp1_4 = c3d->getTemporal();
                    string l1_4="";
                    if(banderaED==2){
                        //heap
                        l1_4= temp1_4+"=heap["+posFinal+"]; //recuperando pos inicial del objeto";
                        c3d->addCodigo(l1_4);

                    }else if(banderaED==1){
                        //stack
                        l1_4= temp1_4+"=stack["+posFinal+"]; //recuperando pos inicial del objeto";
                        string l1_5= temp1_4+"=heap["+temp1_4+"];//apuntador inicial del objeto";
                                this->c3d->addCodigo(l1_4);
                                this->c3d->addCodigo(l1_5);

                    }
                    string l4 = posFinal+"="+temp1_4+"+"+intToCadena(posVar)+"; // posd inicial del otro objeto o valor de una variable comun";
                    c3d->addCodigo(l4);
                    tipoElemento= tabla->obtenerTipoAtributoAcceso(tipoElemento, nombreElemento);
                    banderaED=2;
                }else{
                    erroresEjecucion->insertarError(SEMANTICO, "No existe el elemento "+ nombreElemento,elementoTemporal->fila, elementoTemporal->columna);
                    return r;
                }

             }else if(sonIguales(modoAcceso,"LLAMADA_FUNCION")){
                 rolSimbolo="";
                 nombreElemento= elementoTemporal->valor;
                 QList<nodoArbol*>parametros= elementoTemporal->hijos;
                 string firmaMetodo = tabla->obtenerFirmaMetodo(tipoElemento,parametros.length(),nombreElemento);
                 if(banderaED==1){
                     string temp1_4= c3d->getTemporal();
                     string l1_4 = temp1_4+ "= stack["+posFinal+"]; //recuperando pos inicial del objeto 888";
                     c3d->addCodigo(l1_4);
                     c3d->addCodigo(posFinal+"="+temp1_4+"+0;");
                 }
                 Valor *retLlamda= this->llamadaFuncion(elementoTemporal,clase,metodo,false,posFinal,tipoElemento,firmaMetodo);
                 //var retLlamada = this.llamada_funcion(elementoTemporal,ambitos,tipoElemento,firmaMetodo,posFinal,false);
                 string tipoFuncion = tabla->obtenerTipoFuncion(firmaMetodo);

                 if(sonIguales(retLlamda->tipo, tipoFuncion) /*|| esNulo(retLlamda->tipo)*/){
                     tipoElemento = retLlamda->tipo;
                     posFinal = retLlamda->referencia;
                     banderaED=1;
                 }/*else if(esNulo(retLlamda->tipo)){
                     tipoElemento = NULO;
                     posFinal = posFinal;
                     //banderaED=1;
                 }*/else{
                     erroresEjecucion->insertarError("Semantico", "Tipos no coinciden para la funcion en un acceso "+ firmaMetodo +", con de tipo "+ tipoFuncion+" y viene "+ retLlamda->tipo, elementoTemporal->fila, elementoTemporal->columna);
                     break;
                 }


              }else if(sonIguales(modoAcceso, "POS_ARREGLO")){

                 rolSimbolo="";
                 nombreElemento =elementoTemporal->valor;
                 QList<nodoArbol*> posicionesArreglo = elementoTemporal->hijos.at(0)->hijos;
                 int esAtr = tabla->esAtributo(nombreElemento,this->ambiente);
                 string tipoArreglo = tabla->obtenerTipo(nombreElemento,this->ambiente,esAtr);

                 if(banderaED==1){
                     string temp1_4= c3d->getTemporal();
                     string l1 = temp1_4+"=stack["+posFinal+"]; //recuperando pos inicial del objeto 888 arreglo";
                     string l2 = posFinal+"= "+temp1_4+"+0; ";
                     c3d->addCodigo(l1);
                     c3d->addCodigo(l2);
                 }

                 Valor *apuntadorArreglo= this->obtenerApuntadorPosArreglo(nombreElemento,posicionesArreglo,this->ambiente,clase,metodo,posFinal,true);
                 if(!esNulo(apuntadorArreglo->tipo)){
                     if(sonIguales(tipoArreglo,T_CARACTER)){
                         banderaED=3;
                         //string t = c3d->getTemporal();
                        // c3d->addCodigo(t+"=heap["+apuntadorArreglo->valor+"];");
                        //c3d->addCodigo(posFinal+"="+t+";");
                     }else{
                         banderaED=2;
                     }
                    posFinal = apuntadorArreglo->valor;
                     tipoElemento= tipoArreglo;

                    // bandera = this->esObjeto(tipoElemento);
                 }else{
                     erroresEjecucion->insertarError(SEMANTICO, "Ha ocurrido un error al resolver la posicion del arreglo "+ nombreElemento+", es un acceso",elementoTemporal->fila, elementoTemporal->columna);
                 }

             }

         }//final del for de elmentos acceso

         string val = "heap";
         if(banderaED ==1){
             val = "stack";
         }else if(banderaED==3){
             val= "pool";
         }
         string tempR = c3d->getTemporal();
         string l_R = tempR+"="+val+"["+posFinal+"]; //valor a retornar del acceso";
         c3d->addCodigo(l_R);
         Valor *ret= new Valor();
         ret->valor= tempR;
         ret->tipo= tipoElemento;
         ret->setReferencia(val,posFinal);
         if(sonIguales(rolSimbolo, "ARREGLO"))
         {
             ret->tipoSimbolo= "ARREGLO";
         }
         return ret;

        }



    }else{
        erroresEjecucion->insertarError(SEMANTICO, "No existe el arreglo "+nombreElemento, nodoPosArreglo->fila, nodoPosArreglo->columna);

    }
}

return r;

}



/*======================= RESOLVER EXPRESIONES ==================================*/

Valor * GeneradorCodigo::convertirACadena(nodoArbol *sent, string clase, string metodo){

    Valor *res = this->resolverExpresion(sent->hijos.at(0),clase, metodo);
    res = this->condiABool(res);
    if(this->esInt(res->tipo) || esDecimal(res->tipo) || esBool(res->tipo)){
        string t= c3d->getTemporal();
        c3d->addCodigo("//convertir un  numero a cadena");
        c3d->addCodigo("heap[h]="+res->valor+"; //numero a convertir a cadena");
        c3d->addCodigo("h=h+1;");
        c3d->addCodigo("$$_inStr();");
        c3d->addCodigo(t+"=h;");
        c3d->addCodigo("h=h+1;");
        Valor *v = new Valor();
        v->crearCadena(t);
        return v;
    }else{
        erroresEjecucion->insertarError(SEMANTICO, "Los tipos validos para convertir a cadena son entero, booleano  y decimal. Usted ingreso "+ res->tipo, sent->fila, sent->columna);
        Valor * v = new Valor();
        return v;

    }
}

Valor* GeneradorCodigo::convertirAEntero(nodoArbol *sent, string clase, string metodo){
    nodoArbol *expresion = sent->hijos.at(0);
    Valor *v = this->resolverExpresion(expresion, clase, metodo);
    v= this->condiABool(v);
    if(esDecimal(v->tipo) || esBool(v->tipo)){
        string t= c3d->getTemporal();
        c3d->addCodigo("//convertir a enterro un "+ v->tipo);
         c3d->addCodigo("heap[h]=1;");
         c3d->addCodigo("h=h+1;");
        c3d->addCodigo("heap[h]="+v->valor+"; //valor a convertir a entero");
        c3d->addCodigo("h=h+1;");
        c3d->addCodigo("$$_inNum();");
        c3d->addCodigo(t+"=h;");
        c3d->addCodigo("h=h+1;");
        string t2= c3d->getTemporal();
        c3d->addCodigo(t2+"=heap["+t+"]; //valor del nuevo entero");
        Valor *v = new Valor();
        v->crearEntero(t2);
        return v;
    }else if(esCadena(v->tipo)){
        string t= c3d->getTemporal();
        c3d->addCodigo("//convertir a enterro un "+ v->tipo);
         c3d->addCodigo("heap[h]=2;");
         c3d->addCodigo("h=h+1;");
        c3d->addCodigo("heap[h]="+v->valor+"; //valor a convertir a entero");
        c3d->addCodigo("h=h+1;");
        c3d->addCodigo("$$_inNum();");
        c3d->addCodigo(t+"=h;");
        c3d->addCodigo("h=h+1;");
        string t2= c3d->getTemporal();
        c3d->addCodigo(t2+"=heap["+t+"]; //valor del nuevo entero");
        Valor *v = new Valor();
        v->crearEntero(t2);
        return v;

    }else{
        erroresEjecucion->insertarError(SEMANTICO, "Los tipos validos para convertir a entero son  cadena, booleano  y decimal. Usted ingreso "+ v->tipo, expresion->fila, expresion->columna);
        Valor * v = new Valor();
        return v;
    }
}

Valor* GeneradorCodigo:: resolverExpresion(nodoArbol *nodo,string clase, string metodo){

    string nombreNodo= nodo->getEtiqueta();
    std::cout<< nombreNodo<< endl;
    Valor *y = new Valor();
    if(nombreNodo.compare("raiz") ==0){

        QList<nodoArbol*> hijos = nodo->getHijos();

        for(int i=0;i<hijos.length();i+=1)
        {
            y= resolverExpresion(hijos.at(i),clase,metodo);
        }
        return y;
    }

    if(sonIguales(nombreNodo, "TERNARIO")){
        y= this->resolverTernario(nodo, clase, metodo);
        return y;
    }
    if(sonIguales(nombreNodo, "ACCESO")){
        y = this->resolverAcceso(nodo,this->ambiente, clase,metodo);
        return y;
    }
    if(sonIguales(nombreNodo, "ESTE")){
        y = this->resolverThis(nodo,clase, metodo);
        return y;
    }

    if(sonIguales(nombreNodo, "CONVERTIR_A_ENTERO")){
       y = this->convertirAEntero(nodo, clase, metodo);
       return y;
    }

    if(sonIguales(nombreNodo, "CONVERTIR_A_CADENA")){
       y = this->convertirACadena(nodo, clase, metodo);
       return y;
    }
    if(sonIguales(nombreNodo,"LLAMADA_FUNCION")){
        y = this->llamadaFuncion(nodo,clase, metodo, true,"","","");
        return y;
    }

    if(sonIguales(nombreNodo,"ID")){
        y = this->resolverID(nodo,clase, metodo);
        return y;
    }
    if(nombreNodo.compare(T_ENTERO)==0){
       y->crearEntero(nodo->getValor());
        return y;
    }

    if(nombreNodo.compare(T_DECIMAL)==0){
        //Valor *v = new Valor();
        y->crearDecimal(nodo->getValor());
        return y;
    }

    if(nombreNodo.compare(T_CARACTER)==0){
       // Valor *v = new Valor();
        y->crearCaracter(nodo->getValor());
        return y;
    }

    if(nombreNodo.compare(T_CADENA)==0){
        //Valor *v = new Valor();
        string temp = crearCadena(nodo->getValor());
        y->crearCadena(temp);
        return y;
    }

    if(nombreNodo.compare(T_BOOLEANO)==0){
       // Valor *v = new Valor();
       // //cout<<nodo->valor<<endl;
        y->crearBooleano(nodo->valor);
        return y;
    }

    if(sonIguales(nombreNodo, "NULO")){
        y = new Valor();
        return y;
    }

    if(nombreNodo.compare("SUMA")== 0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1 = resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val2 = resolverExpresion(hijos.at(1), clase, metodo);
        y = this->validarSumaOperacion(val1, val2);
        if(esNulo(y->tipo)){
            addErrorSemantico(SEMANTICO,"Tipos no validos para realizar una suma "+ val1->tipo+" y "+ val2->tipo, nodo->fila, nodo->columna);
        }
        return y;
    }

    if(nombreNodo.compare("RESTA")== 0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1 = resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val2 = resolverExpresion(hijos.at(1), clase, metodo);
        y = this->validarRestaOperacion(val1, val2);
        if(esNulo(y->tipo)){
            addErrorSemantico(SEMANTICO,"Tipos no validos para realizar una resta "+ val1->tipo+" y "+ val2->tipo, nodo->fila, nodo->columna);
        }
        return y;
    }


    if(nombreNodo.compare("MULTIPLICACION")== 0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1 = resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val2 = resolverExpresion(hijos.at(1), clase, metodo);
        y = this->validarMultiplicacionOperacion(val1, val2);
        if(esNulo(y->tipo)){
            addErrorSemantico(SEMANTICO,"Tipos no validos para realizar una multiplicaicon "+ val1->tipo+" y "+ val2->tipo, nodo->fila, nodo->columna);
        }
        return y;
    }

    if(nombreNodo.compare("DIVISION")== 0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1 = resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val2 = resolverExpresion(hijos.at(1), clase, metodo);
        y = this->validarDivisionOperacion(val1, val2);
        if(esNulo(y->tipo)){
            addErrorSemantico(SEMANTICO,"Tipos no validos para realizar una division "+ val1->tipo+" y "+ val2->tipo, nodo->fila, nodo->columna);
        }
        return y;
    }

    if(nombreNodo.compare("POTENCIA")== 0){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1 = resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val2 = resolverExpresion(hijos.at(1), clase, metodo);
        y = this->validarPotenciaOpercion(val1, val2);
        if(esNulo(y->tipo)){
            addErrorSemantico(SEMANTICO,"Tipos no validos para realizar una potencia "+ val1->tipo+" y "+ val2->tipo, nodo->fila, nodo->columna);
        }
        return y;
    }


    //Relacionales

    if(sonIguales("RELACIONAL",nombreNodo)){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1= this->resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val3 = this->resolverExpresion(hijos.at(1), clase, metodo);
        string signo= nodo->valor;
        y = this->validarRelacional(val1,val3,signo);
        if(!esCondicion(y->tipo)){
            addErrorSemantico(SEMANTICO, "Tipos no validos para realizar una relacional "+ val1->tipo+" con "+ val3->tipo, nodo->fila, nodo->columna);
        }
        return y;
    }

    if(sonIguales("NOT",nombreNodo)){
        QList<nodoArbol*>hijos = nodo->hijos;
        Valor *v1= this->resolverExpresion(hijos.at(0), clase, metodo);
        y = this->resolverNOT(v1);
        if(!esCondicion(y->tipo)){
            addErrorSemantico(SEMANTICO, "Tipos no validos para realizar una NOT "+ v1->tipo, nodo->fila, nodo->columna);
        }
        return y;


    }


    if(sonIguales("AND",nombreNodo)){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1= this->resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val3 = this->resolverExpresion(hijos.at(1), clase, metodo);
        y = this->resolverAND(val1,val3);
        if(!esCondicion(y->tipo)){
            addErrorSemantico(SEMANTICO, "Tipos no validos para realizar una AND "+ val1->tipo+" con "+ val3->tipo, nodo->fila, nodo->columna);
        }
        return y;


    }

    if(sonIguales("OR",nombreNodo)){
        QList<nodoArbol*> hijos = nodo->getHijos();
        Valor *val1= this->resolverExpresion(hijos.at(0),clase, metodo);
        Valor *val3 = this->resolverExpresion(hijos.at(1), clase, metodo);
        y = this->resolverOR(val1,val3);
        if(!esCondicion(y->tipo)){
            addErrorSemantico(SEMANTICO, "Tipos no validos para realizar una OR "+ val1->tipo+" con "+ val3->tipo, nodo->fila, nodo->columna);
        }
        return y;

    }

    if(sonIguales("POS_ARREGLO", nombreNodo)){
        y = this->posArreglo(nodo, clase, metodo);
        return y;
    }



return y;
}




Valor* GeneradorCodigo::condiABool(Valor *v){

    if(esCondicion(v->tipo)){
        string t = c3d->getTemporal();
        string lSal= c3d->getEtiqueta();
        c3d->addTransferencia(t,"3");
        c3d->addCodigo(v->cond->codigo);
        c3d->addCodigo(v->cond->getEtiquetasVerdaderas());
        c3d->addTransferencia(t,"1");
        c3d->addSalto(lSal);
        c3d->addCodigo(v->cond->getEtiquetasFalsas());
        c3d->addTransferencia(t,"0");
        c3d->addEtiqueta(lSal);
        Valor *v2= new Valor();
        v2->crearBooleano(t);
        return v2;
    }
    return v;

}




Valor* GeneradorCodigo::resolverID(nodoArbol *id, string clase, string funcion){

        Valor *ret= new Valor();
        int pos=-1;
        string nombreVar=id->valor;
        int esAtributo = tabla->esAtributo(nombreVar, this->ambiente);
        string t1,t2,t3,tPosicion,tVal;
        string tipo=tabla->obtenerTipo(nombreVar,this->ambiente,esAtributo);
        Simbolo *simb = tabla->obtenerSimbolo(nombreVar,ambiente, esAtributo);
        if(simb!= NULL){
            if(esArregloCaracter(simb)){

                Valor *v = this->convertirArregloCadena(nombreVar, ambiente, clase, funcion);
                return v;
            }
        }

        if(esAtributo ==1 && simb!= NULL){
            //es atributo
            pos= tabla->obtenerPosGlobal(nombreVar,this->ambiente);
            string pos1= intToCadena(pos);
            t1=c3d->getTemporal();
            t2= c3d->getTemporal();
            t3= c3d->getTemporal();
            tPosicion = c3d->getTemporal();
            tVal= c3d->getTemporal();
            /*==== Obtenieidno el Acceso de un atributo =====*/
            c3d->addCodigo(t1+" = p + 0; //posicion del this");
            c3d->addCodigo(t2+ "= stack[ "+ t1+"]; //apuntador al heap");
            c3d->addCodigo(t3+" = heap[ "+t2+"];//apuntador donde inicia el objeto");
            c3d->addCodigo(tPosicion+" = "+t3+" + "+pos1+";//posicion real de atributo "+ nombreVar);
            c3d->addCodigo(tVal+"= heap["+tPosicion+"]; //valor de "+nombreVar);
            ret->valor=tVal;
            ret->tipo=tipo;
           ret->setReferencia("heap",tPosicion);
            return ret;

        }else if(esAtributo == 2 && simb!= NULL){
            //es local
            tPosicion= c3d->getTemporal();
            tVal= c3d->getTemporal();
            pos= tabla->obtenerPosLocal(nombreVar, ambiente);
            string pos1= intToCadena(pos);
            c3d->addCodigo(tPosicion+" = p + "+pos1+"; //pos real de var local<4 "+nombreVar);
            c3d->addCodigo(tVal+"= stack["+tPosicion+"]; //valor de "+nombreVar);
            ret->valor=tVal;
            ret->tipo=tipo;
            ret->setReferencia("stack",tPosicion);
           /* if(esArregloCaracter(simb)){
                ret->tipoSimbolo="ARREGLO";
                ret->
            }*/

            return ret;
        }else{
           // //cout<<"ambitos "<<this->ambiente->getAmbitos()<<endl;
            this->addErrorSemantico(SEMANTICO,"No existe la variable "+nombreVar+" en el ambito actual "+this->ambiente->getAmbitos(), id->fila, id->columna);

        }
        return ret;

}

/*================================= Resolver Operaciones ==============================================*/

Valor* GeneradorCodigo::resolverAND(Valor *val1, Valor *val2){
    val1= convertirACondicion(val1);
    val2= convertirACondicion(val2);
    Valor *o = new Valor();
    if(esCondicion(val1->tipo)){
        if(esCondicion(val2->tipo)){
            string codigoAnd = val1->cond->getCodigo()+"\n"+
                    val1->cond->getEtiquetasVerdaderas()+"\n"+
                    val2->cond->getCodigo()+"\n";
            nodoCondicion *c = new nodoCondicion(codigoAnd);
            c->addEtiquetasVerdaderas(val2->cond->verdaderas);
            c->addEtiquetasFalsas(val1->cond->falsas);
            c->addEtiquetasFalsas(val2->cond->falsas);

            o->crearCondicion(c);
            return o;
        }
    }

return o;
}

Valor* GeneradorCodigo::resolverOR(Valor *val1, Valor *val2){
    val1= convertirACondicion(val1);
    val2= convertirACondicion(val2);
    Valor *o = new Valor();
    if(esCondicion(val1->tipo)){
        if(esCondicion(val2->tipo)){
            string codigoAnd = val1->cond->getCodigo()+"\n"+
                    val1->cond->getEtiquetasFalsas()+"\n"+
                    val2->cond->getCodigo()+"\n";
            nodoCondicion *c = new nodoCondicion(codigoAnd);
            c->addEtiquetasVerdaderas(val1->cond->verdaderas);
            c->addEtiquetasVerdaderas(val2->cond->verdaderas);
            c->addEtiquetasFalsas(val2->cond->falsas);
            o->crearCondicion(c);
            return o;
        }
    }

return o;

}

Valor* GeneradorCodigo::resolverNOT(Valor *val1){
    val1= convertirACondicion(val1);
    if(esCondicion(val1->tipo)){
        val1->cond->cambiarEtiquetas();
        return val1;
    }
    Valor *v = new Valor();
return v;
}


Valor * GeneradorCodigo::convertirACondicion(Valor *val1){

    if(sonIguales(T_BOOLEANO,val1->tipo)){
        string valBool= obtenerNoBooleano(val1->valor);
        string eV= c3d->getEtiqueta();
        string eF= c3d->getEtiqueta();
        string cod ="if( 1== "+valBool+") goto "+eV+";\n goto "+eF+";";
        nodoCondicion *r = new nodoCondicion(cod);
        r->addFalsa(eF);
        r->addVerdadera(eV);
        Valor *v= new Valor();
        v->crearCondicion(r);
        return v;
    }

    return val1;
}


string GeneradorCodigo::sumarAsciiCadena(Valor *val){
    string tPosPool, tpos,tVal,tAcum,l1,l2,l3;
    l1= c3d->getEtiqueta();
    l2= c3d->getEtiqueta();
    l3= c3d->getEtiqueta();
    tPosPool= c3d->getTemporal();
    tpos= c3d->getTemporal();
    tVal = c3d->getTemporal();
    tAcum = c3d->getTemporal();
    c3d->addCodigo("//Realizando suma de ascii");
    c3d->sacarHeap(tPosPool,val->valor);
    c3d->agregarSuma(tpos,tPosPool,"1");
    c3d->addCodigo("//tVal");
    c3d->sacarPool(tVal,tpos);
    c3d->addTransferencia(tAcum,"0");
    c3d->addEtiqueta(l1);
    c3d->addIf(tVal,"!=","0",l2,l3);
    c3d->addEtiqueta(l2);
    c3d->agregarSuma(tAcum,tAcum,tVal);
    c3d->agregarSuma(tpos, tpos,"1");
    c3d->sacarPool(tVal,tpos);
    c3d->addSalto(l1);
    c3d->addEtiqueta(l3);
    c3d->addCodigo("//fin de suma de ascii");
    return tAcum;
}



Valor* GeneradorCodigo::validarRelacional(Valor *val1, Valor *val2, string simbolo){

    Valor* retorno = new Valor();
    if(esNumericoNoArreglo(val1, val2)){
        string lv= c3d->getEtiqueta();
        string lf= c3d->getEtiqueta();
        string cod = "if ( "+val1->valor+" "+ simbolo+" "+val2->valor+")goto "+lv+";\n goto "+lf+";";
        nodoCondicion *cond = new nodoCondicion(cod);
        cond->addFalsa(lf);
        cond->addVerdadera(lv);
        retorno->crearCondicion(cond);
        return retorno;
    }
    else if(this->esCaracter(val1->tipo) && esCadena(val2->tipo)){
        Valor *cad1= this->caracterToCadena(val1->valor);
        string t1= this->sumarAsciiCadena(cad1);
        string t2= this->sumarAsciiCadena(val2);
        string lv= c3d->getEtiqueta();
        string lf= c3d->getEtiqueta();
        string cod="if( "+t1+" "+simbolo+" "+t2+")goto "+ lv+";\n goto "+lf+";";
        nodoCondicion *cond = new nodoCondicion(cod);
        cond->addFalsa(lf);
        cond->addVerdadera(lv);
        retorno->crearCondicion(cond);
        return retorno;


    }else if(esCadena(val1->tipo) && esCaracter(val2->tipo)){
        Valor *cad2= this->caracterToCadena(val2->valor);
        string t1= this->sumarAsciiCadena(val1);
        string t2= this->sumarAsciiCadena(cad2);
        string lv= c3d->getEtiqueta();
        string lf= c3d->getEtiqueta();
        string cod="if( "+t1+" "+simbolo+" "+t2+")goto "+ lv+";\n goto "+lf+";";
        nodoCondicion *cond = new nodoCondicion(cod);
        cond->addFalsa(lf);
        cond->addVerdadera(lv);
        retorno->crearCondicion(cond);
        return retorno;

    }else if(esCadena(val1->tipo) && esCadena(val2->tipo)){
        string t1= this->sumarAsciiCadena(val1);
        string t2= this->sumarAsciiCadena(val2);
        string lv= c3d->getEtiqueta();
        string lf= c3d->getEtiqueta();
        string cod="if( "+t1+" "+simbolo+" "+t2+")goto "+ lv+";\n goto "+lf+";";
        nodoCondicion *cond = new nodoCondicion(cod);
        cond->addFalsa(lf);
        cond->addVerdadera(lv);
        retorno->crearCondicion(cond);
        return retorno;


    } else  if((esNulo(val1->tipo) || esNulo(val2->tipo))&&(sonIguales(simbolo, "!=") || sonIguales(simbolo, "=="))){
        string lv = c3d->getEtiqueta();
        string lf = c3d->getEtiqueta();
        string cod="if( "+ val1->valor+" "+simbolo+" "+val2->valor+") goto "+lv+"; \n goto "+lf+";";
        nodoCondicion *c = new nodoCondicion(cod);
        c->addFalsa(lf);
        c->addVerdadera(lv);
        retorno->crearCondicion(c);
        return retorno;
    }else if((esBool(val1->tipo) && esBool(val2->tipo))&&(sonIguales("==",simbolo) || sonIguales(simbolo,"!="))){
        string lv = c3d->getEtiqueta();
        string lf = c3d->getEtiqueta();
        string v1= this->obtenerNoBooleano(val1->valor);
        string  v2= this->obtenerNoBooleano(val2->valor);
        string cod="if( "+ v1+" "+simbolo+" "+v2+") goto "+lv+"; \n goto "+lf+";";
        nodoCondicion *c = new nodoCondicion(cod);
        c->addFalsa(lf);
        c->addVerdadera(lv);
        retorno->crearCondicion(c);
        return retorno;
    }

return retorno;
}


bool GeneradorCodigo::esNumericoNoArreglo(Valor *val1, Valor *val2){

    if(((this->esInt(val1->tipo) || esCaracter(val1->tipo) || esDecimal(val1->tipo))&&
        (this->esInt(val2->tipo) || esCaracter(val2->tipo) || esDecimal(val2->tipo)))&&
            (!sonIguales(val1->tipoSimbolo,"ARREGLO") && !sonIguales(val2->tipo, "ARREGLO"))){
        return true;
    }

    return false;

}


Valor* GeneradorCodigo:: concatenarCadena(Valor *cad1, Valor *cad2){
    string tNueva = c3d->getTemporal();
    string tSize= c3d->getTemporal();
    c3d->addTransferencia(tNueva,"h");
    c3d->igualHeap(tNueva,"s");
    c3d->addH();
    string t1= c3d->getTemporal();
    string t2= c3d->getTemporal();
    string t3= c3d->getTemporal();
    string t4= c3d->getTemporal();
    c3d->sacarHeap(t1,cad1->valor);
    c3d->sacarPool(t2,t1);
    c3d->sacarHeap(t3,cad2->valor);
    c3d->sacarPool(t4,t3);
    c3d->agregarSuma(tSize,t2,t4);
    c3d->igualPool("s",tSize);
    c3d->addS();
    c3d->agregarSuma(t1,t1,"1");
    c3d->agregarSuma(t3,t3,"1");
    c3d->addCodigo("//Copiando la cadena 1 a la nueva cadena");
    string l1= c3d->getEtiqueta();
    string l2= c3d->getEtiqueta();
    string l3= c3d->getEtiqueta();
    string l4= c3d->getEtiqueta();
    string l5= c3d->getEtiqueta();
    string l6= c3d->getEtiqueta();
    c3d->addEtiqueta(l1);
    c3d->sacarPool(t2,t1);
    c3d->addIf(t2,"!=","0",l2,l3);
    c3d->addEtiqueta(l2);
    c3d->igualPool("s",t2);
    c3d->agregarSuma(t1,t1,"1");
    c3d->addS();
    c3d->addSalto(l1);
    c3d->addEtiqueta(l3);
    c3d->addEtiqueta(l4);
    c3d->sacarPool(t4,t3);
    c3d->addIf(t4,"!=","0",l5,l6);
    c3d->addEtiqueta(l5);
    c3d->igualPool("s",t4);
    c3d->agregarSuma(t3,t3,"1");
    c3d->addS();
    c3d->addSalto(l4);
    c3d->addEtiqueta(l6);
    c3d->igualPool("s","0");
    c3d->addS();

    Valor *cad= new Valor();
    cad->crearCadena(tNueva);
    return cad;

}



Valor* GeneradorCodigo:: caracterToCadena(string cadenaCaracter){

   string cad = this->obtenerCadenaCaracter(cadenaCaracter);
    string tempCadena = crearCadena(cad);
    Valor *v= new Valor();
    v->crearCadena(tempCadena);
    return v;

}

string GeneradorCodigo::obtenerCadenaCaracter(string val){
    int n =atoi(val.c_str());
    char c = (char)n;
    string str = string(1,c);
    return str;

}

string GeneradorCodigo::crearCadena(string val)
{
    char c;
    string tRes= c3d->getTemporal();
    c3d->addCodigo(tRes+" = h;");
    c3d->addCodigo("h = h + 1;");
    c3d->addCodigo("heap["+tRes+"] = s;");
    c3d->addCodigo("pool[s]="+this->obtnerCadenaInt(val.length())+";");
    c3d->addCodigo("s=s+1;");
    for (int i=0; i<val.length();i++){
       c= val.at(i);
       int n = (int)c;
       ostringstream str1;
       str1 << n;
       string g= str1.str();
       c3d->addCodigo("pool[s] = "+g+";//"+string(1,c));
       c3d->addCodigo("s = s + 1 ;");
    }
    c3d->addCodigo("pool[s] = 0;");
    c3d->addCodigo("s = s + 1 ;");
    return tRes;
}


void GeneradorCodigo::escribirPotencia(string base, string exponente, string tResultado){

    string tCont =c3d->getTemporal();
    string l0 = c3d->getEtiqueta();
    string l1 = c3d->getEtiqueta();
    string l2= c3d->getEtiqueta();
    c3d->addCodigo(tResultado+" = 1;");
    c3d->addCodigo(tCont+" = 0;");
    c3d->addCodigo(l0+":");
    c3d->addCodigo("if( "+tCont+"< "+exponente+") goto "+l1+";");
    c3d->addCodigo("goto "+ l2+";");
    c3d->addCodigo(l1+":");
    c3d->addCodigo(tResultado+" = "+tResultado+" * "+base+";");
    c3d->addCodigo(tCont+" = "+tCont+"+1;");
    c3d->addCodigo("goto "+l0+";");
    c3d->addCodigo(l2+":");
}



Valor* GeneradorCodigo:: validarPotenciaOpercion(Valor *v1, Valor *v2){
    Valor *retorno = new Valor();

    //retornor node tipo decimal

    if(esInt(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        escribirPotencia(v1->valor,v2->valor,temp);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        escribirPotencia(v1->valor,v2->valor,temp);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esCaracter(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v2->valor);
    escribirPotencia(v1->valor,v,temp);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esCaracter(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v1->valor);
    escribirPotencia(v,v2->valor,temp);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esBool(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v1->valor);

        escribirPotencia(v,v2->valor,temp);

        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esBool(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v2->valor);
        escribirPotencia(v1->valor,v,temp);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        escribirPotencia(v1->valor,v2->valor,temp);
        retorno->crearDecimal(temp);
        return retorno;
    }

    //retorno de tipo entero

    else if(esInt(v1->tipo) && esCaracter(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v2->valor);
            escribirPotencia(v1->valor,n,temp);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esCaracter(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v1->valor);
            escribirPotencia(n,v2->valor,temp);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esBool(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v1->valor);
            escribirPotencia(n,v2->valor, temp);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esBool(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v2->valor);
            escribirPotencia(v1->valor,n,temp);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        escribirPotencia(v1->valor,v2->valor,temp);
        retorno->crearEntero(temp);
        return retorno;
    }


    return  retorno;
}


Valor* GeneradorCodigo:: validarDivisionOperacion(Valor *v1, Valor *v2){
    Valor *retorno = new Valor();

    //retornor node tipo decimal

    if(esInt(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esCaracter(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esCaracter(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esBool(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esBool(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }

    //retorno de tipo entero

    else if(esInt(v1->tipo) && esCaracter(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"/");
            c3d->addCodigo(cod);
            retorno->crearDecimal(temp);
            return retorno;
        }

    else if(esCaracter(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"/");
            c3d->addCodigo(cod);
            retorno->crearDecimal(temp);
            return retorno;
        }

    else if(esBool(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"/");
            c3d->addCodigo(cod);
            retorno->crearDecimal(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esBool(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"/");
            c3d->addCodigo(cod);
            retorno->crearDecimal(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"/");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }


    return  retorno;
}


Valor* GeneradorCodigo:: validarMultiplicacionOperacion(Valor *v1, Valor *v2){
    Valor *retorno = new Valor();

    //retornor node tipo decimal

    if(esInt(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esCaracter(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esCaracter(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esBool(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esBool(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"*");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }

    //retorno de tipo entero

    else if(esInt(v1->tipo) && esCaracter(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"*");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esCaracter(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"*");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esBool(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"*");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esBool(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"*");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"*");
        c3d->addCodigo(cod);
        retorno->crearEntero(temp);
        return retorno;
    }


    return  retorno;
}


Valor* GeneradorCodigo:: validarRestaOperacion(Valor *v1, Valor *v2){
    Valor *retorno = new Valor();

    //retornor node tipo decimal

    if(esInt(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esCaracter(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esCaracter(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esBool(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esBool(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"-");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }

    //retorno de tipo entero

    else if(esInt(v1->tipo) && esCaracter(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"-");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esCaracter(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"-");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esBool(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"-");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esBool(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"-");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"-");
        c3d->addCodigo(cod);
        retorno->crearEntero(temp);
        return retorno;
    }


    return  retorno;
}


Valor* GeneradorCodigo:: validarSumaOperacion(Valor *v1, Valor *v2){
    Valor *retorno = new Valor();


    //retornor node tipo decimal


    if(esInt(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esCaracter(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esCaracter(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerIntCaracter(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esBool(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v1->valor);
        string cod = crearOperacion(temp,v,v2->valor,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esBool(v2->tipo)){
        string temp = c3d->getTemporal();
        string v = obtenerNoBooleano(v2->valor);
        string cod = crearOperacion(temp,v1->valor,v,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }
    else if(esDecimal(v1->tipo) && esDecimal(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"+");
        c3d->addCodigo(cod);
        retorno->crearDecimal(temp);
        return retorno;
    }

    //retorno de tipo entero

    else if(esInt(v1->tipo) && esCaracter(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"+");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esCaracter(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerIntCaracter(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"+");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    else if(esBool(v1->tipo) && esInt(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v1->valor);
            string cod = crearOperacion(temp,n,v2->valor,"+");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esBool(v2->tipo)){
            string temp = c3d->getTemporal();
            string n = obtenerNoBooleano(v2->valor);
            string cod = crearOperacion(temp,v1->valor,n,"+");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }
    else if(esInt(v1->tipo) && esInt(v2->tipo)){
        string temp = c3d->getTemporal();
        string cod = crearOperacion(temp,v1->valor,v2->valor,"+");
        c3d->addCodigo(cod);
        retorno->crearEntero(temp);
        return retorno;
    }
    else if(esBool(v1->tipo) && esBool(v2->tipo)){
            string temp = c3d->getTemporal();
            string n1 = obtenerNoBooleano(v1->valor);
            string n2 = obtenerNoBooleano(v2->valor);
            string cod = crearOperacion(temp,n1,n2,"+");
            c3d->addCodigo(cod);
            retorno->crearEntero(temp);
            return retorno;
        }

    //retorno de tipo cadena

    else if(esCadena(v1->tipo) && esCadena(v2->tipo)){
        retorno = concatenarCadena(v1, v2);
        return retorno;
    }

    else if(esCadena(v1->tipo) && esCaracter(v2->tipo)){
        Valor *c2= caracterToCadena(v2->valor);
        retorno = concatenarCadena(v1,c2);
        return retorno;
    }

    else if(esCaracter(v1->tipo) && esCaracter(v2->tipo)){
        Valor *c1= caracterToCadena(v1->valor);
        Valor *c2= caracterToCadena(v2->valor);
        retorno = concatenarCadena(c1,c2);
        return retorno;

    }
    else if(esCaracter(v1->tipo) && esCadena(v2->tipo)){
        Valor *c2= caracterToCadena(v1->valor);
        retorno = concatenarCadena(c2,v1);
        return retorno;
    }//else if((esCaracter(v1->ti)))
    return  retorno;
}


string GeneradorCodigo::crearOperacion(string resultado, string val1, string val2, string operador){
    string cad = resultado+" = "+val1+operador+" "+val2+";";
    return cad;
}

string GeneradorCodigo::obtenerIntCaracter(string val){

  /*  stringatoi(numero.c_str())


   char c= val.at(0);
    int n = (int)c;
    ostringstream str1;
    str1 << n;
    string g= str1.str();*/


    return val;
}


string GeneradorCodigo::obtenerNoBooleano(string val){
    return val;
}


bool GeneradorCodigo::esNulo(string tipo){

    if(sonIguales(tipo, NULO)){
        return true;
    }
    return false;
}


bool GeneradorCodigo::esCadena(string tipo){
    if(tipo.compare(T_CADENA)==0){
        return true;
    }

    return false;
}


bool GeneradorCodigo::esInt(string tipo){
    if(tipo.compare(T_ENTERO)==0){
        return true;
    }

    return false;
}


bool GeneradorCodigo::esBool(string tipo){
    if(tipo.compare(T_BOOLEANO)==0){
        return true;
    }

    return false;
}


bool GeneradorCodigo::esDecimal(string tipo){
    if(tipo.compare(T_DECIMAL)==0){
        return true;
    }

    return false;
}

bool GeneradorCodigo::esCaracter(string tipo){
    if(tipo.compare(T_CARACTER)==0){
        return true;
    }

    return false;
}

bool GeneradorCodigo::esObjeto(string tipo){
    if(esInt(tipo)||
            esNulo(tipo)||
            esCadena(tipo) ||
            esBool(tipo) ||
            esDecimal(tipo)||
            esCaracter(tipo)){
        return false;
    }
    return true;
}

bool GeneradorCodigo::esCondicion(string tipo){

    if(sonIguales(tipo, T_CONDICION)){
        return true;
    }
    return false;
}

string GeneradorCodigo::obtnerCadenaInt(int numero){
    ostringstream str1;
    str1 << numero;
    string geek = str1.str();
    return geek;
}


Valor* GeneradorCodigo::obtenerApuntadorPosArreglo(string nombreArreglo, QList<nodoArbol *> posicionesArreglo, Ambito *ambiente, string clase, string metodo, string posFinal, bool modo){
    Valor *ret2= new Valor();
    int esAtributo;

    if(modo){
        esAtributo= tabla->esAtributoAcceso(nombreArreglo, ambiente);
    }else{
        esAtributo= tabla->esAtributo(nombreArreglo, ambiente);
    }
    string tipoArreglo = tabla->obtenerTipo(nombreArreglo, ambiente, esAtributo);
    if(esAtributo==1 || esAtributo==2){
        Simbolo *simb = tabla->obtenerSimbolo(nombreArreglo, ambiente, esAtributo);
        cout<<nombreArreglo<<endl;
        cout<<ambiente->getAmbitos()<<endl;
        cout<<esAtributo<<endl;
        if(simb!= NULL){
            cout<<simb->tipoSimbolo<<endl;
        }
        if(sonIguales(simb->tipoSimbolo, "ARREGLO")){
            if(esAtributo==1){
                //es un atributo
                int posA = tabla->obtenerPosGlobal(nombreArreglo,ambiente);
                if(posA!=-1){
                    string posArreglo = intToCadena(posA);
                    c3d->addCodigo("//-------------------- Asingacion a posicion de un arreglo atributo");
                    string temp4= c3d->getTemporal();
                    if(modo){
                        string temp_ = c3d->getTemporal();
                        c3d->addCodigo(temp_+"=heap["+posFinal+"]; // obteniendo apuntado al heap del arreglo");
                        c3d->addCodigo(temp4+"="+temp_+"+"+posArreglo+";");
                    }else{
                        string temp1= c3d->getTemporal();
                        string temp2= c3d->getTemporal();
                        string temp3= c3d->getTemporal();
                        c3d->addCodigo(temp1+"=p+0; //pos del this del objeto");
                        c3d->addCodigo(temp2+"=stack["+temp1+"];//apunt del heap para el objeto");
                        c3d->addCodigo(temp3+"=heap["+temp2+"];//apunt donde incia el objeto");
                        c3d->addCodigo(temp4+"="+temp3+"+"+posArreglo+";//pos del arreglo dentro del heap");		  
                    }

                    string temp5 = c3d->getTemporal();
                    string temp6 = c3d->getTemporal();
                    string temp7 = c3d->getTemporal();
                    c3d->addCodigo(temp5+"=heap["+temp4+"]; //apuntador donde incia el arreglo");
                    c3d->addCodigo(temp6+"=heap["+temp5+"];//size del arreglo "+ nombreArreglo);
                    c3d->addCodigo(temp7+"="+temp5+"+1; // pos 0 del arreglo "+ nombreArreglo);
					QList<string> tamanios = this->calcularArregloNs(posicionesArreglo,ambiente,clase, metodo);
					QList<string> tamanios2 = simb->arregloNs;

                    cout<<tamanios.length()<<endl;
                    cout<<tamanios2.length()<<endl;
				    c3d->addCodigo("// ----------- Calculo de iReal para el arreglo "+ nombreArreglo);
                           if((tamanios2.length() == tamanios.length())){
							   string  nTemporal;
							   string tempRes="";
							   string nSize;
                               for(int k =0; k<tamanios.length(); k++ ){
								   nTemporal = tamanios.at(k);
								   nSize = tamanios2.at(k);
								   if(k == 0){
                                       string temp1_1= c3d->getTemporal();
									   tempRes = this->c3d->getTemporal();
									   this->c3d->addCodigo(temp1_1+"= "+nTemporal+"-0; //calculando el n real ()");
									   this->c3d->addCodigo(tempRes+"="+temp1_1+"-0;// iReal columna "+intToCadena(k));

								   }else{
									   string temp1_1= this->c3d->getTemporal();
									   string temp2_1= this->c3d->getTemporal();
									   string temp3_1= this->c3d->getTemporal();
									   this->c3d->addCodigo(temp1_1+"="+nTemporal+"-0; //calculando el n real ()");
									   this->c3d->addCodigo(temp2_1+"="+tempRes+"*"+nSize+"; //multiplicador por n "+ intToCadena(k));
									   this->c3d->addCodigo(temp3_1+"="+temp2_1+"+"+temp1_1+"; //calculando el n real ()");
									   tempRes = this->c3d->getTemporal();
									   this->c3d->addCodigo(tempRes+"="+temp3_1+"+0; // i real de la columna "+ intToCadena(k));
								   }
							   }// fin ciclo for donde se calcula la posicion

							   if(!sonIguales(tempRes,"")){
								   string temp1_8 = this->c3d->getTemporal();
								   this->c3d->addCodigo(temp1_8+"="+temp7+"+"+tempRes+"; //pos de busca del arreglo atributo "+ nombreArreglo);
                                   Valor  *eler = new Valor();
								   eler->tipo = tipoArreglo;
								   eler->valor = temp1_8;
                                   eler->setReferencia("heap", temp1_8);
								   return eler;
								 }else{
                                     erroresEjecucion->insertarError("Semantico", "Hubo un error al realizar las operaciones para la posicoina a asignar", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
									 return ret2;
								 }
                }else{
                               erroresEjecucion->insertarError("Semantico", "El arreglo "+ nombreArreglo+", no ha sido declarado :(", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
                               return ret2;

                           }
                }else{

                    erroresEjecucion->insertarError("Semantico", "El arreglo "+ nombreArreglo+", no existe Atributo", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
			        return ret2;
                }
            }else{
                //es un arreglo local 
                int posA = tabla->obtenerPosLocal(nombreArreglo,ambiente);
                if(posA!=-1){
                    string posArreglo = intToCadena(posA);
                    string temp1= c3d->getTemporal();
                    string temp2= c3d->getTemporal();
                    string temp3= c3d->getTemporal();
                    string temp4 = c3d->getTemporal();
                    string temp5= c3d->getTemporal();
                    c3d->addCodigo("//Resolviendo posicione de un arreglo local");
                    c3d->addCodigo(temp1+"="+posArreglo+"+p; //pos de arreglo "+nombreArreglo);
                    c3d->addCodigo(temp2+"=stack["+temp1+"];//apunt al heal de arreglo "+ nombreArreglo);
                    c3d->addCodigo(temp3+"=heap["+temp2+"]; //obteniendo el apuntador donde incia el arreglo  "+ nombreArreglo);
                    c3d->addCodigo(temp4+"=heap["+temp3+"];//obteniendo el tamaibo del arreglo "+ nombreArreglo);
                    c3d->addCodigo(temp5+"="+temp3+"+1; //pos 0 del arreglo "+ nombreArreglo);
                    c3d->addCodigo("//====== Calculo de valor de las posiciones");
                    QList<string>tamanios = this->calcularArregloNs(posicionesArreglo,ambiente,clase,metodo);
				    QList<string>tamanios2 = simb->arregloNs;
                    cout<<tamanios.length()<<endl;
                    cout<<tamanios2.length()<<endl;
					this->c3d->addCodigo("// -----------(Obteniendo valor) Calculo de iReal para el arreglo "+ nombreArreglo);
                        if( (tamanios2.length() == tamanios.length())){
							string nTemporal;
							string tempRes="";
							string nSize;
							   for(int k =0; k<tamanios.length(); k++ ){
								   nTemporal = tamanios.at(k);
								   nSize = tamanios2.at(k);
								   if(k == 0){
									   string temp1_1= this->c3d->getTemporal();
									   tempRes = this->c3d->getTemporal();
									   this->c3d->addCodigo(temp1_1+"="+nTemporal+"-0; //calcula el n real ()");
									   this->c3d->addCodigo(tempRes+"="+temp1_1+"-0; //iReal columna "+intToCadena(k));	
								   }else{
									   string temp1_1= this->c3d->getTemporal();
									   string temp2_1= this->c3d->getTemporal();
									   string temp3_1= this->c3d->getTemporal();
									   this->c3d->addCodigo(temp1_1+"="+nTemporal+"-0;//calcula el n real ()");
									   this->c3d->addCodigo(temp2_1+"="+tempRes+"*"+nSize+"; //multilicando por n "+ intToCadena(k));
									   this->c3d->addCodigo(temp3_1+"="+temp2_1+"+"+temp1_1+";");
									   tempRes = this->c3d->getTemporal(); 
									   this->c3d->addCodigo(tempRes+"="+temp3_1+"-0;//i real de la ciolumna "+intToCadena(k));
								   }

							   }

                               if(!sonIguales(tempRes,"")){
								   string temp1_5 = this->c3d->getTemporal();
								   this->c3d->addCodigo(temp1_5+"="+temp5+"+"+tempRes+"; //pos buscada del arreglo "+nombreArreglo);
                                   Valor  *ret = new Valor();
								   ret->tipo = tipoArreglo;
								   ret->valor = temp1_5;
                                   ret->setReferencia("heap", temp1_5);
								   return ret;
								 }else{
                                     erroresEjecucion->insertarError("Semantico", "Hubo un error al realizar las operaciones para la posicoina a asignar", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
									 return ret2;
								 }


                }else{
                               erroresEjecucion->insertarError("Semantico", "El arreglo "+ nombreArreglo+", no ha sido declarado :(", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
                               return ret2;

                           }

                }else{
                    erroresEjecucion->insertarError("Semantico", "El arreglo "+ nombreArreglo+", no existe local", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
			        return ret2;

                }
            }

        }else{
            erroresEjecucion->insertarError(SEMANTICO, "La variable "+ nombreArreglo+", no es de tipo arreglo", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
            return ret2;

        }

    }else{
        erroresEjecucion->insertarError(SEMANTICO, "El arreglo "+ nombreArreglo+", no existe", posicionesArreglo.at(0)->fila, posicionesArreglo.at(1)->columna);
        return ret2;
    }
    return ret2;
}


Valor* GeneradorCodigo::posArreglo(nodoArbol *sentencia, string clase, string metodo){
    string nombreArreglo = sentencia->valor;
    QList<nodoArbol*>dimensiones = sentencia->hijos.at(0)->hijos;
   // //cout<<dimensiones.length()<<endl;
    Valor *apuntadorArreglo = this->obtenerApuntadorPosArreglo(nombreArreglo, dimensiones,this->ambiente, clase, metodo,"",false);
    if(!esNulo(apuntadorArreglo->tipo)){
        int esAtributo =tabla->esAtributo(nombreArreglo,ambiente);
        Simbolo *s = tabla->obtenerSimbolo(nombreArreglo,ambiente, esAtributo);
        if(s!= NULL){
            Valor *v = new Valor();
           string temp2_5 = c3d->getTemporal();
           c3d->addCodigo(temp2_5+"=heap["+apuntadorArreglo->valor+"]; //valor que trae el objeto");
           if(esCaracter(s->tipoElemento)){
              string temp4= c3d->getTemporal();
              c3d->addCodigo(temp4+"=pool["+temp2_5+"];//valor que trael en el pool el caracter "+ nombreArreglo);
              v->tipo= apuntadorArreglo->tipo;
              v->valor= temp4;
              v->setReferencia("pool",temp2_5);
              return v;
           }
           v->tipo= apuntadorArreglo->tipo;
           v->valor= temp2_5;
           v->setReferencia("heap",apuntadorArreglo->valor);
           return v;

        }
    }
    Valor *v = new Valor();
    return v;
}


void GeneradorCodigo::resolverLeerTeclado(nodoArbol *sentencia, string clase, string funcion){
    string nombreVar = sentencia->valor;
    nodoArbol *expresion = sentencia->hijos.at(0);
    int esAtributo= tabla->esAtributo(nombreVar,ambiente);
    Simbolo *simb = tabla->obtenerSimbolo(nombreVar,ambiente, esAtributo);
    if(simb!= NULL){
        bool bandera = false;
        string tipo="";
        if(esInt(simb->tipoElemento)){
            bandera = true;
            tipo ="1";

        }else if(esCaracter(simb->tipoElemento)){
            bandera = true;
            tipo ="2";

        }else if(esDecimal(simb->tipoElemento)){
            bandera = true;
            tipo ="3";

        }else if(esBool(simb->tipoElemento)){
            bandera = true;
            tipo ="4";

        }else{
            erroresEjecucion->insertarError(SEMANTICO, "Tipo no valido para guarda en una variable por leer teclado "+ simb->tipoElemento, sentencia->fila, sentencia->columna);
        }


        if(bandera){
            string tPosicion="";
            int pos=-1;
            if(esAtributo ==1){
                //es atributo
                string t1,t2,t3;

                pos= tabla->obtenerPosGlobal(nombreVar,this->ambiente);
                string pos1= intToCadena(pos);
                t1=c3d->getTemporal();
                t2= c3d->getTemporal();
                t3= c3d->getTemporal();
                tPosicion = c3d->getTemporal();
                /*==== Obtenieidno el Acceso de un atributo =====*/
                c3d->addCodigo(t1+" = p + 0; //posicion del this");
                c3d->addCodigo(t2+ "= stack[ "+ t1+"]; //apuntador al heap");
                c3d->addCodigo(t3+" = heap[ "+t2+"];//apuntador donde inicia el objeto");
                c3d->addCodigo(tPosicion+" = "+t3+" + "+pos1+";//posicion real de atributo "+ nombreVar);
            }else if(esAtributo == 2){
                //es local
                tPosicion= c3d->getTemporal();
                pos= tabla->obtenerPosLocal(nombreVar, ambiente);
                string pos1= intToCadena(pos);
                c3d->addCodigo(tPosicion+" = p + "+pos1+"; //pos real de var local "+nombreVar);
            }

            if(pos!= -1){
                Valor *resp = this->resolverExpresion(expresion,clase, funcion);
                resp= this->condiABool(resp);
                if(esCadena(resp->tipo)){
                    string tVal = c3d->getTemporal();
                    c3d->addCodigo("heap[h]="+tipo+"; //guardando de que tipo es la airable a leer en el teclado");
                    c3d->addCodigo("h=h+1;");
                    c3d->addCodigo("heap[h]="+resp->valor+"; //valor de la cadena");
                    c3d->addCodigo("h=h+1;");
                    c3d->addCodigo("$$_leerTeclado();");
                    c3d->addCodigo(tVal+"=heap[h];");
                    c3d->addCodigo("h=h+1;");
                    if(esAtributo==1){
                        c3d->addCodigo("heap["+tPosicion+"]="+tVal+"; //asignando el valor ingresado desde el teclado");
                    }else{
                        c3d->addCodigo("stack["+tPosicion+"]="+tVal+"; //asignando el valor ingresado desde el teclado");
                    }

                }else{
                    erroresEjecucion->insertarError(SEMANTICO, "La expresion debe de ser tipo cadena para poder mostrar el mensaje en leer el teclado", sentencia->fila, sentencia->columna);
                }

            }
        }
    }

}


Valor* GeneradorCodigo::resolverTernario(nodoArbol *instruccion, string clase, string funcion){
    nodoArbol *condicion = instruccion->hijos.at(0);
    nodoArbol *expresion1 = instruccion->hijos.at(1);
    nodoArbol *expresion2 = instruccion->hijos.at(2);
    Valor *resultado = new Valor();
    c3d->addCodigo("// Resolviendo ternario ");
    Valor *v = this->resolverExpresion(condicion, clase, funcion);
    v = this->convertirACondicion(v);
    if(esCondicion(v->tipo)){
        Valor *resp1 = this->resolverExpresion(expresion1, clase, funcion);
        resp1= this->condiABool(resp1);
        Valor *resp2 = this->resolverExpresion(expresion2, clase, funcion);
        resp2 = this->condiABool(resp2);
        if(sonIguales(resp1->tipo, resp2->tipo)){
            string t = c3d->getTemporal();
            string l1 = c3d->getEtiqueta();
            c3d->addCodigo(v->cond->codigo);
            c3d->addCodigo(v->cond->getEtiquetasVerdaderas()+" // etiquetas verdaderas");
            c3d->addCodigo(t+"= "+ resp1->valor+"; // valor 1 de ambas  expresiones");
            c3d->addCodigo("goto "+l1+"; //etiqueta de salidad");
            c3d->addCodigo(v->cond->getEtiquetasFalsas()+" //etiquetas falsas");
            c3d->addCodigo(t+"="+resp2->valor+"; // valor 2 de las epxreiones");
            c3d->addCodigo(l1+"://etiqueta de salida");
            c3d->addCodigo("// fin de ternario");
            resultado->valor= t;
            resultado->tipo= resp1->tipo;
            return resultado;


        }else{
           erroresEjecucion->insertarError(SEMANTICO, "Los tipos de ambas expresiones deben de ser los mismo y son "+ resp1->tipo+" y "+resp2->tipo, expresion1->fila, expresion1->columna);
           return resultado;
        }

    }else{
        erroresEjecucion->insertarError(SEMANTICO, "Expresion no valida para realizar un ternario "+ v->tipo,condicion->fila, condicion->columna);
        return resultado;
    }


}



void GeneradorCodigo::asignarCadenaArregloPorPosicion(Valor *nodoOperando, Valor *cadena, Ambito *ambiente, string clase, string funcion){

    string tempSize = this->c3d->getTemporal();
    string tempPos0= nodoOperando->referencia;

    c3d->addCodigo(tempSize+"="+tempPos0+"-0;");
    if(sonIguales(nodoOperando->estructura, "stack")){
        string temp2, temp3;
        temp2= c3d->getTemporal();
        temp3= c3d->getTemporal();
        tempSize= c3d->getTemporal();
        tempPos0= c3d->getTemporal();
        c3d->addCodigo("//------- Arreglo local asignar---------");
        string l2, l3, l4,l5;
        l2= temp2+"=stack["+nodoOperando->referencia+"]; // apuntador al heap del arreglo ";
        l3 = temp3+"=heap["+temp2+"]; // apuntador del heap al heap donde inciia la cadena";
        l4 =tempSize+"=heap["+temp3+"];//size del arreglo ";
        l5= tempPos0+"="+temp3+"+1; //pos 0 donde inicial el arreglo";
        c3d->addCodigo(l2);
        c3d->addCodigo(l3);
        c3d->addCodigo(l4);
        c3d->addCodigo(l5);
    }else{
        string temp5 = c3d->getTemporal();
        tempSize= c3d->getTemporal();
        tempPos0= c3d->getTemporal();
        c3d->addCodigo("// -------------------- Arreglo global asignar :) ");
        string l5 = temp5 +"= heap["+tempSize+"];//inicia el arreglo ";
        string l6 = tempSize+"=heap["+temp5+"]; //size del arreglo ";
        string l7 = tempPos0+"="+temp5+"+1; //pos 0 del arreglo ";
        c3d->addCodigo(l5);
        c3d->addCodigo(l6);
        c3d->addCodigo(l7);

    }


    if(esCadena(cadena->tipo)){
        string tempApuntCadena = cadena->valor;
        string temp1 = c3d->getTemporal();
        string tempSizeCadena = c3d->getTemporal();
        string tempPos0Cadena = this->c3d->getTemporal();
        string tempoCaracterCadena = this->c3d->getTemporal();

        c3d->addCodigo(temp1+"=heap["+tempApuntCadena+"];");
        c3d->addCodigo(tempSizeCadena+"=pool["+temp1+"];// size de la cadena");
        c3d->addCodigo(tempPos0Cadena+"= "+temp1+"+1; // pos 0 de la cadena ");
        c3d->addCodigo(tempoCaracterCadena+"= pool["+tempPos0Cadena+"]; //caracter actual ");
        string l1v = c3d->getEtiqueta();
        string l1f = c3d->getEtiqueta();
        string l2v = c3d->getEtiqueta();
        string l2f = c3d->getEtiqueta();
        c3d->addCodigo("if("+tempSizeCadena+"<="+tempSize+") goto "+ l1v+";\n goto "+ l1f+";");
        c3d->addCodigo(l1v+":");
        c3d->addCodigo("if("+tempoCaracterCadena+"!=0) goto "+ l2v+";\n goto "+l2f+";");
        c3d->addCodigo(l2v+":");
        c3d->addCodigo("heap["+tempPos0+"]=s;");
        c3d->addCodigo("pool[s]="+tempoCaracterCadena+";");
        c3d->addCodigo("s=s+1;");
        c3d->addCodigo(tempPos0+"="+tempPos0+"+1;");
        c3d->addCodigo(tempPos0Cadena+"= "+tempPos0Cadena+"+1;");
        c3d->addCodigo(tempoCaracterCadena+"=pool["+tempPos0Cadena+"];");
        c3d->addCodigo("goto "+l1v+";");
        c3d->addCodigo(l2f+":");
        c3d->addCodigo(l1f+":");

    }

}
