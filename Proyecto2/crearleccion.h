#ifndef CREARLECCION_H
#define CREARLECCION_H

#include <sstream>
#include <iostream>
#include <string.h>
#include <string>
#include <QMainWindow>
#include "leccion.h"
#include "listalecciones.h"
#include "pantalla1.h"

namespace Ui {
class CrearLeccion;
}

class CrearLeccion : public QMainWindow
{
    Q_OBJECT

public:
    explicit CrearLeccion(QWidget *parent = 0);
    ~CrearLeccion();
     string tipoLeecion;
     ListaLecciones *lecciones;


private slots:
     void on_btnCrear_clicked();

private:
    Ui::CrearLeccion *ui;
};

#endif // CREARLECCION_H
