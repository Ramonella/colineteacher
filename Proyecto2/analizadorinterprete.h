#ifndef ANALIZADORINTERPRETE_H
#define ANALIZADORINTERPRETE_H


#include <string.h>
#include <string>
#include <sstream>
#include <QList>
#include "nodoarbol.h"
#include "listatemporales.h"
#include <QInputDialog>
#include <QString>
#include <QStringList>
#include <QDir>
#include "principal.h"
#include "listacodigo.h"


using namespace std;

class analizadorInterprete
{
public:
    int noFilas;
    ListaCodigo *codigoT;
    Principal *formP;
    ListaTemporales* temporales;
    nodoArbol* raiz3D;
    QList<nodoArbol*> sentencias;
    double heap[20000];
    double stack[2000];
    double pool[20000];
    string cadenaImpresion="";
    string etiqueta="";
    string ir_a="";
    bool modo;
    void ejecutarAnalizador(string codigo);
    string escribirStack();
    string escribirHeap();
    string escribirPool();
    string escribirTemporales();
    void ejecutar3D(string codigo,string principal);
    void ejecutarInstruccion(nodoArbol* instruccion);

    double resolverValor(nodoArbol* val);
    void resolverOperacion(nodoArbol* sentencia);
    void resolverRelacional(nodoArbol* sentencia);
    bool evaluarCondicion(string simbolo, nodoArbol* val1, nodoArbol* val2);
    void resolverEDD(int tipo,nodoArbol* sentencia);
    void imprimir(nodoArbol *sentencia);
    void agregarImpresion(string valor);
    string imprimir_str(int pos);
    void resolverEtiqueta(nodoArbol *sentencia);
    void saltar(string nombre);
    void llamadaFuncion(string nombreFuncion);
    bool esIgual(string cad1, string cad2);
    void resolverTransferencia(nodoArbol *nodo);
    analizadorInterprete(Principal *p, ListaCodigo *codigo, bool modo);
    string convertirString(double numero);
    void iniciarEDDs();
    void inStr();
    void inNum();
    void leerTeclado();
    void guarTemp(int linea);

};

#endif // ANALIZADORINTERPRETE_H
