#ifndef MPRINCIPAL_H
#define MPRINCIPAL_H

#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <QList>
#include "nodoarbol.h"
#include "mprincipal.h"
#include "listafunciones.h"
#include "constantes.h"
class mPrincipal
{
public:
    nodoArbol*sentencias;
    mPrincipal(nodoArbol* sentencias);
    int fila;
};

#endif // MPRINCIPAL_H
