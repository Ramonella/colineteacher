#ifndef PANTALLA1_H
#define PANTALLA1_H

#include <QMainWindow>
#include "constantes.h"
#include "listalecciones.h"

namespace Ui {
class pantalla1;
}

class pantalla1 : public QMainWindow
{
    Q_OBJECT

public:
    explicit pantalla1(QWidget *parent = 0);
    ~pantalla1();
    ListaLecciones *lecciones;
    pantalla1(ListaLecciones *lecciones);
    void mostrarLeccionesBotones();

private slots:
    void on_Editor_clicked();

    void on_btbACoach_clicked();

    void on_listWidget_doubleClicked(const QModelIndex &index);

    void on_btbGCoach_clicked();

    void on_btnBuscar_clicked();

    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::pantalla1 *ui;
};

#endif // PANTALLA1_H
