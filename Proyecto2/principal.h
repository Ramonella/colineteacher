#ifndef PRINCIPAL_H
#define PRINCIPAL_H

#include <QMainWindow>
#include "generadorcodigo.h"
#include "listaerrores.h"
#include <string>
#include <string.h>
#include<iostream>
#include "listacodigo.h"
#include "listaclases.h"
#include <QThread>
#include "myworker.h"
#include "listalecciones.h"

namespace Ui {
class Principal;
}

class Principal : public QMainWindow
{
    Q_OBJECT

public:

    explicit Principal(QWidget *parent = 0);
    ~Principal();
    ListaErrores *erroresPrograma = new ListaErrores();
    string nombrePrincipal;
    ListaCodigo *listaDeCodigo= new ListaCodigo();
    ListaClases *clases = new ListaClases();
    QThread *m_thread;
    MyWorker *m_worker;
    bool threadStarted;
    bool initThread;
    ListaLecciones *lecciones;
    void cLeccion(QString cad);
    int fact(int n);
    ListaCodigo *lista3D= new ListaCodigo();


    void automatico3D();
    void automaticoAlto();
    void deleayLento();
    void deleayRapido();
    void automticoAltoNivel();
    void lineaLinea3D();

    void generar3D();
    void ejecutar3D(bool modo);


private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_actionRegresar_triggered();

    void on_btn_automatico3D_clicked();

    void on_btn_paso3D_clicked();

    void on_btn_detener3D_clicked();

private:
    Ui::Principal *ui;
};

#endif // PRINCIPAL_H
