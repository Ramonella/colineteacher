#include "listafunciones.h"

ListaFunciones::ListaFunciones()
{

}

bool ListaFunciones::existeFuncion(Funcion *nueva){

    string firmaNueva = nueva->obtenerFirma();
    Funcion *temporal;
    string temporalFirma;

    if(nueva->esConstructor){

        if(sonIguales(nueva->nombreFuncion,nueva->nombreClase)){

            for(int i=0; i< this->funciones.length(); i++){
                temporal = this->funciones.at(i);
                temporalFirma = temporal->obtenerFirma();
                if(sonIguales(firmaNueva,temporalFirma)){
                    return true;
                }
            }
        }else{

        }

    }else{
        for(int i=0; i<this->funciones.length(); i++){
            temporal= this->funciones.at(i);
            temporalFirma = temporal->obtenerFirma();
            if(sonIguales(temporalFirma,firmaNueva)){
                return true;
            }
        }

    }

    return false;
}


void ListaFunciones::insertarFuncion(Funcion *nueva){

    bool existe = this->existeFuncion(nueva);
    if(!existe){
        this->funciones.push_back(nueva);
    }else{

    }
}

void ListaFunciones::agregarNombreClase(string nombre){
    for(int i=0; i<this->funciones.length(); i++){
        funciones.at(i)->nombreClase= nombre;
    }
}



void ListaFunciones::insertarFuncionHeredad(Funcion *nueva,string clase){

    int fila = nueva->fila;
    bool retornoArreglo = nueva->retornoArreglo;
    int noDimensionesRetorno = nueva->noDimensionesRetorno;
    bool sobreEscrita = nueva->sobreEscrita;
    string visibilidad= nueva->visibilidad;
    string tipo= nueva->tipo;
    string nombreFuncion = nueva->nombreFuncion;
    QList<nodoArbol*> parametros = nueva->parametros;
    nodoArbol* sentencias= nueva->sentencias;
    bool esConstructor= nueva->esConstructor;
    nodoArbol *par = new nodoArbol("","");
    par->hijos= parametros;
      Funcion *nFun = new Funcion(visibilidad,tipo,nombreFuncion,par,sentencias);
    nFun->fila = fila;
    nFun->retornoArreglo= retornoArreglo;
    nFun->noDimensionesRetorno= noDimensionesRetorno;
    nFun->sobreEscrita= sobreEscrita;
    nFun->nombreClase= clase;
    nFun->esConstructor= esConstructor;
    this->insertarFuncion(nFun);



}
