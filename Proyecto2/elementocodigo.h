#ifndef ELEMENTOCODIGO_H
#define ELEMENTOCODIGO_H
#include <string>
#include <string.h>
#include <iostream>
#include <sstream>
using namespace std;


class elementoCodigo
{
public:
    int identificador;
    string codigo3D;
    string heap;
    string stack;
    string pool;
    string temporales;
    string erroresAlto;
    string erroresBajo;
    string impresion;
    void setCodigo(string cod, int no);



    elementoCodigo();
};

#endif // ELEMENTOCODIGO_H
