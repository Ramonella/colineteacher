#ifndef PARTICIPARLECCION_H
#define PARTICIPARLECCION_H

#include <QMainWindow>
#include<iostream>
#include <sstream>>
#include <string.h>
#include <string>
#include "listalecciones.h"

namespace Ui {
class participarLeccion;
}

class participarLeccion : public QMainWindow
{
    Q_OBJECT

public:
    explicit participarLeccion(QWidget *parent = 0);
    ~participarLeccion();
    void mostrarDatos();
    string titulo;
    string explicacion;
    string ejemplo;
    string tarea;
    string respuesta;
    ListaLecciones *lecciones;

private slots:
    void on_btnCompilar_clicked();

    void on_actionRegresar_2_triggered();

    void on_btnCalificar_clicked();

private:
    Ui::participarLeccion *ui;
};

#endif // PARTICIPARLECCION_H
