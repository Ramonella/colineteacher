#ifndef LISTAERRORES_H
#define LISTAERRORES_H
#include <string>
#include<string.h>
#include<sstream>
#include"error_.h"
#include<QList>
#include<fstream>


using namespace std;

class ListaErrores
{
public:
    QList<Error_*>errores;
    void insertarError(string tipo, string descripcion, int fila, int col);
    void insertarError2(string tipo, string descripcion);
    string imprimirErrores();
    ListaErrores();
};

#endif // LISTAERRORES_H
