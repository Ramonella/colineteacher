#ifndef CLASE_H
#define CLASE_H

#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <QList>
#include "nodoarbol.h"
#include "mprincipal.h"
#include "listafunciones.h"
#include "constantes.h"
#include "simbolo.h"
#include "ambito.h"
#include "funcion.h"
#include "listaerrores.h"

using namespace std;
class Clase
{
public:
    // Ambito *ambitos;
    int apuntador =0;
    int fila ;
    QList<Simbolo*>lista2;
    string nombreClase;
    string herencia;
    ListaFunciones *funciones;
    QList<nodoArbol*> atributos;
    mPrincipal *principal;
    ListaErrores *erroresPrograma;
    Clase();
    void insertarPrincipal(mPrincipal *metodo);
    QList<Simbolo*> generarSimbolosClase(ListaErrores *errores);
    QList<Simbolo*> generarSimbolosAtributos();
    void obtenersimbolosMetodo(QList<nodoArbol*> sentencias, Ambito *ambiente, QList<Simbolo*> parametros);
    void  simbMet(nodoArbol *sent, Ambito *ambito, QList<Simbolo*> parametros);
    void insertarFuncion(Funcion *nueva);
    void agregarNombreClaseFunciones(string nombreClase);
    void insertarAtributo(nodoArbol *nuevo);
    nodoArbol* crearAsignacion(nodoArbol* id, string tipo, nodoArbol *exp);
    nodoArbol* crearDeclaracion(nodoArbol *declaraion);
    string obtenerTipoSimbolo(string tipo);
    bool existeAtributo(string nombre, string clase, QList<Simbolo*> lista);
    bool existeSimbolo(QList<Simbolo*> lista, Simbolo *simb);
    int existeEnAmbitoLocal(QList<Simbolo*> lista, Ambito *ambito,string nombre, QList<Simbolo*>parametros);
    int existeLista(string cadenaAmbito, string nombre, QList<Simbolo*>lista);
    QList<Funcion*> obtenerFuncionesHeredadas();
    QList<nodoArbol*>obtenerAtributosHeredados();
    void insertarFuncionHeredada(Funcion *nueva);





};

#endif // CLASE_H
