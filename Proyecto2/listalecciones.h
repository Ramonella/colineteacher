#ifndef LISTALECCIONES_H
#define LISTALECCIONES_H
#include <string>
#include <iostream>
#include <sstream>
#include <string.h>
#include "leccion.h"
#include "constantes.h"
#include <QList>
using namespace std;
class ListaLecciones
{
public:
    QList<Leccion*> lecciones;
    void guardarLeccion(Leccion *nueva);
    bool existe(string nombre);
    ListaLecciones();
    Leccion* obtener(string nombre);
    QList<Leccion*>obtenerLeccionesTipo(string tipo);
    Leccion* obtenerPorNombre(string nombre);
};

#endif // LISTALECCIONES_H
