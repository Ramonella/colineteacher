%{
#include "scanner.h"
#include <iostream>
#include <QString>
#include "constantes.h"
#include "nodoarbol.h"
#include <string.h>
#include <string>
#include "clase.h"
#include "listaclases.h"
#include "mprincipal.h"
#include "funcion.h"
#include<sstream>
#include<fstream>
#include "listaerrores.h"

using namespace std;

 
//extern int yylineno=0;
extern int columna;
extern char *yytext;


extern int yylex(void);



ListaClases *nodoRaiz = new ListaClases();
ListaErrores *errores = new ListaErrores();

void setSalida(ListaClases* sal, ListaErrores *erro) {
nodoRaiz = sal;
errores = erro;
yylineno=1;
}



int yyerror(const char* mens){
	std::cout <<mens<<" "<<yytext<<yylineno<<"   "<<columna<< std::endl;
    string cad;
    while (*yytext != '\0') {
        string pp= string(1,*yytext);
        *yytext++;
        cad+=pp;
            }


    string cad2;
    while (*mens != '\0') {
        string pp= string(1,*mens);
        *mens++;
        cad2+=pp;
            }
    errores->insertarError("Sintactico",cad2+" "+cad,yylineno,columna);
    cout<<cad<<endl;
	return 0;
}


//nodoArbol* nodoRaiz = new nodoArbol("raiz","");


%}
%locations
//%error-verbose

%union{
char TEXT [256];
class nodoArbol *NODO;
class Clase *CLASE;
class ListaClases *LISTA_CLASES;
class mPrincipal *PRINC;
class Funcion *FUNC;
}

//Terminales
%token<TEXT> nada
%token<TEXT> punto
%token<TEXT> masMas
%token<TEXT> menosMenos
%token<TEXT> suma
%token<TEXT> resta
%token<TEXT> multiplicacion
%token<TEXT> division
%token<TEXT> potencia
%token<TEXT> caracter
%token<TEXT> cadena
%token<TEXT> identificador
%token<TEXT> entero
%token<TEXT> decimal
%token<TEXT> verdadero
%token<TEXT> falso
%token<TEXT> imprimir
%token<TEXT> ptoComa
%token<TEXT> abrePar1
%token<TEXT> cierraPar1
%token<TEXT> tipo_entero
%token<TEXT> tipo_decimal
%token<TEXT> tipo_caracter
%token<TEXT> tipo_booleano
%token<TEXT> publico
%token<TEXT> privado
%token<TEXT> protegido
%token<TEXT> vacio
%token<TEXT> igualIgual
%token<TEXT> menorIgual
%token<TEXT> mayorIgual
%token<TEXT> menor
%token<TEXT> mayor
%token<TEXT> distintoA
%token<TEXT> abreCor
%token<TEXT> cierraCor
%token<TEXT> abreLlave
%token<TEXT> cierraLlave
%token<TEXT> igual
%token<TEXT> not_
%token<TEXT> and_
%token<TEXT> or_
%token<TEXT> convertirACadena
%token<TEXT> convertirAEntero
%token<TEXT> clase
%token<TEXT> este
%token<TEXT> hereda
%token<TEXT> concatenar
%token<TEXT> importar
%token<TEXT> retornar
%token<TEXT> sobreescribir
%token<TEXT> principal
%token<TEXT> nuevo
%token<TEXT> coma
%token<TEXT> detener
%token<TEXT> mientras
%token<TEXT> si
%token<TEXT> continuar
%token<TEXT> interrogacion
%token<TEXT> dos_puntos
%token<TEXT> sino_si
%token<TEXT> sino
%token<TEXT> selecciona
%token<TEXT> caso
%token<TEXT> defecto
%token<TEXT> hacer
%token<TEXT> para
%token<TEXT> lista
%token<TEXT> pila
%token<TEXT> cola
%token<TEXT> leer_teclado

%token<TEXT> insertar
%token<TEXT> apilar
%token<TEXT> desapilar
%token<TEXT> encolar
%token<TEXT> desencolar
%token<TEXT> obtener
%token<TEXT> buscar
%token<TEXT> tamanio
%token<TEXT> mostrarEDD
%token<TEXT> masIgual
%token<TEXT> menosIgual
%token<TEXT> porIgual
%token<TEXT> divIgual



%type<LISTA_CLASES> INICIO
%type<NODO> EXPRESION
%type<NODO> VALOR
%type<NODO> ARITMETICA
%type<NODO> MUL
%type<NODO> POT
%type<NODO> UNARIO
%type<NODO> NEG
%type<NODO> INSTRUCCION
%type<NODO> INSTRUCCIONES
%type<NODO> IMPRIMIR

%type<FUNC> FUNCION
%type<NODO> CUERPO_FUNCION
%type<NODO> DECLARACION
%type<NODO> DECLA_ATRIBUTO
%type<NODO> PARAMETROS_FUNCION
%type<NODO> VISIBILIDAD
%type<NODO> TIPO_DATO
%type<NODO> OP_RELACIONAL
%type<NODO> RELACIONAL
%type<NODO> NOT
%type<NODO> AND
%type<NODO> OR
%type<NODO> DETENER
%type<NODO> CONTINUAR
%type<NODO> INSTANCIA
%type<NODO> DECLA_ARREGLO
%type<NODO> COL_ARREGLO
%type<NODO> RETORNO
%type<NODO> ID
%type<NODO> L_PARAMETROS;
%type<NODO> LISTA_EXPRESIONES
%type<NODO> PARAMETROS_LLAMADA
%type<NODO> CONCATENAR
%type<NODO> CONVERTIR_A_CADENA
%type<NODO> CONVERTIR_A_ENTERO
%type<NODO> IMPORTAR
%type<CLASE> LISTA_ELEMENTOS_CLASE
%type<NODO> MIENTRAS
%type<NODO> SI
%type<NODO> SINO
%type<NODO> SELECCIONA
%type<NODO> CASO
%type<NODO> DEFECTO
%type<NODO> HACER_MIENTRAS
%type<NODO> PARA
%type<NODO> DECLA_LISTA
%type<NODO> DECLA_PILA
%type<NODO> DECLA_COLA
%type<NODO> LEER_TECLADO
%type<NODO> IF
%type<NODO> SINO_SI
%type<NODO> L_SINO_SI
%type<NODO> CUERPO_SELECCIONA
%type<NODO> LISTA_SELECCIONA
%type<NODO> ELEMENTO_SELECCIONA
%type<NODO> ASIGNACION
%type<NODO> LLAMADA_FUNCION
%type<NODO> ACCESO
%type<NODO> POS_ARREGLO
%type<LISTA_CLASES> LISTA_CLASES_
%type<CLASE> CLASE_
%type<PRINC> PRINCIPAL
%type<CLASE>CUERPO_CLASE
%type<NODO> ATRI
%type<NODO> ESTE
%type<NODO> TERNARIO
%type<NODO> NULO
%type<NODO> MOSTRAR_EDD
%type<NODO> COR_VACIOS
%type<NODO> IGUALES




%left igualIgual distintoA menor menorIgual mayor mayorIgual
%start INICIO
%%


INICIO: LISTA_CLASES_{ cout<<"aceptada"<<endl;nodoRaiz->clasesArchivo= $1->clasesArchivo; nodoRaiz->fila = yylineno; nodoRaiz->esExpresion=false; }
    |EXPRESION {nodoRaiz->esExpresion = true; nodoRaiz->expresion= $1;};


LISTA_CLASES_: CLASE_{ 
    $$ = new ListaClases(); 
    $$->insertarClase($1); 
   
    }
    |LISTA_CLASES_ CLASE_ {
        $$= $1; $$->insertarClase($2);
       
        }
    |IMPORTAR
        {
            $$ = new ListaClases(); 
            $$->addImportacion($1);
            
        }
    |LISTA_CLASES_ IMPORTAR
        {
            $$=$1;
            $$->addImportacion($2);
           

        };
   //|error {yyerror("Error al insertar clase ");};

CLASE_:  clase identificador CUERPO_CLASE
        {
            $$=$3;
            $$->nombreClase=$2;
            $$->agregarNombreClaseFunciones($2);
            
            
        }
    |clase identificador hereda  identificador CUERPO_CLASE
        {
            $$=$5;
            $$->nombreClase=$2;
            $$->herencia=$4;
            $$->agregarNombreClaseFunciones($2);
            
            
        }
        |VISIBILIDAD clase identificador CUERPO_CLASE
        {
            $$=$4;
            $$->nombreClase=$3;
            $$->agregarNombreClaseFunciones($3);

        }

        |VISIBILIDAD clase identificador hereda identificador CUERPO_CLASE
        {
            $$=$6;
            $$->nombreClase=$3;
            $$->herencia=$5;
            $$->agregarNombreClaseFunciones($3);

        };


CUERPO_CLASE: abreLlave LISTA_ELEMENTOS_CLASE cierraLlave
        {
            $$=$2;
            
        }
    |abreLlave cierraLlave{$$= new Clase();}
    |abreLlave error cierraLlave {yyerror("Error al generar cuerpo de la clase ");};


IMPORTAR: importar abrePar1 cadena cierraPar1 ptoComa
    {
        $$= new nodoArbol("IMPORTAR",$3);
        $$->fila = @1.first_line;
        $$->columna = columna;
    };


PRINCIPAL: principal abrePar1 cierraPar1 CUERPO_FUNCION{$$ = new mPrincipal($4);};

LISTA_ELEMENTOS_CLASE: FUNCION
        {
            $$= new Clase();
            $$->insertarFuncion($1);
            
        }
    |sobreescribir FUNCION
        {
            $$= new Clase();
            $2->sobreEscrita= true;
            $$->insertarFuncion($2);
            
        }
    |PRINCIPAL
        {
            $$= new Clase();
            $$->insertarPrincipal($1);
            
        }

    |DECLA_ATRIBUTO ptoComa{
        $$= new Clase();
        $$->insertarAtributo($1);
       $$->fila = @1.first_line; 
    }
    |LISTA_ELEMENTOS_CLASE DECLA_ATRIBUTO ptoComa
        {
            $$=$1;
            $$->insertarAtributo($2);
            
        }
    |LISTA_ELEMENTOS_CLASE sobreescribir FUNCION
        {
            $$= $1;
            $3->sobreEscrita= true;
            $$->insertarFuncion($3);
            
        }
    |LISTA_ELEMENTOS_CLASE FUNCION
        {
        $$= $1;
        $$->insertarFuncion($2);
       

        }
    |LISTA_ELEMENTOS_CLASE PRINCIPAL
        {
            $$= $1;
            $$->insertarPrincipal($2);
            
        };
   // |error{ yyerror("Error de sintaxis en elemento de la clase ");

//};


FUNCION: VISIBILIDAD TIPO_DATO identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion ($1->valor,$2->valor,$3,$4,$5);
            $$->fila = @1.first_line; 
            
        }
    |VISIBILIDAD vacio identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion ($1->valor,VACIO,$3,$4,$5);
            $$->fila = @1.first_line;
            
        }

    |TIPO_DATO identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion (PUBLICO,$1->valor,$2,$3,$4);
            $$->fila = @1.first_line;
            
        }
    |vacio identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion (PUBLICO,VACIO,$2,$3,$4);
            $$->fila = @1.first_line;
            
        }

    |VISIBILIDAD identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion ($1->valor,VACIO,$2,$3,$4);
            $$->cambiarAConstructor();
            $$->fila = @1.first_line;
            
        }
    |identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion (PUBLICO,VACIO,$1,$2,$3);
            $$->cambiarAConstructor();
            $$->fila = @1.first_line;
            
        }
     |   VISIBILIDAD TIPO_DATO COR_VACIOS identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
             $$ = new Funcion ($1->valor,$2->valor,$4,$5,$6);
             $$->setArregloRetorno();
             $$->setNumeroDimenesionesRetorno($3);
             $$->fila = @1.first_line;
             
        }
     |   TIPO_DATO COR_VACIOS identificador PARAMETROS_FUNCION CUERPO_FUNCION
        {
            $$ = new Funcion (PUBLICO,$1->valor,$3,$4,$5);
            $$->setArregloRetorno();
            $$->setNumeroDimenesionesRetorno($2);
            $$->fila = @1.first_line;
            
        };


COR_VACIOS: abreCor cierraCor
         {
             $$= new nodoArbol("COR_VACIOS","");
             nodoArbol *n= new nodoArbol("COR_VACIOS","");  
             $$->addHijo(n);
             $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |COR_VACIOS abreCor cierraCor
        {
             $$= $1;
             nodoArbol *n= new nodoArbol("COR_VACIOS","");  
             $$->addHijo(n);
             $$->fila = @1.first_line;
        $$->columna = columna;
        };

LISTA_EXPRESIONES: OR{$$= new nodoArbol("LISTA_EXPRESION",""); $$->addHijo($1);$$->fila = @1.first_line;
        $$->columna = columna;}
    |LISTA_EXPRESIONES coma OR{$$=$1; $$->addHijo($3);$$->fila = @1.first_line;
        $$->columna = columna;};
   // |error {yyerror("Error al ingresar en lista de expresiones ");};

  PARAMETROS_LLAMADA: abrePar1 cierraPar1{$$= new nodoArbol("LISTA_EXPRESION",""); $$->fila = @1.first_line;
        $$->columna = columna;}
    |abrePar1 LISTA_EXPRESIONES cierraPar1{$$=$2; $$->fila = @1.first_line;
        $$->columna = columna;};

    INSTANCIA: nuevo identificador PARAMETROS_LLAMADA{$$= new nodoArbol("INSTANCIA", $2);
    $$->addHijo($3); $$->fila = @1.first_line;
        $$->columna = columna;};  

TIPO_DATO: tipo_entero {$$= new nodoArbol(T_ENTERO,T_ENTERO); $$->fila = @1.first_line;
        $$->columna = columna;}
	|tipo_decimal{$$= new nodoArbol(T_DECIMAL,T_DECIMAL); $$->fila = @1.first_line;
        $$->columna = columna;}
	|tipo_caracter{$$= new nodoArbol(T_CARACTER,T_CARACTER); $$->fila = @1.first_line;
        $$->columna = columna;}
	|tipo_booleano{$$= new nodoArbol(T_BOOLEANO,T_BOOLEANO); $$->fila = @1.first_line;
        $$->columna = columna;}
	|identificador{$$= new nodoArbol($1,$1); $$->fila = @1.first_line;
        $$->columna = columna;};

VISIBILIDAD: protegido{$$= new nodoArbol($1,$1); $$->fila = @1.first_line;
        $$->columna = columna;}
	|publico{$$= new nodoArbol($1,$1); $$->fila = @1.first_line;
        $$->columna = columna;}
    |privado{$$= new nodoArbol($1,$1); $$->fila = @1.first_line;
        $$->columna = columna;};

L_PARAMETROS: DECLARACION {$$= new nodoArbol("PARAMETROS","");$$->addHijo($1); $$->fila = @1.first_line;
        $$->columna = columna;}
    |L_PARAMETROS coma DECLARACION
    {
        $$= $1;
        $$->addHijo($3);
        $$->fila = @1.first_line;
        $$->columna = columna;
    }
    |DECLA_ARREGLO {$$= new nodoArbol("PARAMETROS", ""); 
    $$->addHijo($1); $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |L_PARAMETROS coma DECLA_ARREGLO
        {
            $$=$1;
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };



PARAMETROS_FUNCION: abrePar1 L_PARAMETROS cierraPar1{$$=$2; $$->fila = @1.first_line;
        $$->columna = columna;}
    |abrePar1 cierraPar1{$$= new nodoArbol("PARAMETROS",""); $$->fila = @1.first_line;
       $$->columna = columna;}
   |abrePar1 error cierraPar1 {yyerror("Error al ingresar parametros de funcion ");};

CUERPO_FUNCION: abreLlave cierraLlave {$$= new nodoArbol("INSTRUCCIONES",""); $$->fila = @1.first_line;
        $$->columna = columna;}
	|abreLlave INSTRUCCIONES cierraLlave{$$=$2; $$->fila = @1.first_line;
        $$->columna = columna;}
    |abreLlave error cierraLlave {yyerror("Error en instruccion ");};


INSTRUCCIONES: INSTRUCCION {$$ = new nodoArbol("INSTRUCCIONES",""); $$->addHijo($1);}
    |INSTRUCCIONES INSTRUCCION{$$=$1; $$->addHijo($2);};
    //|error {yyerror("Error al crear instrucion ");};

INSTRUCCION: IMPRIMIR{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
	|DECLARACION ptoComa{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}
	|DECLA_ARREGLO ptoComa {$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}
    |CONTINUAR{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}
    |RETORNO{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}
    |DETENER{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}
    |CONCATENAR{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |CONVERTIR_A_CADENA ptoComa{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |CONVERTIR_A_ENTERO ptoComa{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |SI{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |MIENTRAS{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |SELECCIONA{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |HACER_MIENTRAS{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |PARA{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |DECLA_COLA ptoComa{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |DECLA_LISTA ptoComa{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |DECLA_PILA ptoComa{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |ACCESO ptoComa {$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |LEER_TECLADO{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |MOSTRAR_EDD{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |ASIGNACION ptoComa {$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;}
    |LLAMADA_FUNCION ptoComa{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;};
    //|error {yyerror("Error al crear instruccion ");};



DECLA_ATRIBUTO: TIPO_DATO ID igual EXPRESION
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO","1");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($4);
        $$->addHijo(v);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
	|TIPO_DATO ID 
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO","2");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo(v);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }

	|TIPO_DATO ID igual INSTANCIA
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO","3");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($4);
        $$->addHijo(v);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
    | TIPO_DATO ID COL_ARREGLO 
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO","4");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo(v);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
	|TIPO_DATO ID COL_ARREGLO igual EXPRESION 
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO","5");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($5);
        $$->addHijo(v);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
    | VISIBILIDAD TIPO_DATO ID igual EXPRESION
    {
        $$= new nodoArbol("DECLA_ATRIBUTO","1");
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($5);
        $$->addHijo($1);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
	| VISIBILIDAD TIPO_DATO ID 
    {
        $$= new nodoArbol("DECLA_ATRIBUTO","2");
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($1);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }

	| VISIBILIDAD TIPO_DATO ID igual INSTANCIA
    {
        $$= new nodoArbol("DECLA_ATRIBUTO","3");
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($5);
        $$->addHijo($1);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
    | VISIBILIDAD TIPO_DATO ID COL_ARREGLO 
    {
        $$= new nodoArbol("DECLA_ATRIBUTO","4");
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($4);
        $$->addHijo($1);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
	| VISIBILIDAD TIPO_DATO ID COL_ARREGLO igual EXPRESION 
    {
        $$= new nodoArbol("DECLA_ATRIBUTO","5");
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($4);
        $$->addHijo($6);
        $$->addHijo($1);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
    |VISIBILIDAD DECLA_LISTA
    { 
        nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO", "6");
        $$->addHijo($1);
          $$= new nodoArbol("DECLA_ATRIBUTO", "6");
          $$->addHijo($2);
          $$->addHijo($1);
          $$->fila = @1.first_line;
        $$->columna = columna;
      }
      |VISIBILIDAD DECLA_COLA
      {
          $$= new nodoArbol("DECLA_ATRIBUTO", "7");
          $$->addHijo($2);
          $$->addHijo($1);
          $$->fila = @1.first_line;
        $$->columna = columna;
      }
      |VISIBILIDAD DECLA_PILA
      {
          $$= new nodoArbol("DECLA_ATRIBUTO", "8");
          $$->addHijo($2);
          $$->addHijo($1);
          $$->fila = @1.first_line;
        $$->columna = columna;
      }
      |DECLA_LISTA
      {
    nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
    $$= new nodoArbol("DECLA_ATRIBUTO", "6");
    $$->addHijo($1);
    $$->addHijo(v);
    $$->fila = @1.first_line;
        $$->columna = columna;


    }
    |DECLA_COLA
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO", "7");
        $$->addHijo($1);
        $$->addHijo(v);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
    |DECLA_PILA
    {
        nodoArbol *v = new nodoArbol(PUBLICO, PUBLICO);
        $$= new nodoArbol("DECLA_ATRIBUTO", "8");
        $$->addHijo($1);
        $$->addHijo(v);
$$->fila = @1.first_line;
        $$->columna = columna;
    };






/*================================= INSTRUCCIONES ===============================*/

IGUALES: divIgual {$$= new nodoArbol("/=","/=");}
    |masIgual {$$= new nodoArbol("+=","+=");}
    |menosIgual {$$= new nodoArbol("-=","-=");}
    |porIgual {$$= new nodoArbol("*=","*=");}
    |igual {$$= new nodoArbol("=","=");};

ASIGNACION: ID masMas
        {
            $$= new nodoArbol("ASIGNACION", "1");
            nodoArbol *num = new nodoArbol(T_ENTERO, "1");
            nodoArbol *exp = new nodoArbol("SUMA","");
            nodoArbol *igual = new nodoArbol("=","=");
            exp->addHijo($1);
            exp->addHijo(num);
            $$->addHijo($1);
            $$->addHijo(igual);
            $$->addHijo(exp);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |ID menosMenos
        {
            $$= new nodoArbol("ASIGNACION", "1");
            nodoArbol *num = new nodoArbol(T_ENTERO, "1");
            nodoArbol *exp = new nodoArbol("RESTA","");
            nodoArbol *igual = new nodoArbol("=","=");
            exp->addHijo($1);
            exp->addHijo(num);
            $$->addHijo($1);
            $$->addHijo(igual);
            $$->addHijo(exp);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |ESTE IGUALES EXPRESION
        {
            $$= new nodoArbol("ASIGNACION", "2");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |ACCESO IGUALES EXPRESION
        {
            $$= new nodoArbol("ASIGNACION", "3");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |ID IGUALES INSTANCIA{
        $$= new nodoArbol("ASIGNACION", "1");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
    }
    |ID IGUALES EXPRESION
        {
            $$= new nodoArbol("ASIGNACION", "1");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }

    |POS_ARREGLO IGUALES EXPRESION
        {
            $$= new nodoArbol("ASIGNACION", "4");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
        |POS_ARREGLO IGUALES INSTANCIA{
            $$ = new nodoArbol("ASIGNACION","5");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

        };
 
    

MOSTRAR_EDD: mostrarEDD abrePar1 EXPRESION cierraPar1 ptoComa
    {
        $$= new nodoArbol("MOSTRAR_EDD", "");
        $$->addHijo($3);
        $$->fila = @1.first_line;
        $$->columna = columna;
    };

DECLARACION: TIPO_DATO ID igual EXPRESION
    {
        $$= new nodoArbol("DECLARACION","1");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($4);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
	|TIPO_DATO ID 
    {
        $$= new nodoArbol("DECLARACION","2");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }

	|TIPO_DATO ID igual INSTANCIA
    {
        $$= new nodoArbol("DECLARACION","3");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($4);
        $$->fila = @1.first_line;
        $$->columna = columna;

    };


COL_ARREGLO: abreCor EXPRESION cierraCor {$$= new nodoArbol("COL_ARREGLO",""); $$->addHijo($2); $$->fila = @1.first_line;
        $$->columna = columna;}
	|COL_ARREGLO abreCor EXPRESION cierraCor{$$=$1; $$->addHijo($3); $$->fila = @1.first_line;
        $$->columna = columna;}
    |error {yyerror("Error al crear posicion de arreglo ");};

DECLA_ARREGLO: TIPO_DATO ID COL_ARREGLO 
    {
        $$= new nodoArbol("DECLA_ARREGLO","1");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($3);
        $$->fila = @1.first_line;
        $$->columna = columna;

    }
	|TIPO_DATO ID COL_ARREGLO igual EXPRESION 
    {
        $$= new nodoArbol("DECLA_ARREGLO","2");
        $$->addHijo($1);
        $$->addHijo($2);
        $$->addHijo($3);
        $$->addHijo($5);
        $$->fila = @1.first_line;
        $$->columna = columna;

    };

CONTINUAR: continuar ptoComa {$$= new nodoArbol("CONTINUAR","CONTINUAR"); $$->fila = @1.first_line;
        $$->columna = columna;};
DETENER: detener ptoComa{$$= new nodoArbol("DETENER","DETENER"); $$->fila = @1.first_line;
        $$->columna = columna;};
RETORNO: retornar EXPRESION ptoComa{$$= new nodoArbol("RETORNO","RETORNO"); $$->addHijo($2); $$->fila = @1.first_line;
        $$->columna = columna;};

IMPRIMIR: imprimir abrePar1 EXPRESION cierraPar1 ptoComa{$$= new nodoArbol("IMPRIMIR",""); $$->addHijo($3); $$->fila = @1.first_line;
        $$->columna = columna;};


CONCATENAR:  concatenar abrePar1 ID coma EXPRESION cierraPar1 ptoComa
    {
        $$= new nodoArbol("CONCATENAR", "1");
        $$->addHijo($3);
        $$->addHijo($5);
        $$->fila = @1.first_line;
        $$->columna = columna;
    }
    |concatenar abrePar1 ID coma EXPRESION coma EXPRESION cierraPar1 ptoComa
    {
        $$= new nodoArbol("CONCATENAR", "2");
        $$->addHijo($3);
        $$->addHijo($5);
        $$->addHijo($7);
        $$->fila = @1.first_line;
        $$->columna = columna;

    };

    CONVERTIR_A_CADENA: convertirACadena abrePar1 EXPRESION cierraPar1
        {
            $$= new nodoArbol("CONVERTIR_A_CADENA","");
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    CONVERTIR_A_ENTERO: convertirAEntero abrePar1 EXPRESION cierraPar1
        {
            $$= new nodoArbol("CONVERTIR_A_ENTERO","");
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };


    MIENTRAS: mientras abrePar1 EXPRESION cierraPar1 CUERPO_FUNCION
        {
            $$= new nodoArbol("MIENTRAS","");
            $$->addHijo($3);
            $$->addHijo($5);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    IF: si abrePar1 EXPRESION cierraPar1 CUERPO_FUNCION 
        {
            $$= new nodoArbol("SI_1","");
            $$->addHijo($3);
            $$->addHijo($5);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    SINO: sino CUERPO_FUNCION
        {
            $$= new nodoArbol("SINO","");
            $$->addHijo($2);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    SINO_SI: sino_si abrePar1 EXPRESION cierraPar1 CUERPO_FUNCION
        {
            $$= new nodoArbol("SI","1");
            $$->addHijo($3);
            $$->addHijo($5);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    L_SINO_SI: SINO_SI
        {
            $$= new nodoArbol("L_SINO_SI","");
            $$->addHijo($1);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
        |L_SINO_SI SINO_SI
        {
            $$= $1;
            $$->addHijo($2);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    SI: IF 
        {
            $$= new nodoArbol("SI","1");
            $$->addHijo($1);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
        |IF SINO
        {
            $$= new nodoArbol("SI","2");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
        |IF L_SINO_SI
        {
            $$= new nodoArbol("SI","3");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
        |IF L_SINO_SI SINO
        {
            $$= new nodoArbol("SI","4");
            $$->addHijo($1);
            $$->addHijo($2);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    SELECCIONA: selecciona abrePar1 EXPRESION cierraPar1 CUERPO_SELECCIONA
        {
            $$= new nodoArbol("SELECCIONA","");
            $$->addHijo($3);
            $$->addHijo($5);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    CASO: caso EXPRESION dos_puntos CUERPO_FUNCION
        {
            $$= new nodoArbol("CASO","");
            $$->addHijo($2);
            $$->addHijo($4);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    DEFECTO: defecto dos_puntos CUERPO_FUNCION
        {
            $$= new nodoArbol("DEFECTO", "");
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    ELEMENTO_SELECCIONA: CASO{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}  
        |DEFECTO{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};

    LISTA_SELECCIONA: ELEMENTO_SELECCIONA {$$= new nodoArbol("CUERPO_SELECCIONA",""); $$->addHijo($1); $$->fila = @1.first_line;
        $$->columna = columna;}
        |LISTA_SELECCIONA ELEMENTO_SELECCIONA{$$=$1; $$->addHijo($2);$$->fila = @1.first_line;
        $$->columna = columna;};

    CUERPO_SELECCIONA: abreLlave LISTA_SELECCIONA cierraLlave {$$=$2; $$->fila = @1.first_line;
        $$->columna = columna;}
        |abreLlave cierraLlave{$$= new nodoArbol("CUERPO_SELECCIONA",""); $$->fila = @1.first_line;
        $$->columna = columna;};

    HACER_MIENTRAS: hacer CUERPO_FUNCION mientras abrePar1 EXPRESION cierraPar1 ptoComa
        {
            $$= new nodoArbol("HACER_MIENTRAS","");
            $$->addHijo($5);
            $$->addHijo($2);
            $$->fila = @1.first_line;
            $$->columna = columna;
        };

    PARA: para abrePar1 DECLARACION ptoComa EXPRESION ptoComa ASIGNACION cierraPar1 CUERPO_FUNCION
        {
            $$= new nodoArbol("PARA","");
            $$->addHijo($3);
            $$->addHijo($5);
            $$->addHijo($7);
            $$->addHijo($9);
            $$->fila = @1.first_line;
            $$->columna = columna;
        };

    DECLA_LISTA: lista  identificador igual nuevo lista abrePar1 TIPO_DATO cierraPar1 
        {
            $$= new nodoArbol("DECLA_LISTA",$2);
            $$->addHijo($7);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    DECLA_COLA: cola identificador igual nuevo cola abrePar1 TIPO_DATO cierraPar1 
        {
            $$= new nodoArbol("DECLA_COLA",$2);
            $$->addHijo($7);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

    DECLA_PILA: pila identificador igual nuevo pila abrePar1 TIPO_DATO cierraPar1 
        {
            $$= new nodoArbol("DECLA_PILA",$2);
            $$->addHijo($7);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };
    
    LEER_TECLADO: leer_teclado abrePar1 EXPRESION coma identificador cierraPar1 ptoComa
        {
            $$ = new nodoArbol("LEER_TECLADO",$5);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        };


/*========================== EXPRESIONES ===========================================*/
OP_RELACIONAL: menor{$$= new nodoArbol("<","<");}
	|mayor{$$= new nodoArbol(">",">");}
	|menorIgual{$$= new nodoArbol("<=","<=");}
	|mayorIgual{$$= new nodoArbol(">=",">=");}
	|distintoA{$$= new nodoArbol("!=","!=");}
	|igualIgual{$$= new nodoArbol("==","==");};

EXPRESION: OR{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;}
    
    |TERNARIO{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};

OR: OR or_ AND
{
            $$= new nodoArbol("OR", "");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
	|AND{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};
 
AND: AND and_ NOT
{
            $$= new nodoArbol("AND", "");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
	|NOT{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};

NOT: not_ RELACIONAL{$$= new nodoArbol("NOT",""); $$->addHijo($2); $$->fila = @1.first_line;
        $$->columna = columna;}
	|RELACIONAL{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};


RELACIONAL: ARITMETICA OP_RELACIONAL ARITMETICA
        {
            $$= new nodoArbol("RELACIONAL", $2->valor);
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
	|ARITMETICA
        {
            $$= $1;
            $$->fila = @1.first_line;
        $$->columna = columna;
        };

ARITMETICA: ARITMETICA suma MUL
        {
            $$= new nodoArbol("SUMA","");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
	|ARITMETICA resta MUL
        {
            $$= new nodoArbol("RESTA","");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |MUL{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};

MUL: MUL multiplicacion POT
        {
            $$= new nodoArbol("MULTIPLICACION","");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
	|MUL division POT
        {
            $$= new nodoArbol("DIVISION","");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |POT{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};

POT: POT potencia UNARIO
        {
            $$= new nodoArbol("POTENCIA","");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |UNARIO{$$=$1; $$->fila = @1.first_line;
        $$->columna = columna;};


UNARIO: NEG masMas
        {
             nodoArbol *nEntero = new nodoArbol(T_ENTERO, "1");
            $$= new nodoArbol("SUMA","");
            $$->addHijo($1);
            $$->addHijo(nEntero);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
	|NEG menosMenos
        { nodoArbol *nEntero = new nodoArbol(T_ENTERO, "1");
            $$= new nodoArbol("RESTA","");
            $$->addHijo($1);
            $$->addHijo(nEntero);
            $$->fila = @1.first_line;
        $$->columna = columna;
        }
    |NEG{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;};


NEG: resta VALOR
        {

            nodoArbol *nEntero = new nodoArbol(T_ENTERO, "-1");
            $$= new nodoArbol("MULTIPLICACION","");
            $$->addHijo($2);
            $$->addHijo(nEntero);
            $$->fila = @2.first_line;
            $$->columna = columna;
        }
|    VALOR{$$=$1;$$->fila = @1.first_line;
        $$->columna = columna;};

VALOR: entero{$$ = new nodoArbol(T_ENTERO, $1);$$->fila = @1.first_line;
        $$->columna = columna;}
    |decimal{$$ = new nodoArbol(T_DECIMAL, $1);$$->fila = @1.first_line;
        $$->columna = columna;}
    |cadena {
                string c ="";
                int t = strlen($1);
                int indice = 1;
                while(indice < t-1){
                c = c + $1[indice];
               indice++;
                }
                $$ = new nodoArbol(T_CADENA, c);
                $$->fila = @1.first_line;
        $$->columna = columna;
         }
    |caracter

        {
               string c1 ="";
               int t = strlen($1);
               int indice = 1;
               while(indice < t-1){
                    c1 = c1+$1[indice];
                    indice++;
                   }

                char c= c1.at(0);
                int n = (int)c;
                ostringstream str1;
                str1 << n;
                string g= str1.str();    
               $$ = new nodoArbol(T_CARACTER, g);
               $$->fila = @1.first_line;
                $$->columna = columna;

        }
    |verdadero{$$ = new nodoArbol(T_BOOLEANO, "1");$$->fila = @1.first_line;
        $$->columna = columna;}
    |falso{$$ = new nodoArbol(T_BOOLEANO, "0");$$->fila = @1.first_line;
        $$->columna = columna;}
    |CONVERTIR_A_CADENA{$$=$1;}
    |CONVERTIR_A_ENTERO{$$= $1;}
    |abrePar1 EXPRESION cierraPar1{$$=$2;$$->fila = @2.first_line;
        $$->columna = columna;}
    |ID {$$=$1;}
    |POS_ARREGLO{$$=$1;}
    |LLAMADA_FUNCION{$$=$1;}
    |ESTE{$$=$1;}
    |ACCESO{$$=$1;}
    |NULO {$$=$1;}
|abrePar1 error cierraPar1{yyerror("Error en expresion ");};
//|error {yyerror("Error al crear expresion ");};
    //|INSTANCIA{$$=$1;};



ID: identificador {$$= new nodoArbol("ID",$1);};

LLAMADA_FUNCION: identificador PARAMETROS_LLAMADA
    {
        $$= new nodoArbol("LLAMADA_FUNCION",$1);
        $$->hijos= $2->hijos;
        $$->fila = @1.first_line;
        $$->columna = columna;
    };

POS_ARREGLO: identificador COL_ARREGLO
    {
        $$= new nodoArbol("POS_ARREGLO",$1);
        $$->addHijo($2);
        $$->fila = @1.first_line;
        $$->columna = columna;
    };


ACCESO: ID punto ATRI
		{
            $$ = new nodoArbol("ACCESO", "1");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;
			
		}
	|POS_ARREGLO punto ATRI
		{
            $$ = new nodoArbol("ACCESO", "2");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|LLAMADA_FUNCION punto  ATRI
		{
            $$ = new nodoArbol("ACCESO", "3");
            $$->addHijo($1);
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		};

ESTE:este punto ACCESO 
		{

            $$ = new nodoArbol("ESTE", "1");
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;
		}
	|este punto ID
		{
            $$ = new nodoArbol("ESTE", "2");
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}

	|este punto POS_ARREGLO
		{
            $$ = new nodoArbol("ESTE", "3");
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|este punto LLAMADA_FUNCION
		{
            $$ = new nodoArbol("ESTE", "4");
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		};


ATRI:ID
		{
            $$= new nodoArbol("ATRI", "");
            $$->addHijo($1);
            $$->fila = @1.first_line;
            $$->columna = columna;
		}
	|POS_ARREGLO
		{
            $$= new nodoArbol("ATRI", "");
            $$->addHijo($1);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|LLAMADA_FUNCION
		{
            $$= new nodoArbol("ATRI", "");
            $$->addHijo($1);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|insertar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("INSERTAR", "");
            n->addHijo($3);
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|apilar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("APILAR", "");
            $$->addHijo($3);
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;
		}
	|desapilar abrePar1 cierraPar1
		{
            nodoArbol *n = new nodoArbol("DESAPILAR", "");
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|encolar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("ENCOLAR", "");
            $$->addHijo($3);
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;


		} 
	|desencolar abrePar1 cierraPar1
		{
            nodoArbol *n = new nodoArbol("DESENCOLAR", "");
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|obtener abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("OBTENER", "");
            $$->addHijo($3);
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|buscar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("BUSCAR", "");
            $$->addHijo($3);
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|tamanio
		{
            nodoArbol *n = new nodoArbol("TAMANIO", "");
            $$= new nodoArbol("ATRI","");
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}

    |ATRI punto ID
		{
            $$=$1;
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto POS_ARREGLO
		{
            $$=$1;
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto LLAMADA_FUNCION
		{
            $$=$1;
            $$->addHijo($3);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto insertar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("INSERTAR", "");
            n->addHijo($5);
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto apilar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("APILAR", "");
            n->addHijo($5);
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto desapilar abrePar1 cierraPar1
		{
            nodoArbol *n = new nodoArbol("DESAPILAR", "");
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto encolar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("ENCOLAR", "");
            n->addHijo($5);
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		} 
	| ATRI punto desencolar abrePar1 cierraPar1
		{
            nodoArbol *n = new nodoArbol("DESENCOLAR", "");
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto obtener abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("OBTENER", "");
            n->addHijo($5);
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;
		}
	|ATRI punto buscar abrePar1 EXPRESION cierraPar1
		{
            nodoArbol *n = new nodoArbol("BUSCAR", "");
            n->addHijo($5);
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		}
	|ATRI punto tamanio
		{
            nodoArbol *n = new nodoArbol("TAMANIO", "");
            $$=$1;
            $$->addHijo(n);
            $$->fila = @1.first_line;
            $$->columna = columna;

		};

	
NULO: nada {$$ = new nodoArbol("NULO", "NULO"); $$->columna= columna; $$->fila = @1.first_line; };

TERNARIO: OR interrogacion OR dos_puntos OR
    {
        $$= new nodoArbol("TERNARIO","");
        $$->columna = columna;
        $$->fila = @1.first_line;
        $$->addHijo($1);
        $$->addHijo($3);
        $$->addHijo($5);
    };
