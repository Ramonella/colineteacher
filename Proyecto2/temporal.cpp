#include "temporal.h"

Temporal::Temporal(string nom, double val)
{
    this->nombre= nom;
    this->valor= val;
    this->esUsado= false;
}


void Temporal::usado(bool val)
{
    this->esUsado= val;
}

string Temporal::obtenerHTML(){
    ostringstream str1;
    str1 << this->valor;
    string geek = str1.str();
    string cad= "<tr><td>"+this->nombre+"</td><td>"+geek+"</td></tr>";
    return cad;
}

Temporal* Temporal::clonar(){

    Temporal *t = new Temporal(this->nombre, this->valor);
    return t;
}
