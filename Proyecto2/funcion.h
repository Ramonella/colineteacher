#ifndef FUNCION_H
#define FUNCION_H

#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <QList>
#include "nodoarbol.h"
#include "constantes.h"

using namespace std;
class Funcion
{
public:
    int fila;
    bool retornoArreglo;
    int noDimensionesRetorno;
    bool sobreEscrita;
    string visibilidad;
    string tipo;
    string nombreFuncion;
    QList<nodoArbol*> parametros;
    nodoArbol* sentencias;
    string  nombreClase;
    bool esConstructor;
    Funcion(string visibilidad,string tipo, string nombre, nodoArbol* parametros, nodoArbol*sentencias);
    void cambiarAConstructor();
    void setNombreClase(string nombre);
    int obtenerNoParametros();
    string obtenerCadenaParametros();
    string obtenerFirma();
    string obtenerFirmaSinClase();
    void setArregloRetorno();
    void setNumeroDimenesionesRetorno(nodoArbol *dimesnieson);
};

#endif // FUNCION_H
