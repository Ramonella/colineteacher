#include<iostream>>
#include "listatemporales.h"
#include <math.h>
#include "constantes.h"

ListaTemporales::ListaTemporales()
{

}



ListaTemporales* ListaTemporales::clonar(){
    Temporal *temp;
    ListaTemporales *nueva = new ListaTemporales();
    for(int i=0; i< this->listado.length();i++){
        temp= this->listado.at(i);
        nueva->insertarTemporal(temp->clonar());
    }
    return nueva;
}

void ListaTemporales::insertarTemporal(Temporal *temp){
    if(this->listado.length()>0){
        if(this->existeTemporal(temp)){
            this->modificarTemporal(temp);
        }else{
            this->listado.push_front(temp);
        }

    }else{
        this->listado.push_front(temp);
    }
}


bool ListaTemporales::existeTemporal(Temporal *temp){

    Temporal *aux;
    for(int i =0; i< this->listado.length(); i++){
        aux=listado.at(i);
        if(aux->nombre.compare(temp->nombre)==0){
            return true;
        }
    }
    return false;
}


void ListaTemporales:: modificarTemporal(Temporal *temp)
{
    Temporal *aux;

    for(int i =0; i< this->listado.length(); i++){
        aux= listado.at(i);
        if(aux->nombre.compare((temp->nombre))==0){

            this->listado[i]->valor= temp->valor;
            this->listado[i]->esUsado= true;
            break;
        }
    }
}

Temporal* ListaTemporales::obtenerTemporal(string nombre)
{

    Temporal *aux;
    for(int i =0; i< this->listado.length(); i++){
        aux= listado.at(i);
        if(aux->nombre.compare((nombre))==0){
            return aux;
        }
    }

    return NULL;
}


double ListaTemporales::obtenerValorTemporal(string nombre){
    Temporal *aux;
   // cout<<nombre<<endl;
    for(int i =0; i< this->listado.length(); i++){
        aux= listado.at(i);

      //  cout<<aux->nombre<<endl;
        if(aux->nombre.compare((nombre))==0){
           // cout<<aux->valor<<endl;
            return aux->valor;
        }
    }

    return noExiste;
}
