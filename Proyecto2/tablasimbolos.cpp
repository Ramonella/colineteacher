#include "tablasimbolos.h"

tablaSimbolos::tablaSimbolos()
{

}


void tablaSimbolos::insertarSimbolosClase(QList<Simbolo *> simbs){
    for(int i=0; i<simbs.length(); i++){
        this->listaSimbolos.push_back(simbs.at(i));
    }
}





int tablaSimbolos::esAtributoAcceso(string nombre, Ambito *ambiente){

    Ambito *ambitoTemporal = ambiente->clonarLista();
    int cont =0;
    string cadenaAmbito="";
    for(int i =0; i<ambiente->ambitos.length();i++){
        cadenaAmbito= ambitoTemporal->getAmbitos();
        cont = cont +this->existeListaGlobal(cadenaAmbito, nombre);
        if(cont>0){
            return 1;
        }
        ambitoTemporal->salirAmbito();
    }

    return 3;
}




Simbolo* tablaSimbolos::obtenerNombreParametro(string ambito, int pos){

//cout<<"======================="<<ambito<<pos<<endl;
      Simbolo *simbTemporal;
      for(int i =0; i< this->listaSimbolos.length(); i++){
        simbTemporal = this->listaSimbolos.at(i);
        //cout<<"esta: "<<simbTemporal->ambito<<" "<<simbTemporal->rol<<" "<<simbTemporal->apuntador<<endl;
        if(sonIguales(simbTemporal->rol,"PARAMETRO") &&
          simbTemporal->apuntador== pos &&
          sonIguales(simbTemporal->ambito, ambito)){
            return simbTemporal;
          }
      }
      return NULL;


}


string tablaSimbolos::obtenerFirmaMetodo(string nombreClase, int noParametros, string nombreFuncion){

 Simbolo *temp;
string c = "buscando en clase: "+ nombreClase+" parametros: "+ intToCadena(noParametros)+" nombreFun: "+ nombreFuncion;
  for(int i =0; i<this->listaSimbolos.length(); i++){
    temp= this->listaSimbolos.at(i);
    //cout<<c<<endl;
    string h = "encontrando en clase "+ temp->ambito+" parmetros: "+ intToCadena(temp->noParametros)+" nombreFun: "+temp->nombreFuncion;
    //cout<<h<<endl;
    if(sonIguales(temp->ambito,nombreClase) &&
       (temp->noParametros ==noParametros) &&
       sonIguales(temp->nombreFuncion,nombreFuncion)){
         return temp->nombrecorto;
       }

  }

  return "";

}


int tablaSimbolos::sizeFuncion(string clase, string firmaMetodo){
     
  Simbolo *item;
  for(int i =0; i<this->listaSimbolos.length(); i++){
    item = this->listaSimbolos.at(i);
    if(sonIguales(item->ambito ,clase) &&
       sonIguales(item->nombrecorto ,firmaMetodo)){
         return item->tamanio;
       }
  }
    return -1;
}

int tablaSimbolos::sizeClase(string nombre){

Simbolo *simbTemporal;
  for(int i = 0; i<this->listaSimbolos.length(); i++){
    simbTemporal = this->listaSimbolos.at(i);
    if(sonIguales(simbTemporal->rol,"CLASE") && 
       sonIguales(simbTemporal->tipoSimbolo,"CLASE") &&
      sonIguales(simbTemporal->nombrecorto, nombre)){
        return simbTemporal->tamanio;
      }

  }

  return -1;

}



string tablaSimbolos::obtenerTipoFuncion(string firma){


      Simbolo *simboloTemporal;
      for(int i =0; i<this->listaSimbolos.length(); i++){
        simboloTemporal = this->listaSimbolos.at(i);
        if(sonIguales(simboloTemporal->rol , "FUNCION")){
          if(sonIguales(simboloTemporal->nombrecorto, firma)){
               return simboloTemporal->tipoElemento;
             }
        }
      }
      return "";
}



int tablaSimbolos::obtenerPosGlobal(string nombre, Ambito *ambitos){
    Ambito *ambitoTemporal = ambitos->clonarLista();
    string cadenaAmbito ="";
    int cont=-1;
    for(int i=0; i<ambitos->ambitos.length();i++){
        cadenaAmbito= ambitoTemporal->getAmbitos();
        cont = this->posGlobal(cadenaAmbito, nombre);
        if(cont>=0){
            return cont;
        }
        ambitoTemporal->salirAmbito();
    }

    return -1;
}


int tablaSimbolos::obtenerPosLocal(string nombre, Ambito *ambitos){
    Ambito *ambitoTemporal = ambitos->clonarLista();
    string cadenaAmbito ="";
    int cont=-1;
    for(int i=0; i<ambitos->ambitos.length();i++){
        cadenaAmbito= ambitoTemporal->getAmbitos();
        cont = this->posLocal(cadenaAmbito, nombre);
        if(cont>=0){
            return cont;
        }
        ambitoTemporal->salirAmbito();
    }

    return -1;
}



int tablaSimbolos::posLocal(string cadenaAmbito, string nombre){

    Simbolo *simbTemporal;

    for(int i=0; i<this->listaSimbolos.length();i++){
        simbTemporal= listaSimbolos.at(i);
        if(sonIguales(simbTemporal->ambito,cadenaAmbito)){
            if(sonIguales(simbTemporal->nombrecorto, nombre) &&
                    !(sonIguales(simbTemporal->rol,"ATRIBUTO"))){
                return simbTemporal->apuntador;
            }
        }
    }
    return -1;
}

int tablaSimbolos::posGlobal(string cadenaAmbito, string nombre){
    Simbolo *simbTemporal;

    for(int i=0; i<this->listaSimbolos.length();i++){
        simbTemporal= listaSimbolos.at(i);
        if(sonIguales(simbTemporal->ambito,cadenaAmbito)){
            if(sonIguales(simbTemporal->nombrecorto, nombre) &&
                    (sonIguales(simbTemporal->rol,"ATRIBUTO"))){
                return simbTemporal->apuntador;
            }
        }
    }
    return -1;
}


string tablaSimbolos::getTipoGlobal(string cadenaAmbito, string nombre){

    Simbolo *simTemporal;

    for(int i=0; i<this->listaSimbolos.length(); i++){
        simTemporal= this->listaSimbolos.at(i);
        if(sonIguales(simTemporal->ambito, cadenaAmbito)){
            if(sonIguales(simTemporal->nombrecorto, nombre) &&
                    sonIguales(simTemporal->rol, "ATRIBUTO")){
                return simTemporal->tipoElemento;
            }
        }
    }
    return "";
}



string tablaSimbolos::getTipoLocal(string cadenaAmbito, string nombre){
    Simbolo *simTemporal;

    for(int i=0; i<this->listaSimbolos.length(); i++){
        simTemporal= this->listaSimbolos.at(i);
        if(sonIguales(simTemporal->ambito, cadenaAmbito)){
            if(sonIguales(simTemporal->nombrecorto, nombre) &&
                    !sonIguales(simTemporal->rol, "ATRIBUTO")){
                return simTemporal->tipoElemento;
            }
        }
    }
    return "";
}

string tablaSimbolos::obtenerTipo(string nombre, Ambito *ambitos, int modo){

    Ambito *ambitoTemporal = ambitos->clonarLista();
    string cadenaAmbito="";
    string cont="";
    if(modo==1){
        for(int i=0; i<ambitos->ambitos.length();i++){
          cadenaAmbito =ambitoTemporal->getAmbitos();
          cont= this->getTipoGlobal(cadenaAmbito, nombre);
          if(!sonIguales(cont,"")){
              return cont;
          }
          ambitoTemporal->salirAmbito();
        }

    }else if(modo ==2){
        for(int i=0; i<ambitos->ambitos.length();i++){
          cadenaAmbito =ambitoTemporal->getAmbitos();
          cont= this->getTipoLocal(cadenaAmbito, nombre);
          if(!sonIguales(cont,"")){
              return cont;
          }
          ambitoTemporal->salirAmbito();
        }

    }

    return "";

}




int tablaSimbolos::existeListaLocal(string cadenaAmbito, string nombre){
   Simbolo *simbTemporal;
   int cont=0;
   for(int i=0; i<this->listaSimbolos.length();i++){
       simbTemporal= this->listaSimbolos.at(i);
       if(sonIguales(simbTemporal->nombrecorto, nombre) &&
               !sonIguales(simbTemporal->rol,"ATRIBUTO") &&
               sonIguales(simbTemporal->ambito,cadenaAmbito)){
           cont++;
       }
   }
   return cont;
}



int tablaSimbolos::existeListaGlobal(string cadenaAmbito, string nombre){
    Simbolo *simbTemporal;
    int cont=0;
    for(int i=0; i<this->listaSimbolos.length();i++){
        simbTemporal= this->listaSimbolos.at(i);
        if(sonIguales(simbTemporal->nombrecorto, nombre) &&
                sonIguales(simbTemporal->rol,"ATRIBUTO") &&
                sonIguales(simbTemporal->ambito,cadenaAmbito)){
            cont++;
        }
    }
    return cont;

}


int tablaSimbolos::esAtributo(string nombre, Ambito *ambitos){

    Ambito *ambitoTemporal = ambitos->clonarLista();
    int cont=0;
    string cadenaAmbito;

    for(int i=0; i<ambitos->ambitos.length(); i++){
        cadenaAmbito = ambitoTemporal->getAmbitos();
        cont = cont+this->existeListaLocal(cadenaAmbito, nombre);
        if(cont>0){
            return 2;
        }
        ambitoTemporal->salirAmbito();
    }


    ambitoTemporal= ambitos->clonarLista();
    cont=0;
    cadenaAmbito ="";

    for(int i=0; i<ambitos->ambitos.length(); i++){
        cadenaAmbito= ambitoTemporal->getAmbitos();
        cont=cont+this->existeListaGlobal(cadenaAmbito, nombre);
        if(cont>0){
            return 1;
        }
        ambitoTemporal->salirAmbito();
    }

    return 0;
}



QList<Simbolo*>tablaSimbolos::obtenerParametrosFuncion(string firmaFuncion){
    QList<Simbolo*> ret;
    Simbolo* temp;
    for(int i=0; i<this->listaSimbolos.length();i++){
        temp= this->listaSimbolos.at(i);
        if(sonIguales(temp->ambito, firmaFuncion) && sonIguales(temp->rol, "PARAMETRO")){
            ret.push_back(temp);
        }
    }
    return ret;
}


QList<Simbolo*> tablaSimbolos::obtenerAtributosClase(string nombre){
    QList<Simbolo*> ret;
    Simbolo* temp;
    for(int i=0; i<this->listaSimbolos.length();i++){
        temp= this->listaSimbolos.at(i);
        if(sonIguales(temp->ambito, nombre) && sonIguales(temp->rol, "ATRIBUTO")){
            ret.push_back(temp);
        }
    }
    return ret;
}



void tablaSimbolos:: setArregloNs(string nombre, Ambito *ambiente, int esAtributo, QList<string> arreglo){
    Ambito *ambitoTemporal;

    if(esAtributo==2){
        ambitoTemporal= ambiente->clonarLista();
        string cadenaAmbito = "";

        for(int i=0; i<ambiente->ambitos.length();i++){
            cadenaAmbito= ambiente->getAmbitos();
            bool b = this->arregloLocal(cadenaAmbito, nombre, arreglo);
            if(b){
                break;
            }
            ambitoTemporal->salirAmbito();
        }

    }else if(esAtributo==1){

        ambitoTemporal= ambiente->clonarLista();
        string cadenaAmbito = "";

        for(int i=0; i<ambiente->ambitos.length();i++){
            cadenaAmbito= ambiente->getAmbitos();
            bool b = this->arregloGlobal(cadenaAmbito, nombre, arreglo);
            if(b){
                break;
            }
            ambitoTemporal->salirAmbito();
        }
    }

}





bool tablaSimbolos::arregloGlobal(string cadenaAmbito, string nombre, QList<string> arreglo){
    Simbolo *simTemporal;
    for(int i =0; i<this->listaSimbolos.length();i++){
        simTemporal= listaSimbolos.at(i);
        if(sonIguales(simTemporal->ambito, cadenaAmbito)){
            if(sonIguales(simTemporal->nombrecorto, nombre)){
                if(sonIguales(simTemporal->rol, "ATRIBUTO")){
                    this->listaSimbolos.at(i)->arregloNs=arreglo;
                    return true;
                }
            }
        }
    }
    return false;
}




bool tablaSimbolos::arregloLocal(string cadenaAmbito, string nombre, QList<string> arreglo){
    Simbolo *simTemporal;
    for(int i =0; i<this->listaSimbolos.length();i++){
        simTemporal= listaSimbolos.at(i);
        if(sonIguales(simTemporal->ambito, cadenaAmbito)){
            if(sonIguales(simTemporal->nombrecorto, nombre)){
                if(!sonIguales(simTemporal->rol, "ATRIBUTO")){
                    this->listaSimbolos.at(i)->arregloNs=arreglo;
                    return true;
                }
            }
        }
    }
    return false;
}




Simbolo* tablaSimbolos::obtenerSimbolo(string nombreArreglo, Ambito *ambiente, int esAtributo){
    Ambito *ambitoTemporal;


    if(esAtributo==2){
        ambitoTemporal = ambiente->clonarLista();
        string cadenaAmbito ="";
        for(int i=0; i<ambiente->ambitos.length(); i++){
            cadenaAmbito = ambitoTemporal->getAmbitos();
            Simbolo *c= this->buscarLocal(cadenaAmbito, nombreArreglo);
            if(c!= NULL){
                return c;
            }
            ambitoTemporal->salirAmbito();
        }

    }else if(esAtributo ==1)
    {
        ambitoTemporal= ambiente->clonarLista();
        string cadenaAmbito="";
        for(int i=0; i<ambiente->ambitos.length();i++){
            cadenaAmbito= ambitoTemporal->getAmbitos();
            Simbolo *c= this->buscarGlobal(cadenaAmbito, nombreArreglo);
            if(c!=NULL){
                return c;
            }
            ambitoTemporal->salirAmbito();
        }


    }
    return NULL;

}


Simbolo* tablaSimbolos::buscarLocal(string cadenaAmbito, string nombre){
    Simbolo *simTemporal;

    for(int i=0; i<this->listaSimbolos.length(); i++){
        simTemporal = this->listaSimbolos.at(i);
        if(sonIguales(simTemporal->ambito, cadenaAmbito)){
            if(sonIguales(simTemporal->nombrecorto, nombre)){
                if(!sonIguales(simTemporal->rol, "ATRIBUTO")){
                    return simTemporal;
                }
            }
        }
    }
    return NULL;
}



Simbolo* tablaSimbolos::buscarGlobal(string cadenaAmbito, string nombre){
    Simbolo *simTemporal;

    for(int i=0; i<this->listaSimbolos.length(); i++){
        simTemporal = this->listaSimbolos.at(i);
        if(sonIguales(simTemporal->ambito, cadenaAmbito)){
            if(sonIguales(simTemporal->nombrecorto, nombre)){
                if(sonIguales(simTemporal->rol, "ATRIBUTO")){
                    return simTemporal;
                }
            }
        }
    }
    return NULL;
}








string tablaSimbolos::escribirHTMLTabla(){
    string inicio = inicioPagina+"<table id =\"customers\">\n<tr>\n";

    inicio+= "<th>Nombre</th>";
            inicio+="<th> Ambito</th>";
            inicio+="<th>Tipo Simbolo </th>";
            inicio+="<th> Rol </th>";
            inicio+="<th>Visibilidad</th>";

            inicio+="<th>Tipo Elemento </th>";
            inicio+="<th> Apuntador</th>";
            inicio+="<th> Tamanio </th>";
            inicio+="<th> No. Parametros</th>";
            inicio+="<th> Cadena Parametros </th>";
            inicio+="<th> Nombre Funcion </th>";
            inicio+="<th> No. Dimensiones </th>";
            inicio+="</tr>";

            for(int i=0; i<this->listaSimbolos.length();i++){
                inicio+=this->listaSimbolos.at(i)->getHTMLSimbolo();
            }
            inicio+="</table></body></html>";
            ofstream fs;
            fs.open("/home/alina/Documentos/arboles/tabla.html");
            fs<<inicio;
            fs.close();

            return inicio;

}


int tablaSimbolos::obtenerPosAtributoAcceso(string nombreClase, string nombreAtributo){

    Simbolo *simbTemporal;
    for(int i=0; i<this->listaSimbolos.length(); i++){
        simbTemporal = listaSimbolos.at(i);
        if(sonIguales(simbTemporal->ambito, nombreClase) &&
         sonIguales(simbTemporal->rol, "ATRIBUTO") &&
         sonIguales(simbTemporal->nombrecorto, nombreAtributo)){
             return simbTemporal->apuntador;
         }
    }
return -1;
}




string tablaSimbolos::obtenerPosTipoSimboloAcceso(string nombreClase, string nombreAtributo){
Simbolo *simbTemporal;
    for(int i=0; i<this->listaSimbolos.length(); i++){
        simbTemporal = listaSimbolos.at(i);
        if(sonIguales(simbTemporal->ambito, nombreClase) &&
         sonIguales(simbTemporal->rol, "ATRIBUTO") &&
         sonIguales(simbTemporal->nombrecorto, nombreAtributo)){
             return simbTemporal->tipoSimbolo;
         }
    }
return "";

}



string tablaSimbolos::obtenerTipoAtributoAcceso(string nombreClase, string nombreAtributo){

Simbolo *simbTemporal;
    for(int i=0; i<this->listaSimbolos.length(); i++){
        simbTemporal = listaSimbolos.at(i);
        if(sonIguales(simbTemporal->ambito, nombreClase) &&
         sonIguales(simbTemporal->rol, "ATRIBUTO") &&
         sonIguales(simbTemporal->nombrecorto, nombreAtributo)){
             return simbTemporal->tipoElemento;
         }
    }
return "";

}

