#ifndef NODOARBOL_H
#define NODOARBOL_H

#include <string.h>
#include <iostream>
#include <QList>
#include <vector>

#include <stdio.h>
#include <stdlib.h>


#include <string>

using namespace std;

class nodoArbol
{

public:
    nodoArbol();
    string etiqueta="";
    string valor="";
    int fila;
    int columna;
    QList<nodoArbol*> hijos;
    nodoArbol(string, string );
    void addHijo(nodoArbol*);
    string getValor();
    void recorrer(nodoArbol *n,FILE *archivo);
string getEtiqueta();
       void graficarArbol(nodoArbol *raiz);
        QList<nodoArbol*>  getHijos();

};

#endif // NODOARBOL_H
