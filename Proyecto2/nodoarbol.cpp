#include "nodoarbol.h"
#include <string.h>
#include <string>
 #include <iostream>
#include <QList>
using namespace std;
nodoArbol::nodoArbol()
{
    this->etiqueta="";
}

nodoArbol::nodoArbol(string etiqueta, string valor){
    this->etiqueta= etiqueta;
    this->valor = valor;
    this->columna=0;
    this->fila=0;

}


void nodoArbol::addHijo(nodoArbol *nuevo){
    this->hijos.push_back(nuevo);
}


string nodoArbol::getValor()
{
    return this->valor;
}

string nodoArbol::getEtiqueta()
{
    return this->etiqueta;
}

QList<nodoArbol*> nodoArbol::getHijos()
{
    return this->hijos;
}

void nodoArbol::recorrer(nodoArbol *n, FILE *archivo)
{
    string r = "";
    string val = n->etiqueta;
   // std::cout<< val<< endl;
    if(n->getValor() == ""){
        string a = n->getEtiqueta();
        fprintf(archivo,"nodo%d [label=\"%s \"];\n", n,a.c_str());

    }else
    {
        string a = n->getValor();
        std::cout<< a << endl;
        string b = n->getEtiqueta();
        std::cout<< b << endl;
        fprintf(archivo,"nodo%d [label=\"%s : %s \"];\n", n,b.c_str(),a.c_str());
    }

    if(n->hijos.length() > 0)
    {
        int i = 0;
        for(i = 0;i<n->hijos.length() ;i++)
        {
            fprintf(archivo,"nodo%d--nodo%d ;\n", n,n->hijos.at(i));
        }

        for(i = 0;i<n->hijos.length() ;i++)
        {
            recorrer(n->hijos.at(i), archivo);
        }
    }
}

void nodoArbol::graficarArbol(nodoArbol* raiz)
{
    FILE *fg;
    fg = fopen("/home/alina/Documentos/arboles/arbol.dot","w");
    fputs("graph arbol{" ,fg);
    recorrer(raiz,fg);
    fputs("}" ,fg);
    fclose ( fg );
    system("dot -Tpng /home/alina/Documentos/arboles/arbol.dot -o /home/alina/Documentos/arboles/arbol.png");
    system("xdg-open /home/alina/Documentos/arboles/arbol.dot ");
    system("PAUSE");

}

