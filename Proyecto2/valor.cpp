#include "valor.h"
#include <string>
#include <string.h>
#include "constantes.h"

Valor::Valor()
{
    this->tipo= NULO;
    this->valor =VAL_NULO;
    this->tipoSimbolo= NULO;
    this->nombreArreglo=NULO;
    this->referencia= NULO;
    this->estructura= NULO;
}



void Valor::setReferencia(string ed, string ref){
    this->estructura= ed;
    this->referencia= ref;
}

void Valor:: crearDecimal(string val){
    this->tipo= T_DECIMAL;
    this->valor= val;
}

void Valor ::crearEntero(string val){
    this->tipo= T_ENTERO;
    this->valor= val;
}

void Valor::crearBooleano(string valor){
    this->tipo = T_BOOLEANO;
    this->valor= valor;
}

void Valor::crearCaracter(string val){

    this->tipo = T_CARACTER;
    this->valor = val;
}

void Valor::crearCadena(string val){
    this->tipo= T_CADENA;
    this->valor= val;
}

void Valor::crearCondicion(nodoCondicion *cond){
    this->tipo= T_CONDICION;
    this->cond= cond;
    this->valor="";
}
