#include "myworker.h"

#include <QDebug>
#include <QThread>
#include <QFile>
#include "constantes.h"

MyWorker::MyWorker(QObject *parent) :
    QObject(parent),
    m_cancelRequested(false),
    m_pauseRequired(false)
{
}

MyWorker::~MyWorker()
{
}

void MyWorker::restart()
{
    this -> m_pauseRequired = false;
    this -> m_pauseManager.wakeAll();

}

void MyWorker::pause()
{
    this -> m_pauseRequired = true;
}

void MyWorker::doWork()
{
    for(int i = 1; i < this->noLineas; i++)
    {

        qDebug() << i;
        this->codTemporal= this->lCodigo->obtenerCodigoPos(i);
        string errores = lCodigo->obtenerErroresAlto(i);
        QString g = QString::fromStdString(codTemporal);
        QString g2 = QString::fromStdString(errores);
        escribirArchivo("/home/alina/salida/temporal.txt",g); //esccribiendo c3d
        int x = QString::compare(g2, "", Qt::CaseInsensitive);
        if(!(x==0)){
           escribirArchivo("/home/alina/salida/erroresAlto.txt",g2);
        }

            qDebug(g.toLatin1());
        m_continue.lock();
        if (m_pauseRequired) {
            //qDebug() << i;
            m_pauseManager.wait(&m_continue);
        }
        m_continue.unlock();
        if (m_cancelRequested) {
            break;
        }

    }

    // Write your code above"

    emit finished();
}


void MyWorker::doWork2()
{
    for(int i = 1; i < this->lineas3D; i++)
    {

        qDebug() << i;
        elementoCodigo *temporal = lCodigo->obtenerCodigoPos2(i);
         if(temporal!=NULL){
             QString cadTEpmorales = QString::fromStdString(temporal->temporales);
             QString cadHeap= QString::fromStdString((temporal->heap));
             QString cadStack = QString::fromStdString(temporal->stack);
             QString cadPool = QString:: fromStdString(temporal->pool);
             QString cadImpresion = QString::fromStdString(temporal->impresion);
             QString cadErroresBajo = QString::fromStdString(temporal->erroresBajo);
             escribirArchivo("/home/alina/salida/heap.txt",cadHeap);
             escribirArchivo("/home/alina/salida/temporales.txt",cadTEpmorales);
             escribirArchivo("/home/alina/salida/stack.txt",cadStack);
             escribirArchivo("/home/alina/salida/pool.txt",cadPool);
             escribirArchivo("/home/alina/salida/impresion.txt",cadImpresion);
             escribirArchivo("/home/alina/salida/erroresBajo.txt",cadErroresBajo);

         }



        m_continue.lock();
        if (m_pauseRequired) {
            //qDebug() << i;
            m_pauseManager.wait(&m_continue);
        }
        m_continue.unlock();
        if (m_cancelRequested) {
            break;
        }

    }

    // Write your code above"

    emit finished();
}


void MyWorker::cancelWork()
{
    this -> restart();
    this -> m_cancelRequested = true;
}
