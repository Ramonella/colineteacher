#ifndef AMBITO_H
#define AMBITO_H
#include <sstream>

#include <stdlib.h>
#include <QList>
#include <fstream>
#include <iostream>
#include <string>
#include <string.h>
using namespace std;


class Ambito
{
public:
    QList<string> ambitos;
    int valSi;
    int valElse;
    int valMientras;
    int valPara;
    int valCaso;
    int valDefecto;
    int valHacer;
    void addAmbito(string ambito);
    void addSi();
    void addElse();
    void addMientras();
    void addPara();
    void addCaso();
    void addDefecto();
    void addHacer();
    string getAmbitos();
    void salirAmbito();
    Ambito();
    Ambito* clonarLista();
};

#endif // AMBITO_H
