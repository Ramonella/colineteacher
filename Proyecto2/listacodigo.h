#ifndef LISTACODIGO_H
#define LISTACODIGO_H
#include <string>
#include <string.h>
#include <sstream>
#include <iostream>
#include "elementocodigo.h"
#include <QList>
using namespace std;


class ListaCodigo
{
public:
    QList<elementoCodigo*> elementos;
    void insertarCodigo(int no, string cod, string errores);
    ListaCodigo();
    string obtenerCodigoPos(int pos);
    elementoCodigo* obtenerCodigoPos2(int pos);
    string obtenerErroresAlto(int pos);
    void mostrarCodigo();

};

#endif // LISTACODIGO_H
