#include "calificarleccion.h"
#include "listacodigo.h"
#include <QFile>
#include "constantes.h"
#include "mprincipal.h"
#include "analizadorinterprete.h"
#include <QTextStream>
#include "participarleccion.h"
#include "listacodigo.h"


extern int yyrestart( FILE* archivo);//METODO QUE PASA EL ARCHIVO A FLEX
extern int yyparse(); //METODO QUE INICIA EL ANALISIS SINTACTICO
extern void setSalida(ListaClases* sal,ListaErrores *errores);

CalificarLeccion::CalificarLeccion()
{

}

bool CalificarLeccion::calificar(string cadena, string sentencia){


    QList<string> palabras;
    char separador ='=';
    string temp;
    for(size_t p=0,q=0; p!=sentencia.npos;p=q){
        temp= sentencia.substr(p+(p!=0),(q=sentencia.find(separador, p+1))-p-(p!=0));
        palabras.push_back(temp);
    }

    if(palabras.length() == 2){
         ListaCodigo *codigoC= new ListaCodigo();

        QString palabra1 =QString::fromStdString(palabras.at(0));
        QString palabra2 =QString::fromStdString(palabras.at(1));

        ListaClases *clases = new ListaClases();
        ListaClases *clases2 = new ListaClases();

        ListaCodigo *listaDeCodigo= new ListaCodigo();

        /*----- generando el primero nodo -----*/
          QFile file("temp.txt");
          if (file.open(QFile::WriteOnly | QFile::Truncate)) {
              QTextStream stream1(&file);
              stream1 << palabra1;
          }
          ListaErrores *errores = new ListaErrores();
          const char* x = "temp.txt";
          FILE* input = fopen(x, "r" );
          yyrestart(input);
          setSalida(clases,errores);
          yyparse();

          /*------- Generadndo segundo nodo------*/

          QFile file2("temp2.txt");
          if (file2.open(QFile::WriteOnly | QFile::Truncate)) {
              QTextStream stream12(&file2);
              stream12 << palabra2;
          }
          const char* x2 = "temp2.txt";
          FILE* input2 = fopen(x2, "r" );
          yyrestart(input2);
          setSalida(clases2, errores);
          yyparse();

          string codigo ="";



          if(clases->esExpresion && clases2->esExpresion){

              /*Crenado la condicion del if*/
              nodoArbol *nuevaRelacional = new nodoArbol("RELACIONAL", "==");
              nuevaRelacional->addHijo(clases->expresion);
              nuevaRelacional->addHijo(clases2->expresion);


              /*crenado la impresion verdadera*/

              nodoArbol *enteroVerdadero = new nodoArbol(T_ENTERO, "262626");
              nodoArbol *imprimirVerdadero = new nodoArbol("IMPRIMIR", "");
              imprimirVerdadero->addHijo(enteroVerdadero);

              nodoArbol *instruccionesVerdaderas =  new nodoArbol("INSTRUCCIONES","");
              instruccionesVerdaderas->addHijo(imprimirVerdadero);

              /*creando la impresion falsa*/
              nodoArbol *enteroFalso = new nodoArbol(T_ENTERO, "252525");
              nodoArbol *imprimirFalso= new nodoArbol("IMPRIMIR", "");
              imprimirFalso->addHijo(enteroFalso);

              nodoArbol *instruccionesFalsas =  new nodoArbol("INSTRUCCIONES","");
              instruccionesFalsas->addHijo(imprimirFalso);

              /*Creando el if */
           nodoArbol *positivo = new nodoArbol("SI_1","");
           positivo->addHijo(nuevaRelacional);
           positivo->addHijo(instruccionesVerdaderas);

           nodoArbol *negativo = new nodoArbol("SINO","");
           negativo->addHijo(instruccionesFalsas);

           nodoArbol *SI = new nodoArbol("SI","2");
           SI->addHijo(positivo);
           SI->addHijo(negativo);

           nodoArbol *instruccionesSI =  new nodoArbol("INSTRUCCIONES","");
           instruccionesSI->addHijo(SI);

           mPrincipal *princ = new mPrincipal(instruccionesSI);


            /*------- GEnerando las clases de para la funcion creada por el usiario*/

           ListaClases *clases3 = new ListaClases();

           QString cadUsuario = QString::fromStdString(cadena);

             QFile file3("temp3.txt");
             if (file3.open(QFile::WriteOnly | QFile::Truncate)) {
                 QTextStream stream13(&file3);
                 stream13 << cadUsuario;
             }

             cout<<cadena<<endl;
             const char* x3 = "temp3.txt";
             FILE* input3 = fopen(x3, "r" );
             yyrestart(input3);
             setSalida(clases3,errores);
             yyparse();

             if(clases3->esExpresion==false){
                 cout<<clases3->clasesArchivo.length()<<endl;
                 if(clases3->clasesArchivo.length()>0){
                     Principal *pp = new Principal();
                     int n = clases3->clasesArchivo.length()-1;
                     clases3->clasesArchivo.at(n)->principal= princ;
                     GeneradorCodigo *c3d = new GeneradorCodigo(clases3, listaDeCodigo);
                     c3d->generar3D();
                     string nombrePrincipal= c3d->buscarPrincipal();
                    analizadorInterprete* interprete = new analizadorInterprete(pp, codigoC,false);
                    interprete->ejecutar3D(c3d->c3d->codigo,nombrePrincipal);
                    string cadResultante = interprete->cadenaImpresion;
                    cout<<cadResultante<<endl;


                    QString str = QString::fromStdString(cadResultante);
                    bool gh =str.contains("262626", Qt::CaseInsensitive);

                    if (gh) {
                        return true;
                      } else {
                        return false;
                      }


                 }
            }


          }


    }
    return false;

}

