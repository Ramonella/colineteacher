#ifndef SIMBOLO_H
#define SIMBOLO_H
#include <iostream>
#include <sstream>
#include <string.h>
#include <string>
#include "nodoarbol.h"
#include "constantes.h"
using namespace std;

class Simbolo
{
public:
    string visibilidad;
    string nombrecorto;
    string tipoSimbolo;//arreglo, objeto, var, edd
    string tipoElemento;//int, persona, char, decimal
    string ambito; //persona1_entero_metodobotnio
    string rol ;//vairable local, parametro, retorno
    int apuntador;
    int tamanio;//size de una clase o metodo
    QList<nodoArbol*> dimensiones;//nodo de las dimensiones del arreglo
    int noParametros ;
    string parametrosFuncionCadena;//entero_caracter_obj
    string nombreFuncion ;
    nodoArbol *declaAtributo;
    nodoArbol *asignaAtributo;
    int noDimensiones;
    QList<string> arregloNs;
    Simbolo();
    void setValoresVariable(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu,int size);
    void setValoresArreglo(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size, int nodimensiones, QList<nodoArbol*>listaDim);
    void setValoresLista(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size);
    void setValoresPila(string nombreC, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size);
    void setValoresCola(string nombrec, string tipoSimb, string tipoEle, string ambito, string rol, int apu, int size);
    void setValoresFuncion(string nombrec,string tipo, string tipoS, string ambito,string rol, int apu, int size, int noParametros, string cadParametros, string nombreFun);
    string getHTMLSimbolo();












};

#endif // SIMBOLO_H
