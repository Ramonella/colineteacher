#ifndef CALIFICARLECCION_H
#define CALIFICARLECCION_H
#include <sstream>
#include <iostream>
#include <fstream>
#include <string.h>
#include <string>
#include "listaclases.h"
#include "nodoarbol.h"
#include "generadorcodigo.h"

using namespace std;

class CalificarLeccion
{
public:
    CalificarLeccion();
    bool calificar(string cadena, string respuesta);
};

#endif // CALIFICARLECCION_H
