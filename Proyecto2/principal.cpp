#include "principal.h"
#include "ui_principal.h"
#include "parser.h"
#include "scanner.h"
#include "nodoarbol.h"
#include <QFile>
#include <QTextStream>
#include <iostream>
#include "generadorcodigo.h"
#include <QString>
#include "valor.h"
#include "analizadorinterprete.h"
#include <stdio.h>
#include <ctype.h>
#include "listaclases.h"
#include "constantes.h"
#include "listaerrores.h"
#include <QDebug>
#include <QMessageBox>
#include "pantalla1.h"
#include <QTime>



//extern int yyrestart( FILE* archivo);//METODO QUE PASA EL ARCHIVO A FLEX
extern int yyparse(); //METODO QUE INICIA EL ANALISIS SINTACTICO
extern void setSalida(ListaClases* sal, ListaErrores *errores);
int no=1;
int no3D=1;
int lineas3D=0;
string cadenaSalida="";
string cadenaTabla="";
string cadenaErrores="";


string heap="";
string pool="";
string stack="";
string temporales="";
string impresion="";
string erroresBajo="";

Principal::Principal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Principal)
{
    ui->setupUi(this);
    this->nombrePrincipal="";
    this -> threadStarted = false;
    this -> initThread = true;
}

Principal::~Principal()
{
    delete ui;
}


void Principal::generar3D(){

    QString v =ui->txtEntrada->toPlainText();
    clases = new ListaClases();
    this->erroresPrograma = new ListaErrores();
    this->listaDeCodigo= new ListaCodigo();
    ui->txtSalida->setPlainText("");
      QFile file("temp.txt");
      if (file.open(QFile::WriteOnly | QFile::Truncate)) {
          QTextStream stream1(&file);
          stream1 << v;
      }
      const char* x = "temp.txt";
      FILE* input = fopen(x, "r" );
      yyrestart(input);
      setSalida(clases, this->erroresPrograma);
      yyparse();
      if(this->erroresPrograma->errores.length()>0){
       cadenaErrores= erroresPrograma->imprimirErrores();

      }else{

       GeneradorCodigo *c3d = new GeneradorCodigo(clases, listaDeCodigo);
       c3d->erroresEjecucion= this->erroresPrograma;
       c3d->generar3D();

      cadenaErrores= c3d->erroresEjecucion->imprimirErrores();

      cadenaTabla = c3d->tabla->escribirHTMLTabla();

      cadenaSalida= c3d->c3d->codigo;

      this->nombrePrincipal= c3d->buscarPrincipal();
      }


}



void Principal::ejecutar3D(bool modo){
    this->erroresPrograma= new ListaErrores();
    this->lista3D= new ListaCodigo();
    analizadorInterprete* interprete = new analizadorInterprete(this,lista3D,modo);
    QString cadena = ui->txtSalida->toPlainText();
    std::string entrada = cadena.toUtf8().constData();
    interprete->ejecutar3D(entrada,nombrePrincipal);
    lineas3D= interprete->noFilas;
    heap =interprete->escribirHeap();
    pool= interprete->escribirPool();
    stack =interprete->escribirStack();
    temporales  = interprete->escribirTemporales();
    impresion = interprete->cadenaImpresion;



}


void Principal::on_pushButton_clicked()
{
    /*

    this->erroresPrograma= new ListaErrores();
    this->lista3D= new ListaCodigo();
    analizadorInterprete* interprete = new analizadorInterprete(this,lista3D);
    QString cadena = ui->txtSalida->toPlainText();
    std::string entrada = cadena.toUtf8().constData();
    interprete->ejecutar3D(entrada,nombrePrincipal);
    lineas3D= interprete->noFilas;
    QString qstr = QString::fromStdString(interprete->cadenaImpresion);
    ui->txtImpresion->setPlainText(qstr);
    string heap =interprete->escribirHeap();
    string pool= interprete->escribirPool();
    string stack =interprete->escribirStack();
    string temporales  = interprete->escribirTemporales();
    QString c_heap = QString::fromStdString(heap);
    QString c_pool = QString::fromStdString(pool);
    QString c_stack = QString::fromStdString(stack);
    QString c_temporales = QString::fromStdString(temporales);
    ui->txtHeap->setHtml(c_heap);
    ui->txtPool->setHtml(c_pool);
    ui->txtStack->setHtml(c_stack);
    ui->txtTemporales->setHtml(c_temporales);

*/
    ejecutar3D(false);

    QString qstr = QString::fromStdString(impresion);
    ui->txtImpresion->setPlainText(qstr);
    QString c_heap = QString::fromStdString(heap);
    QString c_pool = QString::fromStdString(pool);
    QString c_stack = QString::fromStdString(stack);
    QString c_temporales = QString::fromStdString(temporales);
    ui->txtHeap->setHtml(c_heap);
    ui->txtPool->setHtml(c_pool);
    ui->txtStack->setHtml(c_stack);
    ui->txtTemporales->setHtml(c_temporales);

}



void Principal::on_pushButton_2_clicked()
{
   /* QString v =ui->txtEntrada->toPlainText();
    clases = new ListaClases();
    this->listaDeCodigo= new ListaCodigo();
      ui->txtSalida->setPlainText("");

      QFile file("temp.txt");
      if (file.open(QFile::WriteOnly | QFile::Truncate)) {
          QTextStream stream1(&file);
          stream1 << v;
      }
      const char* x = "temp.txt";
      FILE* input = fopen(x, "r" );
      yyrestart(input);
      setSalida(clases);
      yyparse();


       GeneradorCodigo *c3d = new GeneradorCodigo(clases, listaDeCodigo);
       c3d->generar3D();
      string c= c3d->erroresEjecucion->imprimirErrores();
      QString erroresCad = QString::fromStdString(c);
      ui->txtErrores->setHtml(erroresCad);

      string c_tabla = c3d->tabla->escribirHTMLTabla();
      QString cadenaTabla = QString::fromStdString(c_tabla);
      ui->txtTabla->setHtml(cadenaTabla);
      QString qstr = QString::fromStdString(c3d->c3d->codigo);
      cadenaSalida= c3d->c3d->codigo;
      ui->txtSalida->setPlainText(qstr);
      this->nombrePrincipal= c3d->buscarPrincipal();*/
    this->generar3D();

    QString cadenaE = QString::fromStdString(cadenaErrores);
    ui->txtErrores->setHtml(cadenaE);

    QString c_tabla = QString::fromStdString(cadenaTabla);
    ui->txtTabla->setHtml(c_tabla);

   QString qstr = QString::fromStdString(cadenaSalida);
   ui->txtSalida->setPlainText(qstr);

}

void Principal::on_pushButton_3_clicked()
{
    if(no<=clases->fila){

        if(no ==1){
            ui->txtSalida->setPlainText("");
            ui->txtTabla->setHtml("");
            ui->txtErrores->setHtml("");
            this->generar3D();
        }
        elementoCodigo *cod = new elementoCodigo();
        if(initThread){
            qDebug() << "Init thread";
            this -> m_thread = new QThread();
            this -> m_worker = new MyWorker();
            this->m_worker->lCodigo= this->listaDeCodigo;
            this->m_worker->noLineas= clases->fila;
            this->m_worker->codTemp= cod;
            this -> m_worker-> moveToThread(this -> m_thread);
            connect(m_thread, SIGNAL(started()), m_worker, SLOT(doWork()));

            connect(m_worker, SIGNAL(finished()), m_thread, SLOT(quit()));
            initThread = false;


            this -> m_thread -> start();
            this -> m_worker -> pause();
            threadStarted = true;


        }

        if(threadStarted){

            this -> m_worker -> restart();
            this -> m_worker -> pause();
        }
        QString text = leerArchivo("/home/alina/salida/temporal.txt");
        QString erroresy = leerArchivo("/home/alina/salida/erroresAlto.txt");
        int x = QString::compare(text, "", Qt::CaseInsensitive);
        if(!(x==0)){
            ui->txtSalida->setPlainText(text);
            ui->txtErrores->setHtml(erroresy);
        }
        if(no== clases->fila){
            QString qstr = QString::fromStdString(cadenaSalida);
            QString cadE = QString::fromStdString(cadenaErrores);
            QString cadT = QString::fromStdString(cadenaTabla);
            ui->txtSalida->setPlainText(qstr);
            ui->txtErrores->setHtml(cadE);
            ui->txtTabla->setHtml(cadT);
            no =1;
            QMessageBox msgBox;
            msgBox.setText("Se ha finalizado la ejecucion");
            msgBox.exec();
        }

         cout<<"Linea: "<<no<<endl;
         no++;

    }

}



void Principal::lineaLinea3D(){

    if(no3D<=lineas3D){

        if(no3D ==1){
            ui->txtTemporales->setHtml("");
            ui->txtHeap->setHtml("");
            ui->txtStack->setHtml("");
            ui->txtPool->setHtml("");
            ui->txtImpresion->setPlainText("");
            this->ejecutar3D(true);

        };
        elementoCodigo *cod = new elementoCodigo();
        if(initThread){
            qDebug() << "Init thread";
            this -> m_thread = new QThread();
            this -> m_worker = new MyWorker();
            this->m_worker->lCodigo= this->lista3D;
            this->m_worker->lineas3D = lineas3D;
            this->m_worker->codTemp= cod;
            this -> m_worker-> moveToThread(this -> m_thread);
            connect(m_thread, SIGNAL(started()), m_worker, SLOT(doWork2()));

            connect(m_worker, SIGNAL(finished()), m_thread, SLOT(quit()));
            initThread = false;


            this -> m_thread -> start();
            this -> m_worker -> pause();
            threadStarted = true;


        }

        if(threadStarted){

            this -> m_worker -> restart();
            this -> m_worker -> pause();
        }
        QString temporales1 = leerArchivo("/home/alina/salida/temporales.txt");
        QString erroresBajo1 = leerArchivo("/home/alina/salida/erroresBajo.txt");
        QString impresion1 = leerArchivo("/home/alina/salida/impresion.txt");
        QString heap1 = leerArchivo("/home/alina/salida/heap.txt");
        QString stack1 = leerArchivo("/home/alina/salida/stack.txt");
        QString pool1= leerArchivo("/home/alina/salida/pool.txt");

        ui->txtTemporales->setHtml(temporales1);
        ui->txtHeap->setHtml(heap1);
        ui->txtStack->setHtml(stack1);
        ui->txtPool->setHtml(pool1);
        ui->txtImpresion->setPlainText(impresion1);

        if(no3D== lineas3D){
            QString qstr = QString::fromStdString(impresion);
            ui->txtImpresion->setPlainText(qstr);
            QString c_heap = QString::fromStdString(heap);
            QString c_pool = QString::fromStdString(pool);
            QString c_stack = QString::fromStdString(stack);
            QString c_temporales = QString::fromStdString(temporales);
            ui->txtHeap->setHtml(c_heap);
            ui->txtPool->setHtml(c_pool);
            ui->txtStack->setHtml(c_stack);
            ui->txtTemporales->setHtml(c_temporales);

            no3D =1;
            QMessageBox msgBox;
            msgBox.setText("Se ha finalizado la ejecucion");
            msgBox.exec();
        }

         cout<<"Linea: "<<no3D<<endl;
         no3D++;

    }


}


void Principal::on_pushButton_4_clicked()
{
    if(threadStarted){
        this -> m_worker ->cancelWork();
        initThread = true;
        threadStarted = false;

    }
    no =1;
}

void Principal::on_pushButton_5_clicked()
{
    this->generar3D();
    this->automaticoAlto();


}




void Principal::on_actionRegresar_triggered()
{
    pantalla1 *n = new pantalla1();
    n->show();
    n->lecciones= this->lecciones;
    n->mostrarLeccionesBotones();
    this->hide();
}

void Principal::cLeccion(QString cad){
    ui->txtEntrada->setPlainText(cad);
}

void Principal::on_btn_automatico3D_clicked()
{
    this->ejecutar3D(true);
    this->automatico3D();
}



void Principal::automaticoAlto(){

    bool ok = false;
    QString respuesta;
    QStringList items;
    bool bandera = false;
        items << "Lento" << "Rapido";
    while(!ok){
            respuesta= QInputDialog::getItem(this, "",
                                                 "Velocidad del Debuguer: ", items, 0, false, &ok);
    }
    if(ok){
        std::string v = respuesta.toUtf8().constData();
        if(sonIguales(v, "Lento")){
            bandera = true;
        }else{
            bandera= false;
        }
    }


    elementoCodigo *temporal;
    for (int i =1; i<=clases->fila; i++){
        temporal = this->listaDeCodigo->obtenerCodigoPos2(i);
        if(temporal!= NULL){
        QString noLinea = "Linea "+QString::number(i);
        ui->lblLinea->setText(noLinea);
        QString cadErrores= QString::fromStdString(temporal->erroresAlto);
        QString cad3D = QString::fromStdString(temporal->codigo3D);
        ui->txtErrores->setHtml(cadErrores);
        ui->txtSalida->setPlainText(cad3D);
        if(bandera){
            deleayLento();
        }else{
           deleayRapido();
        }
        }
    }
    ui->lblLinea->setText("");
     QString erroresy = leerArchivo("/home/alina/salida/erroresAlto.txt");
    QString qstr = QString::fromStdString(cadenaSalida);
   ui->txtSalida->setPlainText(qstr);
   ui->txtErrores->setHtml(erroresy);

}



void Principal::automatico3D(){

    bool ok = false;
    QString respuesta;
    QStringList items;
    bool bandera = false;
        items << "Lento" << "Rapido";
    while(!ok){
            respuesta= QInputDialog::getItem(this, "",
                                                 "Velocidad del Debuguer: ", items, 0, false, &ok);
    }
    if(ok){
        std::string v = respuesta.toUtf8().constData();
        if(sonIguales(v, "Lento")){
            bandera = true;
        }else{
            bandera= false;
        }
    }


    elementoCodigo *temporal;
    for (int i =1; i<=lineas3D; i++){
        temporal = this->lista3D->obtenerCodigoPos2(i);
        if(temporal!= NULL){
        QString noLinea = "Linea "+QString::number(i);
        ui->lblLinea->setText(noLinea);
        QString cadTEpmorales = QString::fromStdString(temporal->temporales);
        QString cadHeap= QString::fromStdString((temporal->heap));
        QString cadStack = QString::fromStdString(temporal->stack);
        QString cadPool = QString:: fromStdString(temporal->pool);
        QString cadImpresion = QString::fromStdString(temporal->impresion);
        QString cadErroresBajo = QString::fromStdString(temporal->erroresBajo);
        ui->txtTemporales->setHtml(cadTEpmorales);
        ui->txtHeap->setHtml(cadHeap);
        ui->txtStack->setHtml(cadStack);
        ui->txtPool->setHtml(cadPool);
        ui->txtImpresion->setPlainText(cadImpresion);
        if(bandera){
            deleayLento();
        }else{
           deleayRapido();
        }
        }
    }
    ui->lblLinea->setText("");


}

void Principal::deleayRapido(){
    QTime dieTime= QTime::currentTime().addMSecs(500);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}



void Principal::deleayLento(){
        QTime dieTime= QTime::currentTime().addSecs(1);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

}


void Principal::on_btn_paso3D_clicked()
{
    this->lineaLinea3D();
}

void Principal::on_btn_detener3D_clicked()
{
    if(threadStarted){
        this -> m_worker ->cancelWork();
        initThread = true;
        threadStarted = false;

    }
    no3D =1;
}
