#include "clase.h"
#include "principal.h"





Clase::Clase()
{
    this->nombreClase="";
    this->herencia="";
    this->funciones= new ListaFunciones();
    this->principal= NULL;
    this->fila=0;


}

void Clase::insertarFuncionHeredada(Funcion *nueva){
    this->funciones->insertarFuncionHeredad(nueva, this->nombreClase);
}

void Clase::insertarAtributo(nodoArbol *nuevo){
    this->atributos.push_back(nuevo);
}

void Clase::insertarPrincipal(mPrincipal *metodo)
{
    this->principal= metodo;
}


QList<Funcion*> Clase::obtenerFuncionesHeredadas(){
   Funcion *temporal;
   QList<Funcion*> func;
    for(int i=0; i<this->funciones->funciones.length(); i++){
       temporal = funciones->funciones.at(i);
       if(!temporal->esConstructor){
           func.push_back(temporal);
       }
   }

    return func;
}


QList<nodoArbol*>Clase::obtenerAtributosHeredados(){
    nodoArbol *temporal;
    QList<nodoArbol*> atris;
    for(int i =0; i<this->atributos.length();i++){
        temporal = atributos.at(i);
        atris.push_back(temporal);
    }
    return atris;
}


/*================== GENERACION DE SIMBOLOS DE LA CLASE ===============*/

bool Clase::existeAtributo(string nombre, string clase, QList<Simbolo *> lista){
    for(int i=0; i<lista.length(); i++){
        if(sonIguales(nombre, lista.at(i)->nombrecorto) &&
                sonIguales(clase, lista.at(i)->ambito)){
            //this->erroresPrograma->insertarError(SEMANTICO, "Ha ocurrido un error, ya existe un simbolo con el nombre "+ );
            return true;
        }
    }
    return false;
}

string Clase::obtenerTipoSimbolo(string tipo){
    if(sonIguales(T_ENTERO, tipo) ||
           sonIguales(T_DECIMAL, tipo) ||
           sonIguales(T_CADENA, tipo) ||
           sonIguales(T_BOOLEANO, tipo) ||
           sonIguales(T_CARACTER, tipo)){
        return "VARIABLE";
           }
    return "OBJETO";

}


nodoArbol* Clase::crearAsignacion(nodoArbol *id, string tipo, nodoArbol *exp){
    nodoArbol *igual = new nodoArbol("=","=");
    nodoArbol *asigna = new nodoArbol("ASIGNACION", tipo);
    asigna->addHijo(id);
    asigna->addHijo(igual);
    asigna->addHijo(exp);
    return asigna;
}

nodoArbol* Clase :: crearDeclaracion(nodoArbol *declaracion){

    string tipo= declaracion->valor;
    string decla = declaracion->etiqueta;

    if(sonIguales(decla, "DECLA_ATRIBUTO")){
 if(sonIguales(tipo, "1")){
        nodoArbol *nueva = new nodoArbol("DECLARACION", "2");
        nueva->addHijo(declaracion->hijos.at(0));
        nueva->addHijo(declaracion->hijos.at(1));
        return nueva;
    }else if(sonIguales(tipo, "2")){
        return declaracion;
    }else if(sonIguales(tipo,"3")){
         nodoArbol *nueva = new nodoArbol("DECLARACION", "2");
        nueva->addHijo(declaracion->hijos.at(0));
        nueva->addHijo(declaracion->hijos.at(1));
        return nueva;
    }else if(sonIguales(tipo, "4")){
        nodoArbol *nueva = new nodoArbol("DECLA_ARREGLO", "1");
         nueva->addHijo(declaracion->hijos.at(0));
         nueva->addHijo(declaracion->hijos.at(1));
         nueva->addHijo(declaracion->hijos.at(2));
         return nueva;
    }else if(sonIguales(tipo,"5")){
        nodoArbol *nueva = new nodoArbol("DECLA_ARREGLO", "1");
        nueva->addHijo(declaracion->hijos.at(0));
         nueva->addHijo(declaracion->hijos.at(1));
         nueva->addHijo(declaracion->hijos.at(2));
         return nueva;
    }else{
        return declaracion;
    }

    }else if(sonIguales(decla, "DECLARACION")){
         if(sonIguales(tipo, "1")){
        nodoArbol *nueva = new nodoArbol("DECLARACION", "2");
        nueva->addHijo(declaracion->hijos.at(0));
        nueva->addHijo(declaracion->hijos.at(1));
        return nueva;
    }else if(sonIguales(tipo, "2")){
        return declaracion;
    }else if(sonIguales(tipo,"3")){
         nodoArbol *nueva = new nodoArbol("DECLARACION", "2");
        nueva->addHijo(declaracion->hijos.at(0));
        nueva->addHijo(declaracion->hijos.at(1));
        return nueva;
    }
    }else if(sonIguales(decla, "DECLA_ARREGLO")){
        if(sonIguales(tipo, "1")){
        nodoArbol *nueva = new nodoArbol("DECLA_ARREGLO", "1");
         nueva->addHijo(declaracion->hijos.at(0));
         nueva->addHijo(declaracion->hijos.at(1));
         nueva->addHijo(declaracion->hijos.at(2));
         return nueva;
    }else if(sonIguales(tipo,"2")){
        nodoArbol *nueva = new nodoArbol("DECLA_ARREGLO", "1");
        nueva->addHijo(declaracion->hijos.at(0));
         nueva->addHijo(declaracion->hijos.at(1));
         nueva->addHijo(declaracion->hijos.at(2));
         return nueva;
    }
    }
    return declaracion;
}

QList<Simbolo*> Clase::generarSimbolosAtributos(){

    QList<Simbolo*> listaRetorno;
    this->apuntador=0;
    nodoArbol *temporal;
    string tipoDeclaracion;
    QList<nodoArbol*>valores;
    for(int i=0; i< this->atributos.length(); i++){
        temporal = this->atributos.at(i);
        tipoDeclaracion= temporal->valor;
        valores= temporal->hijos;
        if(sonIguales(tipoDeclaracion,"1")){
            string  tipoDato, nombreVariable, visibilidad, tipoSimb;
            tipoDato = valores.at(0)->valor;
            nombreVariable = valores.at(1)->valor;
            if(!existeAtributo(nombreVariable,this->nombreClase, listaRetorno)){
                nodoArbol *asig = this->crearAsignacion(valores.at(1),"1",valores.at(2));
                visibilidad = valores.at(3)->valor;
                tipoSimb = this->obtenerTipoSimbolo(tipoDato);
                Simbolo *nuevoSimb= new Simbolo();
                nuevoSimb->setValoresVariable(nombreVariable,tipoSimb,tipoDato,this->nombreClase,"ATRIBUTO",apuntador,1);
                nuevoSimb->visibilidad= visibilidad;
                nodoArbol *decla = this->crearDeclaracion(temporal);
                nuevoSimb->asignaAtributo= asig;
                nuevoSimb->declaAtributo= decla;
                apuntador++;
                listaRetorno.push_back(nuevoSimb);
            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombreVariable+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }

        }
        else if(sonIguales(tipoDeclaracion,"2")){
            string nombre, visibilidad, tipoDato, tipoSimbolo;
            nombre = valores.at(1)->valor;
            tipoDato= valores.at(0)->valor;
            visibilidad = valores.at(2)->valor;
            tipoSimbolo = this->obtenerTipoSimbolo(tipoDato);
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresVariable(nombre, tipoSimbolo, tipoDato,this->nombreClase,"ATRIBUTO",apuntador,1);
                nuevo->visibilidad= visibilidad;
                apuntador++;
                listaRetorno.push_back(nuevo);
            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }

        }
        else if(sonIguales(tipoDeclaracion,"3")){
            string nombre, visibilidad, tipoDato, tipoSimbolo;
            nombre = valores.at(1)->valor;
            tipoDato= valores.at(0)->valor;
            tipoSimbolo = this->obtenerTipoSimbolo(tipoDato);
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                nodoArbol *asig = this->crearAsignacion(valores.at(1),"1",valores.at(2));
                nodoArbol *decla = this->crearDeclaracion(temporal);
                visibilidad = valores.at(3)->valor;
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresVariable(nombre, tipoSimbolo, tipoDato, this->nombreClase,"ATRIBUTO",apuntador,1);
                apuntador++;
                nuevo->asignaAtributo=asig;
                nuevo->declaAtributo= decla;
                nuevo->visibilidad= visibilidad;
                listaRetorno.push_back(nuevo);
            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }


        }
        else if(sonIguales(tipoDeclaracion,"4")){
            string nombre, visibilidad, tipoDato, tipoSimbolo;
            nombre = valores.at(1)->valor;
            tipoDato= valores.at(0)->valor;
            tipoSimbolo = this->obtenerTipoSimbolo(tipoDato);
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                QList<nodoArbol*>dimensiones = valores.at(2)->hijos;
                visibilidad = valores.at(3)->valor;
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresArreglo(nombre,"ARREGLO", tipoDato, this->nombreClase,"ATRIBUTO",apuntador,1,dimensiones.length(),dimensiones);
                nuevo->visibilidad= visibilidad;
                nodoArbol *decla = this->crearDeclaracion(temporal);
                nuevo->declaAtributo= decla;
                listaRetorno.push_back(nuevo);
                apuntador++;
            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }

        }
        else if(sonIguales(tipoDeclaracion,"5")){
            string nombre, visibilidad, tipoDato, tipoSimbolo;
            nombre = valores.at(1)->valor;
            tipoDato= valores.at(0)->valor;
            tipoSimbolo = this->obtenerTipoSimbolo(tipoDato);
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                QList<nodoArbol*>dimensiones = valores.at(2)->hijos;
                visibilidad = valores.at(4)->valor;
                nodoArbol *decla = this->crearDeclaracion(temporal);
                nodoArbol *asigna= this->crearAsignacion(valores.at(1),"1",valores.at(3));
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresArreglo(nombre,"ARREGLO",tipoDato,this->nombreClase,"ATRIBUTO",apuntador,1,dimensiones.length(), dimensiones);
                nuevo->visibilidad= visibilidad;
                nuevo->asignaAtributo=asigna;
                nuevo->declaAtributo=decla;
                apuntador++;
                listaRetorno.push_back(nuevo);
            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }

        }
        else if(sonIguales(tipoDeclaracion,"6")){

            string visibilidad = valores.at(1)->valor;
            nodoArbol *declaLista = valores.at(0);
            string nombre = declaLista->valor;
            string tipoDato = declaLista->hijos.at(0)->valor;
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                nodoArbol *decla = this->crearDeclaracion(temporal);
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresLista(nombre,"LISTA",tipoDato,this->nombreClase,"ATRIBUTO",apuntador,1);
                apuntador++;
                nuevo->declaAtributo= decla;
                nuevo->visibilidad= visibilidad;
                listaRetorno.push_back(nuevo);

            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }


        }
        else if(sonIguales(tipoDeclaracion,"7")){
            string visibilidad = valores.at(1)->valor;
            nodoArbol *declaLista = valores.at(0);
            string nombre = declaLista->valor;
            string tipoDato = declaLista->hijos.at(0)->valor;
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                nodoArbol *decla = this->crearDeclaracion(temporal);
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresCola(nombre,"COLA",tipoDato,this->nombreClase,"ATRIBUTO",apuntador,1);
                apuntador++;
                nuevo->declaAtributo= decla;
                nuevo->visibilidad= visibilidad;
                listaRetorno.push_back(nuevo);

            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }

        }
        else if(sonIguales(tipoDeclaracion,"8")){
            string visibilidad = valores.at(1)->valor;
            nodoArbol *declaLista = valores.at(0);
            string nombre = declaLista->valor;
            string tipoDato = declaLista->hijos.at(0)->valor;
            if(!existeAtributo(nombre,this->nombreClase,listaRetorno)){
                nodoArbol *decla = this->crearDeclaracion(temporal);
                Simbolo *nuevo = new Simbolo();
                nuevo->setValoresPila(nombre,"PILA",tipoDato,this->nombreClase,"ATRIBUTO",apuntador,1);
                apuntador++;
                nuevo->declaAtributo= decla;
                nuevo->visibilidad= visibilidad;
                listaRetorno.push_back(nuevo);

            }else{
                erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombre+", ya existe en el ambito actual", temporal->fila, temporal->columna);
            }

        }


    }
    return listaRetorno;

}

bool Clase::existeSimbolo(QList<Simbolo*>lista, Simbolo *simb){
    for(int i=0; i<lista.length(); i++){
        if(sonIguales(simb->nombrecorto, lista.at(i)->nombrecorto) &&
                sonIguales(simb->ambito, lista.at(i)->ambito)){
            this->erroresPrograma->insertarError2(SEMANTICO, "Ha ocurrido un error, ya existe un simbolo con el nombre "+simb->nombrecorto+" en el ambito acutal" );
            return true;
        }
    }
    return false;
}

QList<Simbolo*> Clase::generarSimbolosClase(ListaErrores *errores){
    this->erroresPrograma =errores;

    QList<Simbolo*> retornoSimbolos;
    QList<Simbolo*> simbAtributos = this->generarSimbolosAtributos();
    //1. Creamos el simbolo de la clase
    Simbolo *simbClase = new Simbolo();
    simbClase->setValoresVariable(this->nombreClase,"CLASE",this->nombreClase, NO_TIENE,"CLASE",-1,simbAtributos.length());
    retornoSimbolos.push_back(simbClase);

    //2 Guardamos los simbolo de los atributos
    for(int i =0; i< simbAtributos.length(); i++){
        retornoSimbolos.push_back(simbAtributos.at(i));
    }

    //3 Generamos simbolos de cada funcion

    Funcion *funTemporal;
   // Simbolo *simTemporal;
    Simbolo *thisTemporal;
    Simbolo *returnTemporal;
    Ambito *  ambitos = new Ambito();
    //aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii ibaaaa ambiente
    ambitos->addAmbito(this->nombreClase);
    apuntador =0;
cout<<funciones->funciones.length()<<endl;
    for(int i = 0; i< this->funciones->funciones.length(); i++){
        funTemporal = this->funciones->funciones.at(i);
        bool banderaConstructor = funTemporal->esConstructor;
        ambitos->addAmbito(funTemporal->obtenerFirma()); //aquiiiiiiiiiiiiiiiiiiiiiiiiiiii
        apuntador =0;
        thisTemporal = new Simbolo();
        thisTemporal->setValoresVariable("THIS","THIS","THIS",ambitos->getAmbitos(),"THIS",apuntador,1);
        apuntador++;

        //Creamos parametros de la funcion;
        QList<Simbolo*>simbParametros;
        Simbolo *simbTemporal;
        nodoArbol *parTemp;
        for(int j=0; j<funTemporal->parametros.length(); j++){
            simbTemporal= new Simbolo();
            parTemp= funTemporal->parametros.at(j);
            string tipo, nombre;
            if(sonIguales(parTemp->etiqueta,"DECLARACION")){
                tipo = parTemp->hijos.at(0)->valor;
                nombre= parTemp->hijos.at(1)->valor;
                simbTemporal->setValoresVariable(nombre, this->obtenerTipoSimbolo(tipo),tipo,ambitos->getAmbitos(),"PARAMETRO",apuntador,1);
                apuntador++;
                simbTemporal->declaAtributo=parTemp;
                simbParametros.push_back(simbTemporal);
            }
            if(sonIguales(parTemp->etiqueta, "DECLA_ARREGLO")){
                tipo = parTemp->hijos.at(0)->valor;
                nombre = parTemp->hijos.at(1)->valor;
                 QList<nodoArbol*>dimensiones = parTemp->hijos.at(2)->hijos;
                simbTemporal->setValoresArreglo(nombre,"ARREGLO",tipo,ambitos->getAmbitos(),"PARAMETRO",apuntador,1,dimensiones.length(),dimensiones);
                apuntador++;
                simbTemporal->declaAtributo=parTemp;
                simbParametros.push_back(simbTemporal);
            }

        }//fin del ciclo para los parametros

        QList<nodoArbol*>sentFuncion= funTemporal->sentencias->hijos;
        this->lista2.clear();
        this->obtenersimbolosMetodo(sentFuncion,ambitos,simbParametros);

        returnTemporal = new Simbolo();
        returnTemporal->setValoresVariable("RETORNO","RETORNO","RETORNO", ambitos->getAmbitos(),"RETORNO", apuntador, 1);
        apuntador++;
        int sizeFun = 2+simbParametros.length()+lista2.length();

        simbTemporal = new Simbolo();
        if(!banderaConstructor){
            simbTemporal->setValoresFuncion(funTemporal->obtenerFirma(),funTemporal->tipo,"FUNCION",this->nombreClase,"FUNCION",-1,sizeFun,funTemporal->parametros.length(),funTemporal->obtenerCadenaParametros(),funTemporal->nombreFuncion);
            simbTemporal->visibilidad= funTemporal->visibilidad;

        }else{

            simbTemporal->setValoresFuncion(funTemporal->obtenerFirma(), funTemporal->tipo, "CONSTRUCTOR", this->nombreClase, "CONSTRUCTOR", -1, sizeFun,funTemporal->parametros.length(), funTemporal->obtenerCadenaParametros(), funTemporal->nombreFuncion);
            simbTemporal->visibilidad= funTemporal->visibilidad;
        }

        retornoSimbolos.push_back(simbTemporal);
        retornoSimbolos.push_back(thisTemporal);
        //insertando parametrods

        for(int j =0; j<simbParametros.length();j++){
            if(!this->existeSimbolo(retornoSimbolos,simbParametros.at(j))){
                retornoSimbolos.push_back(simbParametros.at(j));
            }
        }

        //insertando los demas dimbolos

        for(int j =0; j<lista2.length();j++){
            if(!existeSimbolo(retornoSimbolos,lista2.at(j))){
                retornoSimbolos.push_back(lista2.at(j));
            }
        }

        retornoSimbolos.push_back(returnTemporal);
        ambitos->salirAmbito();

    }

    //Insertando los simbolos del principal
    if(this->principal!= NULL){
        ambitos->addAmbito(this->nombreClase+"_principal");
        apuntador =0;
        lista2.clear();
        QList<Simbolo*>vacia;
        this->obtenersimbolosMetodo(principal->sentencias->hijos,ambitos,vacia);
         int siz=0;
         siz= lista2.length();
        Simbolo *simbTemporal = new Simbolo();
        simbTemporal->setValoresFuncion(this->nombreClase+"_PRINCIPAL","PRINCIPAL","PRINCIPAL",this->nombreClase,"PRINCIPAL",-1,siz,0,"","PRINCIPAL");
        retornoSimbolos.push_back(simbTemporal);
        for(int j=0; j<lista2.length();j++){
            if(!this->existeSimbolo(retornoSimbolos,lista2.at(j))){
                retornoSimbolos.push_back(lista2.at(j));
            }
        }
        ambitos->salirAmbito();
    }

    ambitos->salirAmbito();
    return retornoSimbolos;
}


void Clase::obtenersimbolosMetodo(QList<nodoArbol*> sentencias, Ambito *ambiente, QList<Simbolo *> parametros){

    for(int i=0; i<sentencias.length();i++){
       this->simbMet(sentencias.at(i),ambiente, parametros);
    }

}







void Clase::simbMet(nodoArbol *sent, Ambito *ambito, QList<Simbolo *> parametros){
    string nombreSent= sent->etiqueta;
    //cout<<nombreSent<<endl;
   // cout<<sent->valor<<endl;

    if(sonIguales("DECLARACION", nombreSent)){
        string tipo = sent->valor;
        if(sonIguales(tipo, "1")){
            QList<nodoArbol*>valores= sent->hijos;
            string  tipoDato, nombreVariable,  tipoSimb;
            tipoDato = valores.at(0)->valor;
            nombreVariable = valores.at(1)->valor;
            int cont = existeEnAmbitoLocal(lista2,ambito,nombreVariable,parametros);
            if(cont ==0){

                    nodoArbol *asig = this->crearAsignacion(valores.at(1),"1",valores.at(2));
                    tipoSimb = this->obtenerTipoSimbolo(tipoDato);
                    Simbolo *nuevoSimb= new Simbolo();
                    nuevoSimb->setValoresVariable(nombreVariable,tipoSimb,tipoDato,ambito->getAmbitos(),"VAR_LOCAL",apuntador,1);
                    nodoArbol *decla = this->crearDeclaracion(sent);
                    nuevoSimb->asignaAtributo= asig;
                    nuevoSimb->declaAtributo= decla;
                    apuntador++;
                    lista2.push_back(nuevoSimb);

            }else{
                 erroresPrograma->insertarError("Semantico","No se ha podido crear el simbolo4 "+ nombreVariable+", debido a que existe en el ambito actual", sent->fila, sent->columna);
            }

        }

        else if(sonIguales(tipo, "2")){

            QList<nodoArbol*>valores= sent->hijos;
            string  tipoDato, nombreVariable,  tipoSimb;
            tipoDato = valores.at(0)->valor;
            nombreVariable = valores.at(1)->valor;
            int cont = existeEnAmbitoLocal(lista2,ambito,nombreVariable,parametros);
            cout<<"Resultado para "<<nombreVariable<<" en:  "<<ambito->getAmbitos()<<" es "<<cont<<endl;
            if(cont ==0){
                    tipoSimb = this->obtenerTipoSimbolo(tipoDato);
                    Simbolo *nuevoSimb= new Simbolo();
                    nuevoSimb->setValoresVariable(nombreVariable,tipoSimb,tipoDato,ambito->getAmbitos(),"VAR_LOCAL",apuntador,1);
                    nodoArbol *decla = this->crearDeclaracion(sent);
                    nuevoSimb->declaAtributo= decla;
                    apuntador++;
                    lista2.push_back(nuevoSimb);

            }else{
                 erroresPrograma->insertarError("Semantico","No se ha podido crear el simbolo4 "+ nombreVariable+", debido a que existe en el ambito actual", sent->fila, sent->columna);
            }

        }

        else if(sonIguales(tipo, "3")){
            QList<nodoArbol*>valores= sent->hijos;
            string  tipoDato, nombreVariable,  tipoSimb;
            tipoDato = valores.at(0)->valor;
            nombreVariable = valores.at(1)->valor;
            int cont = existeEnAmbitoLocal(lista2,ambito,nombreVariable,parametros);
            if(cont ==0){

                    nodoArbol *asig = this->crearAsignacion(valores.at(1),"1",valores.at(2));
                    tipoSimb = this->obtenerTipoSimbolo(tipoDato);
                    Simbolo *nuevoSimb= new Simbolo();
                    nuevoSimb->setValoresVariable(nombreVariable,tipoSimb,tipoDato,ambito->getAmbitos(),"VAR_LOCAL",apuntador,1);
                    nodoArbol *decla = this->crearDeclaracion(sent);
                    nuevoSimb->asignaAtributo= asig;
                    nuevoSimb->declaAtributo= decla;
                    apuntador++;
                    lista2.push_back(nuevoSimb);

            }else{
                 erroresPrograma->insertarError("Semantico","No se ha podido crear el simbolo4 "+ nombreVariable+", debido a que existe en el ambito actual" , sent->fila, sent->columna);
            }
        }


    }

    else if (sonIguales("DECLA_ARREGLO", nombreSent)){
        string tipo = sent->valor;
        QList<nodoArbol*>valores = sent->hijos;
        if(sonIguales(tipo,"1")){
                    string nombre, tipoDato, tipoSimbolo;
                    nombre = valores.at(1)->valor;
                    tipoDato= valores.at(0)->valor;
                    tipoSimbolo = this->obtenerTipoSimbolo(tipoDato);
                    int cont = this->existeEnAmbitoLocal(lista2,ambito, nombre,parametros);
                    if(cont ==0){
                        QList<nodoArbol*>dimensiones = valores.at(2)->hijos;
                        Simbolo *nuevo = new Simbolo();
                        nuevo->setValoresArreglo(nombre,"ARREGLO", tipoDato, ambito->getAmbitos(),"ARREGLO_LOCAL",apuntador,1,dimensiones.length(),dimensiones);
                        lista2.push_back(nuevo);
                        apuntador++;
                    }else{
                        erroresPrograma->insertarError(SEMANTICO,"No se puede crear  "+ nombre+", ya existe en el ambito actual", sent->fila, sent->columna);
                    }

                }
                else if(sonIguales(tipo,"2")){
            string nombre, tipoDato, tipoSimbolo;
            nombre = valores.at(1)->valor;
            tipoDato= valores.at(0)->valor;
            tipoSimbolo = this->obtenerTipoSimbolo(tipoDato);
            int cont = this->existeEnAmbitoLocal(lista2,ambito, nombre,parametros);

                    if(cont ==0){
                        QList<nodoArbol*>dimensiones = valores.at(2)->hijos;

                        nodoArbol *decla = this->crearDeclaracion(sent);
                        nodoArbol *asigna= this->crearAsignacion(valores.at(1),"1",valores.at(3));
                        Simbolo *nuevo = new Simbolo();
                        nuevo->setValoresArreglo(nombre,"ARREGLO",tipoDato,ambito->getAmbitos(),"ARREGLO_LOCAL",apuntador,1,dimensiones.length(), dimensiones);
                        nuevo->asignaAtributo=asigna;
                        nuevo->declaAtributo=decla;
                        apuntador++;
                        lista2.push_back(nuevo);
                    }else{
                        erroresPrograma->insertarError(SEMANTICO,"No se puede crear "+ nombre+", ya existe en el ambito actual", sent->fila, sent->columna);
                    }

                }
    }


    else if (sonIguales("SI", nombreSent)){

        string tipoIF= sent->valor;
        nodoArbol *nIf = sent->hijos.at(0);
        QList<nodoArbol*>sentenciasIf= nIf->hijos.at(1)->hijos;
        ambito->addSi();
        for(int i=0; i< sentenciasIf.length(); i++){
            this->simbMet(sentenciasIf.at(i),ambito,parametros);
        }
        ambito->salirAmbito();

        if(sonIguales(tipoIF,"3") || sonIguales(tipoIF,"4")){
            QList<nodoArbol*> l_sinoSi = sent->hijos.at(1)->hijos;
            for(int i =0; i<l_sinoSi.length();i++){
                QList<nodoArbol*> st= l_sinoSi.at(i)->hijos.at(1)->hijos;
                ambito->addSi();
                for(int j =0; j<st.length();j++){
                    this->simbMet(st.at(j),ambito,parametros);
                }
                ambito->salirAmbito();
            }
        }

        if(sonIguales(tipoIF,"2")){
            QList<nodoArbol*>instrucciones = sent->hijos.at(1)->hijos.at(0)->hijos;
            ambito->addElse();
            for(int i =0; i<instrucciones.length();i++){
                this->simbMet(instrucciones.at(i),ambito,parametros);
            }
            ambito->salirAmbito();

        }

        if(sonIguales(tipoIF,"4")){
            QList<nodoArbol*>instrucciones = sent->hijos.at(2)->hijos.at(0)->hijos;
            ambito->addElse();
            for(int i =0; i<instrucciones.length();i++){
                this->simbMet(instrucciones.at(i),ambito,parametros);
            }
            ambito->salirAmbito();

        }


    }

    else if (sonIguales("MIENTRAS", nombreSent)){
        QList<nodoArbol*> sentencias = sent->hijos.at(1)->hijos;
        ambito->addMientras();
        for(int i=0; i<sentencias.length(); i++){
            this->simbMet(sentencias.at(i),ambito,parametros);
        }
        ambito->salirAmbito();

    }

    else if (sonIguales("SELECCIONA", nombreSent)){

        QList<nodoArbol*> casos= sent->hijos.at(1)->hijos;
        nodoArbol *temp;
        QList<nodoArbol*> sentencias;
        for(int i =0; i<casos.length(); i++){
            temp = casos.at(i);
            if(sonIguales(temp->etiqueta, "CASO")){
             ambito->addCaso();
             sentencias = temp->hijos.at(1)->hijos;
             for(int j=0; j<sentencias.length(); j++){
                 this->simbMet(sentencias.at(j),ambito,parametros);
             }
             ambito->salirAmbito();
            }else{
                ambito->addDefecto();
                sentencias = temp->hijos.at(0)->hijos;
                for(int j=0; j<sentencias.length(); j++){
                    this->simbMet(sentencias.at(j),ambito,parametros);
                }

                ambito->salirAmbito();
            }

        }
    }

    else if (sonIguales("HACER_MIENTRAS", nombreSent)){
        QList<nodoArbol*> sentencias = sent->hijos.at(1)->hijos;
        ambito->addHacer();
        for(int i=0; i<sentencias.length(); i++){
            this->simbMet(sentencias.at(i),ambito,parametros);
        }
        ambito->salirAmbito();
    }

    else if (sonIguales("PARA", nombreSent)){
        QList<nodoArbol*> sentencias = sent->hijos.at(3)->hijos;
        ambito->addPara();
        nodoArbol *declaPara = sent->hijos.at(0);
        this->simbMet(declaPara, ambito, parametros);
        for(int i=0; i<sentencias.length(); i++){
            this->simbMet(sentencias.at(i),ambito,parametros);
        }
        ambito->salirAmbito();

    }

    else if (sonIguales("DECLA_COLA", nombreSent)){
        nodoArbol *declaLista = sent;
        string nombreL = declaLista->valor;
        string tipoDato = declaLista->hijos.at(0)->valor;
        int cont = this->existeEnAmbitoLocal(lista2,ambito, nombreL,parametros);
        if(cont ==0){
            nodoArbol *decla = this->crearDeclaracion(declaLista);
            Simbolo *nuevo = new Simbolo();
            nuevo->setValoresPila(nombreL,"COLA",tipoDato,ambito->getAmbitos(),"COLA_LOCAL",apuntador,1);
            apuntador++;
            nuevo->declaAtributo= decla;
lista2.push_back(nuevo);

        }else{
            erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombreL+", ya existe en el ambito actual", sent->fila, sent->columna);
        }

    }

    else if (sonIguales("DECLA_PILA", nombreSent)){
        nodoArbol *declaLista = sent;
        string nombreL = declaLista->valor;
        string tipoDato = declaLista->hijos.at(0)->valor;
        int cont = this->existeEnAmbitoLocal(lista2,ambito, nombreL,parametros);
        if(cont ==0){
            nodoArbol *decla = this->crearDeclaracion(declaLista);
            Simbolo *nuevo = new Simbolo();
            nuevo->setValoresPila(nombreL,"PILA",tipoDato,ambito->getAmbitos(),"PILA_LOCAL",apuntador,1);
            apuntador++;
            nuevo->declaAtributo= decla;
lista2.push_back(nuevo);

        }else{
            erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombreL+", ya existe en el ambito actual", sent->fila, sent->columna);
        }

    }

    else if (sonIguales("DECLA_LISTA", nombreSent)){
        nodoArbol *declaLista = sent;
        string nombreL = declaLista->valor;
        string tipoDato = declaLista->hijos.at(0)->valor;
        int cont = this->existeEnAmbitoLocal(lista2,ambito, nombreL,parametros);
        if(cont ==0){
            nodoArbol *decla = this->crearDeclaracion(declaLista);
            Simbolo *nuevo = new Simbolo();
            nuevo->setValoresPila(nombreL,"LISTA",tipoDato,ambito->getAmbitos(),"LISTA_LOCAL",apuntador,1);
            apuntador++;
            nuevo->declaAtributo= decla;
lista2.push_back(nuevo);

        }else{
            erroresPrograma->insertarError(SEMANTICO,"No se puede crear el atributo "+ nombreL+", ya existe en el ambito actual", sent->fila, sent->columna);
        }

    }
    }






int Clase::existeEnAmbitoLocal(QList<Simbolo *> lista, Ambito *ambito, string nombre, QList<Simbolo *> parametros){

   Ambito *ambitoTemporal= ambito->clonarLista();
   Ambito *ambitoTemporal2= ambito->clonarLista();
   string cadenaAmbito="";
   int cont=0;

   for(int i=0; i<ambito->ambitos.length(); i++){
       cadenaAmbito= ambitoTemporal2->getAmbitos();
       //cout<<cadenaAmbito<<endl;
       cont = cont+ this->existeLista(cadenaAmbito,nombre,parametros);
        //cout<<cont<<endl;
       ambitoTemporal2->salirAmbito();
   }


   for(int i=0; i<ambito->ambitos.length(); i++){
       cadenaAmbito= ambitoTemporal->getAmbitos();
        //cout<<cadenaAmbito<<endl;
       cont = cont+ this->existeLista(cadenaAmbito,nombre,lista);
       //cout<<cont<<endl;
       ambitoTemporal->salirAmbito();
   }



   return cont;
}



int Clase::existeLista(string cadenaAmbito, string nombre, QList<Simbolo*> lista){
    Simbolo *simbTemporal;
    int cont=0;
    for(int i=0; i<lista.length();i++){
        simbTemporal= lista.at(i);
        //cout<<"Se compara "<< simbTemporal->ambito<<" con: "<<cadenaAmbito<<endl;
        //cout<<"Se comprar "<<simbTemporal->nombrecorto<<" con: "<<nombre<<endl;
        if(sonIguales(simbTemporal->ambito, cadenaAmbito) &&
               sonIguales(simbTemporal->nombrecorto,nombre) ){
            cont++;
        }
    }
    return cont;

}

void Clase::insertarFuncion(Funcion *nueva){
    this->funciones->insertarFuncion(nueva);
}

void Clase::agregarNombreClaseFunciones(string nombreClase)
{
    this->funciones->agregarNombreClase(nombreClase);
}
