#include "constantes.h"
#include <QTextStream>
#include <QFile>


bool sonIguales(string cad1, string cad2){


    string cad11="";
    string cad22="";


    for(int i =0; i<cad1.length();i++){
        char c = toupper(cad1.at(i));
        cad11=cad11+c;
    }

    for(int i =0; i<cad2.length();i++){
        char c = toupper(cad2.at(i));
        cad22=cad22+c;
    }

    if(cad11.compare(cad22)==0){
        return true;
    }else{
        return false;
    }
}

string intToCadena(double d){
    ostringstream str1;
    str1 << d;
    string cad = str1.str();
    return cad;
}


QString leerArchivo(QString ruta){
    QFile file(ruta+"");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return "";
    QTextStream in(&file);
    QString text;
    text = in.readAll();
    file.close();
    return text;
}


void escribirArchivo(QString ruta, QString contenido){
    QFile file(ruta);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream out(&file);
        out << contenido;
        file.close();

}
