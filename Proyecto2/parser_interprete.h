/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_INT3D_PARSER_INTERPRETE_H_INCLUDED
# define YY_INT3D_PARSER_INTERPRETE_H_INCLUDED
/* Debug traces.  */
#ifndef INT3DDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define INT3DDEBUG 1
#  else
#   define INT3DDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define INT3DDEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined INT3DDEBUG */
#if INT3DDEBUG
extern int int3Ddebug;
#endif

/* Token type.  */
#ifndef INT3DTOKENTYPE
# define INT3DTOKENTYPE
  enum int3Dtokentype
  {
    vacio = 258,
    heap_ = 259,
    stack_ = 260,
    pool_ = 261,
    print = 262,
    goto_ = 263,
    call = 264,
    menor = 265,
    mayor = 266,
    menorIgual = 267,
    mayorIgual = 268,
    igualIgual = 269,
    distintoA = 270,
    suma_ = 271,
    resta_ = 272,
    multiplicacion_ = 273,
    division_ = 274,
    abreCor = 275,
    cierraCor = 276,
    igual = 277,
    puntoComa = 278,
    dosPuntos = 279,
    abrePar = 280,
    cierraPar = 281,
    identificador_ = 282,
    entero_ = 283,
    decimal_ = 284,
    si = 285,
    abreLlave = 286,
    cierraLlave = 287,
    coma = 288,
    inStr = 289,
    inNum = 290,
    leerTeclado = 291
  };
#endif

/* Value type.  */
#if ! defined INT3DSTYPE && ! defined INT3DSTYPE_IS_DECLARED

union INT3DSTYPE
{
#line 41 "interprete_sintactico.y" /* yacc.c:1909  */

char TEXT [256];
class nodoArbol *NODO;

#line 104 "parser_interprete.h" /* yacc.c:1909  */
};

typedef union INT3DSTYPE INT3DSTYPE;
# define INT3DSTYPE_IS_TRIVIAL 1
# define INT3DSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined INT3DLTYPE && ! defined INT3DLTYPE_IS_DECLARED
typedef struct INT3DLTYPE INT3DLTYPE;
struct INT3DLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define INT3DLTYPE_IS_DECLARED 1
# define INT3DLTYPE_IS_TRIVIAL 1
#endif


extern INT3DSTYPE int3Dlval;
extern INT3DLTYPE int3Dlloc;
int int3Dparse (void);

#endif /* !YY_INT3D_PARSER_INTERPRETE_H_INCLUDED  */
