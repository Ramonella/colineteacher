#ifndef VALOR_H
#define VALOR_H
#include "constantes.h"
#include <string>
#include <string.h>
#include "nodocondicion.h"

using namespace std;


class Valor
{


public:
    string tipo;
    string valor;
    string tipoSimbolo;
    string nombreArreglo;
    string estructura;
    string referencia;

    nodoCondicion *cond;
    Valor();
    Valor (string , string);
    void crearEntero(string);
    void crearDecimal(string);
    void crearCaracter(string);
    void crearBooleano(string);
    void crearCadena(string val);
    void crearCondicion(nodoCondicion *cond);
    void setReferencia(string ed, string ref);
};

#endif // VALOR_H
