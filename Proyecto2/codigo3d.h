#ifndef CODIGO3D_H
#define CODIGO3D_H




#include <fstream>
#include <sstream>
#include <iostream>
#include <string.h>
#include <string>



using namespace std;

class codigo3D
{


public:
    int noTemporal;
    int noEtiquetas;
    string codigo;
    codigo3D();
    string getTemporal();
    string getEtiqueta();
    void addCodigo(string);
    void escribirCodigo();
    void addH();
    void addS();
    void agregarSuma(string contenedor, string v1, string v2);
    void addTransferencia(string cont, string val);
    void igualHeap(string pos, string val);
    void sacarHeap(string contenedor, string pos);
    void igualPool(string pos, string val);
    void sacarPool(string contenedor, string pos);
    void addEtiqueta(string et);
    void addIf(string v1, string simb, string v2, string e1, string e2);
    void addSalto(string etiqueta);


};

#endif // CODIGO3D_H
