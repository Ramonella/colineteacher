#ifndef LISTATEMPORALES_H
#define LISTATEMPORALES_H
#include <string>
#include <string.h>
#include <sstream>
#include <QList>
#include "temporal.h"
#include "constantes.h"

using namespace std;

class ListaTemporales
{
public:
    QList<Temporal*> listado;

    void insertarTemporal(Temporal *temp);
    bool existeTemporal(Temporal *temp);
    void modificarTemporal(Temporal *temp);
    double obtenerValorTemporal(string nombre);
    Temporal* obtenerTemporal(string nombre);
    ListaTemporales();
    ListaTemporales* clonar();
};

#endif // LISTATEMPORALES_H
