#ifndef NODOCONDICION_H
#define NODOCONDICION_H
#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <QList>
#include "constantes.h"

class nodoCondicion
{
public:
    QList<string> verdaderas;
    QList<string>falsas;
    string codigo;
    void addEtiquetasVerdaderas(QList<string> ets);
    void addEtiquetasFalsas(QList<string> ets);
    void addFalsa(string etq);
    void addVerdadera(string etq);
    string getCodigo();
    string getEtiquetasVerdaderas();
    string getEtiquetasFalsas();
    void cambiarEtiquetas();
    nodoCondicion(string codigo);
};

#endif // NODOCONDICION_H
