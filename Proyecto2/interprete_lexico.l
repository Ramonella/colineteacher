
%{
#include "parser_interprete.h"
#include <iostream>
#include <QString>
#define YY_USER_ACTION {int3Dlloc.first_line = yylineno; int3Dlloc.last_line = yylineno;}
int colInterprete =0;
%}


entero_ [0-9]+
decimal_ [0-9]+("."[0-9]+)
id [a-zñA-ZÑ][a-zñA-ZÑ0-9_]*

comentario_simple "//".*


%option noyywrap
%option yylineno
%option prefix="int3D"

%%




"$$_leerTeclado" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return leerTeclado; }

"$$_inStr" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return inStr; }

"$$_inNum" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return inNum; }

"," {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return coma; }

"if" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return si; }

"{" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return abreLlave; }

"}" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return cierraLlave; }

"void" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return vacio; }

"heap" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return heap_; }

"stack" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return stack_; }

"pool" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return pool_; }


"print" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return print; }

"goto"  {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return goto_; }


"call"  {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return call; }


"<"  {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return menor; }

">" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return mayor; }

"<=" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return menorIgual; }

">=" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return mayorIgual; }

"==" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return igualIgual; }


"!=" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return distintoA; }


"+" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return suma_; }

"-" { /*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return resta_; }

"*" {/*std::cout <<yytext<< /*std::endl;*/  colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return multiplicacion_; }

"/" { /*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return division_; }


"[" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return abreCor; }

"]" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return cierraCor; }

"=" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return igual; }

";" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return puntoComa; }

":" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return dosPuntos; }


"(" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return abrePar; }

")" {/*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return cierraPar; }

{id}          {/*std::cout <<yytext<< /*std::endl;*/  colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return identificador_; }

{entero_}        { /*std::cout <<yytext<< /*std::endl;*/colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return entero_; }

{decimal_}        { /*std::cout <<yytext<< /*std::endl;*/ colInterprete=colInterprete+strlen(int3Dlval.TEXT); strcpy(int3Dlval.TEXT, yytext); return decimal_; }
[\n] {colInterprete=0; }

{comentario_simple}    { /*std::cout <<yytext<< /*std::endl;*/ /*Se ignoran*/ }

[[:blank:]]     { /*Se ignoran*/ }

.               {std::cout <<yytext<<"Error Lexico en Interprete 3D "<< std::endl;}
%%
