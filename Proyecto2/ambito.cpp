#include "ambito.h"

Ambito::Ambito()
{
    this->valSi=0;
    this->valElse=0;
    this->valMientras=0;
    this->valPara=0;
    this->valCaso=0;
    this->valDefecto=0;
    this->valHacer=0;
}

void Ambito::addAmbito(string ambito){
    this->ambitos.push_front(ambito);
}

void Ambito::addSi(){
    valSi++;
    ostringstream str1;
    str1 << valSi;
    string geek = str1.str();
    this->addAmbito("Si"+geek);
}

void Ambito::addElse(){
    valElse++;
    ostringstream str1;
    str1 << valElse;
    string geek = str1.str();
    this->addAmbito("Else"+geek);
}


void Ambito::addMientras(){
    valMientras++;
    ostringstream str1;
    str1 << valMientras;
    string geek = str1.str();
    this->addAmbito("Mientras"+geek);
}


void Ambito::addPara(){
    valPara++;
    ostringstream str1;
    str1 << valPara;
    string geek = str1.str();
    this->addAmbito("Para"+geek);
}


void Ambito::addCaso(){
    valCaso++;
    ostringstream str1;
    str1 << valCaso;
    string geek = str1.str();
    this->addAmbito("Caso"+geek);
}

void Ambito::addDefecto(){
    valDefecto++;
    ostringstream str1;
    str1 << valDefecto;
    string geek = str1.str();
    this->addAmbito("Defecto"+geek);
}

void Ambito::addHacer(){
    valHacer++;
    ostringstream str1;
    str1 << valHacer;
    string geek = str1.str();
    this->addAmbito("Hacer"+geek);
}

void Ambito::salirAmbito(){
    this->ambitos.pop_front();
}


string Ambito::getAmbitos(){
    string contexto ="";
        string valTemporal;
        for(int i =this->ambitos.length()-1; i>=0;i--){
            valTemporal= this->ambitos.at(i);
            if(i==0){
                contexto +=valTemporal;
            }else{
                contexto+=valTemporal+"_";
            }
        }

        return contexto;

}

Ambito* Ambito::clonarLista(){
    Ambito *nueva= new Ambito();
    string temp;
    for(int i = 0; i<this->ambitos.length(); i++){
        temp= ambitos.at(i);
        nueva->ambitos.push_back(temp);
    }
    return nueva;
}
