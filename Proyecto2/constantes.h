#ifndef CONSTANTES
#define CONSTANTES
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <QFile>
#include <QString>



using namespace std;


const string ACOACH= "A-Coach";
const string GCOACH= "G-Coach";
const string PUBLICO = "publico";
const string PRIVADO = "privado";
const string PROTEGIDO = "protegido";
const string VACIO ="vacio";
const string VAL_NULO= "88";

const string NO_TIENE = "NO_TIENE";
const string T_ENTERO = "entero";
const string T_DECIMAL = "decimal";
const string T_CARACTER = "caracter";
const string T_BOOLEANO = "booleano";
const string  VERDADERO = "verdadero";
const string FALSO = "falso";
const string NULO = "nulo";
const string  T_CADENA = "cadena";
const string SEMANTICO= "Semantico";
const string  T_CONDICION = "condicion";
bool sonIguales(string cad1, string cad2);
string intToCadena(double d);
const string inicioPagina ="<!DOCTYPE html><html><head><style>#customers {font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;border-collapse: collapse; width: 100%;}#customers td, #customers th {border: 1px solid #ddd;padding: 8px;}#customers tr:nth-child(even){background-color: #f2f2f2;}#customers tr:hover {background-color: #ddd;}#customers th {padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #4CAF50;color: white;}</style></head><body>";

const double noExiste =1515;
QString leerArchivo(QString ruta);
void escribirArchivo(QString ruta, QString contenido);



#endif // CONSTANTES

