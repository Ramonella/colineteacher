#include "listaclases.h"
extern bool sonIguales(string cad1, string cad2);

ListaClases::ListaClases()
{

    this->esExpresion=false;
}

bool ListaClases::existeClase(string nombre){

    Clase *temporal;
    bool bandera;
    for(int i=0; i<this->clasesArchivo.length(); i++){
        temporal = this->clasesArchivo.at(i);
        bandera = sonIguales(temporal->nombreClase, nombre);
        if(bandera){
            return true;
        }
    }
    return false;
}


void ListaClases::insertarClase(Clase *nueva){
    bool bandera = this->existeClase(nueva->nombreClase);
    if(!bandera){
        this->clasesArchivo.push_back(nueva);
    }else{
        //error
    }
}


void ListaClases::addImportacion(nodoArbol *nueva){
    this->importaciones.push_back(nueva);
}


Clase* ListaClases::obtenerClaseNombre(string nombre){

    Clase *temporal;
    bool bandera;
    for(int i=0; i<this->clasesArchivo.length(); i++){
        temporal = this->clasesArchivo.at(i);
        bandera = sonIguales(temporal->nombreClase, nombre);
        if(bandera){
            return temporal;
        }
    }
    return NULL;

}
