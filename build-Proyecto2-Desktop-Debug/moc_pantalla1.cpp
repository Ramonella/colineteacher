/****************************************************************************
** Meta object code from reading C++ file 'pantalla1.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Proyecto2/pantalla1.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pantalla1.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_pantalla1_t {
    QByteArrayData data[10];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_pantalla1_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_pantalla1_t qt_meta_stringdata_pantalla1 = {
    {
QT_MOC_LITERAL(0, 0, 9), // "pantalla1"
QT_MOC_LITERAL(1, 10, 17), // "on_Editor_clicked"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 20), // "on_btbACoach_clicked"
QT_MOC_LITERAL(4, 50, 27), // "on_listWidget_doubleClicked"
QT_MOC_LITERAL(5, 78, 5), // "index"
QT_MOC_LITERAL(6, 84, 20), // "on_btbGCoach_clicked"
QT_MOC_LITERAL(7, 105, 20), // "on_btnBuscar_clicked"
QT_MOC_LITERAL(8, 126, 31), // "on_comboBox_currentIndexChanged"
QT_MOC_LITERAL(9, 158, 4) // "arg1"

    },
    "pantalla1\0on_Editor_clicked\0\0"
    "on_btbACoach_clicked\0on_listWidget_doubleClicked\0"
    "index\0on_btbGCoach_clicked\0"
    "on_btnBuscar_clicked\0"
    "on_comboBox_currentIndexChanged\0arg1"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_pantalla1[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x08 /* Private */,
       3,    0,   45,    2, 0x08 /* Private */,
       4,    1,   46,    2, 0x08 /* Private */,
       6,    0,   49,    2, 0x08 /* Private */,
       7,    0,   50,    2, 0x08 /* Private */,
       8,    1,   51,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,

       0        // eod
};

void pantalla1::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        pantalla1 *_t = static_cast<pantalla1 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Editor_clicked(); break;
        case 1: _t->on_btbACoach_clicked(); break;
        case 2: _t->on_listWidget_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 3: _t->on_btbGCoach_clicked(); break;
        case 4: _t->on_btnBuscar_clicked(); break;
        case 5: _t->on_comboBox_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject pantalla1::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_pantalla1.data,
      qt_meta_data_pantalla1,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *pantalla1::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *pantalla1::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_pantalla1.stringdata0))
        return static_cast<void*>(const_cast< pantalla1*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int pantalla1::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
