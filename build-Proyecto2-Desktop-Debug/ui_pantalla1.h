/********************************************************************************
** Form generated from reading UI file 'pantalla1.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PANTALLA1_H
#define UI_PANTALLA1_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_pantalla1
{
public:
    QWidget *centralwidget;
    QPushButton *btbACoach;
    QPushButton *btbGCoach;
    QPushButton *Editor;
    QComboBox *comboBox;
    QGroupBox *groupBox;
    QListWidget *listWidget;
    QGroupBox *groupBox_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QPushButton *btnBuscar;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *pantalla1)
    {
        if (pantalla1->objectName().isEmpty())
            pantalla1->setObjectName(QStringLiteral("pantalla1"));
        pantalla1->resize(800, 600);
        centralwidget = new QWidget(pantalla1);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        btbACoach = new QPushButton(centralwidget);
        btbACoach->setObjectName(QStringLiteral("btbACoach"));
        btbACoach->setGeometry(QRect(50, 170, 201, 41));
        btbGCoach = new QPushButton(centralwidget);
        btbGCoach->setObjectName(QStringLiteral("btbGCoach"));
        btbGCoach->setGeometry(QRect(50, 220, 201, 41));
        Editor = new QPushButton(centralwidget);
        Editor->setObjectName(QStringLiteral("Editor"));
        Editor->setGeometry(QRect(50, 270, 201, 41));
        comboBox = new QComboBox(centralwidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(540, 30, 201, 27));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(310, 80, 441, 431));
        listWidget = new QListWidget(groupBox);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 40, 391, 371));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(30, 20, 451, 51));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 51, 17));
        lineEdit = new QLineEdit(groupBox_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(70, 10, 181, 27));
        btnBuscar = new QPushButton(groupBox_2);
        btnBuscar->setObjectName(QStringLiteral("btnBuscar"));
        btnBuscar->setGeometry(QRect(270, 10, 99, 27));
        pantalla1->setCentralWidget(centralwidget);
        menubar = new QMenuBar(pantalla1);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        pantalla1->setMenuBar(menubar);
        statusbar = new QStatusBar(pantalla1);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        pantalla1->setStatusBar(statusbar);

        retranslateUi(pantalla1);

        QMetaObject::connectSlotsByName(pantalla1);
    } // setupUi

    void retranslateUi(QMainWindow *pantalla1)
    {
        pantalla1->setWindowTitle(QApplication::translate("pantalla1", "MainWindow", 0));
        btbACoach->setText(QApplication::translate("pantalla1", "Crear Leccion A-Coach", 0));
        btbGCoach->setText(QApplication::translate("pantalla1", "Crear Leccion G-Coach", 0));
        Editor->setText(QApplication::translate("pantalla1", "Editor Texto", 0));
        groupBox->setTitle(QApplication::translate("pantalla1", "Lecciones", 0));
        groupBox_2->setTitle(QString());
        label->setText(QApplication::translate("pantalla1", "Buscar", 0));
        btnBuscar->setText(QApplication::translate("pantalla1", "Buscar", 0));
    } // retranslateUi

};

namespace Ui {
    class pantalla1: public Ui_pantalla1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PANTALLA1_H
