/********************************************************************************
** Form generated from reading UI file 'participarleccion.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PARTICIPARLECCION_H
#define UI_PARTICIPARLECCION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_participarLeccion
{
public:
    QAction *actionRegresar;
    QAction *actionRegresar_2;
    QWidget *centralwidget;
    QLabel *label_6;
    QLabel *label_4;
    QTextEdit *txtTarea;
    QTextEdit *txtEjemplo;
    QTextEdit *txtIngreso;
    QLabel *label_3;
    QTextEdit *txtExplicacion;
    QLabel *label_5;
    QLabel *label;
    QPushButton *btnCompilar;
    QPushButton *btnCalificar;
    QMenuBar *menubar;
    QMenu *menuArchivo;
    QMenu *menuHola;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *participarLeccion)
    {
        if (participarLeccion->objectName().isEmpty())
            participarLeccion->setObjectName(QStringLiteral("participarLeccion"));
        participarLeccion->resize(800, 844);
        actionRegresar = new QAction(participarLeccion);
        actionRegresar->setObjectName(QStringLiteral("actionRegresar"));
        actionRegresar_2 = new QAction(participarLeccion);
        actionRegresar_2->setObjectName(QStringLiteral("actionRegresar_2"));
        centralwidget = new QWidget(participarLeccion);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 550, 81, 17));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 180, 121, 31));
        txtTarea = new QTextEdit(centralwidget);
        txtTarea->setObjectName(QStringLiteral("txtTarea"));
        txtTarea->setGeometry(QRect(160, 460, 601, 75));
        txtEjemplo = new QTextEdit(centralwidget);
        txtEjemplo->setObjectName(QStringLiteral("txtEjemplo"));
        txtEjemplo->setGeometry(QRect(160, 170, 601, 231));
        txtIngreso = new QTextEdit(centralwidget);
        txtIngreso->setObjectName(QStringLiteral("txtIngreso"));
        txtIngreso->setGeometry(QRect(160, 540, 601, 211));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 110, 101, 21));
        txtExplicacion = new QTextEdit(centralwidget);
        txtExplicacion->setObjectName(QStringLiteral("txtExplicacion"));
        txtExplicacion->setGeometry(QRect(160, 90, 601, 71));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 470, 67, 17));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 20, 331, 41));
        QFont font;
        font.setPointSize(32);
        label->setFont(font);
        btnCompilar = new QPushButton(centralwidget);
        btnCompilar->setObjectName(QStringLiteral("btnCompilar"));
        btnCompilar->setGeometry(QRect(380, 410, 99, 27));
        btnCalificar = new QPushButton(centralwidget);
        btnCalificar->setObjectName(QStringLiteral("btnCalificar"));
        btnCalificar->setGeometry(QRect(370, 760, 99, 27));
        participarLeccion->setCentralWidget(centralwidget);
        menubar = new QMenuBar(participarLeccion);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        menuArchivo = new QMenu(menubar);
        menuArchivo->setObjectName(QStringLiteral("menuArchivo"));
        menuHola = new QMenu(menubar);
        menuHola->setObjectName(QStringLiteral("menuHola"));
        participarLeccion->setMenuBar(menubar);
        statusbar = new QStatusBar(participarLeccion);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        participarLeccion->setStatusBar(statusbar);

        menubar->addAction(menuArchivo->menuAction());
        menubar->addAction(menuHola->menuAction());
        menuArchivo->addAction(actionRegresar_2);

        retranslateUi(participarLeccion);

        QMetaObject::connectSlotsByName(participarLeccion);
    } // setupUi

    void retranslateUi(QMainWindow *participarLeccion)
    {
        participarLeccion->setWindowTitle(QApplication::translate("participarLeccion", "MainWindow", 0));
        actionRegresar->setText(QApplication::translate("participarLeccion", "Regresar", 0));
        actionRegresar_2->setText(QApplication::translate("participarLeccion", "Regresar", 0));
        label_6->setText(QApplication::translate("participarLeccion", "Resolucion", 0));
        label_4->setText(QApplication::translate("participarLeccion", "C\303\263digo Ejemplo", 0));
        label_3->setText(QApplication::translate("participarLeccion", "Explicaci\303\263n", 0));
        label_5->setText(QApplication::translate("participarLeccion", "Tarea", 0));
        label->setText(QApplication::translate("participarLeccion", "Nueva Lecci\303\263n", 0));
        btnCompilar->setText(QApplication::translate("participarLeccion", "Compilar", 0));
        btnCalificar->setText(QApplication::translate("participarLeccion", "Calificar", 0));
        menuArchivo->setTitle(QApplication::translate("participarLeccion", "Regresar", 0));
        menuHola->setTitle(QApplication::translate("participarLeccion", "hola", 0));
    } // retranslateUi

};

namespace Ui {
    class participarLeccion: public Ui_participarLeccion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARTICIPARLECCION_H
