/********************************************************************************
** Form generated from reading UI file 'principal.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRINCIPAL_H
#define UI_PRINCIPAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Principal
{
public:
    QAction *actionGenerar_Codigo;
    QAction *actionEjecutar_Codigo;
    QAction *actionContinuar;
    QAction *actionDetener;
    QAction *actionManual_Tecnico;
    QAction *actionManual_de_Usuario;
    QAction *actionGramatica_Alto_Nivel;
    QAction *actionGramatica_3D;
    QAction *actionQuitar_Punto_de_Interrupcion;
    QAction *actionVer_puntos_de_Interrupcion;
    QAction *actionAgregar_punto_de_interrupcion;
    QAction *actionQuitar_punto_de_interrupcion;
    QAction *actionVer_puntos_de_Interrupcion_2;
    QAction *actionAbrir;
    QAction *actionGuardar;
    QAction *actionExportar_3D;
    QAction *actionRegresar;
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *tab;
    QPlainTextEdit *txtImpresion;
    QWidget *tab_3;
    QTextBrowser *txtErrores;
    QWidget *tab_2;
    QTextBrowser *txtTabla;
    QWidget *tab_4;
    QTextBrowser *txtStack;
    QWidget *tab_5;
    QTextBrowser *txtHeap;
    QWidget *tab_6;
    QTextBrowser *txtPool;
    QWidget *tab_7;
    QTextBrowser *txtTemporales;
    QPushButton *pushButton_3;
    QTabWidget *tabWidget_2;
    QWidget *tab_8;
    QGroupBox *groupBox;
    QPlainTextEdit *txtEntrada;
    QWidget *tab_9;
    QPlainTextEdit *txtSalida;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *btn_paso3D;
    QPushButton *btn_detener3D;
    QPushButton *btn_automatico3D;
    QLabel *lblLinea;
    QMenuBar *menuBar;
    QMenu *menuArchivo;
    QMenu *menuEjecutar;
    QMenu *menuDebuguer_Paso;
    QMenu *menuDebuguer_3D;
    QMenu *menuAcerca_de;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Principal)
    {
        if (Principal->objectName().isEmpty())
            Principal->setObjectName(QStringLiteral("Principal"));
        Principal->resize(1511, 792);
        actionGenerar_Codigo = new QAction(Principal);
        actionGenerar_Codigo->setObjectName(QStringLiteral("actionGenerar_Codigo"));
        actionEjecutar_Codigo = new QAction(Principal);
        actionEjecutar_Codigo->setObjectName(QStringLiteral("actionEjecutar_Codigo"));
        actionContinuar = new QAction(Principal);
        actionContinuar->setObjectName(QStringLiteral("actionContinuar"));
        actionDetener = new QAction(Principal);
        actionDetener->setObjectName(QStringLiteral("actionDetener"));
        actionManual_Tecnico = new QAction(Principal);
        actionManual_Tecnico->setObjectName(QStringLiteral("actionManual_Tecnico"));
        actionManual_de_Usuario = new QAction(Principal);
        actionManual_de_Usuario->setObjectName(QStringLiteral("actionManual_de_Usuario"));
        actionGramatica_Alto_Nivel = new QAction(Principal);
        actionGramatica_Alto_Nivel->setObjectName(QStringLiteral("actionGramatica_Alto_Nivel"));
        actionGramatica_3D = new QAction(Principal);
        actionGramatica_3D->setObjectName(QStringLiteral("actionGramatica_3D"));
        actionQuitar_Punto_de_Interrupcion = new QAction(Principal);
        actionQuitar_Punto_de_Interrupcion->setObjectName(QStringLiteral("actionQuitar_Punto_de_Interrupcion"));
        actionVer_puntos_de_Interrupcion = new QAction(Principal);
        actionVer_puntos_de_Interrupcion->setObjectName(QStringLiteral("actionVer_puntos_de_Interrupcion"));
        actionAgregar_punto_de_interrupcion = new QAction(Principal);
        actionAgregar_punto_de_interrupcion->setObjectName(QStringLiteral("actionAgregar_punto_de_interrupcion"));
        actionQuitar_punto_de_interrupcion = new QAction(Principal);
        actionQuitar_punto_de_interrupcion->setObjectName(QStringLiteral("actionQuitar_punto_de_interrupcion"));
        actionVer_puntos_de_Interrupcion_2 = new QAction(Principal);
        actionVer_puntos_de_Interrupcion_2->setObjectName(QStringLiteral("actionVer_puntos_de_Interrupcion_2"));
        actionAbrir = new QAction(Principal);
        actionAbrir->setObjectName(QStringLiteral("actionAbrir"));
        actionGuardar = new QAction(Principal);
        actionGuardar->setObjectName(QStringLiteral("actionGuardar"));
        actionExportar_3D = new QAction(Principal);
        actionExportar_3D->setObjectName(QStringLiteral("actionExportar_3D"));
        actionRegresar = new QAction(Principal);
        actionRegresar->setObjectName(QStringLiteral("actionRegresar"));
        centralWidget = new QWidget(Principal);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(10, 440, 1491, 281));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        txtImpresion = new QPlainTextEdit(tab);
        txtImpresion->setObjectName(QStringLiteral("txtImpresion"));
        txtImpresion->setGeometry(QRect(10, 0, 1471, 241));
        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        txtErrores = new QTextBrowser(tab_3);
        txtErrores->setObjectName(QStringLiteral("txtErrores"));
        txtErrores->setGeometry(QRect(0, 10, 1481, 231));
        tabWidget->addTab(tab_3, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        txtTabla = new QTextBrowser(tab_2);
        txtTabla->setObjectName(QStringLiteral("txtTabla"));
        txtTabla->setGeometry(QRect(10, 10, 1461, 231));
        tabWidget->addTab(tab_2, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        txtStack = new QTextBrowser(tab_4);
        txtStack->setObjectName(QStringLiteral("txtStack"));
        txtStack->setGeometry(QRect(5, 11, 1471, 231));
        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        txtHeap = new QTextBrowser(tab_5);
        txtHeap->setObjectName(QStringLiteral("txtHeap"));
        txtHeap->setGeometry(QRect(5, 11, 1471, 231));
        tabWidget->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        txtPool = new QTextBrowser(tab_6);
        txtPool->setObjectName(QStringLiteral("txtPool"));
        txtPool->setGeometry(QRect(15, 11, 1461, 231));
        tabWidget->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QStringLiteral("tab_7"));
        txtTemporales = new QTextBrowser(tab_7);
        txtTemporales->setObjectName(QStringLiteral("txtTemporales"));
        txtTemporales->setGeometry(QRect(15, 11, 1461, 231));
        tabWidget->addTab(tab_7, QString());
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(240, 0, 71, 31));
        tabWidget_2 = new QTabWidget(centralWidget);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tabWidget_2->setGeometry(QRect(20, 50, 941, 391));
        tab_8 = new QWidget();
        tab_8->setObjectName(QStringLiteral("tab_8"));
        groupBox = new QGroupBox(tab_8);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(390, -20, 391, 491));
        txtEntrada = new QPlainTextEdit(tab_8);
        txtEntrada->setObjectName(QStringLiteral("txtEntrada"));
        txtEntrada->setGeometry(QRect(10, 10, 921, 361));
        tabWidget_2->addTab(tab_8, QString());
        tab_9 = new QWidget();
        tab_9->setObjectName(QStringLiteral("tab_9"));
        tabWidget_2->addTab(tab_9, QString());
        txtSalida = new QPlainTextEdit(centralWidget);
        txtSalida->setObjectName(QStringLiteral("txtSalida"));
        txtSalida->setGeometry(QRect(970, 10, 521, 451));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(310, 0, 81, 31));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(390, 0, 91, 31));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(140, 0, 91, 31));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(50, 0, 91, 31));
        btn_paso3D = new QPushButton(centralWidget);
        btn_paso3D->setObjectName(QStringLiteral("btn_paso3D"));
        btn_paso3D->setGeometry(QRect(500, 0, 71, 27));
        btn_detener3D = new QPushButton(centralWidget);
        btn_detener3D->setObjectName(QStringLiteral("btn_detener3D"));
        btn_detener3D->setGeometry(QRect(570, 0, 71, 27));
        btn_automatico3D = new QPushButton(centralWidget);
        btn_automatico3D->setObjectName(QStringLiteral("btn_automatico3D"));
        btn_automatico3D->setGeometry(QRect(640, 0, 99, 27));
        lblLinea = new QLabel(centralWidget);
        lblLinea->setObjectName(QStringLiteral("lblLinea"));
        lblLinea->setGeometry(QRect(760, 10, 67, 17));
        Principal->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Principal);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1511, 25));
        menuArchivo = new QMenu(menuBar);
        menuArchivo->setObjectName(QStringLiteral("menuArchivo"));
        menuEjecutar = new QMenu(menuBar);
        menuEjecutar->setObjectName(QStringLiteral("menuEjecutar"));
        menuDebuguer_Paso = new QMenu(menuBar);
        menuDebuguer_Paso->setObjectName(QStringLiteral("menuDebuguer_Paso"));
        menuDebuguer_3D = new QMenu(menuBar);
        menuDebuguer_3D->setObjectName(QStringLiteral("menuDebuguer_3D"));
        menuAcerca_de = new QMenu(menuBar);
        menuAcerca_de->setObjectName(QStringLiteral("menuAcerca_de"));
        Principal->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Principal);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Principal->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Principal);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Principal->setStatusBar(statusBar);

        menuBar->addAction(menuArchivo->menuAction());
        menuBar->addAction(menuEjecutar->menuAction());
        menuBar->addAction(menuDebuguer_Paso->menuAction());
        menuBar->addAction(menuDebuguer_3D->menuAction());
        menuBar->addAction(menuAcerca_de->menuAction());
        menuArchivo->addAction(actionAbrir);
        menuArchivo->addAction(actionGuardar);
        menuArchivo->addAction(actionExportar_3D);
        menuArchivo->addAction(actionRegresar);
        menuEjecutar->addAction(actionGenerar_Codigo);
        menuEjecutar->addSeparator();
        menuEjecutar->addAction(actionEjecutar_Codigo);
        menuDebuguer_Paso->addAction(actionContinuar);
        menuDebuguer_Paso->addAction(actionQuitar_Punto_de_Interrupcion);
        menuDebuguer_Paso->addAction(actionVer_puntos_de_Interrupcion);
        menuDebuguer_3D->addAction(actionAgregar_punto_de_interrupcion);
        menuDebuguer_3D->addAction(actionQuitar_punto_de_interrupcion);
        menuDebuguer_3D->addAction(actionVer_puntos_de_Interrupcion_2);
        menuAcerca_de->addAction(actionManual_Tecnico);
        menuAcerca_de->addAction(actionManual_de_Usuario);
        menuAcerca_de->addSeparator();
        menuAcerca_de->addAction(actionGramatica_Alto_Nivel);
        menuAcerca_de->addAction(actionGramatica_3D);

        retranslateUi(Principal);

        tabWidget->setCurrentIndex(6);
        tabWidget_2->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Principal);
    } // setupUi

    void retranslateUi(QMainWindow *Principal)
    {
        Principal->setWindowTitle(QApplication::translate("Principal", "Principal", 0));
        actionGenerar_Codigo->setText(QApplication::translate("Principal", "Generar Codigo", 0));
        actionEjecutar_Codigo->setText(QApplication::translate("Principal", "Ejecutar Codigo", 0));
        actionContinuar->setText(QApplication::translate("Principal", "Agregar Punto de Interrupcion", 0));
        actionDetener->setText(QApplication::translate("Principal", "Detener", 0));
        actionManual_Tecnico->setText(QApplication::translate("Principal", "Manual Tecnico", 0));
        actionManual_de_Usuario->setText(QApplication::translate("Principal", "Manual de Usuario", 0));
        actionGramatica_Alto_Nivel->setText(QApplication::translate("Principal", "Gramatica Alto Nivel", 0));
        actionGramatica_3D->setText(QApplication::translate("Principal", "Gramatica 3D", 0));
        actionQuitar_Punto_de_Interrupcion->setText(QApplication::translate("Principal", "Quitar Punto de Interrupcion", 0));
        actionVer_puntos_de_Interrupcion->setText(QApplication::translate("Principal", "Ver puntos de Interrupcion", 0));
        actionAgregar_punto_de_interrupcion->setText(QApplication::translate("Principal", "Agregar punto de interrupcion", 0));
        actionQuitar_punto_de_interrupcion->setText(QApplication::translate("Principal", "Quitar punto de interrupcion", 0));
        actionVer_puntos_de_Interrupcion_2->setText(QApplication::translate("Principal", "Ver puntos de Interrupcion", 0));
        actionAbrir->setText(QApplication::translate("Principal", "Abrir", 0));
        actionGuardar->setText(QApplication::translate("Principal", "Guardar", 0));
        actionExportar_3D->setText(QApplication::translate("Principal", "Exportar 3D", 0));
        actionRegresar->setText(QApplication::translate("Principal", "Regresar", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Principal", "Impresion", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("Principal", "Errores", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Principal", "Tabla de Simbolos", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("Principal", "Stack", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("Principal", "Heap", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QApplication::translate("Principal", "Pool", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QApplication::translate("Principal", "Temporales", 0));
        pushButton_3->setText(QApplication::translate("Principal", "Paso", 0));
        groupBox->setTitle(QString());
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_8), QApplication::translate("Principal", "Tab 1", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_9), QApplication::translate("Principal", "Tab 2", 0));
        pushButton_4->setText(QApplication::translate("Principal", "Detener", 0));
        pushButton_5->setText(QApplication::translate("Principal", "Automatico", 0));
        pushButton->setText(QApplication::translate("Principal", "Interprete", 0));
        pushButton_2->setText(QApplication::translate("Principal", "Generar", 0));
        btn_paso3D->setText(QApplication::translate("Principal", "Paso", 0));
        btn_detener3D->setText(QApplication::translate("Principal", "Detener", 0));
        btn_automatico3D->setText(QApplication::translate("Principal", "Automatico", 0));
        lblLinea->setText(QString());
        menuArchivo->setTitle(QApplication::translate("Principal", "Archivo", 0));
        menuEjecutar->setTitle(QApplication::translate("Principal", "Ejecutar", 0));
        menuDebuguer_Paso->setTitle(QApplication::translate("Principal", "Debuguer Alto Nivel", 0));
        menuDebuguer_3D->setTitle(QApplication::translate("Principal", "Debuguer 3D", 0));
        menuAcerca_de->setTitle(QApplication::translate("Principal", "Acerca de", 0));
    } // retranslateUi

};

namespace Ui {
    class Principal: public Ui_Principal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRINCIPAL_H
