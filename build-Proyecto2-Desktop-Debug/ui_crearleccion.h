/********************************************************************************
** Form generated from reading UI file 'crearleccion.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREARLECCION_H
#define UI_CREARLECCION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CrearLeccion
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QTextEdit *txtExplicacion;
    QTextEdit *txtTitulo;
    QTextEdit *txtTarea;
    QTextEdit *txtPrueba;
    QTextEdit *txtEjemplo;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QPushButton *btnCrear;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *CrearLeccion)
    {
        if (CrearLeccion->objectName().isEmpty())
            CrearLeccion->setObjectName(QStringLiteral("CrearLeccion"));
        CrearLeccion->resize(853, 867);
        centralwidget = new QWidget(CrearLeccion);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(260, 20, 331, 41));
        QFont font;
        font.setPointSize(32);
        label->setFont(font);
        txtExplicacion = new QTextEdit(centralwidget);
        txtExplicacion->setObjectName(QStringLiteral("txtExplicacion"));
        txtExplicacion->setGeometry(QRect(170, 150, 601, 111));
        txtTitulo = new QTextEdit(centralwidget);
        txtTitulo->setObjectName(QStringLiteral("txtTitulo"));
        txtTitulo->setGeometry(QRect(170, 100, 601, 41));
        txtTarea = new QTextEdit(centralwidget);
        txtTarea->setObjectName(QStringLiteral("txtTarea"));
        txtTarea->setGeometry(QRect(170, 590, 601, 75));
        txtPrueba = new QTextEdit(centralwidget);
        txtPrueba->setObjectName(QStringLiteral("txtPrueba"));
        txtPrueba->setGeometry(QRect(170, 680, 601, 51));
        txtEjemplo = new QTextEdit(centralwidget);
        txtEjemplo->setObjectName(QStringLiteral("txtEjemplo"));
        txtEjemplo->setGeometry(QRect(170, 270, 601, 311));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 110, 67, 17));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 160, 101, 21));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 270, 121, 31));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 590, 67, 17));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 680, 67, 17));
        btnCrear = new QPushButton(centralwidget);
        btnCrear->setObjectName(QStringLiteral("btnCrear"));
        btnCrear->setGeometry(QRect(310, 760, 201, 41));
        CrearLeccion->setCentralWidget(centralwidget);
        menubar = new QMenuBar(CrearLeccion);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 853, 25));
        CrearLeccion->setMenuBar(menubar);
        statusbar = new QStatusBar(CrearLeccion);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        CrearLeccion->setStatusBar(statusbar);

        retranslateUi(CrearLeccion);

        QMetaObject::connectSlotsByName(CrearLeccion);
    } // setupUi

    void retranslateUi(QMainWindow *CrearLeccion)
    {
        CrearLeccion->setWindowTitle(QApplication::translate("CrearLeccion", "Crear Lecci\303\263n", 0));
        label->setText(QApplication::translate("CrearLeccion", "Nueva Lecci\303\263n", 0));
        label_2->setText(QApplication::translate("CrearLeccion", "Titulo", 0));
        label_3->setText(QApplication::translate("CrearLeccion", "Explicaci\303\263n", 0));
        label_4->setText(QApplication::translate("CrearLeccion", "C\303\263digo Ejemplo", 0));
        label_5->setText(QApplication::translate("CrearLeccion", "Tarea", 0));
        label_6->setText(QApplication::translate("CrearLeccion", "Pruebas", 0));
        btnCrear->setText(QApplication::translate("CrearLeccion", "Crear Lecci\303\263n", 0));
    } // retranslateUi

};

namespace Ui {
    class CrearLeccion: public Ui_CrearLeccion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREARLECCION_H
